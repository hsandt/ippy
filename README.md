# Ippy - Irregular Polygonal Packing

Ippy is an **I**rregular **p**olygon packing program written in **Py**thon for my [Master Thesis on Constrained graph search-based polygon packing](http://longnguyenhuu.com/portfolio-item/2d-polygon-packing-program/) (the page contains a download link for the Master Thesis PDF).

## Usage

The version available in this repository is really the code I used for my Master Thesis, and I offer no guarantee about its functionality or performance. It *did* work back then in 2015, you will find the results I generated at that time in [ippy/benchmark/Results](ippy/benchmark/Results). However, due to code rot and library upgrades, it is now particularly difficult to run correctly. In particular, I did not indicate the specific versions to use for each package and pipenv wasn't really around at the time, so you'd need to find and install the correct version for each dependency.

I suggest going to the website of every dependency, and check the changelog to find which version was out in August 2015. Also remember that at the time, not all packages supported Python 3 so I wrote the application in Python 2.

Even if you want to upgrade the application to Python 3 (it is not that hard) by using the latest, Python-3-compatible version of each package, I'd recommend to first stick to the 2015 versions. Indeed, I have already tried to make the app work with all the latest packages at once, but although it was running, it wasn't working correctly, having major bugs such as giving solutions where polygons overlapped every other.

## Packages

I don't have an exact list of dependencies, so you will have to try and install them until it runs, but the main dependencies should be the following:

- *Shapely* for geometrical computations
- *generateDS* for XML parsing (should include lxml)
- *Tkinter* for GUI
- *matplotlib* for polygon drawing

## Credits

You will find more information on the Master Thesis linked at the top.

## License

See [LICENSE](LICENSE).