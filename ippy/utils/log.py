__author__ = 'hs'

import sys
import logging

# custom logger in case the root logger does not work (not used now)
if sys.platform == 'darwin':
    logger = logging.getLogger('Ippy')
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())
    logger.debug('\n*** Debug log activated for custom logger "Ippy" ***\n')

