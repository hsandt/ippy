import heapq
import random
import collections

from numpy import float_ as fl

__author__ = 'hs'


# copied from the EDX class on AI, by John DeNero (denero@cs.berkeley.edu)
# added object as base class for Python 2
# added sorted method for debug
# ALTERNATIVE: use simpleAI advanced structures

class Stack(object):
    """A container with a last-in-first-out (LIFO) queuing policy."""
    __hash__ = None  # not impossible to build but not needed for now

    def __init__(self, list_=None):
        if list_ is None:
            self.list = []
        else:
            self.list = list_  # passed LIFO

    def fill_with_new_content(self, lifo):
        """
        Keep the properties of the container and fill it with new content.

        """
        self.list = lifo

    def push(self, item):
        """Push 'item' onto the stack"""
        self.list.append(item)

    def pop(self):
        """Pop the most recently pushed item from the stack"""
        return self.list.pop()

    def is_empty(self):
        """Returns true if the stack is empty"""
        return len(self.list) == 0

    def sorted(self):
        """Return the reversed list"""
        return self.list[::-1]

    def clear(self):
        del self.list[:]

    def split(self, n, size):
        """
        Split in n parts of the same type and return them
        Preserve the natural order of the structure and create smaller partial 'copies'

        The n-1 first parts have the passed size

        :rtype: list[Stack]

        """
        # take n parts of size size
        # the last may be empty if n is too big compared to len (n > len(self.list) / size) so stop before
        l = len(self.list)

        # nb_parts = min(n, l / size)
        # # -0 = 0 so prefer using l - 0 to ensure the first interval ends on the last element
        # # (the l is the first split index is optional, the 2nd is not)
        # parts = [self.list[l - (i + 1) * size:l - i * size] for i in xrange(nb_parts)]  # regular parts
        # parts.append(self.list[:-nb_parts * size])  # append last part (irregular)

        # take n parts of size
        # the last may be empty if n is too big compared to len (n > len(self.list) / size) so stop before
        # the first values are at the beginning
        regular_total_size = (n - 1) * size
        if regular_total_size <= l:
            nb_regular_parts = n - 1
        else:
            nb_regular_parts = l / size

        # get regular parts
        parts = [self.list[l - (i + 1) * size:l - i * size] for i in xrange(nb_regular_parts)]

        if regular_total_size < l or (regular_total_size > l and l % size > 0):
            # append last part (may be small or very big for 1st condition, smaller than regular part for 2nd condition)
            parts.append(self.list[:-nb_regular_parts * size:])

        return parts

    def split_into_copies(self, n, size):
        parts = self.split(n, size)
        return map(Stack, parts)

    def __contains__(self, item):
        return item in self.list

    def __iter__(self):
        return iter(self.list)

    def __len__(self):
        return len(self.list)

    def __eq__(self, other):
        return isinstance(other, Stack) and self.list == other.list

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return 'Stack: {}'.format(self.list)


class Queue(object):
    """A container with a first-in-first-out (FIFO) queuing policy."""
    def __init__(self, list_=None):
        if list_ is None:
            self.list = []
        else:
            self.list = list_  # passed FIFO

    def fill_with_new_content(self, fifo):
        """
        Keep the properties of the container and fill it with new content.

        """
        self.list = fifo

    def push(self,item):
        "Enqueue the 'item' into the queue"
        self.list.insert(0, item)

    def pop(self):
        """
          Dequeue the earliest enqueued item still in the queue. This
          operation removes the item from the queue.
        """
        return self.list.pop()

    def isEmpty(self):
        "Returns true if the queue is empty"
        return len(self.list) == 0

    def sorted(self):
        """Return the list in the same order"""
        return self.list

    def clear(self):
        del self.list[:]

    def split(self, n, size):
        """
        Split in n parts of the same type and return them
        Preserve the natural order of the structure and create smaller partial 'copies'

        The n-1 first parts have the passed size

        :rtype: list[Queue]

        """
        # take n parts of size size
        # the last may be empty if n is too big compared to len (n > len(self.list) / size) so stop before
        l = len(self.list)

        regular_total_size = (n - 1) * size
        if regular_total_size <= l:
            nb_regular_parts = n - 1
        else:
            nb_regular_parts = l / size

        # get regular parts
        parts = [self.list[l - (i + 1) * size:l - i * size] for i in xrange(nb_regular_parts)]

        if regular_total_size < l or (regular_total_size > l and l % size > 0):
            # append last part (may be small or very big for 1st condition, smaller than regular part for 2nd condition)
            parts.append(self.list[:-nb_regular_parts * size:])

        return parts

    def split_into_copies(self, n, size):
        parts = self.split(n, size)
        return map(Queue, parts)

    def __iter__(self):
        return iter(self.list)

    def __contains__(self, item):
        return item in self.list

    def __len__(self):
        return len(self.list)

    def __eq__(self, other):
        return isinstance(other, Queue) and self.list == other.list

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return 'Queue: {}'.format(self.list)


class PriorityQueue(object):
    """
    Implements a priority queue data structure. Each inserted item
    has a priority associated with it and the client is usually interested
    in quick retrieval of the lowest-priority item in the queue. This
    data structure allows O(1) access to the lowest-priority item.

    Note that this PriorityQueue does not allow you to change the priority
    of an item.  However, you may insert the same item multiple times with
    different priorities.

    """
    def __init__(self, heap=None, count=None, auto_heapify=True):
        # can pass parameters to build PQ manually
        # only use         auto_heapify=False if you are sure the heap already satisfies the invariant
        if heap is None:
            self.heap = []
            self.count = 0
        else:
            if count is None:
                raise ValueError('Please provide a count for the manually passed heap')
            self.fill_with_new_content(heap, count, auto_heapify)

    def copy_with_new_content(self, heap, count=None, auto_heapify=True):
        """
        Return a new object of the same class, with the same properties, but a new content (count included)

        Pass the count manually so that it is always superior to the highest count in the heap

        """
        return PriorityQueue(heap, count, auto_heapify)

    def fill_with_new_content(self, heap, count, auto_heapify=True):
        """
        Keep the properties of the container and fill it with new content.

        :param heap: the heap used to replace the current one
        :param count: a number higher than the highest count in the heap
        :param auto_heapify: Should we heapify the heap? (useful for arbitrary lists)
        :return:

        """
        if auto_heapify:
            heapq.heapify(heap)
        self.heap = heap
        self.count = count

    def push(self, item, priority):
        # FIXME: restored old behaviour to check against old results better
        # FIXED: restored to stable behaviour
        entry = (priority, self.count, item)
        # entry = (priority, item)
        heapq.heappush(self.heap, entry)
        self.count += 1

    def pop(self):
        (_, _, item) = heapq.heappop(self.heap)
        #  (_, item) = heapq.heappop(self.heap)
        return item

    def pop_kth_smallest(self, k):
        """Pop the k-th smallest priority item"""
        if not self.heap:
            raise IndexError('pop from empty list')
        if k <= 0:
            # REFACTOR: tolerate 0 and negative values, returning the smallest priority item? (as if k==1)
            raise IndexError('cannot pop k-th smallest for k <= 0')
        # O(n) worst case? nsmallest optimized for heap already sorted (Olog(n)?) but heapify O(n)...
        # index the heap by adding the index in last position (should preserve invariant thanks to count)
        indexed_heap = [entry + (i,) for i, entry in enumerate(self.heap)]
        # get the k-th smallest entry and record its index
        nsmallest_entries = heapq.nsmallest(k, indexed_heap)
        # if size k >= len(self.heap), the whole heap will be returned so we will always have a return value
        # in this case, it would be the item with the biggest priority
        # by taking the last element we ensure
        kth_smallest_item, kth_smallest_index = nsmallest_entries[-1][2:4]  # indexed_entry = (priority, count, item, index)
        # remove value and heapify (O(n))
        del self.heap[kth_smallest_index]
        heapq.heapify(self.heap)
        return kth_smallest_item

    def isEmpty(self):
        return len(self.heap) == 0

    def sorted(self):
        """Return the sorted list"""
        return sorted(self.heap)  # rarely used, but useful to debug

    def get_items(self):
        """Return the unsorted list of items contained in the heap, without priority or count"""
        return [entry[2] for entry in self.heap]

    def clear(self):
        del self.heap[:]

    def split(self, n, size):
        """
        Split in n parts and return them
        If the container has too few elements, stop splitting before the end. In the end we have nb_parts
        parts
        The (nb_parts-1) first parts have the passed size, the last one has an undefined size (but smaller than
        size if the container has too few elements)

        For a heap, there is no rule
        Only the head of the heap is guaranteed to also be a heap
        After after, we have to heapify to get a heap again

        Note that we can:
        - either split the listheap regularly even if does not make sense, because
         the split method is only used for random multistart and branching purposes
        - or split the heap by taking the best nodes each time (by re-heapifying every time or getting
        the sorted list once and then applying the usual split) -> CHOSEN

        :type n: int
        :type size: int

        """
        # sort the list O(?)
        # OPTIMIZE: if too crazy, just work on the unsorted list; it roughly contains ordered nodes
        # (you still have to heapify)
        # for the beginning of the heap
        ordered_list = self.sorted()

        # take n parts of size
        # the last may be empty if n is too big compared to len (n > len(self.list) / size) so stop before
        # the first values are at the beginning
        l = len(ordered_list)
        regular_total_size = (n - 1) * size
        if regular_total_size <= l:
            nb_regular_parts = n - 1
        else:
            nb_regular_parts = l / size

        # get regular parts
        parts = [ordered_list[i * size:(i + 1) * size] for i in xrange(nb_regular_parts)]

        if regular_total_size < l or (regular_total_size > l and l % size > 0):
            # append last part (may be small or very big for 1st condition, smaller than regular part for 2nd condition)
            parts.append(ordered_list[nb_regular_parts * size:])

        return parts

    def split_into_copies(self, n, size):
        """
        Split and return the parts as new containers with the same properties

        Memory expensive so use with caution

        """
        parts = self.split(n, size)

        # except the first part, reheapify; use copy_with_new_content to get an instance of the subclass
        priority_queues = [self.copy_with_new_content(parts[0], self.count, auto_heapify=False)]
        priority_queues.extend(
            [self.copy_with_new_content(part, self.count) for part in parts[1:]])  # includes irregular part
        return priority_queues

    def __iter__(self):
        return iter(self.heap)

    def __contains__(self, item):
        # OPTIMIZE: currently O(n), we could sort and bisect to take advantage of the heap
        return item in self.heap

    def __len__(self):
        return len(self.heap)

    def __eq__(self, other):
        return isinstance(other, PriorityQueue) and self.heap == other.heap and self.count == other.count

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return 'PriorityQueue ({1}): {0}'.format(self.heap, self.count)


class BoundedPriorityQueue(PriorityQueue):
    """
    Priority queue with a size limit.
    When limit is reached, lowest priority (highest value) nodes are automatically dropped.

    """
    def __init__(self, limit=None, heap=None, count=None, auto_heapify=True):
        super(BoundedPriorityQueue, self).__init__(heap, count, auto_heapify)
        self.limit = limit

    def copy_with_new_content(self, heap, count=None, auto_heapify=True):
        """
        Return a new object of the same class, with the same properties, but a new content (count included)

        Pass the count manually so that it is always superior to the highest count in the heap

        """
        return BoundedPriorityQueue(self.limit, heap, count, auto_heapify)

    def push(self, item, priority):
        super(BoundedPriorityQueue, self).push(item, priority)

        if self.limit is not None and len(self.heap) > self.limit:
            # limit reached, drop worst element
            # normally we need to heapify after such an operation, but removing the worst element preserves the heap
            # invariant
            # note that the operation is O(n)
            # in general using the key version of nlargest is slow so we prefer appending index at the end as in
            # pop_kth_smallest, but for 1 element the search is optimized so it is okay
            largest_entry_index, largest_entry = heapq.nlargest(1, enumerate(self.heap), key=lambda x: x[1])[0]
            del self.heap[largest_entry_index]
            heapq.heapify(self.heap)

        # IMPROVE: return popped node as extra info, as similar datastructures do?

    def __eq__(self, other):
        return isinstance(other, BoundedPriorityQueue) and self.heap == other.heap and self.count == other.count \
            and self.limit == other.limit

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return 'BoundedPriorityQueue ({1}): {0}'.format(self.heap, self.count)


class PriorityQueueWithFunction(PriorityQueue):
    """
    Implements a priority queue with the same push/pop signature of the
    Queue and the Stack classes. This is designed for drop-in replacement for
    those two classes. The caller has to provide a priority function, which
    extracts each item's priority.

    Accepts a viewer to debug which value is returned by the priority function

    """
    def __init__(self, priorityFunction, viewer=None, heap=None, count=None, auto_heapify=True):
        """priorityFunction (item) -> priority"""
        super(PriorityQueueWithFunction, self).__init__(heap, count, auto_heapify)
        self.priorityFunction = priorityFunction      # store the priority function
        self.viewer = viewer

    def copy_with_new_content(self, heap, count=None, auto_heapify=True):
        """
        Return a new object of the same class, with the same properties, but a new content (count included)

        Pass the count manually so that it is always superior to the highest count in the heap

        """
        return PriorityQueueWithFunction(self.priorityFunction, self.viewer, heap, count, auto_heapify)

    def push(self, item):
        """Adds an item to the queue with priority from the priority function"""
        priority = self.priorityFunction(item)
        super(PriorityQueueWithFunction, self).push(item, priority)
        if self.viewer is not None:
            self.viewer.event('push', item, priority)

    def __eq__(self, other):
        return isinstance(other, PriorityQueueWithFunction) and self.heap == other.heap and self.count == other.count \
            and self.priorityFunction == other.priorityFunction

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return 'PriorityQueueWithFunction ({1}): {0}'.format(self.heap, self.count)

class BoundedPriorityQueueWithFunction(BoundedPriorityQueue):
    """
    Bounded Priority Queue that automates the computation of the priority of the element
    via a predefined function.

    """
    def __init__(self, limit, priorityFunction, viewer=None, heap=None, count=None, auto_heapify=True):
        """priorityFunction (item) -> priority"""
        super(BoundedPriorityQueueWithFunction, self).__init__(limit, heap, count, auto_heapify)
        self.priorityFunction = priorityFunction      # store the priority function
        self.viewer = viewer

    def copy_with_new_content(self, heap, count=None, auto_heapify=True):
        """
        Return a new object of the same class, with the same properties, but a new content (count included)

        Pass the count manually so that it is always superior to the highest count in the heap

        """
        return BoundedPriorityQueueWithFunction(self.limit, self.priorityFunction, self.viewer, heap, count, auto_heapify)

    def push(self, item):
        """Adds an item to the queue with priority from the priority function"""
        priority = self.priorityFunction(item)
        super(BoundedPriorityQueueWithFunction, self).push(item, priority)
        if self.viewer is not None:
            self.viewer.event('push', item, priority)

    def __eq__(self, other):
        return isinstance(other, BoundedPriorityQueueWithFunction) and self.heap == other.heap and self.count == other.count \
            and self.limit == other.limit and self.priorityFunction == other.priorityFunction

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return 'BoundedPriorityQueueWithFunction ({1}): {0}'.format(self.heap, self.count)

# ISSUE: randomizing everything also randomizes important, precise measure such as depth_first_heuristic
# because of this, placements with otherwise low heuristics will be chosen first
# prefer applying the randomize decorator to individual heuristics
class PriorityQueueWithRandomizedFunction(PriorityQueueWithFunction):
    """
    Priority Queue that automates the computation of the priority of the element
    via a predefined function, adding a small random value

    """
    def __init__(self, priorityFunction, random_scale=1e-5, viewer=None):
        # alter priority function by adding a small variation
        # if the function returns a tuple, assume all elements are float and add a small variation to each
        # of course, scale is different for each value (area, length, count...) but just ensure
        # that the scale is inferior to the minimum difference between two values considered different
        # if you just want randomization to shuffle priorities otherwise equal
        super(PriorityQueueWithRandomizedFunction, self).__init__(randomize(priorityFunction, random_scale), viewer)

    def copy_with_new_content(self, heap, count=None, auto_heapify=True):
        """
        Return a new object of the same class, with the same properties, but a new content (count included)

        Pass the count manually so that it is always superior to the highest count in the heap

        """
        # priority function already randomized
        # warning: this will give an instance of PriorityQueueWithFunction, but in Python we duck-type so should be okay
        return PriorityQueueWithFunction(self.priorityFunction, self.viewer, heap, count, auto_heapify)

    def __eq__(self, other):
        return isinstance(other, PriorityQueueWithRandomizedFunction) and self.heap == other.heap and self.count == other.count \
               and self.priorityFunction == other.priorityFunction

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return 'PriorityQueueWithRandomizedFunction ({1}): {0}'.format(self.heap, self.count)


def randomize(function, max_variation=1e-5):
    """
    Return a decorated function that returns one or an array of float values, with a small,
     different variation for each value

    Use it to randomize heuristics in case of draw. However, be careful: randomizing a heuristic at a
    certain level in an ordered master heuristic will make the heuristics at lower levels not meaningful,
    since a draw is very unlikely to happen. So use it only for the bottom levels.

    :param function:
    :param max_variation: the variation will be in [-max_variation, +max_variation]

    :return: decorated function

    """
    def randomized_function(item):
        return add_random_variation_values(function(item), max_variation)

    return randomized_function


def add_random_variation_values(values, max_variation):
    """
    Return a copy of values where a random variation between -max_variation and max_variation
    has been added to each value

    If values contain multiple values, even at different depths, each of them will receive the random
    variation. All values must support + float operation

    :param values:
    :param max_variation:

    :return:

    """
    if not isinstance(values, collections.Sequence):
        # for a single value, just add the variation
        return values + random.uniform(-max_variation, max_variation)
    else:
        # for a tuple, add a variation to each value
        # since we want a different variation for each value, we recompute random value for each
        # in case another sequence is embedded, recurse the function
        return tuple(add_random_variation_values(value, max_variation) for value in values)


class PriorityQueueWithFunctionRandomizedPop(PriorityQueueWithFunction):
    """
    Priority Queue with a priority function that pops randomly nodes among the
    random_span lowest value elements in the heap

    Attributes
        :type random_span: int
        :param random_span: span over which nodes are uniformly randomly picked

    """
    def __init__(self, priorityFunction, random_span, viewer=None):
        super(PriorityQueueWithFunctionRandomizedPop, self).__init__(priorityFunction, viewer)
        self.random_span = random_span

    def pop(self):
        """
        Pop an element uniformly picked among the random_span lowest value elements

        :return: item

        """
        # Determine depth of element to pop from the heap with a uniform distribution
        # over (1..k), where k = min(len(heap), random_span)
        # This is because is the heap is too short, all search of depth superior to random_span
        # will return the last element, which will make the distribution not uniform
        actual_span = min(self.random_span, len(self.heap))
        k = random.randint(1, actual_span)
        return self.pop_kth_smallest(k)
