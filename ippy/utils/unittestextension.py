import difflib
import pprint
from unittest.util import safe_repr

from ippy.utils.functions import identity

__author__ = 'huulong'

# TODO: instead of having multiple independent functions that use test_case
# as 1st parameter, create a class derivated from TestCase with those extra methods

# a custom _type_equality_funcs that refers to our custom KeyEqual assert functions
# only a few types are currently supported, but since this is only used to redirect
# assertKeyEqual to a more specific KeyEqual, you can just use assertSequenceKeyEqual
# to go round this
_type_key_equality_funcs = {
    list: 'assertListKeyEqual',
    tuple: 'assertTupleKeyEqual',
    dict: 'assertDictKeyEqual'
}


def _getAssertKeyEqualityFunc(test_case, first, second):
    """Get a detailed comparison function for the types of the two args.

    Returns: A callable accepting (first, second, msg=None) that will
    raise a failure exception if first != second with a useful human
    readable error message for those types.

    """
    #
    # NOTE(gregory.p.smith): I considered isinstance(first, type(second))
    # and vice versa.  I opted for the conservative approach in case
    # subclasses are not intended to be compared in detail to their super
    # class instances using a type equality func.  This means testing
    # subtypes won't automagically use the detailed comparison.  Callers
    # should use their type specific assertSpamEqual method to compare
    # subclasses if the detailed comparison is desired and appropriate.
    # See the discussion in http://bugs.python.org/issue2578.
    #
    # This method from unittest.case has been copied here only
    # so that it could call our custom _baseAssertEqual, _baseAssertKeyEqual
    if type(first) is type(second):
        asserter = _type_key_equality_funcs.get(type(first))
        if asserter is not None:
            if isinstance(asserter, basestring):
                # here we use functions in the module scope not methods
                # asserter = getattr(test_case, asserter)
                asserter = globals()[asserter]
            return asserter

    return _baseAssertKeyEqual


def _baseAssertKeyEqual(test_case, first, second, key, msg=None):
    """The default assertEqual implementation, not type specific."""
    processed_first = key(first)
    processed_second = key(second)

    if not processed_first == processed_second:
        # standardMsg = '%s != %s\n(key %s was used)' % (safe_repr(first), safe_repr(second), key)
        standardMsg = '%s != %s\n(key %s was used)' % (processed_first, processed_second, key)
        msg = test_case._formatMessage(msg, standardMsg)
        raise test_case.failureException(msg)


def assertKeyEqual(test_case, first, second, key=identity, msg=None):
    """Fail if the two objects are unequal as determined by the '=='
       operator, after key has been applied to each object.
    """
    # # default key is identity
    # if key is None:
    #     key = identity

    # check appropriate assertion before processing elements, since sequences
    # must be processed element by element
    assertion_func = _getAssertKeyEqualityFunc(test_case, first, second)
    assertion_func(test_case, first, second, key, msg=msg)


# TODO: improve comparison message in KeyEqual functions (show both original and
# modified elements, show str not repr when possible...)

def assertSequenceKeyEqual(test_case, seq1, seq2, key=identity, msg=None, seq_type=None):
    """An equality assertion for ordered sequences (like lists and tuples).

    For the purposes of this function, a valid ordered sequence type is one
    which can be indexed, has a length, and has an equality operator.

    Args:
        test_case: TestCase in which the assertion is called. Since this function
            is not a method of unittest.TestCase, the instance must be passed
            manually.
        seq1: The first sequence to compare.
        seq2: The second sequence to compare.
        key: Function to call on each sequence element prior to making
            comparisons. Failure messages show the modified elements.
        seq_type: The expected datatype of the sequences, or None if no
                datatype should be enforced.
        msg: Optional message to use on failure instead of a list of
                differences.
    """
    if seq_type is not None:
        seq_type_name = seq_type.__name__
        if not isinstance(seq1, seq_type):
            raise test_case.failureException('First sequence is not a %s: %s'
                                    % (seq_type_name, safe_repr(seq1)))
        if not isinstance(seq2, seq_type):
            raise test_case.failureException('Second sequence is not a %s: %s'
                                    % (seq_type_name, safe_repr(seq2)))
    else:
        seq_type_name = "sequence"

    differing = None
    try:
        len1 = len(seq1)
    except (TypeError, NotImplementedError):
        differing = 'First %s has no length.    Non-sequence?' % (
                seq_type_name)

    if differing is None:
        try:
            len2 = len(seq2)
        except (TypeError, NotImplementedError):
            differing = 'Second %s has no length.    Non-sequence?' % (
                    seq_type_name)

    if differing is None:
        # custom equal: apply the key before comparing elements
        processed_seq1 = map(key, seq1)
        processed_seq2 = map(key, seq2)
        if processed_seq1 == processed_seq2:
            return

        seq1_repr = safe_repr(seq1)
        seq2_repr = safe_repr(seq2)
        if len(seq1_repr) > 30:
            seq1_repr = seq1_repr[:30] + '...'
        if len(seq2_repr) > 30:
            seq2_repr = seq2_repr[:30] + '...'
        elements = (seq_type_name.capitalize(), seq1_repr, seq2_repr, key)
        differing = '%ss differ: %s != %s\n(key %s was used)\n' % elements

        for i in xrange(min(len1, len2)):
            try:
                item1 = seq1[i]
                processed_item1 = processed_seq1[i]
            except (TypeError, IndexError, NotImplementedError):
                differing += ('\nUnable to index element %d of first %s\n' %
                              (i, seq_type_name))
                break

            try:
                item2 = seq2[i]
                processed_item2 = processed_seq2[i]
            except (TypeError, IndexError, NotImplementedError):
                differing += ('\nUnable to index element %d of second %s\n' %
                              (i, seq_type_name))
                break

            # compare processed items but use the original ones for messages
            if processed_item1 != processed_item2:
                differing += ('\nFirst differing element %d:\n%s\n%s\n' %
                              (i, processed_item1, processed_item2))
        else:
            if (len1 == len2 and seq_type is None and
                type(seq1) != type(seq2)):
                # The sequences are the same, but have differing types.
                return

        if len1 > len2:
            differing += ('\nFirst %s contains %d additional '
                         'elements.\n' % (seq_type_name, len1 - len2))
            try:
                differing += ('First extra element %d:\n%s\n' %
                              (len2, processed_seq1[len2]))
            except (TypeError, IndexError, NotImplementedError):
                differing += ('Unable to index element %d '
                              'of first %s\n' % (len2, seq_type_name))
        elif len1 < len2:
            differing += ('\nSecond %s contains %d additional '
                         'elements.\n' % (seq_type_name, len2 - len1))
            try:
                differing += ('First extra element %d:\n%s\n' %
                              (len1, processed_seq2[len1]))
            except (TypeError, IndexError, NotImplementedError):
                differing += ('Unable to index element %d '
                              'of second %s\n' % (len1, seq_type_name))
    standardMsg = differing
    diffMsg = '\n' + '\n'.join(
        difflib.ndiff(pprint.pformat(processed_seq1).splitlines(),
                      pprint.pformat(processed_seq2).splitlines()))
    standardMsg = test_case._truncateMessage(standardMsg, diffMsg)
    msg = test_case._formatMessage(msg, standardMsg)
    test_case.fail(msg)


def assertListKeyEqual(test_case, list1, list2, key=identity, msg=None):
    """A list-specific equality assertion.

    Args:
        test_case: TestCase in which the assertion is called. Since this function
            is not a method of unittest.TestCase, the instance must be passed
            manually.
        list1: The first list to compare.
        list2: The second list to compare.
        msg: Optional message to use on failure instead of a list of
                differences.

    """
    assertSequenceKeyEqual(test_case, list1, list2, key, msg, seq_type=list)


def assertTupleKeyEqual(test_case, tuple1, tuple2, key=identity, msg=None):
    """A tuple-specific equality assertion.

    Args:
        test_case: TestCase in which the assertion is called. Since this function
            is not a method of unittest.TestCase, the instance must be passed
            manually.
        tuple1: The first tuple to compare.
        tuple2: The second tuple to compare.
        msg: Optional message to use on failure instead of a list of
                differences.
    """
    assertSequenceKeyEqual(test_case, tuple1, tuple2, key, msg, seq_type=tuple)


def assertDictKeyEqual(self, d1, d2, key=identity, msg=None):
    self.assertIsInstance(d1, dict, 'First argument is not a dictionary')
    self.assertIsInstance(d2, dict, 'Second argument is not a dictionary')

    processed_d1 = {k: key(v) for k, v in d1.iteritems()}
    processed_d2 = {k: key(v) for k, v in d2.iteritems()}

    if processed_d1 != processed_d2:
        standardMsg = '%s != %s' % (safe_repr(processed_d1, True), safe_repr(processed_d2, True))
        standardMsg += '\n(key %s was used)' % key
        diff = ('\n' + '\n'.join(difflib.ndiff(
                       pprint.pformat(processed_d1).splitlines(),
                       pprint.pformat(processed_d2).splitlines())))
        standardMsg = self._truncateMessage(standardMsg, diff)
        self.fail(self._formatMessage(msg, standardMsg))
