"""Extension for math and numpy operations"""
import numpy as np
from numpy import float_ as fl
from numpy.core import asarray

__author__ = 'huulong'

def round7(x):
    """Return x rounded on 7 digits"""
    return round(x, 7)


def normalize(v, threshold=1e-7):
    """
    Return a normalized copy of a vector with at least 1D.
    If the norm of v is under a threshold, return a null vector.

    :param v: array-like of dimension 1 or more
    :param threshold: norm threshold under which v is considered null
    :return: normalized vector

    """
    v = asarray(v, dtype=fl)
    norm = np.linalg.norm(v)
    if norm < threshold:
        return fl(0, 0)
    else:
        return v / norm
