import numpy as np
from numpy.linalg import norm
from numpy import float_ as fl

from shapely.affinity import rotate, translate, scale
from collections import Counter
import logging
import itertools
import numpy as np
from numpy.linalg import norm

from shapely.affinity import rotate, translate, affine_transform, scale
from shapely.geometry import MultiPoint, MultiLineString, box, Polygon, LinearRing, LineString, MultiPolygon, Point
from shapely.geometry.base import BaseGeometry
from shapely.geometry.collection import GeometryCollection
from shapely.geos import TopologicalError
from shapely.ops import linemerge, unary_union
from shapely.validation import explain_validity


from ippy.utils.decorator import deprecated
from ippy.utils.geometry import BoundedSide, Position, opposites, get_vector_orientation, Orientation
from ippy.utils.iterables import flatten, overlap_pairwise

from numpy import float_ as fl

from ippy.config.config import config

__author__ = 'hs'


class PolygonExtended(Polygon):
    """
    Extension of Polygon that supports geometry equality, generalized
    member access and easier debugging.

    Since BaseGeometry shapely operations return non-extended BaseGeometry,
    wrapper methods were added to return a PolygonExtended instead, when considered adequate.

    As normal Polygons, a PolygonExtended extended may be empty. Such an object remains of class
    PolygonExtended (as a shapely Polygon), so all methods can be used, but the type member is 'GeometryCollection'

    """
    __hash__ = None  # our equal is a complex operation based on rounding and geometric equality, hard to hash

    def __init__(self, shell=None, holes=None):
        super(PolygonExtended, self).__init__(shell, holes)

    def __eq__(self, other):
        """
        Return True if the other object is a BaseGeometry (we test base class in case it is empty)
        and they are both empty or equal in shapely equals comparison.
        (The shapely equals method returns False if one geometry is empty.)

        :type other: BaseGeometry
        :rtype: bool
        """
        return isinstance(other, BaseGeometry) and check_equal(self, other)

    def __ne__(self, other):
        return not self.__eq__(other)

    # Polygon already uses WKT for string representation
    # def __str__(self):
    #     return self.wkt

    def check_if_bounds_inside(self, other_bounds, threshold=1e-14):
        """
        Return True if the bounds of the polygon are inside other bounds.
        If the polygon is touching the other bounds from the interior, it is considered as inside.

        If this object is empty, return True

        :param other_bounds: (min_x, min_y, max_x, max_y)
        :param threshold: tolerance threshold

        """
        if self.is_empty:
            return True

        # use threshold to be more robust against floating errors due to rotation, and cause unexpected unary constraint
        # violation warning
        return self.bounds[0] - other_bounds[0] >= -threshold and self.bounds[1] - other_bounds[1] >= -threshold and \
            self.bounds[2] - other_bounds[2] <= threshold and self.bounds[3] - other_bounds[3] <= threshold

    def check_if_overlap(self, other_polygon):
        """
        Return True if the polygon is overlapping the other polygon,
        i.e. it is intersecting without touching i.e. it is not disjoint nor touching it.

        If this object is empty, return False

        Note that this test is subject to floating-point issues.
        Copy the code in PolygonWithBoundary.get_bounded_side to add a precision threshold.

        :param other_polygon: Polygon(Extended)
        """
        if self.is_empty:
            return False

        # FIXME? Possible floating-point issues here, inspire from PolygonWithBoundary.get_bounded_side
        # or use phi-nfp to evaluate overlap
        return self.intersects(other_polygon) and not self.touches(other_polygon)

    def place_copy(self, placement):
        """
        Return a copy of the PolygonExtended, placed at placement

        :param placement: Placement
        :return: PolygonExtended

        """
        return PolygonExtended(place_geom(self, placement))


class MultiPolygonExtended(MultiPolygon):
    """Extension for shapely MultiPolygon. Does *not* contain PolygonExtended instances."""
    # TODO


class ComparableBaseGeometry(object):
    """
    Wrapper for BaseGeometry objects to enable comparisons.
    Use it as a key in sorting and assert functions.

    """
    def __init__(self, obj):
        self.obj = obj
    def __eq__(self, other):
        return check_equal(self.obj, other.obj)  # use almost_equals if you have issues
    def __ne__(self, other):
        return not self.__eq__(other)
    # TODO: add within and other comparisons

    def __repr__(self):
        return "<ComparableBaseGeometry: ({0})>".format(self.obj.__repr__())

    def __str__(self):
        return "<ComparableBaseGeometry: ({0})>".format(self.obj)


class GeomWrapper(object):
    """
    Wrapper for BaseGeometry objects that adds meta-information and operations.

    Equality comparison
        Use this class as a key in sorting and assert functions.

    ID
        The ID provides a way to know the shape of the geometry (mainly polygons)
        if we have access to the polygons dictionary of the problem.

    """
    # we implemented __eq__ without a hash for this class
    # isinstance is also important in __eq__ else assertItemsEqual tests would fail
    __hash__ = None

    def __init__(self, obj, geom_id, name=None):
        self.obj = obj
        self.geom_id = geom_id
        if name is not None:
            self.name = name
        else:
            self.name = geom_id  # if single piece of this type, there will be no confusion

    def __eq__(self, other):
        # we compare all attributes for more precise tests
        # (use almost_equals on obj if you have issues)
        return isinstance(other, type(self)) and check_equal(self.obj, other.obj) and \
            self.geom_id == other.geom_id and self.name == other.name

    def __ne__(self, other):
        return not self.__eq__(other)

    # TODO: add within and other comparisons

    def __repr__(self):
        return "<GeomWrapper: ({0}, {1})>".format(self.obj.__repr__(), self.geom_id)

    def __str__(self):
        return "<GeomWrapper: ({0}, {1})>".format(self.obj, self.geom_id)


# FIXME: intersection of polygons can reduce to LineString or event Point! cope with that!
# solution 1: keep the true closed set no matter what, line or whatever
# solution 2: keep the interior of the object, so if an intersection ends up as a line
#   just forget it but as part of the boundary
class PolygonWithBoundary(object):
    """
    Container for a Geometry closed interior: (Multi)Polygon and Boundary: MultiGeometry of LineStrings and Points.
    It represents an open polygon, possibly having boundaries inside, which is impossible with Polygons.
    Use it to describe NFP without losing information on feasible positions on Lines and Points.

    The 'closed interior' is the interior of the Geometry which has been closed. This means all Points and
    LineStrings have disappeared in the process. What we really want is the interior, but Shapely cannot provide a model
    for that.
    The boundary complements the interior. Since we use a closed interior, the boundary has a common intersection
    with the latter wherever there is a trivial boundary.

    Attributes
        closed_interior (property)
            Multi- or Single Polygon representing the closed set of this geometry
        boundary
            Multi or Single Geometry of LineStrings and Points representing the exterior and interior boundaries
            of the open polygon

    """
    # we implemented __eq__ without a hash for this class
    __hash__ = None

    def __init__(self, closed_interior=None, boundary=None, trivial_boundary_from_interior=False):
        """

        :type closed_interior: (Polygon | MultiPolygon | None)
        :param closed_interior:
        :type boundary: (LineString | MultiLineString | Point | MultiPoint | GeometryCollection | None)
        :param boundary:
        :type trivial_boundary_from_interior: bool
        :param trivial_boundary_from_interior: Should we generate the boundary from the closed interior?
            (passed boundary is still used). If no boundary is passed, this argument is ignored.
            If no closed_interior is passed, the result is the same whichever the value.
            In other words, only use this when both the interior and boundaries are passed.
        :return:
        """
        self._closed_interior = GeometryCollection()
        if closed_interior is not None:
            self.closed_interior = closed_interior

        if boundary is None:
            # deduce boundary from closed interior, whatever trivial_boundary_from_interior is
            if self.closed_interior.is_empty:
                self.boundary = GeometryCollection()
            else:
                self.boundary = closed_interior.boundary  # LineString or MultiLineString
        elif not trivial_boundary_from_interior:
            # boundary is provided and you should use this one only
            self.boundary = boundary
        else:
            # non-trivial boundary is provided but closed_interior boundary will complete
            # do not pass a trivial boundary here, it may double the boundary it there is a small offset between them
            if self.closed_interior.is_empty:
                # in this case the closed interior was empty so no extra boundary
                self.boundary = boundary
            else:
                self.boundary = boundary | self.closed_interior.boundary  # this is a shapely boundary
        # we could check that the user completely define the boundary, under the assertion:
        # assert (boundary & self.closed_interior.boundary).equals(self.closed_interior.boundary)
        # however, recently I added a rounding on the boundary after some ops so this may not be exactly True
        # in other words, for now, be responsible and consistent!

    def __deepcopy__(self, memo):
        # keep shallow copy at geometry level, since shapely geom is immutable
        return PolygonWithBoundary(self.closed_interior, self.boundary)

    @property
    def closed_interior(self):
        """
        Multi- or Single Polygon representing the closed interior of this geometry

        :rtype: (GeometryCollection | Polygon | MultiPolygon)
        """
        return self._closed_interior

    @closed_interior.setter
    def closed_interior(self, value):
        """:param value: BaseGeometry"""
        # REFACTOR: only allow MultiPolygon? Convert to MultiPolygon here?
        if not is_polygon_like(value):
            raise ValueError('Cannot set PolygonWithBoundary closed interior to non polygon-like geom: {}'.format(value))
        self._closed_interior = value

    # TODO: add in-place operations

    def __and__(self, other):
        return self.intersection(other)

    def __or__(self, other):
        return self.union(other)

    def __eq__(self, other):
        """Return True if both interiors and boundaries are almost equal"""
        return isinstance(other, type(self)) and check_equal(self.closed_interior, other.closed_interior) and \
            check_equal(self.boundary, other.boundary)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return "<PolygonWithBoundary: ({0}, {1})>".format(repr(self.closed_interior), repr(self.boundary))

    def __str__(self):
        # delegate printing to each BaseGeometry member, which itself uses WKT
        return "PolygonWithBoundary: [closed interior: {0}, boundary: {1}]".format(self.closed_interior, self.boundary)

    # TODO: add assertions after ops to ensure shape and type consistency

    @property
    def type(self):
        """
        'PolygonWithBoundary' string, convenience property when working with usual shapely geometries
        at the same time.

        :rtype: str

        """
        return 'PolygonWithBoundary'

    @property
    def is_empty(self):
        """
        True if the geometry is empty

        :rtype: bool

        """
        # this is empty iff boundary is, but you can check closed_interior too if you prefer
        # we just add an extra check for debug (OPTIMIZE: remove it for a small gain in perf)
        if self.boundary.is_empty:
            assert self.closed_interior.is_empty, 'Boundary of {} is empty but not closed interior!'.format(self)
        return self.boundary.is_empty

    @property
    def is_valid(self):
        """
        True if the closed interior and boundaries are valid, else False

        :rtype: bool

        """
        # check if components are shapely valid, and also if boundary includes the boundary of the closed interior
        # warning: this may not always work! a very small offset will cause a false negative
        is_shapely_valid = self.closed_interior.is_valid and self.boundary.is_valid
        is_boundary_consistent = self.is_boundary_consistent
        return is_shapely_valid and is_boundary_consistent

    @property
    def is_boundary_consistent(self):
        """
        Does the boundary include the closed interior's boundary?

        :rtype: bool

        """
        # OPTIMIZE vs DEBUG: check_equal uses round_coords(), and round_coords check geom validity
        # so there is a loop between assert_Validity and round_coords, very bad for performance!
        # need to cut it somewhere or deactivate assertions completely...
        # Here, we simply check equality without rounding
        # for now, use semidebug or release mode...
        # if you really want to check this, remove the assertion from round_coords!

        return self.closed_interior.is_empty or \
            check_equal(self.boundary & self.closed_interior.boundary, self.closed_interior.boundary)
            # (self.boundary & self.closed_interior.boundary).equals(self.closed_interior.boundary)

    @property
    def area(self):
        """
        Area of the closed interior.
        The boundary is not considered.

        :rtype: float

        """
        return self.closed_interior.area

    @property
    def closed_interior_length(self):
        """
        Perimeter of the closed interior, ie length of the trivial boundary.

        :rtype: float

        """
        return self.closed_interior.length

    @property
    def length(self):
        """
        Length of the whole boundary, trivial and extra.
        Caution: extra lines are counted only once, where intuitively they come from a back-and-forth
         movement when building an NFP for example.
         This is because we care more about the actual remaining domain in domain size measurement.

        To get the length of the extra boundary only, use self.length - self.closed_interior_length

        :rtype: float

        """
        return self.boundary.length

    @property
    def points_cardinal(self):
        """
        Number of individual points in the boundary

        :rtype: int

        """
        # OPTIMIZE: cache this result
        return len(self.get_boundary_multipoint())

    @property
    def bounds(self):
        """:rtype: float"""
        bounds = list(self.closed_interior.bounds)
        update_bounds(bounds, self.boundary.bounds)
        return bounds

    @property
    def convex_hull(self):
        """:rtype: (GeometryCollection | Point | LineString | Polygon)"""
        # the boundary alone should contain the "farthest" points in the geometry
        return self.boundary.convex_hull

    def get_boundary_multipoint(self):
        """
        Return a MultiPoint containing all the inidividual points in the boundary

        :rtype: MultiPoint

        """
        return get_points_as_multipoint(self.boundary)

    def extract_exteriors_holes_boundary_tuple(self):
        """
        Return a tuple containing the list of positive polygons i.e. exteriors, the list of negative polygons i.e. holes
        and the boundary of this geometry.
        The holes are returned as filled Polygons instead of LinearRings as stored in Shapely, to enable
        conversion to XML with a function handling polygons.
        Note that <polygon> in the XML is always a filled polygon, so a LinearRing would actually be the
        correct structure to describe a polygon node.

        This is useful to transform an NFP structure into a fragmented data structure that fits into an XML.

        :rtype: (list[LinearRing], list[LinearRing], (LineString | MultiLineString | Point | MultiPoint | GeometryCollection))

        """
        exteriors = []
        holes = []
        if not self.closed_interior.is_empty:
            if self.closed_interior.type == 'Polygon':
                self._add_polygons_holes(self.closed_interior, exteriors, holes)
            elif self.closed_interior.type == 'MultiPolygon':
                for polygon in self.closed_interior:
                    self._add_polygons_holes(polygon, exteriors, holes)

        return exteriors, holes, self.boundary

    @staticmethod
    def _add_polygons_holes(polygon, exteriors, holes):
        """
        Append individual exteriors and holes from a polygon.
        Both exteriors and holes are LinearRings

        :type polygon: Polygon
        :type exteriors: list[LinearRing]
        :type holes: list[LinearRing]

        """
        # add LinearRing for the exterior
        exteriors.append(polygon.exterior)
        # add all holes (LinearRings) without minding where they come from
        for hole_ring in polygon.interiors:
            holes.append(hole_ring)

    def intersection(self, other):
        """
        Returns the intersection of the geometries.
        If other is a Polygon, it is converted in a PolygonWithBoundary before applying the operation.

        :type other: (PolygonWithBoundary | Polygon)

        """
        if other.type not in ('Polygon', 'PolygonWithBoundary'):
            raise TypeError('Cannot compute intersection of PolygonWithBoundary and geometry type')

        if other.type == 'Polygon':
            other = PolygonWithBoundary(other, trivial_boundary_from_interior=True)  # boundary is deduced

        # apply intersection of 2 polygons with boundary formula (see notes)
        # TODO? polygonize if the result is not clean enough
        # compute intersection of closed interiors, open and reclose to eliminate any Line and Points
        # in practice, manually remove such parts
        # in order to get quick access to the boundary at anytime, we also convert the GeometryCollection
        # to a MultiPolygon
        # WARNING: after an intersection we may have a fake positive with a very small triangle...
        # merging close vertices will result in a point, but the current algo will try to build
        # a Polygon with 1 point, not good!

        # FIXME? if invalidity issue, use buffer(0)

        my_closed_interior = self.closed_interior
        my_closed_interior = my_closed_interior.buffer(0)  # shapely method to clean geometry

        assert_validity(my_closed_interior)
        assert_validity(other.closed_interior)

        # it will also solve the problem of small triangle and flat polygons by making the polygons empty
        # you can choose only to buffer when the polygon is invalid too
        closed_interior = get_closed_interior_as_multipolygon(my_closed_interior & other.closed_interior)
        # UNDEBUG
        assert_validity(closed_interior)


        # reuse bufferer closed interior for boundary!
        try:
            boundary = self.boundary & other.closed_interior | other.boundary & my_closed_interior | (self.boundary & other.boundary)
        except TopologicalError:
            logging.warn('TopologicalError when computing boundary for\nself.boundary: %s\nbuffered self.closed_interior: %s\n'
                         'other.boundary: %s\nother.closed_interior: %s',
                         self.boundary, my_closed_interior, other.boundary, other.closed_interior)
            raise

        assert_validity(boundary)

        return PolygonWithBoundary(closed_interior, boundary)

    def union(self, other):
        """
        Returns the union of the geometries
        If other is a Polygon, it is converted in a PolygonWithBoundary before applying the operation.

        :type other: (PolygonWithBoundary | Polygon)

        """
        # FIXME: wrong formula for IFP-like PWBs! for instance, two contiguous IFPs
        # touching on an edge: after union, the edge should be removed

        if other.type not in ('Polygon', 'PolygonWithBoundary'):
            raise TypeError('Cannot compute union of PolygonWithBoundary and geometry type')

        # FIXME: many unions will add error, round by merging close vertices after each operation
        # you can also apply one of the rounding methods seen in the paper as a preprocessing
        # for now, we will use unary union as much as possible and round as postprocessing when needed

        # apply union of 2 polygons with boundary formula (see notes)
        # NEXT FIXME: seems like small line strings (close to a point) appear in Tangram but they mean nothing,
        # just a redundancy other the polygon
        # maybe use get_closed_interior_as_multipolygon to cleanup as in intersection?
        # for now let's do that
        # TODO: check if it worked (computation takes time, prefer no debug and print!)
        closed_interior = self.closed_interior.union(other.closed_interior)
        closed_interior = get_closed_interior_as_multipolygon(closed_interior)
        # round closed_interior
        # closed_interior = merge_close_successive_vertices(closed_interior)
        closed_interior = closed_interior.buffer(0)  # shapely method to clean geometry
        # UNDEBUG
        assert_validity(closed_interior)

        boundary = (self.boundary - other.closed_interior) | (other.boundary - self.closed_interior) | \
            (self.boundary & other.boundary)
        # UNDEBUG
        assert_validity(boundary)

        # cleanup shape
        # boundary = round_coords(boundary)
        # boundary = linemerge_boundary(boundary)

        return PolygonWithBoundary(closed_interior, boundary)

    @staticmethod
    def unary_union(polygon_with_boundaries):
        """
        Return the union of all polygon with boundaries.

        The union of closed interiors is optimized with Shapely's equivalent function,
        but for the boundaries they are differences involved so it only consists in iterations.

        This should be enough to avoid computational errors, since boundaries are less problematic.

        :param polygon_with_boundaries:
        :return:

        """
        # DOES NOT WORK WELL, says there are null geoms even when polygons are not empty and are valid
        print([list(x.closed_interior.exterior.coords) for x in polygon_with_boundaries])
        print([x.closed_interior.is_valid for x in polygon_with_boundaries])
        closed_interior = unary_union([pwb.closed_interior for pwb in polygon_with_boundaries])
        # optimizing boundary union by generalizing the formula (check if correct)
        # no, difficult because we only make the difference of a boundary regarding OTHER closed interiors
        # so as a unary union it is difficult to compute, except by computing the union
        # of all OTHER closed interiors each time... is it really cheaper?
        # boundary = unary_union([pwb.boundary for pwb in polygon_with_boundaries]) -

        # but no choice, we need partial closed interiors else...
        # boundary = GeometryCollection()
        # for next_pwb in polygon_with_boundaries:
        # boundary = (boundary - other.closed_interior) | (other.boundary - closed_interior) | \
        #            (self.boundary & other.boundary)

        # or we could normally iterate but clean-up the result each time

        # WARNING: not sure, test the result!

        reduced_boundaries = []
        boundary_intersection = GeometryCollection()
        for idx, pwb in enumerate(polygon_with_boundaries):
            reduced_boundary = pwb.boundary - unary_union([polygon_with_boundaries[other_idx].closed_interior for other_idx in xrange(len(polygon_with_boundaries)) if other_idx != idx])
            reduced_boundaries.append(reduced_boundary)
            # no cascaded/unary intersection in Shapely
            boundary_intersection &= pwb.boundary
        boundary = unary_union(reduced_boundaries + [boundary_intersection])

        return PolygonWithBoundary(closed_interior, boundary)

    # TODO: add test for this, including testing that removing a part and adding it by union again gives
    # the initial result
    def ifp_sub_nfp(self, nfp):
        """
        Helper method to substract an NFP from an IFP, should be called on an IFP.
        Warning: the result is not meaningful for any other kind of PolygonWithBoundary.

        """
        # DEBUG
        assert_validity(self)
        assert_validity(nfp)
        if config.weak_rounding:
            rounded_self = round_coords(self, 14)
            nfp = round_coords(nfp, 14)
        else:
            rounded_self = self

        # FIXME: LineString appears because of redundant consecutive coords in closed_interior before op
        closed_interior = rounded_self.closed_interior.difference(nfp.closed_interior)

        # now NFP are rounded on construction (in C-G), but just in case merge closed vertices
        # FIXED? merge_close_successive_vertices -> invalid collision parallelogram overlap not detected
        # merge_close_successive_vertices does not merge holes coords, but at least preserve them now

        # closed_interior = merge_close_successive_vertices(closed_interior)

        # FIXME: rounding turns line intersections into flat polygons
        # FIXED? I added a buffer that removes flat parts (and ties), hoping the shape does not change too much
        # since we remove lines and points with get_closed_interior_as_multipolygon it should be enough

        closed_interior.buffer(0)
        if config.strong_rounding:
            closed_interior = round_coords(closed_interior)

        # IMPROVE: round coords too if required

        # at that point the closed interior may have been reduced to a Point or a LineString
        # we can:
        # a - move that to the boundary and make the closed interior empty
        # or
        # b - count on the fact that, if the closed interior contained a small triangle or was flat,
        # the boundary operation will probably give the same result as the transformed closed interior
        # so simply set the closed interior to empty, and let the boundary calculation retrieve the
        # lower dimensional stuff

        # chosen: b, it is similar to the intersection where we got rid of lower dimensional parts
        # we can only use as_multipolygon method to get a MultiPolygon (MP) but this is optional
        # if it is too annoying to work with MP use get_interior_container() only, but be consistent
        # FIXME: return GeometryCollection of Polygon on iteration 757, greedy tangram. pop smallTriangle, rot 135
        # tolerate it? unary union to clean up?

        closed_interior = get_closed_interior_as_multipolygon(closed_interior)
        # closed_interior = round_coords(closed_interior)

        # UNDEBUG
        assert_validity(closed_interior)

        # # TEMP DEBUG
        # if closed_interior.type == 'Polygon':
        #     counter = Counter(closed_interior.exterior.coords[:-1])
        #     if any(count > 1 for count in counter.values()):
        #         raise ValueError('Redundant vertex in {}, {}'.format(closed_interior, counter))
        # # assert is_polygon_like(closed_interior)

        # FIXME: small offset will create boundary inconsistent with closed interior boundary
        # a - round at very high level (1e-14) before computing, hoping we won't create difference
        # with other geometries (everything should be eventually rounded to 1e-14)
        # b - recreate trivial boundary from closed interior but purge fake boundary remnants before (how?)
        # maybe, since we always consider parts of the original boundary or the other boundary,
        # we do not run the run to add too many wrong things?

        # should we alter nfp data in-place? more consistent but may break previous assumptions... just a rounding, though
        # try to round NFP on creation to start with
        boundary = (rounded_self.boundary - nfp.closed_interior) | (nfp.boundary & rounded_self.closed_interior) | \
            (rounded_self.boundary & nfp.boundary)

        # cleanup shape: fails except if you separate lines from points and ensure there are at least 2 lines
        # linemerge does not work well; unary_union does but may be costly (although for lines should be okay)
        # and new ops on the same object will act as partial cleaners

        # round (will help cleanup during ops)
        # be careful with the number of decimals; containment test use 1e-11
        # FIXME: if floating point issues with boundaries caused by this rounding...
        if config.strong_rounding:
            boundary = round_coords(boundary, 14)

        # UNDEBUG
        assert_validity(boundary)

        # TRY: we try to convert Polygons too close to a point to a Point, easier for visual debug
        # and also avoiding FPV on 3 positions very close to each other
        # (merge vertices does that job but creates invalid shapes in that case, and raises an Error)
        remaining_pwb = PolygonWithBoundary(closed_interior, boundary)  # do NOT generate boundary from interior again
        # remaining_pwb.cleanup_pointlike_polygon()

        # TEMP UNDEBUG because error is about a vertex in the middle of an edge, slightly offset...
        # MULTILINESTRING ((38.48528137423857 5.17157287525381, 9.65685424949238 5.17157287525381), (13.65685424949238 1.17157287525381, 12.82842712474619 2), (14.82842712474619 0, 38.48528137423857 0), (14.82842712474619 0, 13.65685424949238 1.17157287525381), (12.82842712474619 2, 9.65685424949238 5.17157287525381), (38.48528137423857 0, 38.48528137423857 5.17157287525381))
        # vs closed interior
        # POLYGON ((38.48528137423857 5.17157287525381, 38.48528137423857 0, 14.82842712474619 0, 12.82842712474619 2, 9.65685424949238 5.17157287525381, 38.48528137423857 5.17157287525381))
        # assert_validity(remaining_pwb)

        return remaining_pwb

    def translated(self, xoff, yoff):
        """
        Return a copy of this PolygonWithBoundary translated of (xoff, yoff)

        :param xoff:
        :param yoff:

        """
        return PolygonWithBoundary(translate(self.closed_interior, xoff, yoff), translate(self.boundary, xoff, yoff))

    def translate(self, xoff, yoff):
        """
        Translated in-place of (xoff, yoff)

        :param xoff:
        :param yoff:

        """
        self.closed_interior = translate(self.closed_interior, xoff, yoff)
        self.boundary = translate(self.boundary, xoff, yoff)

    def rotated(self, angle, use_radians=False):
        """
        Return a copy of this PolygonWithBoundary, rotated with angle.

        :type angle: Placement
        :type use_radians: bool
        :rtype: PolygonWithBoundary

        """
        return PolygonWithBoundary(rotate_origin(self.closed_interior, angle, use_radians),
                                   rotate_origin(self.boundary, angle, use_radians))

    def rotate(self, angle, use_radians=False):
        """
        Rotate in-place of angle.

        :type angle: Placement
        :type use_radians: bool
        :rtype: PolygonWithBoundary

        """
        self.closed_interior = rotate_origin(self.closed_interior, angle, use_radians)
        self.boundary = rotate_origin(self.boundary, angle, use_radians)

    def scaled(self, factor):
        """
        Return a copy of this PolygonWithBoundary scaled of (factor, factor)

        :param factor: scale factor

        """
        return PolygonWithBoundary(scale(self.closed_interior, factor, factor, origin=(0, 0)),
                                   scale(self.boundary, factor, factor, origin=(0, 0)))

    def scale(self, factor):
        """
        Scale in-place of (factor, factor)

        :param factor: scale factor

        """
        self.closed_interior = scale(self.closed_interior, factor, factor, origin=(0, 0))
        self.boundary = scale(self.boundary, factor, factor, origin=(0, 0))

    def get_bounded_side(self, point, threshold=1e-11):
        """
        Return the BoundedSide on which a point is, relatively to this geometry's boundary.

        :type point: Point

        :rtype: BoundedSide

        """
        # in Shapely, lines contain points on their open set, touches extreme vertices
        # TODO: add tests for this

        if not isinstance(point, Point):
            point = Point(point)  # point must be a 2-array like

        if threshold == 0:
            # OLD, exact version, probably not useful when floating errors arise
            if self.boundary.contains(point) or self.boundary.touches(point):
                return BoundedSide.on_boundary
            elif self.closed_interior.contains(point):
                return BoundedSide.bounded_side
            else:
                return BoundedSide.unbounded_side

        absolute_distance = point.distance(self.boundary)
        if absolute_distance < threshold:
            return BoundedSide.on_boundary
        elif point.within(self.closed_interior):
            return BoundedSide.bounded_side
        else:
            return BoundedSide.unbounded_side

    def contains_or_touch(self, point, threshold=1e-11):
        """
        Return True if point is inside the closed interior or close to the boundary
        with some threshold.

        :type point: (Point | (float, float) | [float, float])
        :type threshold: float
        :rtype: bool

        """
        if not isinstance(point, Point):
            point = Point(point)  # point must be a 2-array like
        return self.get_bounded_side(point, threshold) in (BoundedSide.on_boundary, BoundedSide.bounded_side)

    def strictly_contains(self, point, threshold=1e-11):
        """
        Return True if point is inside the closed interior with some threshold
        distance from the boundary.

        :type point: Point
        :type threshold: float
        :rtype: bool

        """
        # REFACTOR: if 2-array like passed, convert to Point automatically?
        return self.get_bounded_side(point, threshold) == BoundedSide.bounded_side

    def get_boundary_as_pwb(self):
        """
        Return the boundary as a PolygonWithBoundary

        Useful to apply operations with other PolygonWithBoundary objects to the boundary.

        """
        return PolygonWithBoundary(boundary=self.boundary)

    def cleanup_pointlike_polygon(self):
        """
        If closed interior is a point-like polygon, remove it and add the point to the boundary instead.

        """
        # IMPROVE: support MultiPolygon closed interiors
        # TODO


# FIXME: rename CompositeBoundary?
class RawBoundary():
    """
    Boundary in its raw form, closer to an XML representation.
    Lines and points are separated in this structure.

    multiline       -- MultiLineString representing LineStrings of 1 edge in the boundary
    multipoint      -- MultiPoint representing Points in the boundary

    """
    __hash__ = None

    def __init__(self, lines=None, points=None):
        """
        Initialize object with sequence of LineStrings or MultiLineString, sequence of Points
        or MultiPoints, or equivalent coordinate sequences

        If an argument is set to None or anything whose bool() is False,
        the corresponding multi-geometry will be empty.

        """
        # MultiPoint does not work with an empty list as argument, so use None instead
        if not lines:
            lines = None
        if not points:
            points = None

        self.multiline = MultiLineString(lines)
        self.multipoint = MultiPoint(points)

    def __repr__(self):
        return "<RawBoundary: ({0}, {1})>".format(self.multiline, self.multipoint)

    def __eq__(self, other):
        return isinstance(other, type(self)) and check_equal(self.multiline, other.multiline) and \
            check_equal(self.multipoint, other.multipoint)

    def __ne__(self, other):
        return not self.__eq__(other)

    @property
    def is_empty(self):
        """
        Return True if the geometry is empty

        :rtype: bool

        """
        return self.multiline.is_empty and self.multipoint.is_empty

    @deprecated(is_empty)
    def check_if_empty(self):
        """
        Return True if empty geometry

        Warning: this is a method to call!

        """
        return self.multiline.is_empty and self.multipoint.is_empty

    def to_geometry(self):
        """
        Convert a copy of the composite boundary into a single BaseGeometry object representing the whole boundary
        and return it.

        """
        return self.multiline | self.multipoint

def get_exterior_coords(geom, remove_duplicate=False):
    """
    Return the coordinate sequence of the exterior boundary of geom in the Euclidian sense,
     i.e. a Polygon's exterior is the shapely exterior, a LineString's exterior is the LineString itself
    and a Point's exterior is the Point itself.
    Currently, only Polygon, LineString, Point types are supported

    If remove_duplicate is True, redundant vertices will be removed.
    In practice, we only remove the last vertex in the Polygon exterior coords loop.

    :param geom: BaseGeometry (Polygon, LineString, Point)
    :param removeDuplicate:
    :return: coordinate sequence
    """
    # boundary and exterior are different, but if no holes we could also return get_geom_boundary().coords
    if geom.type == 'Polygon':
        coords = geom.exterior.coords
        if remove_duplicate:
            coords = coords[:-1]
    elif geom.type == 'LinearRing':
        coords = geom.coords
        if remove_duplicate:
            coords = coords[:-1]
    elif geom.type == 'LineString':
        coords = geom.coords
    elif geom.type == 'Point':
        coords = geom.coords
    else:
        raise ValueError('geom is a {}, but BaseGeometry of types other than'
                         'Polygon, LinearRing, LineString, Point are not supported.'.format(geom.type))
    return coords


def get_geom_boundary(geom):
    """
    Return the 1D (Euclidian) boundary of the object, depending on its type

    This is *not* the shapely boundary, eg for a Point the shapely boundary is empty

    :rtype: (GeometryCollection | LineString | MultiLineString | Point | MultiPoint)
    """
    # boundary and coords property access depend on subclass of BaseGeometry used by nfp
    # TODO: support non-empty geometry collection
    if geom.is_empty:
        geom_boundary = GeometryCollection()
    elif geom.type == 'Polygon':
        # note that boundary, unlike exterior, is a LineString
        # in addition, if there are holes, it is a MultiLineString
        # hence, you cannot directly iterate over .coords of the returned value if there are holes
        geom_boundary = geom.boundary  # LineString, or MultiLineString if holes
    elif geom.type == 'MultiPolygon':
        geom_boundary = geom.boundary  # MultiLineString
    elif geom.type in ('LinearRing', 'LineString', 'MultiLineString'):
        geom_boundary = geom
    elif geom.type in ('Point', 'MultiPoint'):
        geom_boundary = geom
    elif geom.type == 'GeometryCollection':
        # get union of boundary for each part
        geom_boundary = unary_union(map(get_geom_boundary, geom))
    else:
        raise TypeError('get_geom_boundary does not support geometry type {}'.format(geom.type))
    return geom_boundary


def get_unique_coords(geom_lines):
    """
    Return a list of flattened, unique coords from one or multiple Points, LineStrings or LinearRings.
    Useful to find all the unique geometry vertices and iterate over them.

    :param geom_lines: (Multi)Point, (Multi)LineString or GeometryCollection

    :rtype: list[(float, float)]

    """
    # IMPROVE: for simple types such as LinearRings, do not use a set at all, this will ensure preservation
    # of order of coordinates (useful in get_extreme_vertex method for NFP algo)

    # even if each geometry part return unique coords, the union may contain redundancies
    # so make each element unique at the end
    # (possible issues of precision)
    if geom_lines.is_empty:
        coords = iter(())
    elif geom_lines.type == 'GeometryCollection':
        # recurse for each geom part
        coords = flatten((get_unique_coords(geom) for geom in geom_lines))
    elif geom_lines.type == 'MultiPoint':
        coords = flatten((point.coords for point in geom_lines))
    elif geom_lines.type in ('Point', 'LineString', 'LinearRing'):
        coords = geom_lines.coords
    elif geom_lines.type == 'MultiLineString':
        coords = flatten((line.coords for line in geom_lines))
    else:
        raise TypeError('get_geom_boundary does not support geometry type {}'.format(geom_lines.type))
    # IMPROVE: set uses a normal equal... round coordinates or manually remove close vertices
    # to get truly interesting unique coordinates (if geoms were rounded beforehand, no need to do so)
    # note that it will hide a few floating errors, but to save the back-end in emergency that is quite good
    return list(set(coords))

def get_translated_polygons_from_problem(problem, placements, insert_none=True):
    """
    Return a dictionary {(pieceID, instanceID): copy of polygon in lot translated by a placement}.
    In case the layout containing the placements was incomplete, insert a null value
    instead of the polygon for which no placement could be found.

    :param problem:
    :param placements:
    :return: dict<string, Polygon> a copy of the lot, with all polygons translated with placements

    """
    return get_translated_polygons(problem.get_expanded_lot(), placements, insert_none)


@deprecated('get_placed_lot')  # translation only and no PieceKey... an old function
def get_translated_polygons(expanded_lot, placements, insert_none=True):
    """
    Return a dictionary {(pieceID, instanceID): copy of polygon in lot translated by a placement}.
    If insert_none is True, in case the layout containing the placements was incomplete, insert a None value
    instead of the polygon for which no placement could be found.
    TRANSITION: insert_none=False should be the default. Gradually change this by indicating =True where needed.

    :param expanded_lot: dict<(string, int), BaseGeometry>
    :param placements:
    :return: dict<string, Polygon> a copy of the lot, with all polygons translated with placements

    """
    tr_expanded_lot = {}
    for (piece_id, instance_idx), polygon in expanded_lot.iteritems():
        if (piece_id, instance_idx) not in placements:
            # REFACTOR: ignore and continue, without adding a None value (easier to get relevant keys later)
            # and use get(key) when you really want to check the presence of a key
            # raise ValueError('Provided placements dictionary do not contain entries for piece_id - instance_idx {}'.format(piece_id, instance_idx))
            if insert_none:
                tr_expanded_lot[(piece_id, instance_idx)] = None
            continue
        translation = placements[(piece_id, instance_idx)].translation
        tr_expanded_lot[(piece_id, instance_idx)] = translate(polygon, translation[0], translation[1])
    return tr_expanded_lot


def get_placed_lot(lot, placements):
    """
    Return a dictionary containing the placed polygons, indexed by piece key, according to placements.
    Only the keys in placements are present in the returned dictionary.

    :type lot: dict[PieceKey, BaseGeometry]
    :param lot: lot of piece geometries
    :type placements: dict[PieceKey, Placement]
    :param placements:
    :rtype: dict[PieceKey, Polygon]
    :return: dictionary of polygons placed with placements

    """
    placed_lot = {}
    for piece_key, piece_geom in placements.iteritems():
        placement = placements[piece_key]
        placed_geom = place_geom(lot[piece_key], placement)
        assert_validity(placed_geom)
        placed_lot[piece_key] = placed_geom.buffer(0)  # HOTFIX: unary union crashed on Big Triangle rotated +90 in Tangram
    return placed_lot


def get_size(geom):
    """
    Return the size (width, height) of a BaseGeometry.

    :type geom: BaseGeometry
    :rtype: (float, float)
    """
    if geom.is_empty:
        return 0, 0
    else:
        return geom.bounds[2] - geom.bounds[0], geom.bounds[3] - geom.bounds[1]


# - bounds utils -

def to_size(bounds):
    """Return the (width, height) tuple corresponding to bounds"""
    return bounds[2] - bounds[0], bounds[3] - bounds[1]


def can_bounds_fit(bounds1, bounds2):
    """
    Return True if the both the width and height of bounds1 are inferior or equal to
    the width and height of bounds2 respectively.

    :type bounds1: (float, float, float, float)
    :type bounds2: (float, float, float, float)
    :rtype: bool

    """
    width1, height1 = to_size(bounds1)
    width2, height2 = to_size(bounds2)
    return width1 <= width2 and height1 <= height2

def count_edge(polygon):
    """Return the total number of edges in a polygon (exterior and holes)"""
    return (len(polygon.exterior.coords) - 1) + sum(len(hole.coords) - 1 for hole in polygon.interiors)


def rotate_origin(geom, angle, use_radians=False):
    """
    Convenience function to rotate a geometry around its origin.
    The geometry is unchanged but a transformed copy is returned.
    :param geom: BaseGeometry
    :param angle: float rotation angle
    :param use_radians: set to True to use an angle in radians
    :param geom: BaseGeometry
    :return: BaseGeometry

    """
    # PRECISION: round coordinates by default? too expensive?
    # rotate and round manually in order to build only one new geom instead of two?
    return rotate(geom, angle, (0, 0), use_radians)


def rotate_translate(geom, angle, offset, use_radians=False, origin=(0, 0)):
    """
    Convenience function to rotate a geometry around its origin then translate it.
    The geometry is unchanged but a transformed copy is returned.
    :param geom: BaseGeometry
    :param angle: float rotation angle
    :param offset: Point or coordinate tuple offset
    :param use_radians: set to True to use an angle in radians
    :param origin: origin of the rotation ((0, 0) by default)
    :param geom: BaseGeometry
    :return: BaseGeometry

    """
    return translate(rotate(geom, angle, origin, use_radians), *offset)
    # matrix alternative
    # return affine_transform(geom, [cos(angle), -sin(angle), sin(angle), cos(angle), offset[0], offset[1]])


def place_geom(geom, placement):
    """
    Return a copy of geom rotated and translated according to placement.
    Sincep placement always use rotation in radians, we do not leave the choice of the unit here.

    :param geom: BaseGeometry
    :param placement: Placement
    :return: BaseGeometry - copy of the geometry placed
    """
    return rotate_translate(geom, placement.rotation, placement.translation)


def bounds_union(bounds, added_bounds):
    """
    Return the bounds of the union of bounds (a tuple (xMin, yMin, xMax, yMax),
     or an empty tuple for empty geometries) as a tuple
    If one of the boundaries are empty, the other is returned.
    If both are empty, an empty list is returned.

    :rtype: (float, float, float, float)

    """
    if not bounds:
        return added_bounds
    elif not added_bounds:
        return bounds

    union_bounds = (
        min(bounds[0], added_bounds[0]),
        min(bounds[1], added_bounds[1]),
        max(bounds[2], added_bounds[2]),
        max(bounds[3], added_bounds[3])
    )

    return union_bounds

def update_bounds(bounds, added_bounds):
    """
    Update mutable bounds, a list of [xMin, yMin, xMax, yMax], with union_bounds
    the bounds of a geometry added with the union operation.
    bounds must be mutable.
    Empty bounds, i.e. bounds of empty geometries, are accepted, but must be empty list.

    In any case, shapely bounds must be converted to a list before using this function.

    The bounds can only grow / goes from empty to non-empty in this manner.

    """
    bounds[:] = bounds_union(bounds, added_bounds)

    # if not bounds:
    #     bounds = added_bounds
    # elif not added_bounds:
    #     return  # do not change bounds at all
    #
    # if added_bounds[0] < bounds[0]:
    #     bounds[0] = added_bounds[0]
    # if added_bounds[1] < bounds[1]:
    #     bounds[1] = added_bounds[1]
    # if added_bounds[2] > bounds[2]:
    #     bounds[2] = added_bounds[2]
    # if added_bounds[3] > bounds[3]:
    #     bounds[3] = added_bounds[3]


def lc(geom):
    """
    Return list of coords, if geom has coords.
    Useful for print debugging

    """
    if geom.is_empty:
        return []
    if is_multi(geom):
        return [lc(subgeom) for subgeom in geom]
    if geom.type in ('Point', 'LineString', 'LinearRing'):
        return list(geom.coords)
    if geom.type == 'Polygon':
        return [list(geom.exterior.coords), [list(interior.coords) for interior in geom.interiors]]


def to_bounding_box(geom):
    """Return a box Polygon corresponding to the bounding box of geom."""
    return box(*geom.bounds)


def get_bounds_area(bounds):
    """
    Return the area contained in bounds (0 if empty)
    Raise ValueError if bounds are ill-formed

    :type bounds: (float, float, float, float)
    :rtype: float
    :return: area
    """
    if not bounds:
        return 0

    width, height = bounds[2] - bounds[0], bounds[3] - bounds[1]
    if width < 0 or height < 0:
        raise ValueError('Ill-formed bounds: ' + str(bounds))
    return width * height


def get_bounding_box_area(geom):
    """
    Return the area of the bounding box of geom.
    Use this instead of using to_bounding_box then getting the area, if you do not need
     to create a Polygon.

    :type geom: BaseGeometry
    :return: float area
    """
    return get_bounds_area(geom.bounds)


def get_bounding_box_absolute_waste(geometry):
    """
    Return absolute waste of the bounding box of the geometry
    This includes holes inside the geometry.

    """
    return get_bounding_box_area(geometry) - geometry.area


def get_bounding_box_waste(geometry):
    """
    Return relative waste of the bounding box of the geometry.
    This includes holes inside the geometry.

    """
    bb_area = get_bounding_box_area(geometry)
    return (bb_area - geometry.area) / bb_area


def get_convex_hull_absolute_waste(geometry):
    """
    Return absolute waste of the convex hull of the geometry.
    This includes holes inside the geometry.

    """
    convex_hull_area = geometry.convex_hull.area
    return convex_hull_area - geometry.area

def get_convex_hull_waste(geometry):
    """
    Return relative waste of the convex hull of the geometry.
    This includes holes inside the geometry.

    """
    convex_hull_area = geometry.convex_hull.area
    return (convex_hull_area - geometry.area) / convex_hull_area


# - rounding utils -

def get_merged_close_successive_vertices(coords, rtol=1e-05, atol=1e-08):
    """
    Merge successive vertices if they are close under a relative and absolute threshold,
    and return the list of vertices where neighbors are sufficiently apart from each other.

    Try to do this successively to merge 2+ successive vertices if required.
    The 1st and last vertices are *not* merged, because most of the time we want to create looped
    geometries from that.
    However, it should work even without looping coords manually... Strange behavior
    Anyway for now we do not merge those.

    #OLD BEHAVIOR
    #It will also try to merge the 1st and last coordinate, and if it does it will *remove*
    #the last coordinate too.

    There is no parameter to say whether we want to close or not, the final result never
    closes, but you can call the constructor of a closed structure to obtain a closed shape.

    Useful when a Polygon has a self-intersection after a rounding error on an operation.
    Keep the coords closed for a closed shape to ensure merging between start and end.

    :param coords:
    :param rtol: relative tolerance
    :param atol: absolute tolerance
    :return:
    """
    merged_coord_list = []
    # last_coord = coords[-1]  # to not repeat end point at start
    last_coord = np.nan  # NaN is close to nothing (original behavior restored)
    for idx, coord in enumerate(coords):
        # we use an element-wise close, we could use a distance close too
        if not np.allclose(coord, last_coord, rtol, atol):
            # we have a new coordinate
            # for the last coordinate, extra check regarding the first coordinate
            # if idx == len(coords) - 1 and np.allclose(coord, coords[0], rtol, atol):
            #     break  # continue, but it is over anyway
            merged_coord_list.append(coord)  # a new unique coordinate
            last_coord = coord
        else:
            # this coordinate is too close to the previous one
            # ignore this coord (or choose the average coord, but here we neglect the difference)
            # we do not update last_coord since we use the 1st coord as the representative
            # DEBUG: too frequent, disabled
            # logging.debug('Merge vertex {} with close vertex {} in coords {}'.format(coord, last_coord, list(coords)))
            pass

    assert len(merged_coord_list) > 0, 'Merged too much, 0 vertices remaining!'
    return merged_coord_list


# prefer round_coords when possible, it also calls this function
def merge_close_successive_vertices(geom, rtol=1e-05, atol=1e-08):
    """
    Return a copy of the geom of the same type with closed successive vertices merged,
    with a certain tolerance.

    For polygons, only the exterior is affected.

    Supported types:
    - empty
    - Polygon
    - LineString
    - LinearRing

    Caution: the function does not check if the validity of the new object.

    """
    if geom.is_empty:
        return GeometryCollection()

    if is_multi(geom):
        # we hope they are no more erros induced in the union (the parts should be
        # distant in this Multi-shape)
        constructed_geom = unary_union([merge_close_successive_vertices(part, rtol, atol) for part in geom])
        # UNDEBUG
        assert_validity(constructed_geom)
        return constructed_geom

    # FIXME: be pragmatic with the number of vertices: if get_merged_close_successive_vertices
    # result in a flat triangle -> line or a line reduced to a point. adapt the constructor
    # but log a warning (happened in Tangram before we removed lines / points from the closed interior)
    if geom.type == 'Polygon':
        constructor = Polygon
    elif geom.type == 'LineString':
        constructor = LineString
    elif geom.type == 'LinearRing':
        constructor = LinearRing
    else:
        raise ValueError('unsupported type: {}'.format(geom.type))

    # FIXME: also merge hole vertices
    # SINCE round_coords already support holes, use it instead
    # IMPROVE: also detect flat polygons with more than 2 vertices that become complex LineStrings
    merged_vertices = get_merged_close_successive_vertices(get_exterior_coords(geom), rtol, atol)

    # reduce dimension
    if len(merged_vertices) == 1:
        # anything reduced to 1 vertex is a Point
        constructed_geom = Point(merged_vertices)

    elif geom.type in ('Polygon', 'LinearRing'):
        # caution: looped coordinates so test number of remaining vertices - 1 for LineString generation
        # the case len(merged_vertices) == 2 is theoretically impossible, since if start and end should be equal
        # so they should have merged into 1 coordinate already
        assert len(merged_vertices) != 2, 'Merging looped coordinates of {} resulted in 2 vertices (coords {})'\
            .format(geom.type, get_exterior_coords(geom))
        if len(merged_vertices) - 1 == 2:
            # a Polygon / LinearRing with 2 vertices in a LineString
            constructed_geom = LineString(merged_vertices)
        else:
            # no need to reduce dimension
            if geom.type == 'Polygon':
                # also reproduce holes (not merged, use round coords for that)
                constructed_geom = Polygon(merged_vertices, holes=geom.interiors)
            else:
                constructed_geom = constructor(merged_vertices)  # LinearRing only

    else:
        # LineString and at least 2 coordinates: no need to reduce dimension
        constructed_geom = constructor(merged_vertices)

    # UNDEBUG
    assert_validity(constructed_geom)  # if not valid, convert geom to lower dimension geom if required
    return constructed_geom

def check_equal(geom1, geom2, decimals=7):
    """
    Return True if both geometries are equal, including if they are both empty.
    (The shapely equals method returns False if one geometry is empty.)

    Use decimals to tune the precision threshold. Default is 7 decimals (the 7th decimal is rounded
    farther from zero).

    Always use this instead of .equals() to compare to potentially empty geometries
    that do not have an extended is_empty property.
    .equals() is the only easy way to compare geometries on their actual content, independently
    of their structure and the order their coordinates are stored.
    Unfortunately, almost_equals uses equals_exact which itself depends on the order of the coordinates,
    and equals() is not good at comparing geometries with an error on coordinates of 1e-15
    (and Python float considers 1+1e-16 as equal to 1, and shapely uses the same precision...
    Only numpy.float128 can handle more than that, so basically if your operations are under shapely
    it is over)

    Alternatively, if you are sure at 100% of your coordinates order, you can use almost_equals()

    :type geom1: BaseGeometry
    :type geom2: BaseGeometry
    :param tolerance: absolute tolerance on coordinate for equality test
    :rtype: bool

    """
    # TODO: use another generic function to support all kind of geometries!
    # FIXME: does not return True for a Line vs a Line and a Point almost on that Line,
    # because round_coords will snap the point to the line but does remove it
    # but unary_union does not merge this automatically... linemerge does not support points
    # need to merge manually...
    return geom1.is_empty and geom2.is_empty or round_coords(geom1, decimals).equals(round_coords(geom2, decimals))


def round_coords(geom, decimals=14):
    """
    Return a copy of geom with rounded coords, up to decimals.

    Rounding may set two vertices to the same position so merging is required to avoid invalid geometries.
    We cannot generalize for the moment but merging neighboring vertices seems appropriate.

    Preservation of type is not guaranteed if merging occurs or a multi-part geometry is used.

    Geometry validity is verified, raise Exception otherwise

    :type geom: (Polygon | LineString | LinearRing | Point | BaseMultiPartGeometry | PolygonWithBoundary)

    """
    # IMPROVE: use merge and round techniques seen in paper, relatively to other points and edges with which
    # we operate; requires to modify geometry... do on the fly at each operation rather than globally?
    # but possible inconsistency due to different rounding for different ops...

    if geom.is_empty:
        # return the same geom, in case it is a PolygonWithBoundary so we want to preserve type
        return geom

    if is_multi(geom):
        # IMPROVE: if preservation of type matters, distinguish MultiLineString, GeometryCollection, etc.
        # and reconstruct according to type
        # we hope they are no more erros induced in the union (better if the parts are not touching at all)
        rounded_multigeom = unary_union([round_coords(part, decimals) for part in geom])
        # - merging -
        # remove points that are already on lines or in polygons
        # get all line-like
        # TODO? okay for now, the problem is in operations that happen *before* rounding
        # UNDEBUG
        # assert_validity(rounded_multigeom)
        return rounded_multigeom

    # REFACTOR: try to put reducing dimensions in the end out of the conditional blocks, as in merge_vertices function
    # can also refactor with merge vertex function
    if geom.type == 'Polygon':
        exterior_rounded_coords = np.round(geom.exterior.coords, decimals)
        exterior_rounded_coords = get_merged_close_successive_vertices(exterior_rounded_coords, 0, atol=10**-(decimals - 1))

        # start and end coords are equal so cannot be reduced to 2 coordinates, only 3+ or 1
        assert len(exterior_rounded_coords) != 2, 'Round & merge looped coordinates of Polygon resulted in 2 vertices, {} <- coords {}'.format(exterior_rounded_coords, geom.exterior.coords)

        # FIXME: flat parts create invalid geometry: detect them by checking back and forth movement
        # by checking colinearity of successive edges (same sense can merge; opposite sense not good,
        # split to a LineString... very hard)

        if len(exterior_rounded_coords) - 1 == 2:
            # Polygon reduced to a LineString (3 vertices due to looping), do not bother with holes
            rounded_geom = LineString(exterior_rounded_coords)
        elif len(exterior_rounded_coords) == 1:
            rounded_geom = Point(exterior_rounded_coords[0])
        else:
            # caution: do not apply np.round directly to the sequence of hole coords, only works if all coords list
            # have the same size!
            interiors_rounded_coords = [np.round(interior.coords, decimals) for interior in geom.interiors]
            # merge neighbors rounded to the same location, with a threshold one order above to be sure not to miss some
            interiors_rounded_coords = [get_merged_close_successive_vertices(interior_rounded_coords, 0, atol=10**-(decimals - 1))
                                        for interior_rounded_coords in interiors_rounded_coords]
            # build actual holes and recurse to check if reduced to lines or points; if so, ignore them
            # (since this is not a PWB, no way to model a line-like hole)
            interiors = map(LinearRing, interiors_rounded_coords)
            rounded_interiors = map(round_coords, interiors)
            rounded_interiors = filter(lambda x: x.type == 'LinearRing', rounded_interiors)
            rounded_geom = Polygon(exterior_rounded_coords,
                                   holes=rounded_interiors)

    elif geom.type == 'LinearRing':
        rounded_coords = np.round(geom.coords, decimals)
        # merge neighbors rounded to the same location, with same threshold
        rounded_coords = get_merged_close_successive_vertices(rounded_coords, 0, atol=10**-decimals)

        # start and end coords are equal so cannot be reduced to 2 coordinates, only 3+ or 1
        assert len(rounded_coords) != 2, 'Round & merge looped coordinates of LinearRing resulted in 2 vertices, {} <- coords {}'.format(rounded_coords, geom.coords)

        if len(rounded_coords) - 1 == 2:
            # LinearRing reduced to a LineString (3 vertices due to looping)
            rounded_geom = LineString(rounded_coords)
        elif len(rounded_coords) == 1:
            rounded_geom = Point(rounded_coords)
        else:
            rounded_geom = LinearRing(rounded_coords)

    elif geom.type == 'LineString':
        rounded_coords = np.round(geom.coords, decimals)
        rounded_coords = get_merged_close_successive_vertices(rounded_coords, 0, atol=10**-decimals)
        if len(rounded_coords) == 1:
            # LineString reduced to a Point
            # np array from round does not work for Point coords... use the 1st element instead
            rounded_geom = Point(rounded_coords[0])
        else:
            rounded_geom = LineString(rounded_coords)

    elif geom.type == 'Point':
        rounded_geom = Point(np.round(geom.coords[0], decimals))

    elif geom.type == 'PolygonWithBoundary':
        rounded_closed_interior = round_coords(geom.closed_interior, decimals)
        # cleanup low dimension parts
        rounded_closed_interior = get_closed_interior(rounded_closed_interior)
        rounded_geom = PolygonWithBoundary(rounded_closed_interior,
                                           round_coords(geom.boundary, decimals))

    else:
        raise ValueError('unsupported type: {}'.format(geom.type))

    # UNDEBUG
    # assert_validity(rounded_geom)
    return rounded_geom


def check_equal_round(geom1, geom2, tolerance=1e-7):
    """
    Return True if geom1 and geom2 are empty, or considered shapely equals after rounding the coordinates
    with tolerance.
    This method builds rounded copies of the geometries, and may create weird geometries (does not check
    validity after rounding though little chance to produce one), so use with caution.

    :param geom1:
    :param geom2:
    :param tolerance:
    :rtype: bool

    """
    geom1 = round_coords(geom1)
    geom2 = round_coords(geom2)
    return geom1.is_empty and geom2.is_empty or geom1.equals(geom2)


def linemerge_boundary(geom):
    """
    Apply line merge on the line parts of the geom, saving points for later.
    geom should only contain lines and points.

    :param geom:
    :return:

    """
    if geom.is_empty:
        return geom

    # IMPROVE: also merge individual LineString in a collection, that are not inside a MultiLineString
    if geom.type == 'MultiLineString':
        return linemerge(geom)
    elif geom.type == 'LineString':
        # not that useful but can remove redundant vertices
        return linemerge([geom])
    elif geom.type == 'MultiPoint':
        # not our job to merge identical points, though we could do the service here
        return geom
    elif geom.type == 'Point':
        return geom
    elif geom.type == 'GeometryCollection':
        return unary_union([linemerge_boundary(part) for part in geom])

    raise TypeError('Geometry type not supported : {}'.format(geom.type))


def get_extreme_vertex_index_value(geom, position, secondary_position=None):
    """

    :type geom: (Point | LineString | LinearRing)
    :param geom: BaseGeometry object with coords (*required* to ensure that the index can be found)
    :param position: position P, in which we are searching the P-most vertex
    :param secondary_position: position D to decide which vertex to choose in case of draw on P (orthogonal to P)
    :return: the index-value tuple of one of the P-most vertex in coords, and among them the D-most in case of draw
    """
    target_coord = geom.bounds[position.value]  # we are looking for a vertex with this x or y coordinate
    coord_idx = 0 if position in (Position.left, Position.right) else 1  # index for x or y coordinate

    if secondary_position is not None:
        if secondary_position in (position, opposites[position]):
            raise ValueError('secondary_position {} is parallel to main position {}'.format(secondary_position, position))
        secondary_coord_idx = 0 if coord_idx == 1 else 1
        # best_secondary_coord takes the role of +inf here, so
        # 1 - consider the bound in the *opposite* position
        # 2 - add +1 for bottom/left, -1 for top/right (opposite of cmp) to be sure the 1st value is better
        best_secondary_coord = geom.bounds[opposites[secondary_position].value]
        secondary_position_cmp = -1 if secondary_position in (Position.bottom, Position.left) else 1
        best_secondary_coord += - secondary_position_cmp

    best_idx = -1
    best_vertex = None

    # minor optimization: remove last coords for looped coords of LinearRings
    # (or be pragmatic and remove if LineString looped too)
    # (not recommended to use get_unique_coords, it changes the order of the coords so best_idx difficult to retrieve)
    if geom.type == 'LinearRing':
        unique_coords = geom.coords[:-1]
    else:
        unique_coords = geom.coords
    for idx, vertex in enumerate(unique_coords):
        # only keep extreme coordinate for primary direction
        # add tolerance due to floating errors introduced after rotation that messed up the NFP C-G algorithm
        # if vertex[coord_idx] != target_coord:
        if not np.isclose(vertex[coord_idx], target_coord):
            continue
        # vertex has the best first coordinate, test the second coordinate if asked
        if secondary_position is not None:
            if cmp(vertex[secondary_coord_idx], best_secondary_coord) == secondary_position_cmp:
                best_secondary_coord = vertex[secondary_coord_idx]
            else:
                continue
        best_idx, best_vertex = idx, vertex
        if secondary_position is None:
            # not looking for a second best direction, stop and return the current vertex
            break

    if best_vertex is None:
        raise Exception('Could not find a vertex with {}most coordinate on {}'.format(position.name, geom))

    return best_idx, fl(best_vertex)


def get_extreme_vertices(geom, position):
    """
    Return a list of vertices on the bounds of geom, on the side indicated by position.
    If geom is empty, return an empty list.

    :type geom: (GeometryCollection | Point | MultiPoint | LineString | MultiLineString)
    :param geom: BaseGeometry object that supports get_unique_coords
    :param position: position P, in which we are searching the P-most vertex
    :return: the index-value tuple of one of the P-most vertex in coords, and among them the D-most in case of draw
    """
    if geom.is_empty:
        return []

    target_coord = geom.bounds[position.value]  # extreme coord is given by bound, in the desired direction
    coord_idx = 0 if position in (Position.left, Position.right) else 1  # index for x or y coordinate

    extreme_vertices = []

    for idx, vertex in enumerate(get_unique_coords(geom)):
        if vertex[coord_idx] != target_coord:
            continue
        # add vertex to list of vertices on the desired bound
        extreme_vertices.append(vertex)

    return extreme_vertices


def get_edges(geom):
    """
    Return a list on the edges of a geometry from its boundary

    No loop parameter for this function.

    :type geom: BaseGeometry

    :rtype: list[(float, float), (float, float)]

    """
    # empty geometries and Points have no edges
    if geom.is_empty or geom.type == 'Point':
        return []

    all_edges = []
    # TODO: support geometry collection (do that in get_geom_boundary)
    boundary = get_geom_boundary(geom)
    if is_multi(boundary):
        for part in boundary:
            all_edges.extend(get_edges(part))
    else:
        # deactivate looping; if boundary is LinearRing, the coordinates will be looped already,
        # but if it is a LineString, we do not want to loop the coords
        edges = get_edges_from_coords(boundary.coords, loop=False)
        all_edges.extend(edges)

    return all_edges


def get_edges_from_coords(coords, loop=True):
    """
    Return a list of the edges of a sequence of coords as tuples ((x1, y1), (x2, y2))
    It is recommended to use numpy arrays for the coordinate sequences [x, y]

    If coords is not a closed tuple, i.e. the last element is not equal to the first
    element, and loop is True, then a last element equal to the first one is added to obtain
    the last edge.
    This means you must set loop to False when using it with deliberately open geometry such as
    LineStrings.

    Caution: loop will be set to False by default in later version. Provide loop explicitly
    from now on.

    :rtype: list[(float, float), (float, float)]

    """
    # REFACTOR: loop to False by default
    # if you want to support LineStrings and require explicit coordinate closing,
    # remove this block
    coords_list = list(coords)
    # IMPROVE: add threshold/margin for equality?
    if loop and coords[-1] != coords[0]:
        coords_list.append(coords[0])
    return list(overlap_pairwise(coords_list))


def get_edge_vectors(coords):
    """
    Return an iterator on the edges of a sequence of coords as vectors (x2-x1, y2-y1)
    as numpy float arrays.

    If coords is not a closed list, i.e. the last element is not equal to the first
    element, then a last element equal to the first one is added to obtain the last edge.

    """
    return (get_edge_vector(edge) for edge in get_edges_from_coords(coords))


def get_edge_vector(edge):
    """
    Return an edge vector from an edge as a numpy float array.

    The edge vector associated to an edge is the vector from the first vertex
    of the edge to its second vertex. It only contains information on the relative
    position of the vertices.

    """
    return fl([edge[1][0] - edge[0][0], edge[1][1] - edge[0][1]])


# -- type checking utils --

def is_multi(geom):
    """
    Return True if geom is a MultiGeometry or a non-empty GeometryCollection

    :type geom: (BaseGeometry | PolygonWithBoundary)
    :param geom:
    :return:
    """
    # ALTERNATIVE: check if geom has attr '__iter__' (duck-typing)
    return not geom.is_empty and (geom.type[:5] == 'Multi' or geom.type == 'GeometryCollection')


def is_polygon_like(geom):
    """
    Return True if geom is empty, a Polygon or a MultiPolygon.

    :type geom: BaseGeometry
    :rtype: bool

    """
    return geom.is_empty or geom.type == 'Polygon' or geom.type == 'MultiPolygon'


# -- validity utils --

def assert_validity(geom):
    """
    Verify validity of geom and raise an exception containing invalidity reason if invalid
    Return None, not a boolean

    :type geom: (BaseGeometry | PolygonWithBoundary)

    """
    if not config.debug:
        return
    # TODO: return always True in debug mode? easier to test debug settings here than at each call!
    if not geom.is_valid:
        raise ValueError('Geometry is invalid: {} ({})'.format(explain_validity_generic(geom), str(geom)))


def explain_validity_generic(geom):
    """
    Explain validity of geometry that supports custom geometries

    :type geom: (BaseGeometry | PolygonWithBoundary)

    """
    if geom.type == 'PolygonWithBoundary':
        if geom.is_boundary_consistent:
            boundary_consistent_explanation = 'Boundary is consistent with closed interior'
        else:
            boundary_consistent_explanation = 'Boundary is not consistent with closed interior. Boundary: {}, ' \
                                              'Closed interior boundary: {}'.format(geom.boundary,
                                                                                    geom.closed_interior.boundary)
        return 'Closed interior: {}, Boundary: {}, Boundary consistency: {}' \
            .format(explain_validity(geom.closed_interior), explain_validity(geom.boundary),
                    boundary_consistent_explanation)
    else:
        return explain_validity(geom)


# -- conversion functions --

def to_multipolygon(geom):
    """
    Convert a polygon or a multi-polygon to a MultiPolygon

    :type geom: (Polygon | MultiPolygon)
    :rtype: MultiPolygon

    """
    if geom.is_empty:
        return MultiPolygon()
    elif geom.type == 'Polygon':
        return MultiPolygon([geom])
    elif geom.type == 'MultiPolygon':
        return geom
    else:
        raise TypeError('Unexpected geom type: {}'.format(geom.type))


# -- extraction functions --

def get_closed_interior(geom):
    """
    Return a copy of the geometry with all Line and Point-like parts removed.
    Be careful, as LinearRings as considered as lines and not polygons.

    This correspond to opening (getting the interior) and closing the geometry.
    After the operation, we are guaranteed to have an empty set or one or more truly 2D objects ((Multi)Polygons,
     possibly with holes).
    In other words, the returned value is a geometry: empty, (Multi)Polygon, or GeometryCollection of Polygons
    (theoretically multiple levels of depths possible)

    In addition, the function is compatible with PolygonWithBoundary for generalization,
    but we recommend using the closed_interior member directly when possible.

    In most cases, there will be no second level of GeometryCollections (shapely operations return
    one level of GeometryCollection maximum, second level and deeper only appears by manual construction).

    :param geom:
    :rtype: (BaseGeometry | PolygonWithBoundary)

    """
    if geom.type == 'PolygonWithBoundary':
        return geom.closed_interior

    if geom.is_empty:
        # the geometry is empty, leave it
        return geom
    elif geom.type in ('Point', 'MultiPoint', 'LineString', 'MultiLineString', 'LinearRing'):
        # the geometry is 0D or 1D, reduce to empty set
        return GeometryCollection()
    elif geom.type in ('Polygon', 'MultiPolygon'):
        # the geometry is a Polygon, keep it
        return geom
    elif geom.type == 'GeometryCollection':
        # recreate a collection, ignoring lines and points (the new collection may be empty)
        # since a collection is not flattened automatically, it may contain another collection;
        # recurse to be sure not be miss any possibility
        closed_interior_geoms = []
        for part in geom:
            part_closed_interior = get_closed_interior(part)
            if not part_closed_interior.is_empty:
                closed_interior_geoms.append(part_closed_interior)
        return GeometryCollection(closed_interior_geoms)
    else:
        raise ValueError('Unexpected geom type: {}', geom.type)


def get_closed_interior_as_multipolygon(geom):
    """
    Specialized helper that returns a MultiPolygon containing all Polygonal parts in a geom,
    but only them.

    To be used for PolygonWithBoundary closed interior intersection.

    Precondition: if geom if a collection, must not contain deeper level GeomCollections
    (after a shapely operation, this should be verified)

    :type geom: BaseGeometry
    :rtype: MultiPolygon
    """
    closed_interior = get_closed_interior(geom)

    if closed_interior.type == 'MultiPolygon':
        return closed_interior

    if closed_interior.is_empty:
        return MultiPolygon()  # trick to force instance type, but behavior is the same as any empty geometry
    elif closed_interior.type == 'Polygon':
        return MultiPolygon([closed_interior])
    elif closed_interior.type == 'GeometryCollection':
        return MultiPolygon(closed_interior)
    else:
        raise AssertionError('get_closed_interior did not return empty/Polygon/GeometryCollection, but {}'
                             .format(closed_interior.type))


def get_points(geom):
    """
    Return all the individual points in geom

    :type geom: (BaseGeometry \ PolygonWithBoundary)
    :rtype: list[Point]

    """
    if geom.type == 'PolygonWithBoundary':
        return get_points(geom.boundary)

    points = []
    if geom.is_empty:
        pass
    elif geom.type == 'Point':
        points.append(geom)
    elif geom.type == 'MultiPoint':
        points.extend(geom)
    elif geom.type in ('LineString', 'MultiLineString', 'LinearRing', 'Polygon', 'MultiPolygon'):
        pass
    elif geom.type == 'GeometryCollection':
        # recreate a collection, extracting points only (the new collection may be empty)
        for part in geom:
            points.extend(get_points(part))
    else:
        raise ValueError('Unexpected geom type: {}', geom.type)

    return points


def get_points_as_multipoint(geom):
    """
    Return all the individual points in geom as a MultiPoint

    :type geom: (BaseGeometry \ PolygonWithBoundary)
    :rtype: MultiPoint

    """
    if geom.type == 'PolygonWithBoundary':
        return get_points_as_multipoint(geom.boundary)

    if geom.type == 'MultiPoint':
        return geom
    else:
        points = get_points(geom)
        if points:
            return MultiPoint(points)
        else:
            return MultiPoint()


# -- measurement utils --

def get_nb_vertices(geom):
    """
    Return the number of vertices of the geometry

    :type geom: (GeometryCollection \ Point \ LineString \ LinearRing | Polygon | BaseMultipartGeometry)
    :rtype: int

    """
    if is_multi(geom):
        return sum(get_nb_vertices(part) for part in geom)
    elif geom.type == 'Point':
        return 1
    elif geom.type == 'LineString':
        # we assume that line string is open; if it is closed, the closing vertex will be counted twice
        return len(geom.coords)
    elif geom.type == 'LinearRing':
        return len(geom.coords) - 1  # do not count closing vertex twice
    elif geom.type == 'Polygon':
        return len(geom.exterior.coords) - 1 + sum(len(interior.coords) - 1 for interior in geom.interiors)
    else:
        raise TypeError('Unsupported type: {}'.format(geom.type))


def get_edge_length(edge):
    """
    Return the length of the edge

    :type edge: ((float, float), (float, float))
    :rtype: ndarray

    """
    return norm(fl(edge[1]) - fl(edge[0]))

def get_cumulative_size(geom):
    """
    Return the sum of the widths and heights of the different parts of geom as a 2-array [width, height]

    :type geom: BaseGeometry
    :rtype: numpy.ndarray

    """
    if is_multi(geom):
        return sum(get_cumulative_size(part) for part in geom)

    return fl(get_size(geom))


def get_polygon_diameter(polygon):
    """
    Return the length of the longest diagonal of the polygon.

    :type polygon: Polygon
    :rtype: float

    """
    vertices = polygon.exterior.coords
    return max(norm(fl(vertex1) - fl(vertex2)) for vertex1, vertex2 in itertools.combinations(vertices, 2))

def get_min_size(geom):
    """
    Return the minimum of the width and the height of a geometry.

    :type geom: BaseGeometry
    :rtype: float

    """
    return min(get_size(geom))

def get_min_width_or_height(geoms):
    """
    Return the minimum width or height (the minimum of the min width and the min height)
    among a sequence of geometries

    :type geoms: list[BaseGeometry]
    :rtype: float

    """
    return min(map(get_min_size, geoms))

def raycast_polygon(start_position, ray_vector, polygon, ignore_parallel_edges=True, ):
    """
    Raycast from start_position to start_position + ray_vector against polygon boundary,
    and return the closest intersected position as a numpy 2-array if any, None else

    If ignore_parallel_edges is True, edges that are parallel to ray_vector will be ignored.
    This is convenient to trim translations only against crossing or touching edges

    :type start_position: (float, float)
    :param start_position:
    :type ray_vector: (float, float)
    :param ray_vector:
    :type polygon: Polygon
    :rtype: (numpy.ndarray | None)

    """
    edges = get_edges(polygon)
    # in practice, parallel edges are not the problem, but edges related to touchers in NFP Burke
    # so this can be done but is merely a small optimization
    if ignore_parallel_edges:
        edges = [edge for edge in edges
                 if get_vector_orientation(ray_vector, get_edge_vector(edge)) != Orientation.aligned]
    geom = MultiLineString(map(list, edges))
    return raycast(start_position, ray_vector, geom)


def raycast(start_position, ray_vector, geom):
    """
    Raycast from start_position to start_position + ray_vector against geom,
    and return the closest intersected position as a numpy 2-array if any, None else

    :type start_position: (float, float)
    :param start_position:
    :type ray_vector: (float, float)
    :param ray_vector:
    :type geom: BaseGeometry
    :param geom: geometry against which the ray is cast
    :rtype: (numpy.ndarray | None)

    """
    start_position = np.asarray(start_position)
    ray_vector = np.asarray(ray_vector)
    ray = LineString([start_position, start_position + ray_vector])
    ray_intersection = ray & geom

    if ray_intersection.is_empty:
        return None
    else:
        ray_intersection_coords = get_unique_coords(ray_intersection)
        # OPTIMIZE: square distance is enough for comparison, and cheaper
        closest_coord = min(ray_intersection_coords, key=lambda pos: norm(start_position - pos))
        return fl(closest_coord)
