__author__ = 'huulong'

from hashlib import  md5
from matplotlib.colors import hsv_to_rgb, rgb_to_hsv


def hash_key_to_rgb(id, idx, instance_count):
    """
    Hash a string ID and return a hue from that hash, with a saturation altered by the idx.
    Use it to color the #idx instance of an element of ID id, so that instances of the same ID have similar
    but different colors.

    :type id: string
    :param id:
    :type idx: int
    :param idx: index of the instance bearing id
    :param instance_count: number of instances with that id, used to spread color saturations over all samples
    :return: np.array(3)

    """
    hue = float(int(md5(id).hexdigest(), 16) % 256) / 256
    # the number of instances is equal to the max index + 1, so saturation is never 0 (never completely white)
    saturation = 1 - float(idx) / instance_count
    hsv_color = [hue, saturation, 1]
    return hsv_to_rgb(hsv_color)


def uniform_key_to_rgb(id_value, idx, id_count, instance_count):
    """
    Return a color for an integer ID and instance index, where the color is made uniform among
    all IDs and the saturation altered with the instance index.
    The total number of IDs and indices for this ID are provided to ensure uniformization.

    Use it to color the #idx instance of an element of ID id, so that instances of the same ID have similar
    but different colors.

    :type id_value: int
    :type idx: int
    :param idx: index of the instance bearing id
    :type id_count: int
    :param instance_count: number of instances with that id, used to spread color saturations over all samples
    :return: np.array(3)

    """
    hue = float(id_value) / id_count
    # the number of instances is equal to the max index + 1, so saturation is never 0 (never completely white)
    saturation = 1 - float(idx) / instance_count
    hsv_color = [hue, saturation, 1]
    return hsv_to_rgb(hsv_color)


def change_hsv_value(color, value=0.5):
    """
    Return a new color from color, with a new HSV value.
    A lower value will darken the color, the higher value will make it lighter.

    :param color: (3) array-like RGB color
    :param value: new HSV value

    """
    hsv_color = rgb_to_hsv(color)
    hsv_color[2] = value
    rgb_color = hsv_to_rgb(hsv_color)
    return rgb_color
