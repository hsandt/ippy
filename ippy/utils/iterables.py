from itertools import chain, izip, tee

__author__ = 'huulong'


def pairwise(iterable):
    """s -> (s0,s1), (s2,s3), (s4, s5), ..."""
    # use the same iterator twice in izip so that no value is repeated
    # do not confuse with Python recipe: s -> (s0,s1), (s1,s2), ...
    a = iter(iterable)
    return izip(a, a)


def overlap_pairwise(iterable):
    """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
    # split the iterable in two; do not reuse it after that
    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)


def flatten(iterable):
    """Flatten one level of nesting"""
    return chain.from_iterable(iterable)
