import random
from ippy.utils.decorator import simple_decorator

__author__ = 'hs'


# -- picker decorators --

def random_sampler(picker):
    """
    Decorator for picking functions

    An optional parameter sampling is added.
    Default to None, no effect.
    If an int is used, a random sampling of this size will be applied to the picked
    values before returning

    Useful to limit memory footprint and node expansion

    """
    def outer(piece_key, node, cp, dynamic, sampling=None):
        picked_values = picker(piece_key, node, cp, dynamic)
        if sampling is None:
            return picked_values
        if sampling is not None:
            # python sampling raises a ValueError for too much sampling
            if sampling > len(picked_values):
                return picked_values
            return random.sample(picked_values, sampling)

    return outer


# -- heuristic decorators --

@simple_decorator
def null_for_initial_node(fun):
    """
    Decorator for heuristic functions where computation makes no sense
    for the initial node

    Can be applied to all heuristics whatsoever since the initial node only appears once

    """
    def fun_with_initial_case(node):
        if node.parent is None:
            return 0
        return fun(node)
    return fun_with_initial_case


