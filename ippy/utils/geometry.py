from math import atan2

import enum
import numpy as np
from numpy import float_ as fl
from numpy.linalg import norm
from ippy.utils.mathextension import normalize
from ippy.utils.decorator import deprecated

__author__ = 'huulong'


class Orientation(enum.Enum):
    counter_clockwise = 0
    clockwise = 1
    aligned = 3


class RelativePosition(enum.Enum):
    left = 1
    right = 2
    parallel = 3
    crossed = 4


class Progression(enum.Enum):
    """Progression on an oriented edge"""
    start = 0
    middle = 1
    end = 3


class Position(enum.Enum):
    """Position enumeration. The value is the corresponding index in shapely geometric bounds."""
    top = 3
    bottom = 1
    left = 0
    right = 2


opposites = {
    Position.top: Position.bottom,
    Position.bottom: Position.top,
    Position.left: Position.right,
    Position.right: Position.left,
}

class BoundedSide(enum.Enum):
    """Enumeration describing where a point is located regarding a boundary. Mimics CGAL Bounded_side."""
    bounded_side = 0    # interior
    on_boundary = 1     # boundary
    unbounded_side = 2  # exterior

def get_coord_index_for_position(position):
    """
    :param position: Position
    :return: index of the coordinate corresponding the position (0 for x, 1 for y)
    """
    if position in (Position.left, Position.right):
        return 0  # x
    else:  # (Position.top, Position.bottom)
        return 1  # y

def get_sign_for_position(position):
    """

    :param position: Position
    :return: sign corresponding to the position, -1 for negative positions, +1 for positive positions
    """
    if position in (Position.bottom, Position.left):
        return -1  # x
    else:  # (Position.top, Position.right)
        return 1  # y


# def to_vector_list(coord_tuple_list):
#     """Return a list of numpy arrays from a list of couple of coordinates"""
#     # use the class constructor as the callable function in map to convert
#     return map(fl, coord_tuple_list)


def get_equation_params(edge):
    """
    Return the parameters of the interior half-plane equation associated
    to the edge whose vertices are described in CCW around the interior, or
    in CW around the exterior for a hole or a container.

    Parameters
        edge
            a tuple of two vectors (2-array like)

    Return
        (a, b, c) for the equation a * x + b * y + c <= 0
        of the half-plane located on the left of the oriented edge
        where (a, b) is a unit vector

    """
    # get the unit orientation unit vector
    orientation_vector = (edge[1][0] - edge[0][0], edge[1][1] - edge[0][1])
    orientation_vector = normalize(orientation_vector)
    # rotate it clockwise to get (a, b) the unit normal vector
    a, b = orientation_vector[1], -orientation_vector[0]
    # compute the offset c from the expression a * x1 + b * y1 + c == 0
    c = - a * edge[0][0] - b * edge[0][1]
    return a, b, c


def get_signed_distance(point, a, b, c):
    """
    Return the signed distance of a point to a line,

    Params
        point: (float, float)
        a, b, c: parameters of the line's equation,
            where (a, b) is a unit vector pointing to the positive distance area

    """
    return a * point[0] + b * point[1] + c


def get_angle(vector):
    """Return the direction angle of the vector

    Optimization: use numpy.arctan2() instead
    """
    return atan2(vector.y, vector.x)


def get_orientation(v):
    """
    Return either clockwise or counter_clockwise for the orientation
    of the polygon.

    (prefer shapely method)
    """

    area = 0.0
    # Compute the area (times 2) of the polygon
    for i in range(len(v)):
        area += v[i - 1][0] * v[i][1] - v[i - 1][1] * v[i][0]

    # if area == 0, can also return Orientation.aligned
    if area >= 0.0:
        return Orientation.counter_clockwise
    return Orientation.clockwise


def get_points_orientation(p1, p2, p3, threshold=0):
    """
    Return the orientation of the sequence of points p1, p2, p3

    Threshold is used to detect orientations close to alignment.

    :param threshold: tolerance threshold on the normalized cross-product

    """

    v1 = fl(p2) - fl(p1)
    v2 = fl(p3) - fl(p1)
    return get_vector_orientation(v1, v2, threshold)


def get_vector_orientation(v1, v2, threshold=0):
    """
    Return the orientation of vector v2 relatively to vector v1

    Threshold is used to detect orientations close to alignment.

    :param threshold: tolerance threshold on the normalized cross-product

    """
    determ = np.cross(v1, v2)
    # determ = v1[0] * v2[1] - v2[0] * v1[1]
    max_determ = norm(v1) * norm(v2)

    # if the max determ / area is null, then one of the vectors is null; consider the vectors aligned
    if max_determ == 0:
        return Orientation.aligned

    # normalize by product of vector magnitudes (area if vectors were orthogonal)
    # to make the threshold meaningful
    # (the normalized determinant correspond to the sinus of the angle between v1 and v2)
    normalized_determ = determ / max_determ

    if normalized_determ > threshold:
        return Orientation.counter_clockwise
    elif normalized_determ < -threshold:
        return Orientation.clockwise
    else:
        return Orientation.aligned


def get_edge_relative_position(reference_edge, other_edge, threshold=0):
    """
    Return the position of the other edge relatively to the position and orientation of the reference edge.
    Not optimized for edges sharing a common point or line, but generic.
    If required, create a more specialized function.

    :param reference_edge: oriented edge used as reference for left and right
    :param other_edge: edge (orientation does not matter)
    :param threshold: tolerance threshold to decide from when the edges are not parallel
    :return: relative position
    """

    # test CCW orientation of both vertices of other_edge, relatively to a
    orientation1 = get_points_orientation(reference_edge[0], reference_edge[1], other_edge[0], threshold)
    orientation2 = get_points_orientation(reference_edge[0], reference_edge[1], other_edge[1], threshold)

    if orientation1 == Orientation.counter_clockwise and not orientation2 == Orientation.clockwise \
            or orientation2 == Orientation.counter_clockwise and orientation1 == Orientation.aligned:
        # 2 CCW, or CCW and 1 vertex aligned -> LEFT
        return RelativePosition.left
    elif orientation1 == Orientation.clockwise and not orientation2 == Orientation.counter_clockwise \
            or orientation2 == Orientation.clockwise and orientation1 == Orientation.aligned:
        # 2 CW, or CW and 1 vertex aligned -> RIGHT
        return RelativePosition.right
    elif orientation1 == orientation2:
        # 2 vertices aligned -> PARALLEL
        return RelativePosition.parallel
    else:
        # 1 vertex on each side -> CROSSED (does not necessarily intersect, but crosses extended line)
        return RelativePosition.crossed


def get_vector_relative_position(reference_vector, other_vector, threshold=0):
    """
    Return the position of the other vector relatively to the position and orientation of the reference vector.
    Not optimized for edges sharing a common point or line, but generic.

    :param reference_vector: oriented vector used as reference for left and right
    :param other_vector: vector (orientation *does* matter)
    :param threshold: tolerance threshold to decide from when the vectors are not parallel
    :return: relative position
    """
    # OPTIMIZE: directly use ccw or np.cross: we are basically creating origin positions, then
    # computing the difference back to the vector!
    return get_edge_relative_position([(0, 0), reference_vector], [(0, 0), other_vector], threshold)


@deprecated(get_edge_relative_position)
def get_relative_position_vector(stationary_vector, orbiting_vector, orbiting_touch_position_ratio):
    """

    :param stationary_vector:
    :param orbiting_vector:
    :param orbiting_touch_position_ratio:
    :return: relative position of the orbiting vector compared to the stationary vector
    """
    assert orbiting_touch_position_ratio == 0 or orbiting_touch_position_ratio == 1,\
        "Vectors are touching in the middle, cannot compute relative position."
    # if the orbiting vector is touching at its end, reverse sense to consider relative position
    if orbiting_touch_position_ratio == 1:
        orbiting_vector = - orbiting_vector
    cross_product = np.cross(stationary_vector, orbiting_vector)
    if cross_product > 0:
        return RelativePosition.left
    elif cross_product < 0:
        return RelativePosition.right
    else:
        # return parallel; if you prefer to distinguish forward and backward
        # compute the dot product or check if vectors are equal or opposite
        return RelativePosition.parallel


def check_if_point_inside_segment(point, segment):
    """
    Return True if point in inside segment
    """

    np_point = fl(point)
    np_segment = fl(segment)
    # point is inside segment if aligned on the straight line and inside both delimiting hyperplans (dot product)
    # alternative: compute the normalized dot product once and check if between 0 and 1
    return get_points_orientation(np_segment[0], np_segment[1], np_point) == Orientation.aligned and \
           np.dot(np_segment[1] - np_segment[0], np_point - np_segment[0]) >= 0 and \
           np.dot(np_segment[0] - np_segment[1], np_point - np_segment[1]) >= 0


def get_floating_container_limit_bounds(a_bounds, container_size):
    """
    Return the limit bounds of a floating container of size container_size
    that is moving in all directions but must always contain a_bounds.
    It is never bigger than 2x container_size, and is smaller when a_bounds is bigger.

    :type a_bounds: (float, float, float, float)
    :type container_size: (float, float)

    """
    container_width, container_height = container_size
    x_min = a_bounds[2] - container_width
    x_max = a_bounds[0] + container_width
    y_min = a_bounds[3] - container_height
    y_max = a_bounds[1] + container_height
    return x_min, y_min, x_max, y_max


@deprecated('Use get_floating_container_limit_bounds() + get_ifp_with_rectangular_container to handle flat shapes')
def get_translation_bounds_to_fit_floating_container(a_bounds, b_bounds, container_size):
    """
    Return the bounds corresponding to the limit translation of B, so that AuB can fit inside
    the floating container C.

    :type a_bounds: (float, float, float, float)
    :param b_bounds: Bounds (float, float, float, float)
    :type container_size: (float, float)

    """
    # start from the floating container limit bounds, ignoring bounds of the moving object B
    # (or think of B as a point)
    floating_container_limit_bounds = fl(get_floating_container_limit_bounds(a_bounds, container_size))
    # then consider B's size and shrink the bounds accordingly
    return floating_container_limit_bounds - fl([b_bounds[0], b_bounds[1], b_bounds[2], b_bounds[3]])


