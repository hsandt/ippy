"""
Deprecated decorator.

Author: Giampaolo Rodola' <g.rodola [AT] gmail [DOT] com>
License: MIT

Adapted to clean Python by huulong and with
https://wiki.python.org/moin/PythonDecoratorLibrary#Generating_Deprecation_Warnings
"""

import warnings
import functools

warnings.simplefilter('always', DeprecationWarning)


def simple_decorator(decorator):
    """
    This decorator can be used to turn simple functions
    into well-behaved decorators, so long as the decorators
    are fairly simple. If a decorator expects a function and
    returns a function (no descriptors), and if it doesn't
    modify function attributes or docstring, then it is
    eligible to use this. Simply apply @simple_decorator to
    your decorator and it will automatically preserve the
    docstring and function attributes of functions to which
    it is applied.

    """
    def new_decorator(f):
        g = decorator(f)
        g.__name__ = f.__name__
        g.__doc__ = f.__doc__
        g.__dict__.update(f.__dict__)
        return g
    # Now a few lines needed to make simple_decorator itself
    # be a well-behaved decorator.
    new_decorator.__name__ = decorator.__name__
    new_decorator.__doc__ = decorator.__doc__
    new_decorator.__dict__.update(decorator.__dict__)
    return new_decorator


# IMPROVE: use @wraps to preserve docstirng, etc.
def deprecated(replacement=None):
    """A decorator which can be used to mark functions as deprecated.
    replacement is a callable that will be called with the same args
    as the decorated function.

    >>> @deprecated()
    ... def foo(x):
    ...     return x
    ...
    >>> ret = foo(1)
    DeprecationWarning: foo is deprecated
    >>> ret
    1
    >>>
    >>>
    >>> def newfun(x):
    ...     return 0
    ...
    >>> @deprecated(newfun)
    ... def foo(x):
    ...     return x
    ...
    >>> ret = foo(1)
    DeprecationWarning: foo is deprecated; use newfun instead
    >>> ret
    0
    >>>
    """
    def outer(fun):
        msg = "Call to deprecated function {}".format(fun.__name__)
        if replacement is not None:
            if hasattr(replacement, '__name__'):
                msg += '; use {} instead'.format(replacement.__name__)
            elif isinstance(replacement, str):
                # string have a doc, so check this before __doc__
                # useful if you do not want to import the replacement module at all but still name it
                msg += '; use "{}" instead'.format(replacement)
            elif hasattr(replacement, '__doc__') and replacement.__doc__:
                # properties do not have a __name__ so allow using their doc is not empty
                msg += '; use "{}" instead'.format(replacement.__doc__)

        if fun.__doc__ is None:
            fun.__doc__ = msg

        @functools.wraps(fun)
        def inner(*args, **kwargs):
            warnings.warn(msg, category=DeprecationWarning, stacklevel=2)
            return fun(*args, **kwargs)

        return inner
    return outer


# http://stackoverflow.com/questions/11217878/python-2-7-combine-abc-abstractmethod-and-classmethod
class abstractclassmethod(classmethod):

    __isabstractmethod__ = True

    def __init__(self, meth):
        meth.__isabstractmethod__ = True
        super(abstractclassmethod, self).__init__(meth)
