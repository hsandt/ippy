from os.path import dirname, join

__author__ = 'huulong'

# this module is located in ippy/utils so the main directory is ippy/
MAIN_DIRECTORY = dirname(dirname(__file__))


def get_full_path(*path):
    return join(MAIN_DIRECTORY, *path)
