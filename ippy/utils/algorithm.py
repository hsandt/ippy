__author__ = 'huulong'


class IterationCounter(object):
    """
    Counter for iterations that can check if a maximum number of iterations has been reached.

    :type max_iterations: int
    :param max_iterations: maximum number of iterations
    :type iteration: int
    :param iteration: current number of iterations recorded

    """
    def __init__(self, max_iterations):
        self.max_iterations = max_iterations
        self.iteration = 0

    def increment_and_check_exceed(self):
        """
        Increment the iteration counter and return True if the counter has exceeded
        the maximum number of iterations.

        No Exception is thrown. Instead the calling process must check the returned value and
        handle the exceeding case itself. If nevertheless the calling process continues to iterate,
        next time this method is called the counter will increment again (more than 1 over the maximum
        expected) and the method will return True again.

        :rtype: bool

        """
        self.iteration += 1
        return self.max_iterations and self.iteration > self.max_iterations

    def __repr__(self):
        return '<IterationCounter: {} / {}>'.format(self.iteration, self.max_iterations)


class MaxIterationsException(Exception):
    """
    Exception to raise when the maximum number of iterations expected has been exceeded

    Attributes:

        :type max_iterations: int
        :param max_iterations: max number of iterations expected

    """
    def __init__(self, max_iterations):
        super(MaxIterationsException, self).__init__('Exceeded max iterations {}'.format(max_iterations))
        self.max_iterations = max_iterations


class GeometryException(Exception):
    """
    Exception to raise when some inconsistency of geometry occurs.

    Contains a geometry that will be plot as a final debug step

    """
    def __init__(self, msg, geometry):
        super(GeometryException, self).__init__(msg)
        self.msg = msg
        self.geometry = geometry
