from matplotlib import pyplot as plt, animation
from ippy.utils.decorator import deprecated
from ippy.utils.shapelyextension import rotate_translate, update_bounds
from ippy.view.constraintviewer import BaseConstraintViewer
from ippy.view.plot import to_patch_collection, get_uniform_dimension_bounds, add_geom_patches

__author__ = 'hs'


# *OLD* pure constraint search; see constraintsearchviewer.NestingPlotCSEventHandler instead
class NestingPlotConstraintViewer(BaseConstraintViewer):
    """
    Constraint solver view that displays an animated plot of the search process

    Attributes
        :param fig: plotting figure containing the animation
        :type axis: matplotlib.axes.Axes
        :param axis: axis to plot on (occupies all the figure)
        :param geoms: list of geometries to plot sequentially
        :param artists_sequence: sequence of list of artists to draw on each step of the animation
        :param piece_patch_collections: list of patches of placed pieces up to now

    Inherited attributes with specific types
        :type cp: (ippy.search.constraint.ConstraintProblem | ippy.packmath.nestingconstraintproblem.InputMinimizationNestingProblem)

    """
    def __init__(self, cp):
        super(NestingPlotConstraintViewer, self).__init__(cp)
        self.fig = None
        self.axis = None
        self.axis_bounds = [float('+inf'), float('+inf'), float('-inf'), float('-inf')]

        # self.geoms = []
        self.artists_sequence = []
        self.piece_patch_collections = []
        # initialize bounds with infinite values in the opposite direction to be replaced on 1st update

    def handle_started(self):
        super(NestingPlotConstraintViewer, self).handle_started()
        self.fig = plt.figure(figsize=(10, 10))
        self.axis = self.fig.add_subplot(111)

    def handle_assigned(self, piece_key, placement, new_placements):
        super(NestingPlotConstraintViewer, self).handle_assigned(piece_key, placement, new_placements)
        # place piece (if original was PolygonExtended, a normal Polygon is still returned)
        piece_geom = self.cp.lot[piece_key]

        # ignore empty piece, otherwise will cause a bug with bounds
        # (add a blank piece if you need a uniform format for the animation data)
        if piece_geom.is_empty:
            return

        # we display the container + all previous placed pieces + the last piece

        # CONTAINER
        # the container may be floating so we delegate definition of the container to the problem
        container_patch_collections = self.get_container_patch_collections(new_placements)

        # PREVIOUS PIECES
        # previous_patch_collections = self.piece_patch_collections  # empty on first step

        # NEW PIECE
        placed_piece_geom = rotate_translate(piece_geom, placement.rotation, placement.translation)
        # in match_original=True mode, you cannot set properties on the collection
        # instead, set zorder manually after creation
        new_patch_collection = to_patch_collection(placed_piece_geom, {'facecolor': 'r'})
        new_patch_collection.set_zorder(2)

        # same as above, register axis
        self.axis.add_collection(new_patch_collection)
        self.piece_patch_collections.append(new_patch_collection)

        self.artists_sequence.append(container_patch_collections + self.piece_patch_collections)
        # self.artists_sequence.append(previous_patch_collections + [new_patch_collection])
        # self.artists_sequence.append([new_patch_collection])
        update_bounds(self.axis_bounds, placed_piece_geom.bounds)

    def handle_backtracked(self, variable, new_placements):
        super(NestingPlotConstraintViewer, self).handle_backtracked(variable, new_placements)
        # we pop the last piece, but no need to add a drawing step except if we clearly want
        # to see multiple backtrackings in chain
        self.piece_patch_collections.pop()

    def handle_finished(self, assignment, reason):
        super(NestingPlotConstraintViewer, self).handle_finished(assignment, reason)
        if self.fig is None or self.axis is None:
            raise Exception("NestingPlotConstraintSearchViewer's figure/axis is None, is 'finished' event called before 'started' event?")

        # TODO: check that at least one non-empty geom was added

        # plot the search process if a solution was found (to show how we reached it)
        # OR no solution was found (to show how we reached a dead-end)
        # OR the max number of iterations were reached (to show where we were slowing down)
        # 'cost span' is sent at the meta level of the dichotomy so no need to plot again, sub-problems did
        if reason in ('solution found', 'no solution found', 'max iterations'):

            # provide xy limits to show all pieces + a small margin at a uniform scale
            uniform_bounds = get_uniform_dimension_bounds(self.axis_bounds)
            self.axis.set_xlim(uniform_bounds[0] - 1, uniform_bounds[2] + 1)
            self.axis.set_ylim(uniform_bounds[1] - 1, uniform_bounds[3] + 1)

            # logging.debug([repr(artist.get_figure()) for artist_collection in self.artists for artist in artist_collection])
            anim = animation.ArtistAnimation(self.fig, self.artists_sequence, interval=1000, repeat=False)
            plt.show()

    def get_container_patch_collections(self, placements):
        """
        Return list of patch collections representing the fixed / floating container given the current
        placements.

        The list often contains only one patch collection, but it is easier to manipulate and concatenate lists.

        :return: list[PatchCollection]

        """
        maximum_container = self.cp.get_maximum_container(placements)
        # before first placement in floating origin problem, no container is defined; just ignore it
        if maximum_container is None:
            container_patch_collections = []
        else:
            container_patch_collection = to_patch_collection(maximum_container, {'facecolor': 'w'})
            # let the artist live in our axis, or it cannot be animated (we don't provide the axis later)
            self.axis.add_collection(container_patch_collection)
            container_patch_collections = [container_patch_collection]
        return container_patch_collections


class NestingPlotConsoleConstraintViewer(NestingPlotConstraintViewer):
    """
    Constraint solver view that displays an animated plot of the search process
    and also outputs events to the console to track progression in real-time.

    """
    def __init__(self, cp):
        super(NestingPlotConsoleConstraintViewer, self).__init__(cp)

    def event(self, name, *params):
        super(NestingPlotConsoleConstraintViewer, self).event(name, *params)

        self.output('EVENT {}: {}'.format(self.last_event.name, self.last_event.description))

    def output(self, text):
        print('Viewer: ' + text)
