from copy import deepcopy
import logging
from matplotlib.colors import rgb_to_hsv
import numpy as np
from shapely.geometry import box
from numpy import float_ as fl

from matplotlib import patches, f
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.collections import PatchCollection
from shapely.geometry.polygon import orient
from ippy.packmath.packhelper import get_lot_bounds
from ippy.utils.decorator import deprecated
from ippy.utils.iterables import flatten
from ippy.utils.shapelyextension import get_translated_polygons, get_translated_polygons_from_problem, \
    PolygonWithBoundary, get_size, is_multi, update_bounds, to_size, get_placed_lot

__author__ = 'hs'

# http://stackoverflow.com/questions/14720331/how-to-generate-random-colors-in-matplotlib
import matplotlib.cm as cmx
import matplotlib.colors as colors

def get_cmap(n):
    """
    Returns a function that maps each index in 0, 1, ... N-1 to a distinct
    RGB color.

    """
    color_norm = colors.Normalize(vmin=0, vmax=n-1)
    scalar_map = cmx.ScalarMappable(norm=color_norm, cmap='hsv')

    def map_index_to_rgb_color(index):
        return scalar_map.to_rgba(index)

    return map_index_to_rgb_color


def to_patch_list(geom, **params):
    """
    Convert a BaseGeometry or a PolygonWithBoundary to a list of Patches

    Keyword parameters are passed to the patch constructor

    :type geom: (shapely.geometry.Polygon | shapely.geometry.MultiPolygon |
                shapely.geometry.LineString | shapely.geometry.MultiLineString |
                shapely.geometry.LinearRing |
                shapely.geometry.Point | shapely.geometry.MultiPoint |
                shapely.geometry.GeometryCollection |
                PolygonWithBoundary)
    :param params: patch constructor parameters
    :rtype: list[Patch]

    """
    patch_list = []

    # ignore empty geometries
    if geom.is_empty:
        # keep patch_list empty, this will create an empty PathCollection
        # this case is totally acceptable, as Polygon with Boundary may have an empty closed interior for example
        # however, do not create a PatchCollection from a completely empty geometry, it will raise an Exception
        # in match_original mode because some attributes are expected
        pass

    elif isinstance(geom, PolygonWithBoundary):
        # patch for the closed interior
        patch_list.extend(to_patch_list(geom.closed_interior, **params))

        # patch for the boundary (facecolor will be removed and replaced with 'none')
        patch_list.extend(to_patch_list(geom.boundary, **params))

    elif geom.type == 'Polygon':
        # orient the polygon CCW and its holes CW
        # REFACTOR: do it when creating the lot instead. You can still maintain a check here
        geom = orient(geom)

        # create patch for polygon with holes #

        # shapely provides looped coordinates so we do not need to add a dummy vertex to close the exterior
        # and hole loops (the closing vertex will be ignored under the CLOSEPOLY instruction)

        # exterior boundary
        exterior_boundary_vertices = list(geom.exterior.coords)
        # imitate path module's source code to generate appropriate path codes quickly
        exterior_boundary_codes = np.empty(len(exterior_boundary_vertices), dtype=Path.code_type)
        exterior_boundary_codes[0] = Path.MOVETO
        exterior_boundary_codes[1:-1] = Path.LINETO
        exterior_boundary_codes[-1] = Path.CLOSEPOLY

        # hole boundaries: CLOSEPOLY act as a hole terminator, so use flattened coordinates of all hole vertices
        hole_boundary_vertices_list = [list(hole.coords) for hole in geom.interiors]
        hole_boundary_all_vertices = list(flatten(hole_boundary_vertices_list))
        # as above, prepare path codes, but iterate CW
        hole_boundary_all_codes = np.empty(len(hole_boundary_all_vertices), dtype=Path.code_type)  # will contain codes of all holes chained
        next_idx = 0  # next index from which to update the array content
        for hole_boundary_vertices in hole_boundary_vertices_list:
            nb_codes = len(hole_boundary_vertices)
            hole_boundary_codes = np.empty(nb_codes, dtype=Path.code_type)
            hole_boundary_codes[0] = Path.MOVETO
            hole_boundary_codes[1:-1] = Path.LINETO
            hole_boundary_codes[-1] = Path.CLOSEPOLY
            hole_boundary_all_codes[next_idx:next_idx + nb_codes] = hole_boundary_codes
            next_idx += nb_codes

        all_vertices = exterior_boundary_vertices + hole_boundary_all_vertices
        # to concatenate numpy arrays, must use concatenate not use __add__ operation
        # OPTIMIZE: build a big empty array and fill without changing size; but concatenate probably okay for a debug
        all_codes = np.concatenate((exterior_boundary_codes, hole_boundary_all_codes))

        # linewidth of 2 by default, but custom parameters can override this
        if params.get('lw') is None:
            params['lw'] = 2

        polygon_with_holes_patch = patches.PathPatch(Path(all_vertices, all_codes), **params)
        patch_list.append(polygon_with_holes_patch)
        # patch_list.append(patches.Polygon(list(geom.exterior.coords), lw=3, **params))
    elif geom.type in ('LineString', 'LinearRing'):
        # if facecolor was defined because the params are shared with other geoms
        # such as Polygon or Point, replace it with 'none' locally (to avoid filling the convex hull of the lines)
        params['facecolor'] = 'none'

        # linewidth of 2 by default, but custom parameters can override this
        if params.get('lw') is None:
            params['lw'] = 2

        patch_list.append(patches.PathPatch(Path(list(geom.coords)), **params))
    elif geom.type == 'Point':
        params['lw'] = 2  # no need to have wider lines for a visible point

        position = fl(geom.coords[0])
        patch_list.append(patches.Circle(geom.coords[0], radius=0.1, **params))
        # cross for more precision
        patch_list.append(patches.PathPatch(Path([position + fl([-0.1, 0]), position + fl([0.1, 0])]), **params))
        patch_list.append(patches.PathPatch(Path([position + fl([0, -0.1]), position + fl([0, 0.1])]), **params))
        # axis.plot(1,1,'o', color='black')
    elif is_multi(geom):
        for part in geom:
            # FIXME: multine string should delegate to string and let them remove facecolor,
            # but for some reason a gray area appears in some concavities, which probably comes from LineString facecolor
            patch_list.extend(to_patch_list(part, **params))

    return patch_list


def to_patch_collection(geom, geom_params=None, zorder=2, **params):
    """
    Convert a BaseGeometry or a PolygonWithBoundary to a Patch Collection

    Keyword parameters are passed to the patch constructor

    :type geom: (shapely.geometry.Polygon | shapely.geometry.MultiPolygon |
                shapely.geometry.LineString | shapely.geometry.MultiLineString |
                shapely.geometry.LinearRing |
                shapely.geometry.Point | shapely.geometry.MultiPoint |
                shapely.geometry.GeometryCollection |
                PolygonWithBoundary)
    :type geom_params: dict[str, T]
    :param geom_params: parameters to pass to the patch constructor, as a dictionary
    :param params: parameters of the patch collection itself
    :rtype: PatchCollection

    """
    if geom_params is None:
        geom_params = {}
    patch_collection = PatchCollection(to_patch_list(geom, **geom_params), match_original=True, **params)

    # in match_original=True mode, you cannot set properties on the collection directly
    # instead, set zorder manually after creation
    patch_collection.set_zorder(zorder)
    return patch_collection


def to_patch_collection_list(geom, geom_params=None, zorder=2, **params):
    """
    Convert a BaseGeometry or a PolygonWithBoundary to a list of Patch Collections

    Use this method if you experience graphical glitches with to_patch_collection applied to
    complex geometries, as it will draw geometries more independently.

    Keyword parameters are passed to the patch constructor

    :type geom: (shapely.geometry.Polygon | shapely.geometry.MultiPolygon |
                shapely.geometry.LineString | shapely.geometry.MultiLineString |
                shapely.geometry.LinearRing |
                shapely.geometry.Point | shapely.geometry.MultiPoint |
                shapely.geometry.GeometryCollection |
                PolygonWithBoundary)
    :type geom_params: dict[str, T]
    :param geom_params: parameters to pass to the patch constructor, as a dictionary
    :param params: parameters of the patch collection itself
    :rtype: list[PatchCollection]

    """
    if geom_params is None:
        geom_params = {}
    patch_collection_list = [PatchCollection([patch], match_original=True, **params) for patch in to_patch_list(geom, **geom_params)]

    # in match_original=True mode, you cannot set properties on the collection directly
    # instead, set zorder manually after creation
    for patch_collection in patch_collection_list:
        patch_collection.set_zorder(zorder)
    return patch_collection_list


def add_geom_patches(axis, *geom_params_tuples):
    """
    Add the patches representing the BaseGeometry to the axis,
    and set adequate visual bounds.
    Warning: If you call this function more than once, bounds may not be adapted.

    A collection of all the patches is created from a list of piece patches at once.

    Currently supported:
    - Polygon exterior and MultiPolygon
    - LineString and MultiLineString

    :param axis: axis on which to plot the polygons
    :param geom_params_tuples: geometries to plot (exterior) with dict of params
    """
    if not geom_params_tuples:
        raise ValueError('No polygon-color tuples provided.')

    # use number of polygons to reduce alpha opacity gradually
    n = len(geom_params_tuples)

    min_x, min_y = float('+inf'), float('+inf')
    max_x, max_y = float('-inf'), float('-inf')
    axis_bounds = [min_x, min_y, max_x, max_y]

    patch_list = []
    for idx, geom_params_tuple in enumerate(geom_params_tuples):
        geom, params = geom_params_tuple

        # ignore empty geometries
        if geom.is_empty:
            continue

        # we create a big list of all piece patches
        patch_list.extend(to_patch_list(geom, alpha=0.5, **params))

        # patches[-1].set_alpha(1 - float(idx) / n)
        # patch.set_alpha(0.3)
        update_bounds(axis_bounds, geom.bounds)

    # we make one collection of all the patches
    axis.add_collection(PatchCollection(patch_list, match_original=True))

    # provide xy limits to show all polygons + a small margin
    # axis.set_xlim(min_x - 1, max_x + 1)
    # axis.set_ylim(min_y - 1, max_y + 1)

    # provide xy limits to show all polygons + a small margin at a uniform scale
    uniform_bounds = get_uniform_dimension_bounds(axis_bounds)
    axis.set_xlim(uniform_bounds[0] - 1, uniform_bounds[2] + 1)
    axis.set_ylim(uniform_bounds[1] - 1, uniform_bounds[3] + 1)

def get_uniform_dimension_bounds(bounds):
    """
    Return the bounds with uniform dimensions, i.e. with the shape of a regular square.
    The shorter side of the rectangle is sized up to the bigger size.
    The bottom-left corner is preserved.

    :param bounds: rectangle bounds
    :return: square bounds
    """
    bigger_side = max(to_size(bounds))  # bigger side used as square side
    return bounds[0], bounds[1], bounds[0] + bigger_side, bounds[1] + bigger_side

def plot_geom_colors(*geom_color_tuples):
    """
    Plot (multi)geometry associated to their colors in the parameter.
    Depending on the type of geometry, the appropriate parameter is colored.

    :param geom_color_tuples: list of ((Multi)Polygon, Color) tuple arguments
    """
    fig = plt.figure(figsize=(10, 10))
    axis = fig.add_subplot(111)
    # axis.set_autoscale_on(False)
    geom_params_tuples = []
    for geom, color in geom_color_tuples:

        # ignore empty geometries, but log a warning
        if geom.is_empty:
            logging.warning("Empty geometry passed in one of plot_geom_colors's tuples")
            continue

        # special test for PolygonWithBoundary
        if isinstance(geom, PolygonWithBoundary):
            # draw the interior and the boundary (TODO: with a color variation)
            geom_params_tuples.append((geom.closed_interior, {'facecolor': color}))  # may be empty
            # lines will ignore facecolor, but useful for points
            geom_params_tuples.append((geom.boundary, {'facecolor': color}))
            continue

        # for BaseGeometry, test type
        if geom.type in ('Polygon', 'MultiPolygon', 'Point', 'MultiPoint'):
            geom_params_tuples.append((geom, {'facecolor': color}))
        elif geom.type in ('LineString', 'MultiLineString'):
            geom_params_tuples.append((geom, {'edgecolor': color}))
        elif geom.type == 'GeometryCollection':
            # REFACTOR & IMPROVE: use recursion to associate correct color parameter to line or filled shape,
            # including for multiple depth of collections (although rare)
            # for now, just color everything the same (but difficult to see)
            geom_params_tuples.append((geom, {'facecolor': color, 'edgecolor': color}))
        else:
            raise ValueError('Only Polygon, LineString, Point and their Multi- are supported.')

    add_geom_patches(axis, *geom_params_tuples)
    plt.show()


def plot_polygon_colors(*polygon_color_tuples):
    """
    Plot (multi)polygons associated to their colors in the parameter

    :param polygon_color_tuples: list of ((Multi)Polygon, Color) tuple arguments

    """
    # fig = plt.figure()
    fig = plt.figure(figsize=(10, 10))
    axis = fig.add_subplot(111)
    # axis.set_autoscale_on(False)
    polygon_params_tuples = ((polygon, {'facecolor': color}) for polygon, color in polygon_color_tuples)
    add_geom_patches(axis, *polygon_params_tuples)
    plt.show()


def plot_linestring_colors(*line_color_tuples):
    """
    Plot (multi)lines associated to their colors in the parameter

    :param line_color_tuples: list of ((Multi)LineString, Color) tuple arguments
    """
    # fig = plt.figure()
    fig = plt.figure(figsize=(10, 10))
    axis = fig.add_subplot(111)
    # axis.set_autoscale_on(False)
    line_params_tuples = ((line, {'edgecolor': color}) for line, color in line_color_tuples)
    add_geom_patches(axis, *line_params_tuples)
    plt.show()


def plot_solution_with_colors(placements, piece_instance_tuple_color_dict=None, problem=None, expanded_problem=None,
                              floating_container=False, floating_width=False, container_width=None):
    """
    Plot layout applied to problem for each piece instance.
    If piece_piece_instance_tuple_color_dict is provided, the values will be used as colors for each piece key.
    Otherwise, random colors will be applied.
    If floating_container is True, the container will be plot from the bottom-left of the placement.
    container_width can be tuned too.
    If floating_width is True but not floating_container, only the container's X is readjusted
    (useful for fixed origin open dimension search, since they focus on the actual width obtained)

    :param placements:
    :type piece_instance_tuple_color_dict: (dict[(str, int), Color] | None)
    :param piece_instance_tuple_color_dict: dictionary of color per piece_id - instance_idx tuple key
    :type problem: ippy.packmath.problem.NestingProblem
    :param problem:
    :type expanded_problem: ippy.packmath.problem.ExpandedNestingProblem
    :param expanded_problem: If you set this parameter, it will be used instead of problem (recommended)
    :return:

    """
    # REFACTOR: pass CSP/COP because it has problem-specific information on how to draw the container,
    # fixed or floating, etc. and also contains a reference to the expanded problem as the attribute 'data'

    # if placements are None, the solver found no solution and you should not plot or should plot the container only
    # in any case, warn the user
    if placements is None:
        logging.warn('Cannot plot when placements are None. The solver seems not to have found a solution.')
        return

    if expanded_problem is None:
        expanded_lot = problem.get_expanded_lot()
        expanded_container = problem.get_expanded_container()
    else:
        expanded_lot = expanded_problem.expanded_lot
        expanded_container = expanded_problem.expanded_container


    # OPTIMIZE? translate view in pyplot instead of geometrical objects
    placed_lot = get_placed_lot(expanded_lot, placements)

    container = expanded_container[1]
    original_container_width, container_height = get_size(container)

    # if we are in floating-origin and there is at least one piece placed, move the container accordingly
    # in floating-width mode, only move X (useful for fixed origin container but open dimension problems)
    if floating_container and placed_lot:
        container_bottomleft = get_lot_bounds(placed_lot)[:2]
    elif floating_width and placed_lot:
        container_bottomleft = get_lot_bounds(placed_lot)[0], 0
    else:
        container_bottomleft = (0, 0)

    if floating_container and placed_lot:
        container_topright = get_lot_bounds(placed_lot)[2:4]
    elif floating_width and placed_lot:
        container_topright = get_lot_bounds(placed_lot)[2], container_height
    else:
        container_topright = original_container_width, container_height

    container = box(container_bottomleft[0], container_bottomleft[1],
                    container_topright[0], container_topright[1])

    # if len() != len(tr_polygons):
    #     raise ValueError('{} colors provided but {} polygons (+ container) must be plotted.'.format(len(colors), len(tr_polygons)))

    # plot all the polygons that have a placement (value in translated lot is not None), ignore the others
    # IMPROVE: hash piece ID - instance tuple to get a unique but consistent color
    if piece_instance_tuple_color_dict:
        tr_polygon_color_tuples = [(placed_lot[(piece_id, instance_idx)], piece_instance_tuple_color_dict[(piece_id, instance_idx)])
                               for piece_id, instance_idx in placements.iterkeys()]
    else:
        cmap = get_cmap(len(placed_lot))
        # use layout.placement keys, not tr_lot, the latter containing all piece keys but None values
        tr_polygon_color_tuples = [(placed_lot[(piece_id, instance_idx)], cmap(absolute_idx))
                               for absolute_idx, (piece_id, instance_idx) in enumerate(placements.iterkeys())]

    plot_polygon_colors(
        (container, 'white'),
        *tr_polygon_color_tuples
    )


class PlotOnFailureContext(object):
    """
    A context manager used in unit tests to plot something when the body raises
    an AssertionError (should actually be the TestCase.failureException).

    After that, the context lets the exception pass, so that the test can fail.

    Other exceptions are normally passed.
    """

    def __init__(self, *geom_color_tuples):
        self.geom_color_tuples = geom_color_tuples

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type is None:
            # no exception raised, continue (usually, it means the test succeeded)
            return True

        if issubclass(exc_type, AssertionError):
            # plot and let expection pass
            # unpack values to work with multiple args
            plot_geom_colors(*self.geom_color_tuples)
            return False
        else:
            # let other exceptions pass through (usually, it means the test failed)
            return False


@deprecated()
def get_shapely_polygon_exterior_path_old(polygon):
    """
    Return the pyplot path of the exterior of a polygon

    :param polygon: polygon to plot
    :return: matplotlib.path.Path representing the polygon's exterior
    """

    # get vertices from polygon (length N+1)
    vertices = list(polygon.exterior.coords)

    # codes consist in one MOVE TO the first vertex,
    # N-1 LINE TO the next vertex and one CLOSE POLYGON
    codes = [Path.MOVETO] + [Path.LINETO] * (len(vertices) - 2) + [Path.CLOSEPOLY]

    path = Path(vertices, codes)


