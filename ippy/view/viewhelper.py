from shapely.affinity import scale
from ippy.utils.shapelyextension import get_size

__author__ = 'huulong'


def get_fit_scale_factor(box_size, container_size):
    """
    Return the scale factor to apply to a rectangular box so that
    it just fits inside a rectangular container,
    i.e. the maximum scale so that the box can be placed inside the container

    Parameters
        box_size: (width, height) of the object to scale to fit
        container_size: (width, height) of the object to be fitted with

    """
    if 0 in box_size:
        raise ValueError('box_size contains null value: {0}'.format(box_size))
    # the fit scale factor is the minimum ratio between respective widths
    # and heights of the 2 objects
    return min(container_size[0] / box_size[0], container_size[1] / box_size[1])


def get_cell_coords(polygon, cell_size, padding=0):
    """
    Return the list of relative coordinates of a Shapely polygon
    scaled into a cell of size cell_size, with the given padding.
    (Prefer GridLayout's padding and spacing properties when possible)

    Coordinates are relative to the bottom-left of the cell.

    If polygon is None, return an empty list.

    """
    if polygon is None:
        return []
    # polygon coordinates may be negative, so compute scale factor with xMax - xMin and yMax - yMin
    # by choosing the direction with the minimum ratio to avoid overflowing out of the cell (including padding)
    polygon_size = get_size(polygon)
    scale_factor = get_fit_scale_factor(polygon_size, (cell_size[0] - 2 * padding, cell_size[1] - 2 * padding))
    scaled_size = (scale_factor * polygon_size[0], scale_factor * polygon_size[1])
    # compute offset in each direction so that the center of the scaled polygon is the center of the cell
    # for the direction chosen for the scale_factor, the offset equals the padding
    offset = ((cell_size[0] - scaled_size[0]) / 2, (cell_size[1] - scaled_size[1]) / 2)
    # move the bottom-left corner to the origin, scale, and offset again
    return [((x - polygon.bounds[0]) * scale_factor + offset[0], (y - polygon.bounds[1]) * scale_factor + offset[1]) for x, y in polygon.boundary.coords]


def get_layout_coords(polygon, layout_widget):
    """
    Return the list of relative coordinates of a Shapely polygon
    scaled by the LayoutWidget's fit scale factor.

    Coordinates are relative to the bottom-left of the cell.
    The origin is preserved, so that the coordinates can be used for precise placement
    in a layout.

    If polygon is None, return an empty list.

    """
    if polygon is None:
        return []
    # scale with shapely to easily handle holes
    # note: we could do the calculation manually since we do not need any geometrical interpretation
    # however, to make computations faster we should have to use a C library anyway

    # scale from the origin
    scale_factor = layout_widget.fit_scale_factor
    scaled_polygon = scale(polygon, scale_factor, scale_factor, origin=(0, 0))
    return scaled_polygon.boundary.coords
