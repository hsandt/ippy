__author__ = 'hs'

from Tkinter import *
from tVector import *

class MyWin:
    def __init__(self, parent):
        self.layout = tCol(tW(Label, 'L1',
                              text="Window Heading",
                              font="helvetica 16 bold"),
                           tW(Checkbutton, 'Chk1',
                              text="A checkbutton"),
                           tCol(
                               tRow(tW(Button, 'but1',
                                       text="Button 1",
                                       command=self.on_button1),
                                    tW(Button, 'but2',
                                       text="Button 2",
                                       command=self.on_button2),
                                    tW(Button, 'but3',
                                       text="Button 3",
                                       command=self.on_button3)),
                               tRow(tW(Entry, 'fld1',
                                       text="field 1"),
                                    tW(Entry, 'fld2',
                                       text="field 2"))),
                           tCol(
                               tRow(tW(Button, 'but1',
                                       text="Button 1",
                                       command=self.on_button1),
                                    tW(Button, 'but2',
                                       text="Button 2",
                                       command=self.on_button2),
                                    tW(Button, 'but3',
                                       text="Button 3",
                                       command=self.on_button3)),
                               tRow(tW(Entry, 'fld1',
                                       text="field 1"),
                                    tW(Entry, 'fld2',
                                       text="field 2"))
                           ))

        self.layout.tBuild(parent)

    def on_button1(self):
        print "clicked button1"
        self.layout.wid('but1', text="New Button1 label")

    def on_button2(self):
        print "clicked button2"
        self.layout.wid('but1', text='Button 1')

    def on_button3(self):
        print "clicked button3"

    def mainloop(self):
        self.layout.mainloop()


root = Tk()
app = MyWin(root)
app.mainloop()
