import matplotlib
import time

matplotlib.use('TkAgg')

import cPickle as pickle
import os
from os import path


from ippy.config.config import config
from ippy.packmath.strategy import cache_values, filter_functions, heuristic_functions, dynamic_cost_functions, \
    execution_level_value_tuples, rounding_level_value_tuples, fringe_names, Strategy
from ippy.packmath.strategy import picker_functions
from ippy.utils.datastructure import PriorityQueueWithFunctionRandomizedPop, PriorityQueueWithFunction, \
    BoundedPriorityQueueWithFunction
from ippy.utils.files import get_full_path
from ippy.view.tkinterextension import GridFrameManager, get_listbox_value, get_listbox_idx


from functools import partial
import logging
from ippy.packmath.constraintsearch import ordered_master_heuristic, value_picking_constraint_graph_search
from ippy.packmath.constraintsearchviewer import EventConstraintSearchViewer, ConsoleCSEventHandler, \
    NestingPlotCSEventHandler
from ippy.packmath.nestingconstraintsearch import ValuePickingNestingSearchProblem
from ippy.packmath.problemfactory import create_expanded_packing_problem_from_file, \
    create_fixed_origin_single_bin_size_problem, create_floating_origin_single_bin_size_problem, \
    create_fixed_origin_open_dimension_problem, create_floating_origin_open_dimension_problem

import tkFileDialog
from Tkinter import Tk, Button, Checkbutton, Frame, IntVar, Entry, StringVar, \
    END, \
    W, DoubleVar
from Tkconstants import SUNKEN, MULTIPLE

__author__ = 'hs'

# paths
PROJECT_PATH = os.getcwd()
BENCHMARK_PATH = get_full_path('benchmark')
CONFIG_FILEPATH = get_full_path('user', 'config.p')
LISTBOX_CONFIG_FILEPATH = get_full_path('user', 'lb_config.p')

# file extension
OPEN_FILE_OPTIONS = dict(defaultextension='.xml',
                         filetypes=[('XML file', '*.xml'), ('All files', '*.*')])  # on Linux, first is used as default

# config var names and default values
default_config = dict(
    problem_filepath='',
    floating_origin=0,
    open_dim=0,
    picker_sampling_active=0,
    picker_sampling=10,
    max_iterations=1200,
    bound_limit=50,
    random_span=5,
    console_view=1,
    log_level=20,
    plot_view=1,
    force_fixed_length_value=0,
    fixed_length_value=10.,
    multistart=1,
    log_result=1,
    save_plot=1,
    notext=0,
)

# config for listboxes (name = variable without _listbox, used to define self.[name]_selection list of indices
default_listbox_config = dict(
    fringe=(0,),
    cache=(),
    picker=(0,),
    filter=(0,),
    heuristic1=(1,),
    heuristic2=(2,),
    heuristic3=(0,),
    heuristic4=(0,),
    dynamic_cost=(1,),
    execution_level=(2,),
    rounding_level=(0,),
)


class Application(Frame):
    """
    Main application to execute and visualize packing

    Variable names and their meaning, also used in user config:

    problem_filepath
        init variables with default or saved config
    floating_origin
        False: fixed-origin problem, True: floating-origin problem
    open_dim
        False: single size, True: open dimension
    max_iterations
        nb of max iterations
    bound_limit
        max number of nodes (bounded fringe only)
    random_span
        random span (random fringe only)
    console_view
        view search log in console?
    log_level
        log level for the console view (20: DEBUG, 10: DETAILS such as successor priority values)
    plot_view
        plot search at the end?
    multistart
        Integer: number of concurrent branches for the search (1 for single search)
    notext
        No text on plot
    saveplot
        Should we save the best plot at the end of the search?

    """
    def __init__(self, master=None):
        Frame.__init__(self, master)

        # bind quit event to root (or bind_all on this frame)
        self.master.bind("<Escape>", self.quit_callback)
        self.master.protocol("WM_DELETE_WINDOW", self._quit)

        # dummy initialization, just to help IDE with attributes
        self.problem_filepath = None,
        self.floating_origin = None,
        self.open_dim = None,
        self.picker_sampling_active = None,
        self.picker_sampling = None,
        self.max_iterations = None,
        self.bound_limit = None,
        self.random_span = None,
        self.console_view = None,
        self.log_level = None,
        self.plot_view = None,
        self.force_fixed_length_value = None,
        self.fixed_length_value = None
        self.multistart = None
        self.log_result = None
        self.notext = None
        self.save_plot = None

        # load default values from config file (values from last session)
        self.load_config()

        self.create_widgets()
        self.pack()

    def create_widgets(self):
        """
        Create all widgets

        """
        gf = GridFrameManager(self, sticky=W, padx=5, pady=2)

        # -- PROBLEM FRAME --

        # problem entry: input problem XML file path there
        f = gf.new_frame()
        gf.new_row()
        self.problem_entry = Entry(f, textvariable=self.problem_filepath, width=80)
        # REFACTOR: subclass Entry to standardize our custom attributes
        self.problem_entry.default_msg = 'problem filepath (.xml)'  # custom attribute to store gray default message
        self.problem_entry.bind('<FocusIn>', focus_in_entry)
        self.problem_entry.bind('<FocusOut>', focus_out_entry)

        # if no text loaded from config, use default message in gray, otherwise just use the config value
        if self.problem_filepath.get():
            # black font by default, but if you use another color also set it here
            # set to True when user enter text (in black), including empty text
            self.problem_entry.contains_user_text = True
        else:
            self.problem_entry.contains_user_text = False
            set_gray_message(self.problem_entry, self.problem_entry.default_msg)

        gf.next_column(self.problem_entry, columnspan=4)

        # Open button: to choose local XML file for problem entry
        open_button = Button(f, text="Open", command=self.open)
        gf.next_column(open_button)  # doubled...

        # -- SOLVER FRAME --
        f = gf.new_frame()

        # VARIANT COLUMN
        gf.new_column()

        gf.next_row(Checkbutton(f, text="Floating origin", variable=self.floating_origin))
        gf.next_row(Checkbutton(f, text="Open dimension", variable=self.open_dim))

        # fringe
        self.fringe_listbox = gf.add_listbox('Fringe', fringe_names, selection=self.fringe_selection, in_column=True)

        # VIEW & CACHE COLUMN
        gf.new_column()

        # view
        gf.next_row(Checkbutton(f, text="Console view", variable=self.console_view))
        gf.next_row(Checkbutton(f, text="Plot view", variable=self.plot_view))

        # cache values
        self.cache_listbox = gf.add_listbox('Cached values', cache_values, selection=self.cache_selection,
                                            in_column=True, selectmode=MULTIPLE, width=16, height=8)

        # PICKER COLUMN
        gf.new_column()
        self.picker_listbox = gf.add_listbox('Picker', value_tuples=picker_functions,
                                             selection=self.picker_selection, in_column=True)
        gf.next_row(Checkbutton(f, text="Picker sampling", variable=self.picker_sampling_active))
        gf.int_field("Samples", self.picker_sampling, in_column=True, width=6)

        # FILTER COLUMN
        gf.new_column()

        # advanced filtering
        self.filter_listbox = gf.add_listbox('Filter', value_tuples=filter_functions,
                                             selection=self.filter_selection, in_column=True)

        # HEURISTIC COLUMNS
        gf.new_column()
        self.heuristic1_listbox = gf.add_listbox('Heuristic', value_tuples=heuristic_functions,
                                                selection=self.heuristic1_selection, in_column=True,
                                                height=10)

        gf.new_column()
        self.heuristic2_listbox = gf.add_listbox('Heuristic', value_tuples=heuristic_functions,
                                                selection=self.heuristic2_selection, in_column=True,
                                                height=10)

        gf.new_column()
        self.heuristic3_listbox = gf.add_listbox('Heuristic', value_tuples=heuristic_functions,
                                                selection=self.heuristic3_selection, in_column=True,
                                                height=10)

        gf.new_column()
        self.heuristic4_listbox = gf.add_listbox('Heuristic', value_tuples=heuristic_functions,
                                                selection=self.heuristic4_selection, in_column=True,
                                                height=10)

        # DYNAMIC COLUMN
        gf.new_column()

        self.dynamic_cost_listbox = gf.add_listbox('Dynamic cost', value_tuples=dynamic_cost_functions,
                                                selection=self.dynamic_cost_selection, in_column=True,
                                                width=20, height=6)

        # -- NUMERICAL FRAME --

        f = gf.new_frame()

        # max iterations
        gf.new_row()
        gf.int_field("Max iterations", self.max_iterations, width=6)

        # log level
        gf.new_row()
        gf.int_field("Log level", self.log_level, width=3)

        # bounded limit
        gf.new_row()
        gf.int_field("Bounded fringe limit", self.bound_limit, width=6)

        # random span
        gf.new_row()
        gf.int_field("Random span", self.random_span, width=3)

        # fixed length
        gf.new_row()
        gf.next_column(Checkbutton(f, text="Force fixed length", variable=self.force_fixed_length_value))
        gf.next_column(Entry(f, textvariable=self.fixed_length_value, width=5))

        # multistart
        gf.new_row()
        gf.int_field("Multistart", self.multistart, width=3)

        # log result
        gf.new_row()
        gf.next_column(Checkbutton(f, text="Log result", variable=self.log_result))

        # display text?
        gf.new_row()
        gf.next_column(Checkbutton(f, text="No text", variable=self.notext))

        # display text?
        gf.new_row()
        gf.next_column(Checkbutton(f, text="Save best plot", variable=self.save_plot))

        # -- CONFIG FRAME --
        f = gf.new_frame(bd=2, relief=SUNKEN)
        gf.new_row()
        self.execution_level_listbox = gf.add_listbox("Execution level", value_tuples=execution_level_value_tuples,
                                                      selection=self.execution_level_selection, height=3)
        gf.new_row()
        self.rounding_level_listbox = gf.add_listbox("Rounding level", value_tuples=rounding_level_value_tuples,
                                                      selection=self.rounding_level_selection, height=3)

        # -- ACTION FRAME --
        f = gf.new_frame()

        gf.new_row()
        # Solve button: click to run search on the loaded problem
        gf.next_column(Button(f, text="Solve", command=self.solve))

        gf.new_row()
        # Save button: save the current config
        gf.next_column(Button(f, text='Save', command=self.save_config))

        gf.new_row()
        # Quit button: quit the application
        gf.next_column(Button(f, text='Quit', command=self._quit))


    # def test_sum(self):
    #     def callback():
    #         print 'CALLBACK START'
    #         time.sleep(5)
    #         print 'CALLBACK END'
    #
    #     print 'START'
    #     t = threading.Thread(target=callback)
    #     t.start()
    #     print 'END'


    def open(self):
        current_file_dir = path.dirname(self.problem_filepath.get())
        # if the current file directory is valid, use it as a starting point
        # otherwise start at benchmark directory
        # on OS X, the Finder automatically loads the last path
        # on Linux, the initial directory is this script's directory so changing the start dir
        # is needed; however, after one selection, it remembers the last directory so the
        # change of directory stops being effective regarding the file browser's behaviour
        if path.isdir(current_file_dir):
            os.chdir(current_file_dir)
        else:
            os.chdir(BENCHMARK_PATH)
        filename = tkFileDialog.askopenfilename(**OPEN_FILE_OPTIONS)

        # move back to original dir because config file R/W is relative to that
        os.chdir(PROJECT_PATH)

        # if we cancelled (empty string), do not set
        if filename:
            self.problem_filepath.set(filename)
            self.problem_entry.contains_user_text = True
            self.problem_entry.config(fg='black')
            self.problem_entry.icursor(END)

    def solve(self):
        # do not try to solve if dummy welcome text or empty user text (possible if still focusing entry field)
        if not self.problem_entry.contains_user_text or not self.problem_filepath.get():
            print 'Cannot solve: no problem path defined'
            return

        if not self.fringe_listbox.curselection():
            print 'Cannot solve: no fringe selected'
            return

        # save config if it seems correct
        self.save_config()

        # immediately change execution level (some debugging and assertions may occur already during
        # problem creation)
        execution_level = get_listbox_value(self.execution_level_listbox, name_value_tuples=execution_level_value_tuples)
        config.set_execution_level(execution_level)
        rounding_level = get_listbox_value(self.rounding_level_listbox, name_value_tuples=rounding_level_value_tuples)
        config.set_rounding_level(rounding_level)

        # only one fringe selected at a time [0]
        fringe_name = get_listbox_value(self.fringe_listbox, fringe_names)
        # print fringe_name

        # fixed length
        if self.force_fixed_length_value.get():
            fixed_length = self.fixed_length_value.get()
            logging.debug('Fixed length: ' + str(fixed_length))
        else:
            fixed_length = None  # use default length
            logging.debug('Fixed length: None')

        # DEBUG
        print 'SOLVE: {} [floating origin: {}]'.format(self.problem_filepath.get(), self.floating_origin.get())
        precomputation_start_time = time.clock()  # start clock for NFP precomputation time
        expanded_problem = create_expanded_packing_problem_from_file(self.problem_filepath.get(), fixed_length)
        precomputation_stop_time = time.clock()
        precomputation_time = precomputation_stop_time - precomputation_start_time
        print 'PRECOMPUTATION TIME (s): {}'.format(precomputation_time)

        if not self.floating_origin.get():
            if not self.open_dim.get():
                problem = create_fixed_origin_single_bin_size_problem(expanded_problem)
            else:
                problem = create_fixed_origin_open_dimension_problem(expanded_problem)
        else:
            if not self.open_dim.get():
                problem = create_floating_origin_single_bin_size_problem(expanded_problem)
            else:
                problem = create_floating_origin_open_dimension_problem(expanded_problem)

        value_picking_nsp = ValuePickingNestingSearchProblem(problem)

        # MULTISTART
        multistart = self.multistart.get()

        # Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first
        event_handlers = []
        if self.console_view.get():
            event_handlers.append(partial(ConsoleCSEventHandler, min_loglevel=self.log_level.get()))
        if self.plot_view.get():
            event_handlers.append(partial(NestingPlotCSEventHandler, nb_axes=multistart, notext=self.notext.get(),
                                          save_plot=self.save_plot.get()))
        log_result = bool(self.log_result.get())

        # we will set the viewer once the strategy is completely decided

        # HEURISTIC

        # order of heuristics is very important
        selected_heuristics = [get_listbox_value(heuristic_listbox, name_value_tuples=heuristic_functions)
                               for heuristic_listbox in (self.heuristic1_listbox, self.heuristic2_listbox,
                                                         self.heuristic3_listbox, self.heuristic4_listbox)]
        print selected_heuristics

        heuristic = ordered_master_heuristic(*selected_heuristics)

        # PICKER

        picker = get_listbox_value(self.picker_listbox, name_value_tuples=picker_functions)
        if self.picker_sampling_active.get():
            picker = partial(picker, sampling=self.picker_sampling.get())

        # ADVANCED FILTER

        advanced_filter = get_listbox_value(self.filter_listbox, name_value_tuples=filter_functions)
        # print advanced_filter

        # DYNAMIC COST
        dynamic_cost_fun = get_listbox_value(self.dynamic_cost_listbox, name_value_tuples=dynamic_cost_functions)

        # CACHE

        cached_values = map(self.cache_listbox.get, self.cache_listbox.curselection())
        # print cached_values

        # VIEWER
        viewer = EventConstraintSearchViewer(value_picking_nsp.cp, event_handlers, log_result, problem_filepath=self.problem_filepath.get())

        if fringe_name == 'normal':
            fringe_factory = partial(PriorityQueueWithFunction, heuristic, viewer)
        elif fringe_name == 'bounded':
            fringe_factory = partial(BoundedPriorityQueueWithFunction, self.bound_limit.get(), heuristic, viewer)
        elif fringe_name == 'random pop':
            fringe_factory = partial(PriorityQueueWithFunctionRandomizedPop, heuristic, self.random_span.get(), viewer)
        else:
            raise ValueError('Unknown fringe name: ' + fringe_name)

        # MAX ITERATIONS
        max_iterations = self.max_iterations.get()

        # setup strategy
        strategy = Strategy(get_listbox_idx(self.fringe_listbox), get_listbox_idx(self.picker_listbox),
                            get_listbox_idx(self.filter_listbox),
                            [get_listbox_idx(heuristic_listbox) for heuristic_listbox
                             in (self.heuristic1_listbox, self.heuristic2_listbox,
                                 self.heuristic3_listbox, self.heuristic4_listbox)], cached_values, max_iterations,
                            self.bound_limit.get(), self.random_span.get(),
                            self.picker_sampling_active.get(), self.picker_sampling.get(), multistart,
                            self.force_fixed_length_value.get(), fixed_length,
                            get_listbox_idx(self.dynamic_cost_listbox), execution_level, rounding_level)

        # setup viewer strategy
        viewer.strategy = strategy

        # DEBUG START TIMER (search only, setup excluded)
        # start_time = time.clock()
        # print 'START TIME: ' + str(start_time)

        placements_node = value_picking_constraint_graph_search(value_picking_nsp, fringe_factory,
                                                                optimize=bool(self.open_dim.get()), value_picker=picker,
                                                                dynamic_cost_fun=dynamic_cost_fun, viewer=viewer,
                                                                max_iterations=max_iterations,
                                                                cached_values=cached_values,
                                                                cache_filter_fun=advanced_filter,
                                                                multistart=multistart)

        # DEBUG STOP TIMER
        # stop_time = time.clock()
        # print 'STOP TIME: ' + str(stop_time)

        # DEBUG TOTAL SEARCH TIME
        # search_time = stop_time - start_time
        # print 'SEARCH TIME: ' + str(search_time)

        logging.debug(str(viewer.stats))

        # Chocolate, FC, free space
        # {'best cost': 0, 'optimize drop': 0, 'new pops': 1200, 'expanded': 112, 'iterations': 1200, 'max_fringe_size': 593, 'old pops': 0, 'filter2': 313, 'forward check empty': 775}

    def save_config(self):
        # gather all config options in one dictionary
        # we assume a config option has the same name as the corresponding
        # attribute storing the Tkinter variable
        config = {key: getattr(self, key).get() for key in default_config}
        listbox_config = {key: getattr(self, key + '_listbox').curselection() for key in default_listbox_config}

        # do not record the gray default message for the problem path
        if not self.problem_entry.contains_user_text:
            config['problem_filepath'] = ''

        with open(CONFIG_FILEPATH, 'wb') as config_file:
            pickle.dump(config, config_file)
        with open(LISTBOX_CONFIG_FILEPATH, 'wb') as listbox_config_file:
            pickle.dump(listbox_config, listbox_config_file)

        logging.debug('Saved session config in {}'.format(CONFIG_FILEPATH))
        logging.debug('Saved session listbox config in {}'.format(LISTBOX_CONFIG_FILEPATH))

    def load_config(self):
        try:
            with open(CONFIG_FILEPATH, 'rb') as config_file:
                config = pickle.load(config_file)
        except IOError as e:
            config = default_config  # default values

        try:
            with open(LISTBOX_CONFIG_FILEPATH, 'rb') as listbox_config_file:
                listbox_config = pickle.load(listbox_config_file)
        except IOError as e:
            listbox_config = default_listbox_config  # default values


        # for each config attribute, set the same attribute to this object
        # check the type of attribute to use the correct Tkinter variable
        # prefer using our own dictionary of values so that we can detect if the pickled
        # config is incomplete
        for var_name, default_value in default_config.iteritems():
            if type(default_value) == str:
                var_class = StringVar
            elif type(default_value) == int:
                # use int by default, since checkbutton is 0/1 and listbox uses index
                var_class = IntVar
            elif type(default_value) == float:
                var_class = DoubleVar
            else:
                logging.warn('unknown tkinter variable equivalent for type {!s}, cannot load config var'
                             .format(type(default_value)))
            # custom config passed, use it
            value = config[var_name] if var_name in config else default_value
            setattr(self, var_name, var_class(value=value))

        # for list boxes, store the indices to select in an attribute until actually selecting them
        # after listbox creation
        for listbox_name, default_selection in default_listbox_config.iteritems():
            selection = listbox_config[listbox_name] if listbox_name in listbox_config else default_selection
            setattr(self, listbox_name + '_selection', selection)

    def quit_callback(self, event):
        self._quit()

    def _quit(self):
        # save session config for next time, but still quit even if it fails (incorrect var format)
        try:
            self.save_config()
        except Exception as e:
            self.quit()
            raise
        else:
            self.quit()     # stop mainloop
            # self.destroy()  # this is necessary on Windows to prevent
                            # Fatal Python Error: PyEval_RestoreThread: NULL tstate


def focus_in_entry(event):
    widget = event.widget
    if not widget.contains_user_text:
        widget.delete(0, END)
        widget.config(fg='black')
        widget.contains_user_text = True

def focus_out_entry(event):
    widget = event.widget
    # if entry was empty, display default message in gray again
    if not widget.get():
        set_gray_message(widget, widget.default_msg)
        widget.contains_user_text = False
    else:
        widget.contains_user_text = True
        # self.problem_filepath.set(filename)
    # print widget.get()

def set_gray_message(entry, message):
    entry.config(fg='gray')
    # !! any StringVar bound to the entry will receive that value!
    entry.insert(0, message)


def main():
    root = Tk()
    app = Application(master=root)

    # f = Figure(figsize=(5,4), dpi=100)
    # axis = f.add_subplot(111)

    # p = Polygon([(0, 0), (10, 0), (10.54, 10), (0, 10)], holes=[[(1, 1.578), (2, 1), (2, 2), (1, 2)],
    #                                                             [(3, 3), (5, 3.56897), (5, 5)]])
    #
    # p_patch_collections = [axis.add_collection(to_patch_collection(p))]
    # # p_patch_collections[0].set_visible(False)
    # artists_sequence = [p_patch_collections]
    #
    # axis.set_xlim(0, 20)
    # axis.set_ylim(0, 20)
    #
    # # a tk.DrawingArea
    # app.create_canvas(f)

    app.mainloop()
    # if root.winfo_exists():
    root.destroy()


if __name__ == '__main__':
    main()
