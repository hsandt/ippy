from simpleai.search.viewers import Event

__author__ = 'hs'

class BaseConstraintViewer(object):
    """
    Base class for constraint solver view

    :type cp: (ippy.search.constraint.ConstraintProblem | ippy.packmath.nestingconstraintproblem.InputMinimizationNestingProblem)
    :param cp: constraint problem observed, used by some viewers to show problem-specific information
    :param last_event: last event logged, for easier access than getting the last element of events
    :param events: list of all events logged
    :param stats: dictionary of information on the running of the constraint problem search, updated in real-time

    """
    def __init__(self, cp):
        # self.successor_color = '#DD4814'
        # self.fringe_color = '#20a0c0'
        # self.solution_color = '#adeba8'
        # self.font_size = 11
        #
        self.cp = cp
        self.last_event = None
        self.events = []

        self.stats = {
            # backtracking currently uses variable selection only so fringe has just the length of the list
            # returned by variable selectors, or 0/1 if it returns an iterator (no need to remember anything,
            # but in practice you probably store some vertices somewhere)
            'max_fringe_size': 0,
            'assignments': 0,
            'forward_check_empty': 0,  # how many times an assignment was dropped before being logged thanks to FC
            'iterations': 0,
            'backtrackings': 0,
        }

        # self.clear_nodes_data()

    def log_event(self, name, description):
        self.last_event = Event(name=name,
                                description=description)
        self.events.append(self.last_event)

    def event_to_str(self):
        event_description = 'Events:\n'
        for event in self.events:
            event_description += '{}:\t{}\n'.format(event.name, event.description)
        return event_description

    def event(self, name, *params):
        getattr(self, 'handle_' + name)(*params)

    def handle_started(self):
        self.log_event('started', 'Algorithm just started.')

    def handle_new_iteration(self, internal_iteration):
        """
        Record number of total iterations of the viewer, and also log the number of iterations internally
        recorded by the search (in case of multi-search, the numbers are different). If no such number is
        provided, only the total number is shown.

        :type internal_iteration: (int | None)
        :param internal_iteration: number of iterations as recorded by the search (reset for each new search)
        :return:
        """
        # self.current_fringe = fringe
        # self.stats['max_fringe_size'] = max(self.stats['max_fringe_size'], len(fringe))
        self.stats['iterations'] += 1

        # description = 'New iteration with %i elements in the fringe:\n%s'
        # description = description % (len(fringe), str(fringe))
        if internal_iteration is None:
            self.log_event('new_iteration', 'total {}'.format(self.stats['iterations']))
        else:
            self.log_event('new_iteration', '#{} (total {})'.format(internal_iteration, self.stats['iterations']))

    def handle_assigned(self, variable, value, new_assignment):
        self.stats['assignments'] += 1
        self.log_event('assigned', '{0} <- {1} ({2} vars assigned)'.format(variable, value, len(new_assignment)))

    def handle_forward_check_empty(self, variable, value, empty_domain_tail):
        self.stats['forward_check_empty'] += 1
        self.log_event('forward_check_empty', 'Forward check dropped {0} <- {1} (first empty domain tail found {2})'.format(variable, value, empty_domain_tail))

    def handle_backtracked(self, variable, new_assignment):
        self.stats['backtrackings'] += 1
        self.log_event('backtracked', 'All values failed for variable {}, backtracking ({} remaining vars assigned)'.format(variable, len(new_assignment)))

    def handle_finished(self, assignment, reason):
        """

        :param assignment:
        :param reason: reason for stopping the search among 'solution found', 'no solution found',
            'max iterations', 'cost span'
        :return:

        """
        description = 'Finished algorithm for reason: "{}"'.format(reason)
        if assignment is not None and assignment:
            description += '\nLast assignment: {}'.format(assignment)

        self.log_event('finished', description)


class ConsoleConstraintViewer(BaseConstraintViewer):
    """Constraint solver view that outputs each step to the console"""
    def __init__(self, cp):
        super(ConsoleConstraintViewer, self).__init__(cp)

    def event(self, name, *params):
        super(ConsoleConstraintViewer, self).event(name, *params)

        self.output('EVENT {}: {}'.format(self.last_event.name, self.last_event.description))

    def output(self, text):
        print('Viewer: ' + text)
