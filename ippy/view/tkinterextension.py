from Tkconstants import TOP, BOTH, END, MULTIPLE, EXTENDED, N
from Tkinter import Frame, Label, Entry, Listbox
import copy

__author__ = 'hs'

# tkinter widget construction helpers
# we do not use context managers since we only open, not close,
# but a with column as c: c.add_row() syntax is also possible

class GridFrameManager(object):
    def __init__(self, master, **defaults):
        self.master = master
        self.defaults = defaults
        self.row = IndexCounter()
        # self.last_row = 0
        self.column = IndexCounter()
        # self.last_column = 0
        self.last_frame = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type is None:
            # no exception raised, continue
            return True

        # do something for exceptions that may be raised when building the grid here
        # if issubclass(exc_type, Exception):
        #     return False
        else:
            # let other exceptions pass through
            return False

    def new_frame(self, side=TOP, **args):
        self.last_frame = Frame(self.master, **args)
        self.last_frame.pack(side=side, fill=BOTH)
        # reset row and column indices for this frame
        self.row.reset()
        self.column.reset()
        return self.last_frame

    # create a row below the rest; do not mix with new_column!
    def new_row(self):
        self.row.next()
        self.column.reset()

    # create a column after the rest; do not mix with new_row!
    def new_column(self):
        self.column.next()
        self.row.reset()

    def next_row(self, widget, **args):
        # override default parameters with args
        # we can either copy self.defaults and update it with args,
        # or update args with only the entries of self.defaults that are not present in args
        d = copy.copy(self.defaults)
        d.update(args)
        # args.update({k: v for k, v in self.defaults.iteritems() if k not in args})
        widget.grid(row=self.row.next(), column=self.column.last, **d)
        # if columnspan argument is passed, also prepare jump to the right column index next time
        if 'rowspan' in args:
            self.row.next_increment = args['rowspan']

    def next_column(self, widget, **args):
        d = copy.copy(self.defaults)
        d.update(args)
        widget.grid(row=self.row.last, column=self.column.next(), **d)
        if 'columnspan' in args:
            self.column.next_increment = args['columnspan']

    # def update_max_row(self, index):
    #     if index > self.row.last:
    #         self.row.last = index
    #
    # def update_max_column(self, index):
    #     if index > self.column.last:
    #         self.column.last = index

    def int_field(self, text, intvar, in_column=False, **entryargs):
        next_fun = self.next_row if in_column else self.next_column  # in a column, we add rows...

        label = Label(self.last_frame, text=text)
        entry = Entry(self.last_frame, textvariable=intvar, **entryargs)
        next_fun(label)
        next_fun(entry)

    def listbox(self, values=None, value_tuples=None, selection=None, **user_cnf):
        """
        :param values: list of values for the listbox
        :param value_tuples: list of tuples value - Python object; if set, values is ignored
        :param selection: default selection (list of indices)

        """
        cnf = dict(width=12, height=4)
        cnf.update(user_cnf)
        listbox = Listbox(self.last_frame, exportselection=0, **cnf)
        if value_tuples is not None:
            values = zip(*value_tuples)[0]
        for name in values:
            listbox.insert(END, name)
        if selection:
            # a non empty selection has been passed
            for index in selection:
                listbox.selection_set(index)
        else:
            # by default, in SINGLE/BROWSE selection mode, select the 1st option by default
            if 'selectmode' not in cnf or cnf['selectmode'] not in (MULTIPLE, EXTENDED):
                listbox.selection_set(0)
        return listbox

    def add_listbox(self, name, values=None, value_tuples=None, selection=None, in_column=False, grid_params=None, **user_cnf):
        """

        :param name: name of the listbox
        :param values: list of values for the listbox
        :param value_tuples: list of tuples value - Python object; if set, values is ignored
        :param selection: default selection (list of indices)
        :param in_column: if set to True, we will add a rows instead of columns
        :param grid_params: params for next_row or next_column (dict params)

        :rtype: Listbox

        """
        if grid_params is None:
            grid_params = {}

        next_fun = self.next_row if in_column else self.next_column  # in a column, we add rows...
        # REFACTOR: pass listboxconfig dictionary and get value for key=name here?
        listbox = self.listbox(values, value_tuples, selection, **user_cnf)
        next_fun(Label(self.last_frame, text=name))
        next_fun(listbox, sticky=N, **grid_params)
        return listbox


class IndexCounter(object):
    """
    Counter for grid rows and columns with Tkinter
    Use an instance of this class to increment and reuse last row/column number

    This class provides a slightly easier interface than using Python itertools.count()
    (infinite iterator) because you do not need to store the last returned value

    - use .next() to step to next value and get it
    - use .last to get last value without modifying it
    - use reset() to reset value to -1 (next next() will return 0)
    - set .next_increment to prepare a jump of some values next time (useful after rowspan/columnspan)

    Attributes:
        last            --  last index returned (initial value: -1)
        next_increment  --  increment to use when next() will be called next time (used once)

    """
    def __init__(self):
        self.reset()
        self.next_increment = 1

    def next(self):
        self.last += self.next_increment
        self.next_increment = 1  # reset to default increment
        return self.last

    def reset(self):
        self.last = -1  # start at -1 so that the first is 0
        self.next_increment = 1  # reset to default increment


def get_listbox_idx(listbox):
    """
    Return listbox index, meaningful for SINGLE selection

    :param listbox:
    :rtype: int

    """
    return listbox.curselection()[0]


def get_listbox_value(listbox, names=None, name_value_tuples=None):
    """
    Return the value corresponding to the current selection in a listbox,
    following the sequence of string names if provided,
    else the sequence of string name - V value in listbox_tuples

    Only makes sense for a SINGLE selection listbox

    :type listbox: Listbox
    :type names: list[str]
    :type name_value_tuples: list[str, V]
    :rtype: V

    """
    if names is not None:
        return names[listbox.curselection()[0]]
    else:
        return name_value_tuples[listbox.curselection()[0]][1]
