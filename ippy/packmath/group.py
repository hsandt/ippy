__author__ = 'hs'


class ComponentPiece(object):
    """
    Piece with component role in the Composite pattern.

    """
    def __init__(self):
        pass

    def __repr__(self):
        return "<ComponentPiece: ()>".format()


class LeafPiece(ComponentPiece):
    """
    Piece with leaf role in the Composite pattern.
    It can only hold one polygon.

    Attributes
        piece_id: string
        instance_idx: int
        piece_geometry: BaseGeometry

    """
    def __init__(self, piece_id, instance_idx, piece_geometry):
        self.piece_id = piece_id
        self.instance_idx = instance_idx
        self.piece_geometry = piece_geometry  # REFACTOR: do not refer to geom here?

    def __repr__(self):
        return "<LeafPiece: ({0}, {1})>".format(self.piece_id, self.instance_idx)


class CompositePiece(ComponentPiece):
    """
    Piece with composite role in the Composite pattern.
    Aka Group of pieces.

    Attributes
        children
            list<(ComponentPiece, Placement)>

    """
    def __init__(self, *component_piece_placement_tuples):
        self.children = component_piece_placement_tuples

    def __len__(self):
        return len(self.children)

    def __repr__(self):
        return "<CompositePiece: ({0})>".format(self.children)
