from ippy.packmath.phifunction import phi, phi2

__author__ = 'huulong'

def intersects_area(polygon1, polygon2, threshold=1e-7):
    """
    Return True if the intersection depth between polygon1 and polygon2,
    at their current position, is above a given threshold

    The intersection depth is estimated by dividing the intersection area
    by twice the perimeter of the intersection. (See Robustness: method b)

    """
    intersection = polygon1 & polygon2
    # if the intersection is empty or a Point or a LineString, polygons are touching, no deep intersection
    if intersection.is_empty or intersection.type in ('Point', 'MultiPoint', 'LineString'):
        return False

    # TODO: Polygon with holes, Multi- objects

    # perimeter is given by length property as long as there are no holes
    # if there are holes, we must refine method b (see sum-up notes)
    return 2 * intersection.area / intersection.length > threshold


def intersects_phi(polygon1, polygon2, threshold=1e-7):
    """
    Return True if the intersection depth between polygon1 and polygon2,
    at their current position, is above a given threshold

    The intersection depth is estimated by the opposite of the phi-function between the two
    polygons. (See Robustness: method c)

    """
    return phi(polygon1, polygon2) < -threshold
