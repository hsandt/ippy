from abc import ABCMeta, abstractmethod
from copy import deepcopy, copy
from functools import partial
import heapq
from itertools import izip
import logging
from shapely.geos import TopologicalError
from ippy.config.config import config
from ippy.packmath.constraintsearchviewer import EmptyConstraintSearchViewer
from ippy.packmath.dynamiclength import get_min_length_for_non_empty_domain, get_current_dynamic_cost
from ippy.search.constraint import ConstraintOptimizationProblem, check_if_conflicts, ConstraintSatisfactionProblem
from ippy.utils.algorithm import IterationCounter, GeometryException
from ippy.utils.datastructure import Stack, Queue, PriorityQueueWithFunction, BoundedPriorityQueue, \
    BoundedPriorityQueueWithFunction
from ippy.utils.decorator import deprecated
from ippy.utils.hashabledict import hashdict

__author__ = 'hs'


class ValuePickingConstraintSearchProblem(object):
    """
    Abstract base class for a CSP defined as a Graph Search Problem,
    that pick a finite number of values among the constraint problem's domain
    at each assignment state.

    The problem is associated to a Constraint problem, and always
    start from an empty assignment.
    At each step, we assign a new variable. Each action in this graph search
    is a couple (variable, value), we directly pass those values in methods.

    Attributes:
        # should be a generic class, we only give InputMinimizationNestingProblem type for better auto-completion
        :type cp: (ippy.search.constraint.ConstraintProblem | ippy.packmath.nestingconstraintproblem.InputMinimizationNestingProblem)
        :param cp: Constraint problem associated to this search problem

    """
    __metaclass__ = ABCMeta

    def __init__(self, cp):
        self.cp = cp

    @abstractmethod
    def create_node(self, assignment=None, parent=None, variable_assigned=None, problem=None, cached_values=None,
                    dynamic=None):
        """
        Create an AssignmentSearchNode specific to the problem subclass

        :rtype: AssignmentSearchNode

        """

    # REFACTOR: move to ConstraintProblem?
    def is_complete(self, assignment):
        """
        Return True if the assignment is complete.
        Does not check for constraint satisfaction.

        :type assignment: dict

        """
        return len(assignment) == len(self.cp.variables)

    def result(self, assignment, next_variable, next_value):
        """
        Return the new assignment resulting from the last assignment next_variable <- next_value
        applied from an original assignment, as a copy.

        :param assignment: original assignment
        :param next_variable: variable assigned
        :param next_value: value assigned

        :rtype: hashdict[T, U]

        """
        return hashdict(assignment + {next_variable: next_value})

    def get_successors(self, node, value_picker, dynamic):
        """
        Return the list of AssignmentSearchNode that are derived from an assignment state.
        The second value is not required in a constraint search, but useful for debugging.

        Do not sort anything here. The fringe data structure will.

        :type node: AssignmentSearchNode
        :type value_picker: (AssignmentSearchNode, ConstraintSearchProblem) -> list[U]
        :param value_picker: for a given variable at the current node, pick and return a list of values to assign
            to that variable for this problem
        :type dynamic: bool
        :param dynamic: should we use a variable picker for dynamic problems?
        :rtype: list[AssignmentSearchNode]

        """
        successors = []
        # pick variables to consider (generally only consists in ignoring redundant variables)
        next_variables = self.pick_variables(node)
        for next_variable in next_variables:
            # REFACTOR: create a Solver class, initialized with value picker, filtering method, etc. and call them
            next_values = value_picker(next_variable, node, self.cp, dynamic)
            for next_value in next_values:
                # print(next_value)
                # do not mute hashdict assignment (referred in node and as dict key)
                # DEBUG: verify if value is in the current domain, may detect errors in FC

                # NEXT TODO FIXME: regular picker seems to pick 100, 1e-15 <-- okay with threshold
                # BUT 100 not inside the domain (stops at around 80) so check cached NFPs and so on
                if not node.domains[next_variable].contains(next_value):
                    # TERRIBLE
                    raise GeometryException('Next var {0}: picked value {2} is not in domain {1}'
                                            .format(next_variable, node.domains[next_variable], next_value),
                                            node.domains[next_variable][next_value.rotation])
                # NEXT TODO NEXT FIXME
                if not node.dynamic_domains[next_variable].contains(next_value):
                    raise GeometryException('Next var {0}: picked value {2} is not in dynamic domain {1}'
                                            .format(next_variable, node.dynamic_domains[next_variable], next_value),
                                            node.dynamic_domains[next_variable][next_value.rotation])
                next_assignment = self.result(node.assignment, next_variable, next_value)
                successor_node = node.problem.create_node(next_assignment, node, next_variable, dynamic=dynamic)
                successors.append(successor_node)

        return successors

    def pick_variables(self, node):
        """
        Return a list of variables to assign, from a given assignment state.
        For a complete search on variables, all variables should eventually be picked.
        If variables are equivalent (same instances of one category), it is recommended
        to pick one instance of each.

        Override this method with a custom picker

        :type node: AssignmentSearchNode
        :rtype: list

        """
        # by default, pick all unassigned variables
        return self.cp.get_unassigned_variables(node.assignment)

    # @abstractmethod
    # def pick_values(self, assignment, variable):
    #     """
    #     Return a list of values to try for variable, given a current assignment.
    #
    #     :type assignment: dict
    #     :param variable:
    #     :rtype: list
    #     """

    # TODO: add heuristic here (or a way to delegate heuristic calculation)

    def assignment_representation(self, assignment):
        """
        Return a string representation of an assignment.

        :param assignment:
        :return:
        """
        return str(assignment)

    def __repr__(self):
        return "<ValuePickingConstraintSearchProblem: ({0})>".format(self.cp)


class AssignmentSearchNode(object):
    """
    Search node for a constraint search problem that pick values for assignments.

    Attributes
        :type assignment: hashdict[T, U]
        :param assignment: assignment state located at this node
        :type parent: AssignmentSearchNode
        :param parent: predecessor node (None if initial node) [optional since only the result matters]
        :type variable_assigned: T
        :param variable_assigned: the last variable assigned, that led to this node (None if initial node)
        :type problem: ValuePickingConstraintSearchProblem
        :param problem: problem this node belongs to
        # :type domains: dict[T, ippy.search.constraint.Domain]
        # :param domains: dictionary of upper bound domains, indexed by variable
        :type dynamic_domains: dict[ippy.packmath.problem.PieceKey, ippy.packmath.nestingconstraintproblem.NestingDomain]
        :param dynamic_domains: domains based on the dynamic length, indexed by piece keys.
        :type min_cost: float
        :param min_cost: minimum cost that could be reached when completing the assignment (any lower bound)
        :type dynamic_cost: float
        :param dynamic_cost: current length used to define domains, IFP, wasted regions
            Smaller than the usual domains, that represent the max domains
        :type cache: ippy.packmath.constraintsearch.Cache
        :param cache: cache containing useful information on this node's assignment, to avoid recomputing them
        :type cached_values: list[str]
        :param cached_values: list of values' names to cache
        :type dynamic: bool
        :param dynamic: should we use dynamic domains based on dynamic cost?

    """
    def __init__(self, assignment=None, parent=None, variable_assigned=None, problem=None, cached_values=None,
                 dynamic=None):
        self.assignment = assignment if assignment is not None else hashdict()
        self.parent = parent
        self.variable_assigned = variable_assigned  # should be None for initial node

        # derived attribute: get value assigned if not the initial node
        self.value_assigned = assignment[variable_assigned] if variable_assigned is not None else None

        if problem is not None:
            self.problem = problem
            assert dynamic is not None, 'Initial node but dynamic attribute was not set'
            self.dynamic = dynamic
        elif parent is not None and parent.problem is not None:
            self.problem = parent.problem
            self.dynamic = parent.dynamic  # keep consistency of mode during the search
        else:
            raise ValueError('No problem passed and no parent or parent problem is None')

        if parent is None:
            # initial node, start with initial problem domains
            # self.domains = deepcopy(problem.cp.domains)

            # initialize min/dynamic cost, required for complete cache initialization
            # REFACTOR? a complete lazy cache system that even computes the initial value and
            # then retrace all the updates up to the current value if needed
            self.min_cost = self.dynamic_cost = self.get_initial_dynamic_cost()
            # OPTIMIZE: if dynamic set to False do not compute nor use dynamic domains at all?
            # IMPROVE: detect string 'all' and expand to all possible cached values names?
            if cached_values is None:
                cached_values = []
            self.cache = problem.cp.get_initial_cache(self, cached_values)
        else:
            # copy previous domains (we can filter them in-place later)
            # self.domains = deepcopy(parent.domains)
            self.cache = parent.cache.get_delayed_copy(next_node=self)

        # REFACTOR: delegate dynamic domains creation to subclass (required for caching dynamic domains
        # as used in initialization above!!)
        # (prefer delegating through get_ or create_ method than in the subclass constructor)
        # self.dynamic_domains = None

    # FIXME / IMPROVE: 2 nodes which swap the positions of two instances of the same piece
    # in the Nesting Problem are not equal; use a subclass for the Nesting problem that
    # compares equal when only the instance index differs
    def __eq__(self, other):
        return isinstance(other, AssignmentSearchNode) and self.assignment == other.assignment

    def __ne__(self, other):
        return not self.__eq__(other)

    def state_representation(self):
        return self.problem.assignment_representation(self.assignment)

    def __repr__(self):
        if self.parent is not None:
            return '<AssignmentSearchNode: ({0}, {1})>'.format(self.assignment, self.parent.assignment)
        else:
            return '<AssignmentSearchNode: ({0}) (root)>'.format(self.assignment)

    def __str__(self):
        if self.parent is not None:
            return 'Node {} <- {}'.format(self.variable_assigned, self.value_assigned)
        else:
            return 'Root node'.format(self.assignment)

    # WARNING: MUST cache domains and dynamics! should be made automatic!

    @property
    def domains(self):
        """
        Shortcut to get domains from cache, and ensure backward compatibility
        Be careful, it will cause an automatic cache update
        :rtype: ippy.packmath.nestingconstraintproblem.NestingDomain

        """
        return self.cache.get('domains')

    @property
    def dynamic_domains(self):
        """
        Shortcut to get dynamic domains from cache, and ensure backward compatibility
        Be careful, it will cause an automatic cache update
        :rtype: ippy.packmath.nestingconstraintproblem.NestingDomain

        """
        return self.cache.get('dynamic_domains')

    @abstractmethod
    def get_initial_dynamic_cost(self):
        """
        Return the initial dynamic cost

        """

    @abstractmethod
    def get_min_cost_for_non_empty_domain(self, variable):
        """
        Return the minimum cost limit so that variable has a non-empty domain.

        """

    @deprecated('cache compute_dynamic_domains')
    def update_all_dynamic_domains(self):
        """
        Update the dynamic domains from the max domains and dynamic cost.
         Useful for COP only.

        """
        for variable in self.unassigned_variables:
            self.update_dynamic_domain(variable)

    @abstractmethod
    def update_dynamic_domain(self, variable):
        """
        Update the dynamic domain of variable, from the max domains and dynamic cost.
         Useful for COP only.

        :param variable:
        """

    @property
    def is_complete(self):
        """
        Return True if the node has a complete assignment
        :rtype: bool

        """
        return self.problem.is_complete(self.assignment)

    @property
    def cost(self):
        """
        Return the cost of this node, None if not defined
        :rtype: float

        """
        if hasattr(self.problem.cp, 'get_cost'):
            return self.problem.cp.get_cost(self.assignment)
        else:
            return None

    @property
    def unassigned_variables(self):
        """
        Return the list of unassigned variables for this assignment

        :rtype: list[T]

        """
        return self.problem.cp.get_unassigned_variables(self.assignment)


class Cache(object):
    """
    Abstract base class for search cache

    Contains both normally cached and incrementally cached values

    Subclasses must implement an appropriate __deepcopy__

    Attributes
        :type cache_dict: dict[str, X]
        :param cache_dict: internal dictionary of cached values, with a key only for cached values

        :type cp: ippy.search.constraint.ConstraintProblem
        :param cp: problem for which to build the cache; only pass it for initial node,
            otherwise pass manually other attributes

        :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
        :param node: node to which this cache is associated (required to update)

        :type delay_per_value: dict[str, int]
        :param delay_per_value: dictionary of number of assignment steps this cache is behind, for each value
            if 0 for a value, the cache is up-to-date for this value
            For non-incrementally cached values, a value of 1 or more means 'dirty'

        :type non_incremental_values: list[str]
        :param non_incremental_values: list of value names for values that must be updated at once
            There is typically one per Cache subclass, so each subclass defines the list of all
            such values, whether they are actually cached or not

        :type dynamic_values: list[str]
        :param dynamic_values: list of value names for values that are considered dynamic
            Such values are dirtied after a dynamic cost change during the search
            Should at least contain dynamic domains

    """
    __metaclass__ = ABCMeta

    def __init__(self, node, cache_dict=None, cached_values=None, delay_per_value=None, non_incremental_values=None,
                 dynamic_values=None):
        """
        :type cached_values: list[str]
        :param cached_values: list of attribute names that must be cached (other values are not updated)
            Used only when cached_dict is None

        """
        # IMPROVE: add a dict of bool values to tell if a cached value has been updated since last cloning or if
        # it may be outdated?
        self.cp = node.problem.cp  # need problem reference to initialize / update / compute cache value
        self.node = node
        self.delay_per_value = delay_per_value if delay_per_value is not None else dict.fromkeys(cached_values, 0)  # start up-to-date
        self.non_incremental_values = non_incremental_values if non_incremental_values is not None else []
        self.dynamic_values = dynamic_values if dynamic_values is not None else []

        if cache_dict is None:
            # this *must* be the initial node
            assert not node.assignment

            # initialize cache dict with cp
            self.cache_dict = {}
            for value_name in cached_values:
                self.init_value(value_name)
        else:
            # this must be called from get_delayed_copy

            # we assume the developer only computes and provides values only when the value must be cached
            # the passed dictionary must have been deepcopied beforehand to avoid conflicts
            self.cache_dict = cache_dict

    # for now, prefer using get than __get__ so that it is clear that something more happens
    def get(self, value_name):
        """
        Return the cached value called value_name if any, otherwise recompute it.
        If there is no cached value for value_name you must provide a node. Otherwise, it is not used.

        If no direct computation has been implemented, raise a NotImplementedError.

        :type value_name: str
        :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
        :return: cached or computed value

        """
        if value_name in self.cache_dict:
            # update cached value if needed, and return it
            # note: for composite cache such as NFP or IFP, *all* subvalues will be updated
            # we consider that a strategy that uses nfp with previous layout for a piece
            # does that afor all remaining pieces so the computation is worth it
            if self.delay_per_value[value_name] > 0:
                self.update(value_name)  # delay is reseted inside
            return self.cache_dict[value_name]
        else:
            # compute value from scratch, if possible
            logging.warning('Compute %s from scratch, prefer caching', value_name)
            return self.compute(value_name)

    # REFACTOR: get_init_value() and assign to dict value manually?
    def init_value(self, value_name):
        """
        Initialize cached value called value_name

        For non-incremental cached values, directly compute it for the initial node,
        that must be this node (compute_ method must be implemented for such values)

        :type value_name: str

        """
        if value_name in self.non_incremental_values:
            self.cache_dict[value_name] = self.compute(value_name)
        else:
            self.cache_dict[value_name] = getattr(self, 'get_initial_' + value_name)()

    def compute(self, value_name):
        """
        Compute and return the value called value_name, for the current node
        If direct computation is not implemented, raise NotImplementedError

        :type value_name: str

        """
        if not hasattr(self, 'compute_' + value_name):
            raise NotImplementedError('Cannot compute directly {}, please use cache'.format(value_name))
        return getattr(self, 'compute_' + value_name)()

    # only init and update methods should set cache so no need to check if value is cached
    @deprecated('self.cache_dict[] = value')
    def set(self, value_name, value):
        """
        Directly set the cache value called value_name
        Useful to update the cache

        :type value_name: str
        :type value: X

        """
        if value_name not in self.cache_dict:
            return KeyError('Value {} must not be cached.'.format(value_name))

        self.cache_dict[value_name] = value

    def copy_cache_dict(self):
        """
        Return a copy of the cache dict, at the appropriate depth so that
        computation is cheap but there is no risk to modify shared information between caches

        Override this method if you do not want the dictionary to be completely copied

        :rtype: Cache

        """
        return deepcopy(self.cache_dict)

    # def increment_delay(self):
    #     """
    #     Increment the delay. Call this after copying the cache to a successor node,
    #     as copy preserves the delay.
    #
    #     """
    #     self.delay += 1

    def get_delayed_copy(self, next_node):
        """
        Return a copy of the cache, delayed by one assignment step

        :type next_node: AssignmentSearchNode
        :rtype: Cache

        """
        cache_dict_copy = self.copy_cache_dict()
        incremented_delay_per_value = {value_name: delay + 1 for value_name, delay in self.delay_per_value.iteritems()}
        # assume all Cache subclasses have the same constructor
        delayed_cache_copy = self.__class__(next_node, cache_dict_copy, delay_per_value=incremented_delay_per_value)
        return delayed_cache_copy

    def increment_delay(self, value_name):
        """
        Increment the delay of a cached value

        Often used to set dirty a non-incremental value (in this case, setting the delay to any
        positive value is enough)

        :type value_name: str
        :param value_name: name of the cached value

        """
        self.delay_per_value[value_name] += 1

    def increment_delay_dynamic_values(self):
        """
        Increment delay for all dynamic values
        Those must be declared as such in the super of the sub class

        """
        for value_name in self.dynamic_values:
            self.increment_delay(value_name)

    def update(self, value_name):
        """
        Update the cache for a value

        For incremental values, as many times as the delay requires,
        by retracing the missed updates from the level the cache stopped being updated

        For non-incremental values, update it once following the update function

        We prefer delegating the complete update to the subclass rather than getting the new value
        and then assigning it here, because when deleting or modifying values e.g. from a dict
        it means you have to copy the value, return it, reassign it rather than directly modify it

        """
        # for now, we can only update the value if it is cached
        if value_name not in self.cache_dict:
            raise ValueError('Value {} is not cached (dict keys {})'.format(value_name,
                                                                     self.cache_dict.keys()))

        assert self.delay_per_value[value_name] > 0, 'Updating up-to-date value (not critical, may use warning instead)'

        # non-incremental values: one update
        if value_name in self.non_incremental_values:
            # delay value does not matter; we assume it is 1 or more here but we reset it in the end anyway
            # compute directly value for this node (independent from path) AND assign it
            self.cache_dict[value_name] = self.compute(value_name)

        else:
            # get the chain of all next nodes until this one
            # Ex: if the delay is 3, then we get the node associated to this cache, its parent and its grandparent
            node_chain = []
            current_node = self.node
            for previous_level in xrange(self.delay_per_value[value_name]):
                node_chain.append(current_node)
                current_node = current_node.parent  # not used the last time, but exists anyway (not None)

            # update the cache on transition next_node.parent -> next_node, starting with the oldest node
            for next_node in node_chain[::-1]:
                self._update_on_assignment(next_node, value_name)

        # reset delay: cache is up-to-date
        self.delay_per_value[value_name] = 0

    def _update_on_assignment(self, next_node, value_name):
        """
        Update the cache of a value on transition next_node.parent -> next_node
        The node is required because next_node.variable/value_assigned matter

        Each Cache subclass must implement its own update, update_[value_name]_on_assignment
        based on what happens at that moment.
        It does not need to implement an incremental method. Direct calculation, as used
        in the compute_ methods, are acceptable if the computation is cheap.

        Note that we do not update the cache of all parent nodes on the way,
        only this cache.

        :type next_node: AssignmentSearchNode
        :type value_name: str
        :param value_name: name of the cached value to update

        """
        assert next_node.parent is not None

        # for now, we can only update the value if it is cached
        # later, maybe, there will be no difference between cached and computed values:
        # if a value is required, it will be updated from the last update no matter what
        # (the only advantage of keeping purely computed values is in the case a direct computation
        # is faster than an incremental computation, eg unary_union faster than incremental union;
        # but the advantage of incremental computation is that it is deterministic, independent of
        # when the cache value is retrieved)
        if value_name not in self.cache_dict:
            raise ValueError('Value {} is not cached (dict keys {})'.format(value_name, self.cache_dict.keys()))

        update_method_name = 'update_' + value_name + '_on_assignment'
        if not hasattr(self, update_method_name):
            raise NotImplementedError('Update method for value {} ({}) is not implemented for class {}'
                                      .format(value_name, update_method_name, self.__class__))

        getattr(self, update_method_name)(next_node)


def value_picking_constraint_graph_search(problem, fringe_factory, optimize, value_picker,
                                          dynamic_cost_fun=get_current_dynamic_cost, viewer=None, max_iterations=300,
                                          cached_values=None, cache_filter_fun=None, multistart=1):
    """
    Apply backtracking search to a ValuePickingConstraintSatisfactionProblem and
    return a complete assignment as a node, or None (failure) if no solutions were found.
    Returning the goal node is not important in a constraint search, but may be useful for debug.

    The function uses a fringe that can be a priority queue or any kind of container
    that can give a priority for the search strategy.

    In case two different branches discover the same node (possible with very regular picking),
    the search uses a closed set to keep track of expanded nodes.
    Since we are only interested in the result, there is no need to keep multiple nodes
    with the same state even if they have different parents (but for intensive debugging).

    :type optimize: bool
    :param optimize: are we looking for the model node with the lowest cost?
    :type problem: ValuePickingConstraintSearchProblem
    :param fringe_factory: parameterless factory (use partial if needed) that creates a container that supports:
        - append(), pop() for node manipulation
        - __iter__() and __len__() for boolean tests (__contains__() when code is optimized)
        - sorted() for debug (or use a comparator and Python built-in sorted())
    :type value picker: (dict[T, U], T) -> list[U]
    :param value picker: Value picking function (assigment, variable) -> list[values]
    :type dynamic_cost_fun: (ippy.packmath.constraintsearch.AssignmentSearchNode) -> float
    :param dynamic_cost_fun: function that return the next dynamic cost on each iteration  (COP)
    :type viewer: (ippy.packmath.constraintsearchviewer.BaseConstraintSearchViewer | None)
    :param viewer:
    :param max_iterations: max number of iterations before the search stops
    :type cached_values: list[str]
    :param cached_values: list of values to cache
    :type cache_filter_fun: (AssignmentSearchNode, BaseConstraintViewer) -> float
    :param cache_filter_fun: advanced filter function that uses cached information that returns the extra cost required
    :type multistart: int
    :param multistart: number of separate frines used for the search (1 for single-start search)

    :rtype: AssignmentSearchNode
    :return: a node containing a model (complete and valid assignment)

    """
    # verification that optimize is active when the problem is a COP (DEBUG until CSP and COP are merged?)
    # assert optimize and problem.cp.open_dim or not optimize and not problem.cp.open_dim,\
    #     'Open Dim and optimize/dynamic parameter must match'
    # Caution: Open dim is specific to packing problem! COP is not
    assert optimize and isinstance(problem.cp, ConstraintOptimizationProblem) or \
           not optimize and isinstance(problem.cp, ConstraintSatisfactionProblem),\
        'CSP/COP and optimize/dynamic parameter must match'

    if viewer is None:
        viewer = EmptyConstraintSearchViewer()

    viewer.event('started')

    # the closed set in actually a list because dict are non-hashable
    # OPTIMIZE: create a hashable dict http://stackoverflow.com/questions/1151658/python-hashable-dicts
    # and use that in a set; do not forget to make all classes used hashable too (Placement)
    # complexity of adding element and check presence of a key: set -> O(1), list -> O(n)
    # closed_set = list()
    closed_set = set()  # ok with hashdict

    # build fringes from factory
    fringes = [fringe_factory() for i in xrange(multistart)]

    # build initial node (empty assignment by default)
    initial_node = problem.create_node(problem=problem, cached_values=cached_values, dynamic=optimize)
    fringes[0].push(initial_node)

    current_fringe_idx = -1  # to start at 0 after 1st increment, then pop node from each fringe turn by turn
    empty_fringe_indices = set(xrange(1, multistart))  # set of indices of empty fringes; used to know which ones are free and can be (re)used

    iteration_counter = IterationCounter(max_iterations)

    last_node = None
    best_node = None
    best_cost = float('+inf')

    finish_reason = 'fringe empty'  # if no other reasons, leave loop because all fringes are empty

    try:

        while any(fringes):

            current_fringe_idx = (current_fringe_idx + 1) % multistart  # also len(fringes)

            # inspect fringe for current index; if empty, pass
            fringe = fringes[current_fringe_idx]
            if not fringe:  # equivalent to current_fringe_idx in empty_fringe_indices
                continue

            # if there is at least one empty fringe, split the current fringe with the empty fringes
            # IMPROVE: if the fringe is a PQ, only split the fringe with the best nodes (eg the lowest of the
            # lowest values) to focus the search on good positions; or at least split regularly each fringe,
            # but not just the first that comes
            if empty_fringe_indices:
                nb_target_fringes = len(empty_fringe_indices) + 1  # +1 for the part that will remain in current fringe (the 'head')
                size = 10  # arbitrary size, IMPROVE: maybe better to cut groups for each assigned variable?
                fringe_parts = fringe.split(nb_target_fringes, size)  # may get fewer parts than that
                # current fringe keeps the head (already heapified), other must be re-heapified
                # all start from the current count
                fringe.fill_with_new_content(fringe_parts[0], fringe.count, auto_heapify=False)
                # iterate on copy of set (as a list) to avoid modifying set while iterating
                for i, fringe_index in enumerate(list(empty_fringe_indices), 1):  # undetermined order for a set
                    # in case there were too few elements to get enough splits, stop if nb of parts actually
                    # obtained is reached (-1 since we have already filled the original fringe but enumerate
                    # starts at 1 so just the len)
                    if i == len(fringe_parts):
                        break
                    fringes[fringe_index].fill_with_new_content(fringe_parts[i], fringe.count)
                    empty_fringe_indices.remove(fringe_index)

            # from there, work on the original fringe, we will try the others later (undetermined order)

            # if max iterations exceeded, stop
            # [could be placed after in closed_set test if we want the counter to track the size of the closed set]
            # [could be placed after filtering if filtering is not too long]
            if iteration_counter.increment_and_check_exceed():
                finish_reason = 'max iterations'
                break

            viewer.event('new_iteration', iteration_counter.iteration, len(fringe))

            # pop next node according to fringe data structure priority
            # we have not studied the node until now so domains and cache are still identical to the parent node's
            last_node = fringe.pop()
            assignment = last_node.assignment

            # fringe may be empty now but wait to see if new successors will be replace it before marking it as empty

            # if node already explored and expanded, pass it
            # (we already have a check when pushing successors, but we may have explored two similar successors
            # in the meantime)
            # FIXED? in Tangram, small triangle is placed 3x at the same position,
            # one having a 1e-15 error and 2 being really identical
            if assignment in closed_set:
                viewer.event('old_popped', last_node)
                continue

            # the test below ensured the assignment was only pushed if not in the closed set already
            closed_set.add(assignment)  # set with hashdict version
            viewer.event('new_popped', last_node)

            # check conflicts
            if config.semidebug:
                if check_if_conflicts(assignment, problem.cp):
                    logging.warning('Node violates some constraints')
                    continue

            # early bad node drop: drop the node if the actual cost is worse or equal to the best cost found
            # do not bother with the dynamic cost, it is still a copy of the parent's dynamic cost
            # this avoids finding multiple goals with the same cost
            if last_node.cost >= best_cost:
                viewer.event('optimize_drop', last_node, best_cost)
                continue

            # OPTIONAL
            # - singleton domain - (non-initial node only)
            # we can update the domain of the last variable assigned in this node to a singleton for debug
            # however, in practice, we never use this since the piece is already placed;
            # if we have to move it, we must use an updated domain but this is for the local search phase
            if last_node.variable_assigned is not None:
                # FIXME: MultineLineString back-and-forth issue
                last_node.domains[last_node.variable_assigned] = problem.cp.create_domain_singleton(
                    last_node.variable_assigned, last_node.value_assigned)
 
            # if goal node i.e. assignment is complete, stop and return that node
            if problem.is_complete(assignment):
                # in any case, log that so that we can see if we found a model node, even a worse one
                viewer.event('found_model', last_node, current_fringe_idx)
                if not optimize:
                    finish_reason = 'solution found'
                    break
                else:
                    if not hasattr(problem.cp, 'get_cost'):
                        raise ValueError('optimize is set to True but the constraint problem has no get_cost method')
                    last_cost = problem.cp.get_cost(assignment)
                    if last_cost < best_cost:
                        best_node = last_node
                        best_cost = last_cost
                        viewer.event('better_solution', best_node, best_cost, current_fringe_idx)
                    else:
                        # continue searching for better nodes until max iterations
                        # IMPROVE: use another, lower iteration limit
                        # e.g. a limit on the number of successively better solutions found
                        pass
                # since filtering, caching and getting successors are only used
                # to deepen the search, they are not useful from a model node
                continue

            # CSP filter (drop decision only)
            # with cache lazy evaluation, no need to revise domains manually anymore
            # only check for empty domains, including at the initial node in case the container is too small
            # whatever (you probably need a COP + length extension in this case)
            if not optimize:  # not cp.open_dim?
                # domains and dynamic domains should be the same
                empty_domain_tails = [tail for tail in last_node.unassigned_variables if last_node.domains[tail].is_empty]
                if empty_domain_tails:
                    # the domains of empty_domain_tails were reduced to the empty set, pass that last assignment and log this
                    viewer.event('forward_check_empty', last_node.variable_assigned,
                                 last_node.value_assigned, empty_domain_tails)
                    continue

            # COP equivalent: length extension
            # put outside non-initial node condition so that if the container is too small for even the first placement,
            # it can stretch
            # OPTIMIZE: may be some extra computations since called even with no filter but should be okay
            else:
                # accept that FC has emptied a domain, but extend the cost limit in that case
                assert isinstance(problem.cp, ConstraintOptimizationProblem), 'Cost estimation method only makes sense for COP'
                new_cost = dynamic_cost_fun(last_node)
                # only update cost if it has changed (currently grows but shrink should work in theory)
                if new_cost != last_node.dynamic_cost:  # precision?
                    viewer.event('increase_dynamic_cost', last_node.dynamic_cost, new_cost, current_fringe_idx)
                    # the dynamic cost has changed, so the cached dynamic domains, and all other dynamic values, are dirty
                    last_node.cache.increment_delay_dynamic_values()

                    # TEMP: don't mind min cost for now
                    # later, important for node dropping when cost is worse (dynamic is more of an estimation)
                    last_node.min_cost = new_cost
                    last_node.dynamic_cost = new_cost
                    # last_node.update_all_dynamic_domains()

            # execute even on initial node (cache should be correctly initialized)
            # IMPROVE: I think only free space is enough but need to refactor that filter stuff anyway
            # try to apply cache filter anyway; if not cached, the cache will raise an Error

            # REFACTOR: unify both types of filtering (FC good for all regions dead regarding one piece,
            # dead region analysis good for one region dead regarding all pieces -> use jug approach)
            # - cache filtering -
            # filtering function that requires cache; is technically the same as normal filtering
            # but more convenient to put here
            if cache_filter_fun is not None:
                # the cache_filter_fun should return the extra cost required for this node to be valid
                # in CSP mode, if extra cost is positive ignore this node
                extra_cost = cache_filter_fun(last_node, viewer)
                if extra_cost > 0:
                    if optimize:
                        # PRECISION: should not be accumulation of precision errors
                        # since next computation of extra_cost will be based on new cost
                        new_cost = last_node.dynamic_cost + extra_cost
                        viewer.event('increase_dynamic_cost', last_node.dynamic_cost, new_cost)
                        last_node.min_cost = new_cost  # for now, we assume we really got a lower bound
                        last_node.dynamic_cost = new_cost
                        # after updating dynamic cost, must update dynamic domains too
                        # last_node.update_all_dynamic_domains()
                    else:
                        viewer.event('filter2', last_node)
                        continue

            # BETTER COST ENFORCEMENT (after cache filter fun because new dynamic cost is checked to
            # drop node immediately, but before expanding since children cannot be better)
            # verify that the node has a lower dynamic cost than the best cost found until now
            # indeed, dynamic cost currently only grows so there is no hope of finding a better solution from there

            # since this line is accessed even without cache (because FC alone can update the dynamic cost)
            # and Knapsack1D goes there, we had to make the distinction between min cost and dynamic cost
            # as dynamic cost (max bound) is set to infinity (and unfortunately inf >= inf in Python)
            # but min cost is set to 0, which is okay
            # IMPROVE: use max(min cost, current cost (get_cost)) if not too costly,
            # so that we can drop nodes with too many
            # orders in the infinite stock in Knapsack, while preferring the improved min cost estimation in IP
            # or BETTER: always keep min cost superior or equal to current cost, even if no cache

            # 2nd node drop on cost comparison: this time, dynamic cost may have changed, if it is worse
            # than the best cost found until now, drop it
            if last_node.min_cost >= best_cost:
                viewer.event('optimize_drop', last_node, best_cost)
                continue

            successors = problem.get_successors(last_node, value_picker, optimize)
            viewer.event('expanded', last_node, successors, current_fringe_idx)
            for successor in successors:
                # search optimized for a set of elements with a good hash
                if successor.assignment not in closed_set:
                    # push successor to fringe; if a priority queue, will be sorted *now* not later!
                    fringe.push(successor)

            # if fringe has been emptied on last pop and still no successors, mark as empty
            if not fringe:
                empty_fringe_indices.add(current_fringe_idx)

    except TopologicalError:
        viewer.event('finished', last_node, 'topological error')
        raise
    except GeometryException as e:
        # robust: finish viewer in case of exception, and plot extra geometry
        # related to the problem
        viewer.event('finished', last_node, 'exception', e)
        raise
    except Exception:
        # robust: finish viewer in case of exception
        viewer.event('finished', last_node, 'exception')
        raise

    if optimize and best_node is not None:
        # in optimization mode, it is still a success if we found something
        viewer.event('finished', best_node, finish_reason)
        return best_node
    # viewer.event('finished', fringe.sorted())
    # just a change of term for CSP
    if finish_reason == 'fringe empty':
        finish_reason = 'no solution found'
    # can also have found a solution, and return it; else return None
    viewer.event('finished', last_node, finish_reason)
    return last_node


def beam_search(problem, value_picker, alpha, beta, global_evaluation=None, local_evaluation=None, filter_fun=None,
                viewer=None, max_iterations=300, cached_values=None, cache_filter_fun=None, dynamic=False):
    """
    Apply beam search on a picking value constraint problem, with a value picker, filter factor alpha,
    beam width beta.

    :param problem:
    :param value_picker:
    :param alpha: filter factor
    :param beta: beam width
    :param global_evaluation: global evaluation function, used in the main priority queue, of size beta
    :param local_evaluation: local evaluation function, used in the local priority queue, of size alpha
    :param filter_fun: filtering function
    :param viewer:
    :param max_iterations:
    :param cached_values:
    :type cache_filter_fun: (AssignmentSearchNode, BaseConstraintViewer) -> float
    :param cache_filter_fun: advanced filter function that uses cached information that returns the extra cost required
    :param dynamic:

    :rtype: list[AssignmentSearchNode]
    :return: list of model nodes found, max size beta

    """
    # verification that optimize is active when the problem is a COP (DEBUG until CSP and COP are merged?)
    assert dynamic and isinstance(problem.cp, ConstraintOptimizationProblem) or \
           not dynamic and isinstance(problem.cp, ConstraintSatisfactionProblem),\
        'CSP/COP and optimize/dynamic parameter must match'

    # store solution nodes here; we expect to have beta solutions, but we may find fewer
    model_nodes = []

    if viewer is None:
        viewer = EmptyConstraintSearchViewer()

    viewer.event('started')

    # initialize global evaluation fringe, size beta = beam width
    initial_node = problem.create_node(problem=problem, cached_values=cached_values,
                                        dynamic=dynamic)
    global_fringe = BoundedPriorityQueueWithFunction(beta, global_evaluation)
    global_fringe.push(initial_node)

    iteration_counter = IterationCounter(max_iterations)

    # main loop, global fringe is empty when all nodes are models so they have no successors
    while global_fringe:

        # if max iterations exceeded, stop and return models found (probably none since only at bottom of search tree)
        if iteration_counter.increment_and_check_exceed():
            # IMPROVE: show final nodes of each branch too (they are in global fringe,
            # but need to tweak handle_finished to support a list of Nodes)
            viewer.event('finished', None, 'max iterations')
            return model_nodes

        viewer.event('new_iteration', iteration_counter.iteration, len(global_fringe))

        # prepare candidates for next global fringe (max size alpha*beta, only beta will be chosen)
        next_candidates_fringe = []  # OPTIMIZE: local list should not leak, but check garbage collector

        # Since we work in dependent beam search, there are not truly different branches
        # so we cannot represent consistent branches with a search branch index.
        # However, we know that we pop better nodes first so when plotting, at least
        # we know that subplots on the left (lower index) have a better global evaluation (more stable too)
        search_index = 0

        # get next generation of nodes with filter factor alpha; expecting beta iterations
        while global_fringe:
            node = global_fringe.pop()
            viewer.event('new_popped', node)  # no closed set here, consider everything is new

            # if the popped is a model node (assignment is complete and valid), store that node as a solution
            if problem.is_complete(node.assignment):
                model_nodes.append(node)
                viewer.event('found_model', node, search_index)
                continue  # a model node should have no successors

            # create local evaluation fringe
            local_fringe = BoundedPriorityQueueWithFunction(alpha, local_evaluation)

            # expand successors
            successors = problem.get_successors(node, value_picker, dynamic)
            viewer.event('expanded', node, successors, search_index)

            for successor in successors:
                logging.debug('pushing next successor to local fringe')
                local_fringe.push(successor)
            # extend the actual list of the heap (order does not matter) of max size alpha
            next_candidates_fringe.extend(local_fringe.get_items())

            search_index += 1

        # final expected (max) size of next_global_fringe: alpha*beta
        logging.debug('next global fringe candidates')

        # global fringe is now empty, drain from next fringe
        for successor in next_candidates_fringe:
            # TODO (optional): reduce domain of assigned var to singleton

            # apply forward check now: it is useful for global evaluation that simulates quick layout completion
            # in general, invalid layouts are quite compact, they would probably be popped soon enough anyway

            if not dynamic:  # not cp.open_dim?
                # domains and dynamic domains should be the same
                empty_domain_tails = [tail for tail in successor.unassigned_variables if successor.domains[tail].is_empty]
                if empty_domain_tails:
                    # the domains of empty_domain_tails were reduced to the empty set, pass that last assignment and log this
                    viewer.event('forward_check_empty', successor.variable_assigned,
                                 successor.value_assigned, empty_domain_tails)
                    continue

            # TODO: COP + optimize not supported yet, but is it meaningful? to grow the container and have a solution
            # rather than nothing, I guess; if so, copy code in function vp_search above

            # optimize in beam search? Stops at the end with all nodes at the same time, so difficult
            # if dynamic:  # cp.open_dim?
            #     # accept that FC has emptied a domain, but extend the cost limit in that case
            #     assert problem.cp.open_dim, 'Length estimation method only makes sense for COP'
            #     new_cost = dynamic_cost_fun(last_node)
            #     # only update cost if it has changed (currently grows but shrink should work in theory)
            #     if new_cost != last_node.dynamic_cost:  # precision?
            #         viewer.event('increase_dynamic_cost', last_node.dynamic_cost, new_cost)
            #         # TEMP: don't mind min cost for now
            #         # later, important for node dropping when cost is worse (dynamic is more of an estimation)
            #         last_node.min_cost = new_cost
            #         last_node.dynamic_cost = new_cost
            #         last_node.update_all_dynamic_domains()

            # filter 2 in beam search?
            if cache_filter_fun is not None:
                # the cache_filter_fun should return the extra cost required for this node to be valid
                # in CSP mode, if extra cost is positive ignore this node
                extra_cost = cache_filter_fun(node, viewer)
                if extra_cost > 0:
                    if dynamic:
                        # PRECISION: should not be accumulation of precision errors
                        # since next computation of extra_cost will be based on new cost
                        new_cost = node.dynamic_cost + extra_cost
                        viewer.event('increase_dynamic_cost', node.dynamic_cost, new_cost)
                        node.min_cost = new_cost  # for now, we assume we really got a lower bound
                        node.dynamic_cost = new_cost
                        # after updating dynamic cost, must update dynamic domains too
                        node.update_all_dynamic_domains()
                    else:
                        viewer.event('filter2', node)
                        continue

            logging.debug('pushing node from alpha fringes to global fringe')
            global_fringe.push(successor)

    # VIEWER: no single final node, instead each branch took care of signaling its model node to the viewer
    viewer.event('finished', None, 'fringe empty')
    return model_nodes


# -- node heuristics --

# - master heuristics (aggregate heuristics) -

def ordered_master_heuristic(*heuristics):
    """
    Master heuristic that gives priority to the first heuristics passed,
    then to the next heuristics in order in case of draw.

    Return a heuristic that returns a tuple of value, coming from the provided heuristics, in order

    :param heuristics: sequence of heuristics, by descending priority
    :rtype: (AssignmentSearchNode, ((AssignmentSearchNode) -> V)) -> V
    :return: a heuristic

    """
    def heuristic(node):
        """
        :type node: AssignmentSearchNode
        :rtype: tuple[float]

        """
        return tuple(h(node) for h in heuristics)

    return heuristic


def mixed_master_heuristic(heuristics, weights=None):
    """
    Master heuristic that combines value of the heuristics with their respective weights.
    Weights have not necessarily a sum of 1.

    If no weights are passed, uniform weights are used.

    Return a heuristic that returns a linear combination of heuristic values

    :param heuristics: sequence of heuristics, by descending priority (not a * parameter)
    :param weights: list of weights corresponding resp. to the heuristics, or None
    :rtype: (AssignmentSearchNode, ((AssignmentSearchNode) -> V)) -> V
    :return: a heuristic

    """
    def heuristic(node):
        """
        :type node: AssignmentSearchNode
        :rtype: float

        """
        # do not use weights a variable name, it will redefine it to local scope, making parameter weights access invalid
        h_weights = weights if weights is not None else [1./len(heuristics)] * len(heuristics)
        return sum(weight * h(node) for h, weight in izip(heuristics, h_weights))

    return heuristic


# - basic heuristics -

def null_heuristic(node):
    """
    A trivial heuristic function that always return 0.
    With a PriorityQueueWithFunction, this will redirect any comparison to the
     element count, turning it effectively into a FIFO.

    :type node: AssignmentSearchNode
    :type problem: ValuePickingConstraintSearchProblem
    :rtype: float

    """
    return 0


# - depth heuristics -

# RESURRECTED FOR A LAST TRY
def deeper_node_heuristic_wrapper(sub_heuristic=null_heuristic):
    def heuristic(node):
        """
        :type node: AssignmentSearchNode
        :type problem: ValuePickingConstraintSearchProblem
        """
        # we prefer non-negative values, so we use N - nb vars assigned, but this is optional
        # this is easier to debug: we have the number of variables to assign, equal to 0 for a model node
        problem = node.problem
        nb_remaining_variables = len(problem.cp.variables) - len(node.assignment)
        return nb_remaining_variables, sub_heuristic(node, problem)

    return heuristic


def depth_first_heuristic(node):
    """
    Heuristic that simulates a depth-first search.

    Since we assume that each action assigns a variable and never unassign one,
    this corresponds to evaluating the length of the current assignment, reversed.
    In order to return 0 for a model node, we choose to return the number of variables yet to assign.

    Use it as a first-priority heuristic inside a master heuristic to automatically select
    nodes with more assignments.

    :type node: AssignmentSearchNode

    """
    # return number of remaining variables to assign (equal to 0 for a model node)
    nb_remaining_variables = len(node.problem.cp.variables) - len(node.assignment)
    return nb_remaining_variables


# -- value-based heuristics

# - dynamic / optimization heuristics -

def min_cost_heuristic(node):
    """
    Value/cost-based heuristic
    Constraint Optimization Problem only

    Return the cost of the partial assignment at this node.

    Effect: gives higher priority to assignment with a lower partial cost.

    Note that the partial cost is merely an indication of the cost. It may denote
    a lower or an upper bound.
    For more tuning, use lower, upper and estimated costs.

    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
    :param node:
    :rtype: float

    """
    problem = node.problem
    assert isinstance(problem.cp, ConstraintOptimizationProblem)
    return problem.cp.get_cost(node.assignment)


def min_dynamic_cost_heuristic(node):
    """
    Cost-based heuristic
    Constraint Optimization Problem only

    Return the dynamic cost of at this node.

    Effect: gives higher priority to assignment that targets a lower cost (the final cost may be higher,
    but if the dynamic cost is close to the min cost, it should not be lower)

    The dynamic cost is often the min cost + a small margin to avoid floating-point issues and even more margin
    if the problem is known to have some waste, or free region analysis showed that it should have some area leak.
    For more tuning, use lower, upper and estimated costs.

    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
    :param node:
    :rtype: float

    """
    problem = node.problem
    assert isinstance(problem.cp, ConstraintOptimizationProblem)
    return node.dynamic_cost

# -- search methods --

def depth_first_vp_search(problem, value_picker, filter_fun=None, dynamic_cost_fun=get_current_dynamic_cost,
                          viewer=None, max_iterations=300, cached_values=None, cache_filter_fun=None):
    """
    Search the deepest nodes in the search tree first, with value picking (vp).

    :param dynamic_cost_fun:
    :type problem: ValuePickingConstraintSearchProblem

    :rtype: AssignmentSearchNode
    :return: a node containing a model (complete and valid assignment)

    """
    fringe_factory = Stack
    return value_picking_constraint_graph_search(problem, fringe_factory, False, value_picker, dynamic_cost_fun,
                                                 viewer=viewer, max_iterations=max_iterations,
                                                 cached_values=cached_values, cache_filter_fun=cache_filter_fun)


def breadth_first_vp_search(problem, value_picker, filter_fun=None, dynamic_cost_fun=get_current_dynamic_cost, viewer=None, max_iterations=400, cached_values=None,
                            cache_filter_fun=None):
    """
    Search the shallowest nodes in the search tree first, with value picking (vp).

    :type problem: ValuePickingConstraintSearchProblem

    :rtype: AssignmentSearchNode
    :return: a node containing a model (complete and valid assignment)

    """
    # init fringe as a queue (FIFO) with the start node
    fringe_factory = Queue
    return value_picking_constraint_graph_search(problem, fringe_factory, False, value_picker, dynamic_cost_fun,
                                                 viewer=viewer, max_iterations=max_iterations,
                                                 cached_values=cached_values, cache_filter_fun=cache_filter_fun)


def greedy_vp_search(problem, value_picker, optimize=False, heuristic=null_heuristic, filter_fun=None,
                     dynamic_cost_fun=get_current_dynamic_cost,
                     viewer=None,
                     max_iterations=300, cached_values=None, cache_filter_fun=None):
    """
    Search the node that has the lowest heuristic first, and return a model node, or None if no solutions
    were found.

    In a constraint search, heuristic does not represent the distance to the goal,
     but the (inverse) quality of current assignment (the lower the value, the best).
    Partial assignments of higher quality are assumed to are more likely to lead to complete assignments
    that satisfy all constraint conditions.

    In addition, in a constraint search, we are mainly interested in assignments, deeper assignments in particular.
    Any heuristic using the node and problem information can be used, but most strategies being interested in deeper
    nodes (derivative and improvements of the brutal DFS), using a tuple with the opposite of the depth
    (number of assignments) as 1st element is a good way to ensure the heap keeps the deepest nodes on top
    (default Python heap keeps lower values on top, and Python compares tuple values from left to right)

    In constraint search, many strategies are interested by the action rather than the final state. Indeed,
    the problem can define no relevant cost as in common graph search, so we cannot simply split what has
    happened before and what happens now. Instead, we want to pick specific variables first, and then
    specific values first. That is why we do not

    :type optimize: bool
    :param optimize: is it a search to optimize cost?
    :type problem: ValuePickingConstraintSearchProblem
    :type heuristic: AssignmentSearchNode -> V
    :param heuristic: heuristic function that gives a lower value for a better assignment (V is any comparable type)
    :param fringe: container that supports:
        - append(), pop() for node manipulation
        - __iter__() and __len__() for boolean tests (__contains__() when code is optimized)
        - sorted() for debug (or use a comparator and Python built-in sorted())
    :type filter_fun: ((AssignmentSearchNode, bool, ippy.packmath.constraintsearchviewer.BaseConstraintSearchViewer) -> (T | None) | None)
    :param filter_fun: filter_fun function to apply to the domains after an assignment i.e. a node selection
        (T variable, U value)
        Return the first tail for which the domain was reduced to empty, if any.
    :type viewer: (ConstraintSearchViewer | None)
    :param viewer:
    :param max_iterations: max number of iterations before the search stops
    :type cached_values: list[str]
    :param cached_values: list of values to cache
    :param cache_filter_fun: advanced filter function that uses cached information


    :rtype: AssignmentSearchNode
    :return: a node containing a model (complete and valid assignment)

    """
    # REFACTOR: pass directly problem.cp instead of problem?
    fringe_factory = partial(PriorityQueueWithFunction, heuristic, viewer)
    return value_picking_constraint_graph_search(problem, fringe_factory, optimize, value_picker, dynamic_cost_fun,
                                                 viewer=viewer, max_iterations=max_iterations,
                                                 cached_values=cached_values, cache_filter_fun=cache_filter_fun)



# -- common value pickers --

def iterable_domain_value_picker(variable, node, cp, dynamic):
    """
    Return the domain itself, which should contain all the values we want.
    For this to work, the domain must be iterable. This typically works for problems with discrete domains.

    :type variable: T
    :param variable: variable<<
    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
    :param node: node containing the assignment
    :type cp: ippy.packmath.constraintproblem.ConstraintProblem
    :param cp: Constraint Problem, can be useful to get more information on the problem
    :type dynamic: bool
    :param dynamic: should we consider the dynamic domains?
    :rtype: list[U]

    """
    domains = node.dynamic_domains if dynamic else node.domains
    return domains[variable]


# -- filter functions --

# Forward Check: use function in ippy.search.constraint

# def forward_check():
#     pass


