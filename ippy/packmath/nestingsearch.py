from copy import deepcopy
from ippy.utils.shapelyextension import get_unique_coords
from simpleai.search.models import SearchProblem

__author__ = 'hs'

class OrderedPlacementState():
    """
    Search state used by SearchProblem where pieces are placed in a specific order.

    Attributes:

    piece_key_order
        ordered list of (piece_id, instance_idx) of pieces to place

    layout
        dictionary {(piece_id, instance_idx): Placement} of placements until now

    next_piece_key_idx
        index of the next piece to place in piece_key_order (not a piece_key, but an element index)
        should be equal to len(layout), but provided for easier access and flexibility
        if the strategy changes

    """
    def __init__(self, piece_key_order, layout, next_piece_key_idx):
        self.piece_key_order = piece_key_order
        self.layout = layout
        self.next_piece_key_idx = next_piece_key_idx

    def __repr__(self):
        return "<OrderedPlacementState: ({0}, {1}, {2})>".format(self.piece_key_order, self.layout, self.next_piece_key_idx)

class NFPVNestingSearchProblem(SearchProblem):
    """
    Search Problem where each state is a list of translation placements
    of pieces, where each translation is a vertex of the NFP of the piece
    to place with all previous pieces (NFP Vertex Strategy)

    We work with a search tree with backtracking thanks to a fringe,
    rather than a search graph, since we do not need operations that could
    jump to another branch such as piece exchange.

    Problem summary:
        - data contained in problem or expanded_problem (except metadata such as name or date)
        - heuristic data such a NFPs is required to quickly find NFP vertices

    State summary:
        - each piece placed by ID-instance, with the corresponding translation
        (order of placement does not matter in this method)
        - next piece ID-instance to place, or index of the next piece to place
        if we store all pieces in a list (in this case, pieces are ordered,
        but only the right part of the list matters, i.e. we can remove
        the ID of the piece we place (which is leftmost) at each step)

    Problem encoding:
        - ExpandedNestingProblem expanded_problem

    State encoding:
        For a fixed placement order: an OrderedPlacementState structure

    """
    def __init__(self, expanded_problem, initial_state=None):
        super(NFPVNestingSearchProblem, self).__init__(initial_state)
        self.expanded_problem = expanded_problem

    def actions(self, state):
        """
        Return a list of translations to NFP vertices.

        :param state: OrderedPlacementState

        """
        # get the fit-polygon of the next piece regarding placed pieces and the container
        if state.next_piece_key_idx == len(state.piece_key_order):
            return []  # last piece was placed

        piece_id = state.piece_key_order[state.next_piece_key_idx][0]
        # OLD: action cannot place rotated pieces so leave orientation=0
        # (otherwise, prepare a successor for each orientation)
        fit_polygon_with_boundary = self.expanded_problem.get_fit_polygon_with_boundary(piece_id, 0,
                                                                                        state.layout.placements)

        # since the next piece to place is already known in this problem, we only
        # provide the translations
        return get_unique_coords(fit_polygon_with_boundary.boundary)


    def result(self, state, action):
        """
        Return the new OrderedPlacementState containing the layout copy after a placement to translation
        given by action, and the index of the next piece to place (out of bounds if last piece was placed).

        :param state:
        :param action:
        :return: OrderedPlacementState
        """
        # since we are transiting from state, state must not be the last one so next_piece_key_idx must be in range
        piece_key = state.piece_key_order[state.next_piece_key_idx]

        new_layout = deepcopy(state.layout)
        new_layout.add_placement(piece_key[0], piece_key[1], action)
        next_piece_key_id = state.next_piece_key_idx + 1
        return OrderedPlacementState(state.piece_key_order, new_layout, next_piece_key_id)

    def is_goal(self, state):
        """Returns true if a state is the goal state."""
        return state.layout.is_complete  # and valid, but should be guaranteed in this strategy
