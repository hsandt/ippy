from copy import deepcopy
from ippy.packmath.constraintsearch import ValuePickingConstraintSearchProblem, AssignmentSearchNode, Cache
from ippy.search.constraint import ConstraintOptimizationProblem, Domain

__author__ = 'hs'


class ValuePickingMultipleKnapsackProblem1D(ValuePickingConstraintSearchProblem):
    """
    Concrete class for Nesting search problems by value picking.

    Attributes:
        :type cp: MultipleKnapsackProblem1D
        :param cp: Constraint problem associated to this search problem

    """
    def __init__(self, cp):
        super(ValuePickingMultipleKnapsackProblem1D, self).__init__(cp)

    def create_node(self, assignment=None, parent=None, variable_assigned=None, problem=None, cached_values=None,
                    dynamic=None):
        """
        Create an AssignmentSearchNode specific to the problem subclass

        :rtype: AssignmentSearchNode

        """
        # TODO: use subclass to implement all abstract methods
        return Knapsack1dAssignmentSearchNode(assignment, parent, variable_assigned, problem, cached_values, dynamic)

    # def pick_variables(self, node):
    #     """
    #     Return a list of variables (piece keys) to assign.
    #     Only pick the first instance of each piece type.
    #     Use the dictionary of next instance indices contained in the node.
    #
    #     :type node: NestingAssignmentSearchNode
    #     :rtype: list
    #
    #     """
    #     # TODO: create something similar to piece ID - instance idx system by grouping all orders of equal length,
    #     # or group pick any variable (default behavior)
    #     next_piece_ids, next_piece_inst_indices = zip(*node.next_piece_inst_idx_dict.iteritems())
    #     return map(PieceKey, next_piece_ids, next_piece_inst_indices)


class MultipleKnapsackProblem1D(ConstraintOptimizationProblem):
    """
    Multiple Heterogeneous Large Object Knapsack Problem, 1D

    As many 1D orders (pieces) must be placed, in terms of total length, in the fixed set of
    heterogeneous 1D stocks (containers). An extra stock, with infinite capacity, is automatically added.
    The total length of orders placed in the infinite stock constitute the cost of the problem.
    Non-placed orders are not counted in the cost, so the cost can only grow.

    There are different, equivalent ways to choose variables and values.
    We use one variable per order, where the variable is the order's index.
    We assign to each variable a value corresponding to the stock to place the order in. The value is the stock's index.

    Indeed, our constraint search solver considers an assignment for each step, and if we were using stock as variables
    and a list of placed orders as values, an assignment would correspond to the modification of a value rather
    than the assignment of a value to a new variable.
    Note that the way we choose variables has a strong impact on how we use domains and filters, hence on
    the search itself. For a sub-problem like this one, this is not a problem.

    However, in order to get all the orders placed in an order, we will have to do the inverse query:
    O(np) for n the number of orders, p the number of stocks.

    An assignment is a placement, {order_index: stock_index, ...}

    All indices start at 0. Stock and order sizes are accessed by the same indices.

    Order indices must be accessed through the 'variables' attribute.

    Attributes
        :type stock_sizes: list[float]
        :param stock_sizes: sequence of sizes of stocks
        :type order_sizes: list[float]
        :param order_sizes: sequence of sizes of orders
        :type domains: (dict[int, MultipleKnapsackDomain] | None)
        :param domains: domains, can be passed manually for more information, but filter yourself before
            In addition, all variables automatically contain the infinite stock in their domain
        stock_indices: list[int]
            list of stock indices

    """
    def __init__(self, stock_sizes, order_sizes, domains=None):
        self.order_sizes = order_sizes
        self.stock_sizes = stock_sizes + [float('+inf')]  # infinite stock
        order_indices = range(len(order_sizes))
        self.stock_indices = range(len(stock_sizes)) + [-1]  # infinite stock

        if domains is None:
            # we can initialize the domains by removing the stock indices for which the stock itself is too small
            # for this single order (unlikely, though)
            # otherwise, we have to apply filtering on the initial node too (not FC), so that we are sure
            # that any value assignment in the domain ensures constraint satisfaction at each step
            domains = {
                order_index: MultipleKnapsackDomain(
                    {stock_index for stock_index in self.stock_indices if order_sizes[order_index] <= self.stock_sizes[stock_index]}
                )
                for order_index in order_indices
            }
        else:
            # add infinite stock to each order's domain (if not already)
            for order_index in order_indices:
                assert isinstance(domains[order_index], MultipleKnapsackDomain), 'Domains does not contain MultipleKnapsackDomain values'
                domains[order_index].add(-1)
        super(MultipleKnapsackProblem1D, self).__init__(order_indices, domains, global_constraints=[self.global_constraint])

    def global_constraint(self, placements):
        """
        Return True if sum of order sizes in each stock is inferior or equal to stock size

        :param placements:

        """
        for stock_index in self.stock_indices:
            # OPTIMIZE: compute orders in stock for all stocks first
            # OPTIMIZE: do not compute for infinite stock
            if sum(self.order_sizes[order_index]
                   for order_index in self.get_orders_in_stock(placements, stock_index)) > self.stock_sizes[stock_index]:
                return False
        return True

    def create_domain_singleton(self, order_index, stock_index):
        """
        Return a singleton containing value, in the problem-specific domain format.
        Here we will return a list of one element, the stock index.

        :param order_index: variable for which the singleton is generated
        :param stock_index: unique value of the singleton
        :return: singleton in the domain format

        """
        return MultipleKnapsackDomain.create_singleton(stock_index)

    def get_cost(self, placements):
        """
        Return the sum of the lengths of orders placed in the infinite stock
        As any assignment is also a complete one, this is a true cost

        :param placements: dict[int, int]
        :rtype: float

        """
        # unassigned_order_indices = self.get_unassigned_variables(assignment)
        # return sum(self.order_sizes[order_index] for order_index in unassigned_order_indices)
        return self.get_total_order_size_in_stock(placements, -1)  # infinite stock is last

    def get_min_cost(self):
        """
        Return 0, as apart from doing more filtering and search, the cost may be reduced to 0 in general
        The search, however, can keep in its scope a lower bound of the cost by having verified that
        there was no solution for a given fixed limitation on unplaced orders total length.

        :rtype: float

        """
        return 0

    def get_initial_cache(self, initial_node, cached_values):
        return Knapsack1dCache(initial_node, cached_values=cached_values)

    def update_cache(self, node):
        pass

    # Remark: we do not use dynamic cost here, though we could: tolerate more and more pieces not to be placed
    # and solve iteratively as a CSP... Here we use a direct COP approach.
    # REFACTOR: Should that be another solving strategy at all? Do not use dynamic bool argument?

    # TODO: create concrete class of Node for this problem
    # the node should have a cache that stores the assigned orders to each stock (the reverse of our model)
    # otherwise we would have to add a method to query the assigned orders to each stock (for one stock or,
    # more efficiently, for all stocks at once by filling lists by iterating over the orders)
    # Question: should the cache be split into directly accessible attributes for the node?

    # BUT even if we do this, if we do not pass the node instead of placements as parameter in global_constraints
    # we cannot check them! Of course, in practice, global constraints is rarely called, so this is okay.
    # If the cache is still useful for other purpose, why not?
    # I suggest creating the query method for one or all stocks first, then optimize if required.

    def get_orders_in_stock(self, placements, containing_stock_index):
        """
        Return the list of order indices placed in stock #stock_index.
        If you need this result for all the stocks, use get_orders_in_stocks instead for more performance.

        :type containing_stock_index: int
        :rtype: list[int]

        """
        # if the index corresponds to the last container, always use -1 (useful for automated tests)
        if containing_stock_index == len(self.stock_indices) - 1:
            containing_stock_index = -1

        contained_order_indices = []
        for order_index, stock_index in placements.iteritems():
            if stock_index == containing_stock_index:  # if last element, compared to -1
                contained_order_indices.append(order_index)
        return contained_order_indices

    # TODO: version for all stocks at once

    def get_total_order_size_in_stock(self, placements, containing_stock_index):
        """
        Return the sum of the size of the orders placed in stock #stock_index.

        :type placements: dict[int, int]
        :type containing_stock_index: int
        :rtype: float

        """
        orders_in_stock = self.get_orders_in_stock(placements, containing_stock_index)
        return sum(self.order_sizes[order_index] for order_index in orders_in_stock)


class MultipleKnapsackDomain(Domain, set):
    """
    Domain for the Multiple Heterogeneous Large Object Knapsack Problem, 1D

    The domain is the set of stock indices in which the order variable can be placed.

    Attributes
        :type stock_indices: (set[int] | list[int])
        :param stock_indices: sequence of indices of orders allowed for this stock for further placements

    """
    def __init__(self, stock_indices):
        Domain.__init__(self)
        set.__init__(self, stock_indices)

    def __repr__(self):
        return '<MultipleKnapsackDomain: {{{0}}}>'.format(', '.join([str(stock_index) for stock_index in self]))

    def __str__(self):
        return 'MultipleKnapsackDomain: {{{0}}}>'.format(', '.join([str(stock_index) for stock_index in self]))

    @property
    def is_empty(self):
        """
        Return True if the list of stock indices is empty

        :rtype: bool

        """
        return not self

    def contains(self, stock_index):
        """
        Return True if the stock index is in the list.

        :rtype: bool

        """
        return stock_index in self

    @staticmethod
    def create_singleton(stock_index):
        """
        Return a singleton containing value

        This method is currently used to create a singleton from the problem equivalent method, but may become
        the main way to create domain singletons is the future

        :type stock_index: int
        :rtype: list[int]

        """
        return MultipleKnapsackDomain({stock_index})  # {} are redundant with set construction, just to be clear


class Knapsack1dAssignmentSearchNode(AssignmentSearchNode):
    """
    Subclass of AssignmentSearchNode for the 1D Knapsack problem

    """
    def __init__(self, assignment=None, parent=None, variable_assigned=None, problem=None, cached_values=None,
                 dynamic=None):
        """
        super parameters:

        :type assignment: hashdict[T, U]
        :param assignment: assignment state located at this node
        :type parent: Knapsack1dAssignmentSearchNode
        :param parent: predecessor node (None if initial node) [optional since only the result matters]
        :type variable_assigned: T
        :param variable_assigned: the last variable assigned, that led to this node (None if initial node)
        :type problem: ValuePickingConstraintSearchProblem
        :param problem: problem this node belongs to

        """
        super(Knapsack1dAssignmentSearchNode, self).__init__(assignment, parent, variable_assigned, problem,
                                                             cached_values, dynamic)

        if parent is None:
            self.min_cost = 0
            # use max cost by default for dynamic cost for now: never extend
            # either use float('+inf') or the theoretical max, ie sum(order.area)
            self.dynamic_cost = float('+inf')

            # always dynamic (unless we separate optimize and dynamic)
            # self.dynamic_domains = deepcopy(self.domains)

        else:
            # kept in case we use instance-index system later
            # # increment instance index for the ID of the last piece placed
            # # if it exceeds the quantity of that piece, ie the last piece was placed, remove the entry instead
            # inst_idx_dict = copy(parent.next_piece_inst_idx_dict)  # copy is enough for primitive types
            # # problem was not passed as parameter, but super set it as attribute, so use self
            # if inst_idx_dict[variable_assigned.id] < self.problem.cp.pieces[variable_assigned.id].quantity - 1:
            #     inst_idx_dict[variable_assigned.id] += 1
            # else:
            #     del inst_idx_dict[variable_assigned.id]
            # self.next_piece_inst_idx_dict = inst_idx_dict
            self.min_cost = parent.min_cost
            self.dynamic_cost = parent.dynamic_cost
            # self.dynamic_domains = parent.dynamic_domains

class Knapsack1dCache(Cache):
    """
    Cache for 1D Knapsack search

    No attributes for now so the cache is just here for compatibility with search algorithms
    Later, may add sum of order sizes, etc.

    No deepcopy yet either

    """
    # TODO: add dynamic domains to non-incremental values, check what else we can cache...
    def __init__(self, node=None, cache_dict=None, cached_values=None, delay_per_value=None):
        super(Knapsack1dCache, self).__init__(node, cache_dict, cached_values, delay_per_value,
                                              dynamic_values=['dynamic_domains'])


# NEXT TODO

#
# class GlobalConstraint(GlobalConstraint):
#     """
#     Abstract base class for global constraints
#
#     Attributes
#         :type cp: ConstraintProblem
#         constraint problem the constraint is applied to (required to query NFPs)
#
#     """
#     __metaclass__ = ABCMeta
#
#     def __init__(self, cp):
#         self.cp = cp
#
#     @abstractmethod
#     def check_if_consistent(self, assignment):
#         """
#         :type assignment: dict[T, U]
#
#         Return True if the partial assignment currently satisfies the constraint
#
#         """
#
#     @abstractmethod
#     def revise_forward(self, node, tail):
#         """
#         Filter tail's domain based on value assigned to head
#         Value assigned to head must be retrieved as placements[head].
#         We keep the whole assignment as a parameter because some implementations use them.
#
#         :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
#         :type tail: T
#
#         """
