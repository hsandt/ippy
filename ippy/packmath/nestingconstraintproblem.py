from abc import ABCMeta, abstractmethod
from collections import Counter
from copy import deepcopy, copy
from shapely.geometry import Point, box, MultiPolygon
from ippy.packmath.constraintsearch import Cache
from ippy.packmath.nfp import get_ifp_with_rectangular_container
from ippy.packmath.packhelper import get_layout_bounds
from ippy.packmath.problem import PieceKey
from ippy.search.constraint import BinaryConstraint, ConstraintSatisfactionProblem, ConstraintOptimizationProblem, \
    Domain, forward_check
from ippy.utils.decorator import deprecated
from ippy.utils.geometry import BoundedSide, get_translation_bounds_to_fit_floating_container, \
    get_floating_container_limit_bounds
from ippy.utils.shapelyextension import PolygonWithBoundary, can_bounds_fit, get_size, to_size, PolygonExtended, \
    rotate_origin, place_geom, to_multipolygon, get_closed_interior_as_multipolygon, assert_validity, round_coords, \
    get_nb_vertices
from ippy.view.plot import plot_geom_colors

__author__ = 'huulong'


class NestingDomain(Domain, dict):
    """
    Domain for a ConstraintProblem that is an InputMinimizationNestingProblem,
    associated to a piece variable.

    Format:
        dict[float, ippy.utils.shapelyextension.PolygonWithBoundary]
        dictionary of translation domains, indexed by rotation

        For a piece variable, placements in the domain are placements for which the translation
        belongs to the PolygonWithBoundary index by the placement's rotation.
        Even if some rotations have an empty value associated, the dict must contain one key for each
        rotation allowed for the piece in the problem.

    """
    def __init__(self, *args, **kwargs):
        Domain.__init__(self)
        dict.__init__(self, *args, **kwargs)

    # REFACTOR: singleton construction here, taking problem as parameter (instead of being a problem method)

    @property
    def is_empty(self):
        """
        Return True if for any rotations, the translation domain is empty

        """
        # None stands for "all set", so not empty
        return all(translation_domain is not None and translation_domain.is_empty for translation_domain in self.itervalues())

    def contains(self, placement):
        translation_domain = self[placement.rotation]
        # if translation domain is None (all set), any value will do
        if translation_domain is None:
            return True
        else:
            # FIXED? default threshold of 1e-11 too precise for 1e-7 for the IFP: correct either
            return translation_domain.contains_or_touch(Point(placement.translation), threshold=1e-7)

    def __repr__(self):
        return '<NestingDomain: {0}>'.format(self.items())

    def __str__(self):
        return 'NestingDomain: {0}'.format(', '.join('{}: {}'.format(rotation, translation_domain)
                                                   for rotation, translation_domain in self.iteritems()))

    def __deepcopy__(self, memo):
        return NestingDomain(self)  # or copy(self)

    @staticmethod
    def create_singleton(piece_key, placement, pieces):
        """
        :type piece_key: PieceKey
        :type placement: Placement
        :type pieces: dict[str, Piece]

        """
        orientations = pieces[piece_key.id].orientations
        # verify if rotat
        if placement.rotation not in orientations:
            raise ValueError('Placement rotation {} not among allowed rotations {}. Cannot create domain singleton'
                             .format(placement.rotation, orientations))

        # only placement.rotation key has a Point as PWB, other entries have an empty PWB
        singleton_domain = NestingDomain()
        for orientation in orientations:
            if orientation == placement.rotation:
                # the only orientation with something: a point
                singleton_domain[orientation] = PolygonWithBoundary(boundary=Point(placement.translation))
            else:
                # other PWB are empty
                singleton_domain[orientation] = PolygonWithBoundary()

        return singleton_domain

class NoOverlapConstraint(BinaryConstraint):
    """
    Binary constraint: two pieces must not overlap

    Attributes
        :type cp: (ConstraintProblem | InputMinimizationNestingProblem)
        constraint problem the constraint is applied to (required to query NFPs)

    """

    def __init__(self, cp):
        super(NoOverlapConstraint, self).__init__(cp)

    def check_if_consistent(self, piece_key_tuple, placement_tuple):
        """
        Return True if the 2 pieces in piece_key_tuple are not overlapping
        when placed at placement_tuple respectively

        :param piece_key_tuple: couple of piece keys
        :param placement_tuple: couple of placements respectively to piece_key_tuple

        """
        # NFP version
        (piece_id1, instance_idx1), (piece_id2, instance_idx2) = piece_key_tuple
        nfp = self.cp.nfps[((piece_id1, piece_id2), (placement_tuple[0].rotation, placement_tuple[1].rotation))]
        # compute refB - refA; unlike when translating an NFP, the difference is really in this order
        relative_translation = placement_tuple[1].translation - placement_tuple[0].translation
        # constraint is satisfied if the point is on the boundary or inside the NFP
        return not nfp.strictly_contains(Point(relative_translation))

        # generic version: place piece and test for overlap
        # NOT RECOMMENDED (may have low perf), prefer generating one NFP per rotation combination
        # placed_piece1, placed_piece2 = (self.cp.lot[piece_key].place_copy(placement)
        #                                 for piece_key, placement in zip(piece_key_tuple, placement_tuple))
        # return not placed_piece1.check_if_overlap(placed_piece2)

    def revise_forward(self, node, tail, revised_node=None):
        """
        Reduce the tail piece's domain by removing the NFP(head,tail) translated
        Value assigned to head must be retrieved as placements[head].
        We keep the whole placements as a parameter because some implementations use them.

        :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
        :type tail: PieceKey
        :param revised_node: extra parameter used for domain update in cache with backward compatibility
            If left to None, revised_node will be node. Else, the cached domain of revised node will be revised
            instead, but the last placement will come from node, the "next node"

        """
        if revised_node is None:
            revised_node = node

        head = node.variable_assigned
        # domains = revised_node.domains  # do not use property for cache access, infinite recursion since try to update
        domains = revised_node.cache.cache_dict['domains']  # direct cache access to avoid get() that will try to update (infinite recursion)
        # OPTIMIZE: do not update dynamic cost if dynamic is False (add dynamic parameter)
        # but do that only if you ensure CSP use domains only; or the inverse, use dynamic domains only
        # dynamic_domains = revised_node.dynamic_domains  # not here, or infinite recursion
        placements = node.assignment

        head_placement = placements[head]
        head_orientation = head_placement.rotation

        tail_orientations = self.cp.pieces[tail.id].orientations  # available rotations for the tail

        # OPTIMIZE? use the cache when available
        # first put the update cache before the forward check in the backtracking recursion
        # then check if cache if present and if so, use it for IFP
        # one advantage of this method is that, whether fixed or floating origin,
        # we recompute the domain from scratch (but for NFP union) so floating container positions
        # are reseted and always correct: no need to differentiate both cases
        # however, the most expensive is NFP and unfortunately nothing guarantees that IPF \ NFP with previous layout
        # is really cheaper than current domain \ last NFP
        # disadvantage: requires cache, but strongly advised anyway
        # disadvantage: if the next nfp is small, it is more expensive to recompute IFP \ union of NFPs

        for tail_orientation in tail_orientations:
            # REFACTOR: always delegate behavior depending on whether we use floating origin

            # floating-origin only: initialize domain with concrete problem class get_ifp method override
            # get_ifp should not return None as at least one placement has occured, since we filter
            if domains[tail][tail_orientation] is None:
                domains[tail][tail_orientation] = node.cache.get_ifp(tail.id, tail_orientation, dynamic=False)
                # DEBUG
                assert_validity(domains[tail][tail_orientation])
            else:
                # reduce current domain to its intersection with the current IFP
                # this is only required for floating origin problems, where the IFP shrinks progressively
                # since we shrink, we do not lose subtraction of previous NFPs and can just remove
                # the last NFP after that
                # if node.problem.cp.floating_origin:
                domains[tail][tail_orientation] &= node.cache.get_ifp(tail.id, tail_orientation, dynamic=False)
                # DEBUG
                assert_validity(domains[tail][tail_orientation])

            nfp = self.cp.nfps[((head.id, tail.id), (head_orientation, tail_orientation))]
            tr_nfp = nfp.translated(*head_placement.translation)  # do not rotate the NFP! (rotation changes shape)

            # OPTIMIZE: do not repeat the operation on each instance of piece

            # For now, we will not apply domain update to placed pieces as explained (it is normal for a CP)
            # and will be careful if some ops can completely eliminate a solution with a small error.

            # OPTIMIZE: to avoid garbage, use in-place ops you would define in the PWB class
            # (if so, ensure you use deepcopy everywhere so that domains are not shared at all)
            # do it once for all identical shapes! (difficult to combine with generic variable-based FC)
            domains[tail][tail_orientation] = domains[tail][tail_orientation].ifp_sub_nfp(tr_nfp)  # subtract NFP from the domain
            # after updating the domains, must update dynamic domain of this tail too
            # node.update_dynamic_domain(tail)

            # DEBUG
            if domains[tail][tail_orientation] is not None:
                assert_validity(domains[tail][tail_orientation])
            # if dynamic_domains[tail][tail_orientation] is not None:
            #     assert_validity(dynamic_domains[tail][tail_orientation])


class NestingCache(Cache):
    """
    Cache for nesting problem CP search

    Access cached values with get(value_name)
    If values are not cached, they will be computed in runtime
    For now, accessing a non-cached value twice in a row will make this object compute it twice
    (no automatic caching)
    This also means we can directly test the presence in the keys of cache_dict to check if a value
    is currently cached, and must be cached, rather than using a cached_values attribute.
    So cached_values is not stored as an attribute, use 'in self.cached_dict' instead

    Caching is not an actual cache here, and is only computed by increments. Indeed, when we need a value
    at one step of the search we often need it at every step, so it is meaningful to compute it for each step
    whatever.

    However, since many nodes are expanded (branching factor d) but only a few are popped (the best heuristics),
    it may also be interesting to use lazy computation + caching in case we need to compute the values
    on node expansion sometimes but on popping at other times.

    --> requires dirty attribute + update on last step

    The node reference is not stored, so that deepcopy works fine to transmit a cache to a successor node.
    To update cached values, however, we need a node reference, so the update must be called from the node,
    passing itself as parameter.

    If only a part of a value is required, as the NFP of two specific pieces, specific get / compute can
     be added.
    Otherwise, to avoid recomputing the whole dictionary of NFPs each time we seek one of them, we could
     use automatic caching.
    Of course, the best is to declare the NFPs as cached if you use them.

    Cached values names:

    nfps
    ifps
    dynamic_ifps
    free_space
    dynamic_free_space
    contact_graph
    domains
    dynamic_domains

    Meaning
        nfps
        Actually the nfps_with_current_layout: dict[(str, float), PolygonWithBoundary]
        Dictionary of NFP with the union of all pieces placed previously,
        indexed by piece id - orientation tuple

        ifps: dict[(str, float), PolygonWithBoundary]
        Dictionary of IFP of pieces, indexed by piece id - orientation tuple
        It is useful to cache them for floating origin problems because the ifps
        will get updated on the first placement (from None values).
        For other placements, they are strictly equal to those in the ifps members
        of the InputMinimizationProblem.

        dynamic_ifps: dict[(str, float), PolygonWithBoundary]
        dynamic version

        free_space: MultiPolygon
        Set of positions that are either not intersection or touching any placed piece
            for that assignment. It is not dependent on any pieces nor on their reference points.

        dynamic_free_space: MultiPolygon
        dynamic version

        contact_graph: ContactGraph

        domains:
        placement domains

        dynamic_domains:
        placement domains for the dynamic length

    Attributes
        :type cache_dict: dict[str, X]
        :param cache_dict: internal dictionary of cached values, with a key only for cached values

    """
    def __init__(self, node=None, cache_dict=None, cached_values=None, delay_per_value=None):
        """
        :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
        :param node: node associated to this cache

        :type cached_values: list[str]
        :param cached_values: list of attribute names that must be cached (other values are not updated)
            Used only when cached_dict is None

        """
        super(NestingCache, self).__init__(node, cache_dict, cached_values, delay_per_value,
                                           non_incremental_values=['ifps', 'dynamic_ifps',
                                                                   'dynamic_free_space',
                                                                   'dynamic_domains'],
                                           dynamic_values=['dynamic_ifps', 'dynamic_free_space', 'dynamic_domains'])

    # REFACTOR: use assignment or node? node contains information on last variable assigned, useful for update
    def get_nfp(self, piece_id, orientation):
        """
        Retrieve or compute nfp of remaining piece_id for orientation with layout in current node
        Helper method, can be inlined without any problems

        :param assignment:
        :param piece_id:
        :param orientation:
        :return:
        """
        # we must call get() not the cache_dict get to update the cache appropriately
        return self.get('nfps')[(piece_id, orientation)]

    def get_ifp(self, piece_id, orientation, dynamic):
        """
        Retrieve or compute dynamic of max ifp of remaining piece_id for orientation with layout in current node
        Helper to avoid producing the string 'ifps' or 'dynamic_ifps' each time we want to get it

        :param piece_id:
        :param orientation:
        :param dynamic:
        :return:
        """
        prefix = 'dynamic_' if dynamic else ''
        value_name = prefix + 'ifps'
        # we must call get() not the cache_dict get to update the cache appropriately
        return self.get(value_name)[(piece_id, orientation)]

    def get_initial_nfps(self):
        """
        Return the initial value of NFPs with the current layout for each piece
        The initial NFPs are all empty

        :rtype: dict[(str, float), PolygonWithBoundary]

        """
        return {(piece_id, orientation): PolygonWithBoundary() for piece_id in self.cp.pieces
                for orientation in self.cp.pieces[piece_id].orientations}

    def get_initial_ifps(self):
        """
        :rtype: dict[(str, float), PolygonWithBoundary]

        """
        return self.cp.ifps

    def get_initial_free_space(self):
        """
        :rtype: MultiPolygon

        """
        # IMPROVE: dynamic container to better COP search
        # initial free space is container space, but convert it to MultiPolygon for easy iteration
        # (we lose the advantage of using a PolygonExtended in the conversion process)
        maximum_container = self.cp.get_maximum_container({})  # result is None for floating-origin, first iteration
        return get_closed_interior_as_multipolygon(maximum_container) if maximum_container is not None else None

    def get_initial_contact_graph(self):
        """
        :rtype: ContactGraph

        """
        return ContactGraph()

    def get_initial_domains(self):
        """
        :rtype: NestingDomain

        """
        return deepcopy(self.cp.domains)

    # not used for non-incremental cache
    # but probably better for GC to have an init and update the content, even non-incrementally

    # def get_initial_dynamic_domains(self):
    #     """
    #     :rtype: NestingDomain
    #
    #     """
    #     if self.cp.open_dim:
    #         # should be a COP, create dynamic domains for dynamic IFPs
    #         ifps = self.cp.generate_ifps_for_container(self.cp.lot, self.cp.pieces,
    #                                                       self.cp.container_height,
    #                                                       self.node.dynamic_cost)
    #         return self.cp.create_domains_from_ifps(ifps)
    #     else:
    #         # in CSP, dynamic domains are the same are the maximum domains
    #         # just return the problem domains (IFPs) copied, in case dynamic domains are initialized
    #         # before the max domains
    #         return deepcopy(self.cp.domains)
    #         # return deepcopy(self.cache_dict['domains'])

    def compute_nfps(self, node):
        """
        Direct NFP union computation for all pieces (not recommended)

        :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode

        """
        nfps = {}
        for piece_id in node.remaining_piece_ids:
            for orientation in self.cp.pieces[piece_id].orientations:
                nfps[(piece_id, orientation)] = self.compute_nfp(node.assignment, piece_id, orientation)
        return nfps

    def compute_nfp(self, assignment, piece_id, orientation):
        """
        Direct NFP union computation (not recommended)

        :type assignment: ippy.utils.hashabledict.hashdict[PieceKey, ippy.packmath.problem.Placement]
        :param piece_id: Piece ID of a remaining piece
        :param orientation: orientation of the remaining piece

        """
        # very low performance, and floating errors occur
        nfp_with_previous_layout = self.cp.data.get_nfp_with_previous_layout(piece_id, orientation, assignment)
        nfp_with_previous_layout = round_coords(nfp_with_previous_layout, 14)
        return nfp_with_previous_layout

    def compute_dynamic_ifps(self):
        """

        :rtype: dict[(str, float), PolygonWithBoundary]

        """
        return self.compute_ifps_for_dynamic(True)

    def compute_ifps(self):
        """

        :rtype: dict[(str, float), PolygonWithBoundary]

        """
        return self.compute_ifps_for_dynamic(False)

    def compute_ifps_for_dynamic(self, dynamic):
        """
        Compute ifps or dynamic_ifps depending on dynamic parameter, for current node

        :param dynamic: True to update dynamic_ifps, False to update ifps
        :rtype: dict[(str, float), PolygonWithBoundary]

        """
        ifps = {}
        for piece_id in self.node.remaining_piece_ids:
            for orientation in self.cp.pieces[piece_id].orientations:
                ifps[(piece_id, orientation)] = self.cp.get_ifp(piece_id, orientation, self.node, dynamic)
        return ifps

    # IFP - NFPS
    # def compute_domains(self):
    #     """
    #     :rtype: NestingDomain
    #
    #     """
    #     return self.get('')

    def compute_dynamic_domains(self):
        """

        """
        # this call will update the max domains if needed
        domains = self.get('domains')
        # OPTIMIZE: memory: replace dynamic domains content instead of creating new dictionary
        dynamic_domains = {}

        # OPTIMIZE: compute once per piece ID, copy result for each instance
        for piece_key in self.cp.variables:
            dynamic_domains[piece_key] = NestingDomain()
            for orientation in self.cp.pieces[piece_key.id].orientations:
                # optionally create singletons separately for assigned vars (or group with the rest below, as the IFP
                # should not disturb the singleton anyway)
                if piece_key in self.node.assignment:
                    dynamic_domains[piece_key][orientation] = domains[piece_key][orientation]
                else:
                    # OPTIMIZE: make this test once, by checking any key with any orientation (one domain is None iff all are)
                    if domains[piece_key][orientation] is None:  # FIXME? self.domains?? will probably not happen as not the initial node?
                        # do not just return, you need to put None for each orientation!
                        dynamic_domains[piece_key][orientation] = None
                    else:
                        # OPTIMIZE: if intersection is too slow, try difference with the complement (right to infinity part of max IFP)
                        dynamic_domains[piece_key][orientation] = domains[piece_key][orientation] & \
                            self.get_ifp(piece_key.id, orientation, dynamic=True)

        return dynamic_domains

    def compute_dynamic_free_space(self):
        """"""
        free_space = self.get('free_space')
        if free_space is None:
            return None  # FO, first move

        dynamic_container = self.cp.get_current_container(self.node)
        # if one some vertical lines remain, remove them, and be sure to convert to multipolygon
        dynamic_free_space = get_closed_interior_as_multipolygon(free_space & dynamic_container)
        return dynamic_free_space

    def copy_cache_dict(self):
        """
        Return a copy of the cache dict, at the appropriate depth so that
        computation is cheap but there is no risk to modify shared information between caches

        :rtype: NestingCache

        """
        # delegate deepcopy to dict and PolygonWithBoundary
        # MultiPolygon is immutable, you can copy reference safely
        # REFACTOR: create FreeSpace or MultiPolygonExtended class so that you can tune deepcopy
        # to avoid copying a geometry, then you can just call deepcopy of self.cache_dict
        cache_dict_copy = {}
        for key, value in self.cache_dict.iteritems():
            if key == 'free_space':
                # shallow copy (shared reference)
                cache_dict_copy[key] = value
            else:
                # deep copy
                cache_dict_copy[key] = deepcopy(value)

        return cache_dict_copy

    def update_nfps_on_assignment(self, next_node):
        """
        Update nfps in cache of piece shapes that have to be placed later, orbiting around already placed
        pieces given through placements.

        :type next_node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
        :param next_node:

        """
        placements = next_node.assignment
        last_piece_key = next_node.variable_assigned
        remaining_piece_ids = next_node.remaining_piece_ids
        cp = next_node.problem.cp

        # for all remaining piece shapes...
        for piece_id in remaining_piece_ids:
            # add the union of the translated NFP to the current NFP
            # (this may include another instance with the same id as the last placed piece)
            # because this class does not inherit from ConstraintProblem, we do not have a variables attribute
            # so we use the lot to access piece keys
            # later, will probably move this code somewhere else and then we can use a more direct access

            # translate nfp as the last placed piece was translated
            placement = placements[last_piece_key]
            for remaining_piece_orientation in cp.pieces[piece_id].orientations:
                tr_nfp = next_node.problem.cp.nfps[
                    ((last_piece_key.id, piece_id), (placement.rotation, remaining_piece_orientation))]. \
                    translated(*placement.translation)
                # nfp with previous layout always consider the next piece as the orbiting piece
                self.cache_dict['nfps'][(piece_id, remaining_piece_orientation)] |= tr_nfp

        # in addition, optionally remove the NFP for the piece shape placed if we placed the last instance
        # of that ID (not needed anymore)
        if last_piece_key.id not in remaining_piece_ids:
            for orientation in cp.pieces[last_piece_key.id].orientations:
                del self.cache_dict['nfps'][(last_piece_key.id, orientation)]

    def update_free_space_on_assignment(self, next_node):
        """
        Update the free space attribute of cache by removing the space occupied by the last piece placed.

        :type next_node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
        :param next_node:

        """
        cache = next_node.cache
        placements = next_node.assignment
        last_piece_key = next_node.variable_assigned
        cp = next_node.problem.cp

        free_space = self.cache_dict['free_space']

        # floating-origin problems need to reduce free space based on updated container first
        if cp.floating_origin:
            # reduce free space in space if the container has shrunk

            # if free space has not been initialized, init it with maximum container
            # otherwise, reduce it by intersection with shrunk container, so that space occupied by previous pieces is kept
            if free_space is None:
                free_space = get_closed_interior_as_multipolygon(cp.get_maximum_container(placements))
            else:
                # here is the only place where get_closed_interior is really necessary: an intersection can reduce a Polygon
                # to a LineString
                # OPTIMIZE? Convert to multipolygon only once, for the final update of free space?
                # (can use get() &= but less intuitive)
                free_space = free_space & get_closed_interior_as_multipolygon(cp.get_maximum_container(placements))

        last_piece_geom = cp.pieces[last_piece_key.id].geom
        last_placed_piece = place_geom(last_piece_geom, placements[last_piece_key])
        last_placed_piece = round_coords(last_placed_piece)
        # getting closed interior is not necessary after a difference, but keep multipolygon type
        free_space = get_closed_interior_as_multipolygon(free_space - last_placed_piece)
        # ROUNDING: we can obtain flat polygons after that operation
        # besides, is_valid does not return False... as_multipolygon is not good?
        self.cache_dict['free_space'] = free_space

    def update_contact_graph_on_assignment(self, next_node):
        """
        Update the contact graph with the last placement in next_node

        :type next_node: NestingAssignmentSearchNode

        """
        last_piece_key = next_node.variable_assigned
        last_placement = next_node.value_assigned

        # always add the node for the last piece, even if no contacts
        self.cache_dict['contact_graph'].add_node(last_piece_key)
        # print 'CONTACT GRAPH: Adding node {}'.format(last_piece_key)

        # for each piece placed previously, verify if the last piece touches it
        for previous_piece_key in next_node.parent.assignment:
            previous_piece_placement = next_node.assignment[previous_piece_key]
            # compute NFP of last placed piece with previously placed piece (only consider translation of previous piece,
            # since the translation of the last piece is already used in the contact test)
            nfp = next_node.problem.cp.nfps[(
            (previous_piece_key.id, last_piece_key.id), (previous_piece_placement.rotation, last_placement.rotation))]. \
                translated(*previous_piece_placement.translation)
            # test for touch (we assume there is no overlap so we just test for touch or overlap)
            if nfp.contains_or_touch(last_placement.translation):
                self.cache_dict['contact_graph'].connect_nodes(previous_piece_key, last_piece_key)
                # print 'CONTACT GRAPH: Connect nodes {} - {}'.format(previous_piece_key, last_piece_key)

    def update_domains_on_assignment(self, next_node):
        """
        Update the domains of all pieces with the last placement in next_node

        :type next_node: NestingAssignmentSearchNode

        """
        forward_check(next_node, revised_node=self.node)


class ContactGraph(object):
    """
    Contact graph

    Attributes:
        :type nodes: list[PieceKey]
        :param nodes: piece keys already placed
        :type connected_nodes_dict: dict[PieceKey, list[PieceKey]]
        :param connected_nodes_dict: dictionary of successors of each node, indexed by node

    """
    def __init__(self):
        self.nodes = []
        self.connected_nodes_dict = {}

    def __repr__(self):
        return '<ContactGraph: {}, {}>'.format(str(self.nodes), str(self.connected_nodes_dict))

    def __str__(self):
        return '<ContactGraph: {}>'.format(', '.join('{}: {}'.format(k, v) for k, v in self.connected_nodes_dict.iteritems()))

    def add_node(self, piece_key):
        """
        Add the piece key to the list of nodes, with no connections

        :type piece_key: ippy.packmath.problem.PieceKey

        """
        self.nodes.append(piece_key)
        self.connected_nodes_dict[piece_key] = []

    def connect_nodes(self, node1, node2):
        """
        Connect both nodes symmetrically

        :type node1: ippy.packmath.problem.PieceKey
        :type node2: ippy.packmath.problem.PieceKey

        """
        self.connected_nodes_dict[node1].append(node2)
        self.connected_nodes_dict[node2].append(node1)

    def get_nb_edges(self, node):
        """
        Return the number of edges connected to node (bidrectional but counted once)

        :rtype: int

        """
        return len(self.connected_nodes_dict[node])


class InputMinimizationNestingProblem(object):
    """
    2D Nesting problem where a fixed set of polygonal pieces must be completely placed inside a rectangular container,
    without overlap between the pieces.

    Variable: PieceKey
        (piece_id, instance_idx)
    Value: Placement
        Placement(translation, rotation)
    Domain: list[(float, PolygonWithBoundary)]
        [(rotation0, fit-polygon for rotation0), ...]

    Attributes
        :type data: ippy.packmath.problem.ExpandedNestingProblem
        :param data: the problem data from which this problem was created. Also contains helper methods.

        :type name: str
        :param name: human-readable name of the problem

        :type floating_origin: bool
        :param floating_origin: is the problem a floating-origin variant?

        :type open_dim: bool
        :param open_dim: is the problem an open dimension variant?

        :type lot: dict[PieceKey, PolygonExtended]
        :param lot: lot of pieces to place, indexed by (piece id, instance index)

        :type pieces: dict[str, ippy.packmath.problem.Piece]
        :param pieces: dictionary of piece ID - Piece pairs, useful to retrieve piece quantity and orientations

        :type nfps: dict[((str, str), (float, float)), PolygonWithBoundary]
        :param nfps: polygons of two pieces from the origin, indexed by (piece id, piece id)

        :type ifps: dict[(str, float), PolygonWithBoundary]
        :param ifps: inner-fit polygons of each piece, indexed by piece id
            All the piece IDs must have an entry in this dictionary, so that all domains are defined.

    """
    __metaclass__ = ABCMeta
    __hash__ = None

    def __init__(self, data, name, floating_origin, open_dim, lot, pieces, nfps, ifps):
        self.data = data
        self.name = name
        self.floating_origin = floating_origin
        self.open_dim = open_dim
        self.lot = lot
        self.pieces = pieces
        self.nfps = nfps
        self.ifps = ifps

    def __repr__(self):
        return '<InputMinimizationNestingProblem "{0}": lot {1}}>'.format(self.name, self.lot)

    def __str__(self):
        return '{0} (input minimization)'.format(self.name)

    def __eq__(self, other):
        return isinstance(other, InputMinimizationNestingProblem) and \
            self.name == other.name and self.lot == other.lot and \
            self.nfps == other.nfps and self.ifps == other.ifps

    def __ne__(self, other):
        return not self.__eq__(other)

    # python allows to use a method from an unrelated blase class (InputMinimizationNestingProblem) to
    # implement an abstract method from some abstract class (ConstraintProblem)
    # In C++, would need to delegate the call.
    def create_domain_singleton(self, piece_key, placement):
        """
        Return a dictionary of PolygonWithBoundary where only the rotation associated to the placement
         is not empty, located a the placement's point.

        :type piece_key: PieceKey
        :type placement: Placement
        :rtype: NestingDomain
        :return: NestingDomain({rotation: point as PolygonWithBoundary})

        """
        return NestingDomain.create_singleton(piece_key, placement, self.pieces)

    def create_domains_from_ifps(self, ifps):
        variables = sorted(self.lot.keys())  # REFACTOR: let input minimization problem be aware of the CP
        domains = {
            piece_key: NestingDomain({
                # if IFP is None (no restriction), create a None domain (same meaning), else use the IFP
                orientation: None if ifps[(piece_key.id, orientation)] is None else ifps[(piece_key.id, orientation)]
                for orientation in self.pieces[piece_key.id].orientations
                })
            for piece_key in variables
        }
        return domains

    # same as above
    def get_initial_cache(self, initial_node, cached_values=None):
        """
        Return cache with empty nfp with previous layout.

        :type cached_values: list[str]
        :rtype: NestingCache

        """
        return NestingCache(initial_node, cached_values=cached_values)

    @deprecated('Never update yourself, wait for the cache to do so')
    def update_cache(self, node):
        """
        Update the NFP with the previous layout considering the last piece placed.
        Placements have already been updated with the last piece.

        :type node: ippy.packmath.constraintsearch.AssignmentSearchNode

        """
        cache = node.cache
        if 'ifps' in cache.cache_dict:
            cache.update('ifps')
        if 'nfps' in node.cache.cache_dict:
            cache.update('nfps')
        if 'free_space' in node.cache.cache_dict:
            cache.update('free_space')
        # TODO: update contact graph, but timing may be a little different

    # OLD version for no-graph constraint search... will not work because called methods have changed!
    @deprecated(update_cache)
    def update_cache_constraint(self, cache, placements, last_piece_key):
        """
        Update the NFP with the previous layout considering the last piece placed.
        Placements have already been updated with the last piece.

        :type cache: NestingCache
        :type placements: dict[PieceKey, Placement]
        :type last_piece_key: PieceKey

        """
        remaining_piece_ids = self.get_remaining_piece_ids(placements)
        self.update_cache_ifps(cache)
        self.update_cache_nfps(cache)
        self.update_cache_free_space(cache)

    def get_piece_ids(self):
        """
        Return the sorted list of all piece IDs.

        :rtype: list[str]

        """
        return sorted(set(piece_key.id for piece_key in self.lot))

    # REFACTOR: move methods that use a placements parameter to NestingAssignmentSearchNode?
    def get_remaining_piece_ids(self, placements):
        """
        Return the sorted list of IDs of pieces remaining after placements.

        :type placements: dict[PieceKey, Placement]
        :rtype: list[str]

        """
        # REFACTOR: the first line corresponds to get_unassigned_variables in ConstraintProblem, unfortunately
        # the class in unrelated... by refactoring the class tree we can solve this
        unassigned_variables = (piece_key for piece_key in self.lot.iterkeys() if piece_key not in placements)
        remaining_piece_ids = set(piece_key.id for piece_key in unassigned_variables)
        return sorted(remaining_piece_ids)

    def get_remaining_piece_instance_counter(self, placements):
        """
        Return the number of remaining instances per piece, as a dict

        :type placements: dict[PieceKey, Placement]
        :rtype: dict[str, int]

        """
        # get all IDs of unplaced pieces, repeating for multiple instances
        remaining_piece_redundant_ids = (piece_key.id for piece_key in self.lot.iterkeys() if piece_key not in placements)
        return Counter(remaining_piece_redundant_ids)

    def get_all_piece_instance_counter(self):
        """
        Return a dictionary containing the total number of instances per piece ID.
        It is a problem data, independent from any assignment.

        :rtype: dict[str, int]

        """
        # count number of instances for each piece (we do not have access to piece quantity directly)
        return Counter((piece_key.id for piece_key in self.lot.iterkeys()))

    def get_placed_piece_instance_counter(self, placements):
        """
        Return a dictionary containing the number of placed instances per piece ID.
        For piece with no instancec placed yet, the ID does not appear as a key in the dictionary.

        :type placements: dict[PieceKey, Placement]
        :rtype: dict[str, int]

        """
        # count number of instances for each placed piece
        return Counter((piece_key.id for piece_key in placements))

    def get_sum_area_remaining_pieces(self, placements):
        """
        Return the sum of the areas of all remaining pieces in placements

        :param placements:
        :return:

        """
        return sum(self.pieces[piece_id].geom.area * count for piece_id, count
                   in self.get_remaining_piece_instance_counter(placements).iteritems())

    def get_sum_area_all_pieces(self):
        """
        Return the sum of the areas of all pieces placed in placements

        :param placements:
        :return:

        """
        # OPTIMIZE: cache this value
        return sum(self.pieces[piece_id].geom.area * count for piece_id, count
                   in self.get_all_piece_instance_counter().iteritems())

    def get_sum_area_placed_pieces(self, placements):
        """
        Return the sum of the areas of all pieces placed in placements

        :param placements:
        :return:

        """
        return sum(self.pieces[piece_id].geom.area * count for piece_id, count
                   in self.get_placed_piece_instance_counter(placements).iteritems())

    def get_total_vertices(self):
        """
        Return the total number of vertices over all the pieces

        :rtype: int

        """
        return sum(get_nb_vertices(self.pieces[piece_id].geom) * count for piece_id, count
                   in self.get_all_piece_instance_counter().iteritems()
                   )

    @abstractmethod
    def get_container_height(self):
        """
        Return the height of the container.

        :rtype: float

        """

    @abstractmethod
    def get_current_container(self, node):
        """
        Return the container, fixed or floating, with the fixed length for CP, dynamic length for COP

        :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
        :rtype: PolygonExtended

        """

    @abstractmethod
    def get_maximum_container(self, placements):
        """
        Return the maximum possible container where next pieces can be placed without breaking the
        container unary / global constraints, depending on the problem type.

        For floating origin problems, the container is fictive, and for open dimension problems,
        the maximum width is just a hint.

        :rtype: PolygonExtended

        """

    @abstractmethod
    def get_ifp(self, piece_id, orientation, node, dynamic=False):
        """
        Return IFP of piece with orientation, from container bounds and previous placements,
        based on the problem type

        :param dynamic:
        :type orientation: float
        :rtype: PolygonWithBoundary

        """

    # REFACTOR: logically, this should be in a superclass of floating origins only,
    # but we have no such things for now. The closest would be an Interface in Java or C#
    def get_ifp_for_floating_origin_container_bounds(self, piece_id, orientation, container_size, placements):
        """
        Return the IFP of piece_id inside floating container_bounds for previous placements,
        for a given orientation.
        Used in get_ifp of floating origin problems.

        :type piece_id: str
        :param orientation: orientation of the piece in degrees
        :type placements: dict[PieceKey, Placement]
        :type container_size: (float, float)
        :return: PolygonWithBoundary

        """
        if not placements:
            # this is the first piece, return None to indicate absence of constraint
            # (actually the ALL set, but cannot be represented in Shapely)
            return None
        else:
            # generate limit domain considering all pieces already placed
            placed_pieces_bounds = get_layout_bounds(self.data.expanded_lot, placements)
            next_piece_geom = self.pieces[piece_id].geom  # no rotation yet

            # start from the floating container limit bounds, ignoring bounds of the moving object B
            # (or think of B as a point)
            x_min, y_min, width, height = get_floating_container_limit_bounds(placed_pieces_bounds, container_size)
            # let function specialized in IFP deduce it, it will handle empty and flat cases correctly
            ifp = get_ifp_with_rectangular_container(next_piece_geom, width, height, x_min, y_min, orientation)
            return ifp

    @abstractmethod
    def get_length(self, assignment):
        """"""


class SingleBinSizeBinPackingProblem(InputMinimizationNestingProblem, ConstraintSatisfactionProblem):
    """
    2D Nesting problem where a fixed set of polygonal pieces must be completely placed inside a rectangular container
    of fixed dimensions, without overlap between the pieces.

    It is a CSP where:
    - variables are piece keys
    - values are placements, domain is R^2 x R (translation x rotation)
    - domain is encoded as a list of rotation - fit-polygon tuple, where the fit-polygon
    corresponds to the set of feasible positions for the given rotation
    list<(float, PolygonWithBoundary)>
    Ex: [(0, IFP(piece 2, rot=0) \ NFP(piece1, piece2, rot=0)),
         (pi/2, IFP(piece 2, rot=pi/2) \ NFP(piece1, piece2, rot=pi/2))]
    # if required, we wil use a dictionary indexed by rotation
    # At the moment, rotations are not supported so the domain is always [(0, ...)]
    - unary constraints are that each piece is inside the container
    - binary constraints are that two pieces do not overlap
    - an assignment is a layout's placements dictionary

    Attributes
        :container: (string, PolygonExtended)
            - tuple of container piece id and fixed container geometry

    """
    def __init__(self, data, name, floating_origin, lot, pieces, nfps, container, ifps=None):
        """

        :param floating_origin:
        :type data: ExpandedNestingProblem
        :type ifps: dict[(str, float), PolygonWithBoundary]
        :param ifps: dictionary of IFPs, key = (piece ID, orientation)

        """
        InputMinimizationNestingProblem.__init__(self, data, name, floating_origin, False, lot, pieces, nfps, ifps)

        variables = sorted(lot.keys())  # sorted by piece id, then by instance index

        domains = self.create_domains_from_ifps(ifps)

        ConstraintSatisfactionProblem.__init__(self, variables, domains=domains)
        self.register_binary_constraint_all_vars(NoOverlapConstraint(self))

        self.container = container

    def __repr__(self):
        return '<SingleBinSizeBinPackingProblem "{0}": lot {1}, container {2}>' \
            .format(self.name, self.lot, self.container)

    def __str__(self):
        return '{0} (single bin size)'.format(self.name)

    def __eq__(self, other):
        # we do not compare data, ExpandedNestingProblem has no __eq__ yet
        return isinstance(other, SingleBinSizeBinPackingProblem) and \
            self.name == other.name and self.lot == other.lot and self.nfps == other.nfps and \
            self.container == other.container and self.ifps == other.ifps

    def __ne__(self, other):
        return not self.__eq__(other)

    def get_container_height(self):
        return get_size(self.container[1])[1]


class FixedOriginSingleBinSizeBinPackingProblem(SingleBinSizeBinPackingProblem):
    """
    SingleBinSizeBinPackingProblem description where the container has a fixed position and
    unary constraints are defined on these positions.

    Additional problem description:
    - unary constraints are that each piece is inside the container

    """

    def __init__(self, data, name, lot, pieces, nfps, container, ifps):
        """

        :param floating_origin:
        :type data: ExpandedNestingProblem
        """
        super(FixedOriginSingleBinSizeBinPackingProblem, self).__init__(data, name, False, lot, pieces, nfps, container, ifps)
        self.register_unary_constraint_all_vars(self.unary_constraint)

    def __repr__(self):
        return '<FixedOriginSingleBinSizeBinPackingProblem "{0}": lot {1}, container {2}>' \
            .format(self.name, self.lot, self.container)

    def __str__(self):
        return '{0} (single bin size, fixed origin)'.format(self.name)

    def __eq__(self, other):
        return isinstance(other, FixedOriginSingleBinSizeBinPackingProblem) and \
            self.name == other.name and self.lot == other.lot and self.nfps == other.nfps and \
            self.container == other.container and self.ifps == other.ifps

    def __ne__(self, other):
        return not self.__eq__(other)

    def unary_constraint(self, piece_key, placement):
        """
        Check inside container constraint

        Return True if the piece key variable is assigned a placement so that the corresponding
        piece geometry is inside the container

        :param piece_key:
        :param placement:
        """
        placed_piece = self.lot[piece_key].place_copy(placement)
        return placed_piece.check_if_bounds_inside(self.container[1].bounds)

    def get_current_container(self, node):
        """
        Return the fixed container with fixed length

        :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
        :rtype: PolygonExtended

        """
        return self.container[1]  # dynamic cost should be equal to fixed length

    def get_maximum_container(self, placements):
        """
        Return fixed container geometry. Placements are unused

        :param placements:
        :rtype: PolygonExtended

        """
        return self.container[1]

    def get_ifp(self, piece_id, orientation, node, dynamic=False):
        """
        Return ifps directly from pre-computations. Placements is not used,
        but still here for a common signature.
        Dynamic is not used either because container size is fixed.

        :param dynamic:
        :param orientation:
        :rtype: PolygonWithBoundary

        """
        return self.ifps[(piece_id, orientation)]

    def get_length(self, assignment):
        """
        Return the width of the bounding box of all placed pieces, i.e. the minimum width
        of the container that could contain all the pieces.

        :param assignment: dictionary of {piece_key: placement}
        :return: float - width of the bounding box of all placed pieces
        """
        if assignment:
            # since we support rotation, we must not convert to bounding box before placing (includes rotating)
            placed_lot_bounds = get_layout_bounds(self.lot, assignment)
            # in fixed origin, the bottom-left of the container is always at the origin; length = xMax of the placed lot
            return placed_lot_bounds[2]
        else:
            return 0

class FloatingOriginSingleBinSizeBinPackingProblem(SingleBinSizeBinPackingProblem):
    """
    SingleBinSizeBinPackingProblem description where the container has a floating position and there is one
    global (N-ary, N number of pieces) constraint that uses the bounding box of the final placement and compare it to the size of the container.

    Additional problem description:
    - global constraint: the bounding box of all pieces would fit inside the floating rectangular container

    The ifps are still accessible from the data (ExpandedNestingProblem), but since they lose their meaning
    in floating origin, they should not be used directly. Instead, the container size should always be used.


    """

    def __init__(self, data, name, lot, pieces, nfps, container):
        """

        :param floating_origin:
        :type data: ExpandedNestingProblem
        """
        # for cache, prepare None ifp for each piece ID
        piece_ids = set(piece_key.id for piece_key in data.expanded_lot.keys())
        ifps = dict.fromkeys([(piece_id, orientation) for piece_id in piece_ids
                              for orientation in pieces[piece_id].orientations])

        super(FloatingOriginSingleBinSizeBinPackingProblem, self).__init__(data, name, True, lot, pieces, nfps, container, ifps)
        self.register_global_constraint(self.global_constraint)

    def __repr__(self):
        return '<FloatingOriginSingleBinSizeBinPackingProblem "{0}": lot {1}, container {2}>' \
            .format(self.name, self.lot, self.container)

    def __str__(self):
        return '{0} (single bin size, floating origin)'.format(self.name)

    def __eq__(self, other):
        return isinstance(other, FloatingOriginSingleBinSizeBinPackingProblem) and \
            self.name == other.name and self.lot == other.lot and self.nfps == other.nfps and \
            self.container == other.container

    def __ne__(self, other):
        return not self.__eq__(other)

    def global_constraint(self, placements):
        """
        Return True if the bounding box of all placed pieces fits into a floating container
        Always True if no placements

        :param placements:

        """
        if not placements:
            return True

        placement_bounds = get_layout_bounds(self.lot, placements)
        container_bounds = self.container[1].bounds

        return can_bounds_fit(placement_bounds, container_bounds)

    def _get_container_with_size(self, placements, size):
        """
        Return the floating container for the given width (problem default height is used)

        :param placements:
        :param size:
        :return:
        """

    def get_current_container(self, node):
        """
        Return the fixed container with fixed length

        :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
        :rtype: PolygonExtended

        """
        return self.get_maximum_container(node.assignment)

    def get_maximum_container(self, placements):
        """
        Return floating container geometry.

        :param placements:
        :rtype: PolygonExtended

        """
        # no container defined before 1st placement in floating origin
        if not placements:
            return None

        placed_pieces_bounds = get_layout_bounds(self.lot, placements)
        floating_container_limit_bounds = get_floating_container_limit_bounds(placed_pieces_bounds, get_size(self.container[1]))
        return PolygonExtended(box(*floating_container_limit_bounds))

    def get_ifp(self, piece_id, orientation, node, dynamic=False):
        """
        Return fictive IFP from container bounds and previous placements


        :param piece_id:
        :param orientation:
        :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
        :param node: node providing the placements from which to compute the IFP
        :type dynamic: bool
        :param dynamic: should we use dynamic length?
        :rtype: PolygonWithBoundary

        """
        container_size = get_size(self.container[1])
        if dynamic:
            container_size = node.dynamic_cost, container_size[1]
        return self.get_ifp_for_floating_origin_container_bounds(piece_id, orientation, container_size, node.assignment)

    def get_length(self, assignment):
        if assignment:
            # since we support rotation, we must not convert to bounding box before placing (includes rotating)
            bounding_box_size = to_size(get_layout_bounds(self.lot, assignment))
            return bounding_box_size[0]
        else:
            return 0.


class OpenDimensionProblem(InputMinimizationNestingProblem, ConstraintOptimizationProblem):
    """
    2D Nesting problem where a fixed set of polygonal pieces must be completely placed inside a rectangular container
    of variable width, without overlap between the pieces and with a minimal container width.

    It is a constraint optimization problem where:
    - variables are piece keys
    - values are placements, domain is R^2 x R (translation x rotation)
    - unary constraints are that each piece is vertically between 0 and container_height,
      horizontally on the right of x = 0
    - binary constraints are that two pieces do not overlap
    - an assignment is a layout's placements dictionary
    - the cost is the width of the bounding box of all the placed pieces

    Attributes
        :name: string
            - human-readable name of the problem
        :lot: dict<(string, int), PolygonExtended>
            - lot of pieces to place, indexed by (piece id, instance index)
        :container_height: float
            - fixed height of the container
        :max_container_width: float
            - maximum, fixed width of the container (very huge and probably never reached, unlike the floating width)
    """

    def __init__(self, data, name, floating_origin, lot, pieces, nfps, ifps, max_container_width, container_height):
        InputMinimizationNestingProblem.__init__(self, data, name, floating_origin, True, lot, pieces, nfps, ifps)

        variables = sorted(lot.keys())  # sorted by piece id, then by instance index

        # use the None domain convention to indicate there are no restrictions (the ALL set)
        domains = self.create_domains_from_ifps(ifps)

        ConstraintOptimizationProblem.__init__(self, variables, domains)
        self.register_binary_constraint_all_vars(NoOverlapConstraint(self))

        self.max_container_width = max_container_width
        self.container_height = container_height

    def __repr__(self):
        return '<OpenDimensionProblem "{0}": lot {1}, container height {2}}>' \
            .format(self.name, self.lot, self.container_height)

    def __str__(self):
        return '{0} (variable width)'.format(self.name)

    def __eq__(self, other):
        return isinstance(other, OpenDimensionProblem) and \
            self.name == other.name and self.lot == other.lot and self.nfps == other.nfps and \
            self.container_height == other.container_height

    def __ne__(self, other):
        return not self.__eq__(other)

    # TODO: add test
    def get_min_cost(self):
        """
        Return the extreme minimum width in case all pieces can change shape but not area
        and stack completely on the left of the container.

        :return:
        """
        # Scrivener: Problem, CSP Representation
        # W_m = A/H = S(A[i])/H
        # OPTIMIZE: cache this value
        total_area_pieces = self.get_sum_area_all_pieces()
        # total_area_pieces = sum(piece_geom.area for piece_geom in self.lot.itervalues())
        return total_area_pieces / self.container_height

    @deprecated('use dynamic_cost and dynamic_domains')
    def update_data_on_cost(self, max_width):
        """
        Update the problem data IFPs based on the max width.

        """
        # REFACTOR: prefer modifying the domains
        # semantically, the problem data does not change when trying a parameter or limiting the cost!
        self.data.set_container_size(width=max_width)
        # TODO: also update domains
        # TEMP for uniform access as with CSP
        self.ifps = {piece_id: ifp for (container_id, piece_id), ifp in self.data.piece_ifps.iteritems()}
        self.ifps = self.generate_ifps_for_container(self.lot, self.container_height, max_width)

    @abstractmethod
    def generate_ifps_for_container(self, lot, pieces, container_height, max_container_width):
        """
        Generate ifps for a container of size (container_height, max_container_width), for each piece id

        lot is passed as argument because it may not be initialized yet in __init__
        and this method is used in __init__; otherwise we could just use self.lot

        same for pieces

        """

    def get_container_height(self):
        return self.container_height


class FixedOriginOpenDimensionProblem(OpenDimensionProblem):
    """
    Fixed origin version of the OpenDimensionProblem.

    Additional problem description:
    - unary constraints are that each piece is inside the container

    """

    def __init__(self, data, name, lot, pieces, nfps, max_container_width, container_height):
        # Generate ifps from that biggest possible container
        ifps = self.generate_ifps_for_container(lot, pieces, container_height, max_container_width)

        super(FixedOriginOpenDimensionProblem, self).__init__(data, name, False, lot, pieces, nfps, ifps, max_container_width,
                                                              container_height)
        self.register_unary_constraint_all_vars(self.unary_constraint)

    def __repr__(self):
        return '<FixedOriginOpenDimensionProblem "{0}": lot {1}, container height {2}}>' \
            .format(self.name, self.lot, self.container_height)

    def __str__(self):
        return '{0} (variable width)'.format(self.name)

    def __eq__(self, other):
        return isinstance(other, FixedOriginOpenDimensionProblem) and \
            self.name == other.name and self.lot == other.lot and self.nfps == other.nfps and \
            self.container_height == other.container_height

    def __ne__(self, other):
        return not self.__eq__(other)

    def unary_constraint(self, piece_key, placement):
        """
        Check inside variable width container constraint

        Return True if the piece key variable is assigned a placement so that the corresponding
        piece geometry is inside a fictive container box (0, 0, +inf, container height)

        """
        placed_piece = self.lot[piece_key].place_copy(placement)
        return placed_piece.check_if_bounds_inside((0, 0, float('+inf'), self.container_height))

    def get_cost(self, assignment):
        return self.get_length(assignment)

    def get_length(self, assignment):
        """
        Return the width of the bounding box of all placed pieces, i.e. the minimum width
        of the container that could contain all the pieces.

        :param assignment: dictionary of {piece_key: placement}
        :return: float - width of the bounding box of all placed pieces
        """
        if assignment:
            # since we support rotation, we must not convert to bounding box before placing (includes rotating)
            placed_lot_bounds = get_layout_bounds(self.lot, assignment)
            # in fixed origin, the bottom-left of the container is always at the origin; length = xMax of the placed lot
            return placed_lot_bounds[2]
        else:
            return 0

    def get_current_container(self, node):
        """
        Return the fixed container with dynamic length

        :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
        :rtype: PolygonExtended

        """
        return PolygonExtended(box(0, 0, node.dynamic_cost, self.container_height))

    def get_maximum_container(self, placements):
        """
        Return semi-fixed container geometry, with utmost width.

        :param placements:
        :rtype: PolygonExtended

        """
        return PolygonExtended(box(0, 0, self.max_container_width, self.container_height))

    def get_ifp(self, piece_id, orientation, node, dynamic=False):
        """
        Return big ifps directly from pre-computations. Placements is not used,
        but still here for a common signature.

        :param dynamic:
        :param orientation:
        :rtype: PolygonWithBoundary

        """
        if dynamic:
            return get_ifp_with_rectangular_container(self.pieces[piece_id].geom,
                                               node.dynamic_cost,
                                               self.container_height,
                                               orientation=orientation)
        else:
            return self.ifps[(piece_id, orientation)]

    def generate_ifps_for_container(self, lot, pieces, container_height, max_container_width):
        """
        Generate ifps for a container of size (container_height, max_container_width), for each piece id

        Lot is passed because this method is also called in initialization, where self.lot is not initialized yet.
        Same for pieces.

        """
        ifps = {}
        piece_ids = pieces.keys()  # cannot use self.get_piece_ids() if lot != self.lot
        for piece_id in piece_ids:
            # REFACTOR: prefer storing piece geom from piece_id, and piece_keys separately to get variables too
            # unfortunately, see docstring, we cannot use self.pieces for now
            for orientation in pieces[piece_id].orientations:
                ifps[(piece_id, orientation)] = get_ifp_with_rectangular_container(lot[(piece_id, 0)],
                                                                                   max_container_width,
                                                                                   container_height,
                                                                                   orientation=orientation)
        return ifps


class FloatingOriginOpenDimensionProblem(OpenDimensionProblem):
    """
    Floating origin version of the OpenDimensionProblem.

    Additional problem description:
    - unary constraints are that each piece is inside the container

    """

    def __init__(self, data, name, lot, pieces, nfps, max_container_width, container_height):
        # for cache, prepare None ifp for each piece ID
        ifps = self.generate_ifps_for_container(lot, pieces, max_container_width, container_height)

        super(FloatingOriginOpenDimensionProblem, self).__init__(data, name, True, lot, pieces, nfps, ifps,
                                                                 max_container_width, container_height)

    def __repr__(self):
        return '<FloatingOriginOpenDimensionProblem "{0}": lot {1}, container height {2}}>' \
            .format(self.name, self.lot, self.container_height)

    def __str__(self):
        return '{0} (variable width)'.format(self.name)

    def __eq__(self, other):
        return isinstance(other, FloatingOriginOpenDimensionProblem) and \
            self.name == other.name and self.lot == other.lot and self.nfps == other.nfps and \
            self.container_height == other.container_height

    def __ne__(self, other):
        return not self.__eq__(other)

    def global_constraint(self, placements):
        """
        Return True if the bounding box of all placed pieces fits into a floating container,
        in height.
        Always True if no placements.

        """
        if not placements:
            return True

        placement_bounds = get_layout_bounds(self.lot, placements)
        placement_height = to_size(placement_bounds)[1]

        return placement_height <= self.container_height

    def get_cost(self, assignment):
        return self.get_length(assignment)

    def get_length(self, assignment):
        """
        Return the width of the bounding box of all placed pieces, i.e. the minimum width
        of the container that could contain all the pieces.

        :param assignment: dictionary of {piece_key: placement}
        :rtype: float
        :return: width of the bounding box of all placed pieces
        """
        if assignment:
            # since we support rotation, we must not convert to bounding box before placing (includes rotating)
            bounding_box_size = to_size(get_layout_bounds(self.lot, assignment))
            return bounding_box_size[0]
        else:
            return 0.

    def _get_container_with_width(self, placements, width):
        """
        Return the floating container for the given width (problem default height is used)

        :param placements:
        :param width:
        :return:
        """
        # no container defined before 1st placement in floating origin
        if not placements:
            return None

        placed_pieces_bounds = get_layout_bounds(self.lot, placements)
        floating_container_limit_bounds = get_floating_container_limit_bounds(placed_pieces_bounds, (
            width, self.container_height))
        return PolygonExtended(box(*floating_container_limit_bounds))

    def get_current_container(self, node):
        """
        Return the floating container with dynamic length

        :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
        :rtype: PolygonExtended

        """
        return self._get_container_with_width(node.assignment, node.dynamic_cost)

    def get_maximum_container(self, placements):
        """
        Return floating container geometry.

        :param placements:
        :rtype: PolygonExtended

        """
        # no container defined before 1st placement in floating origin
        return self._get_container_with_width(placements, self.max_container_width)

    def get_ifp(self, piece_id, orientation, node, dynamic=False):
        """
        Return fictive IFP for a piece with some orientation, from maximized container bounds

        :param dynamic:
        :param orientation: orientation of the piece

        """
        if dynamic:
            container_width = node.dynamic_cost
        else:
            container_width = self.max_container_width
        return self.get_ifp_for_floating_origin_container_bounds(piece_id, orientation,
                                                                 (container_width, self.container_height),
                                                                 node.assignment)

    def generate_ifps_for_container(self, lot, pieces, container_height, max_container_width):
        """
        Return None ifps for each piece id (container sizes are not used in this override)

        :rtype: dict[str, PolygonWithBoundary]

        """
        piece_ids = pieces.keys()
        return dict.fromkeys([(piece_id, orientation) for piece_id in piece_ids
                              for orientation in pieces[piece_id].orientations])
