"""
Module containing dynamic cost functions for the open dimension packing problem

"""
from ippy.utils.packdecorator import null_for_initial_node

__author__ = 'huulong'


def get_current_dynamic_cost(node):
    """
    A dummy dynamic cost evaluator that returns the current dynamic cost
    It has no effect on the dynamic cost so a COP search will keep its initial cost
    (probably the min cost) and if no solutions exist for that the search will continue
    until the fringe is empty

    :param node:
    :return:

    """
    return node.dynamic_cost

# must not be null for initial node, because we need to extend from the start sometimes
def get_min_length_for_non_empty_domain(node):
    """
    Generic cost extension function that returns the minimum dynamic cost required
    so that each piece has a non empty domain.

    We use it for length but as long as get_min_cost_for_non_empty_domain in implemented
    on your node, it can be applied to any COP

    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
    :rtype: float

    """
    unassigned_variables = node.unassigned_variables

    # start from current cost and increase when needed
    min_cost = node.dynamic_cost

    for tail in unassigned_variables:
        # if tail has a non empty dynamic domain, no need to extend
        if node.dynamic_domains[tail].is_empty:
            # increase_cost_limit(node)
            # update the min cost to just the cost needed to have a non-empty domain for that variable
            min_cost_for_non_empty_domain = node.get_min_cost_for_non_empty_domain(tail)
            if min_cost_for_non_empty_domain > min_cost:
                min_cost = min_cost_for_non_empty_domain

    return min_cost


# use area_based_final_length_estimation
@null_for_initial_node
def estimate_final_length_from_usage(node):
    """
    Evaluate the current packing usage (density) and estimate the final length of the layout
    from there.

    Return the estimated final length

    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
    :rtype: float

    """
    cp = node.problem.cp

    # start from current cost and increase when needed
    min_cost = node.dynamic_cost

    # evaluate usage in the bounding box of length: the layout length and height: the container height
    # in floating origin the length is the X span of the layout, in fixed origin it it the span
    # from 0 to the right of the layout, so delegate this calculation to get_cost()
    layout_length = cp.get_cost(node.assignment)
    container_height = cp.get_container_height()
    full_height_bb_area = layout_length * container_height

    placed_pieces_area = cp.get_sum_area_placed_pieces(node.assignment)

    local_usage = placed_pieces_area / full_height_bb_area

    # find ratio of the area of placed pieces / area of all pieces
    completion_ratio = placed_pieces_area / cp.get_sum_area_all_pieces()

    # final length * height = final full height BB area = sum area all pieces / usage
    # which is estimated to sum area all pieces / (placed pieces area / current full height bb area)
    # so final length estimation = sum area all pieces / sum area placed pieces * current bb length
    # which is exactly area_based_final_length_estimation heuristics!!

    # I keep this function in case the calculation changes a bit (the hull mon which local usage
    # is computed may change) but otherwise just use the heuristics, it has the same signature

    # another way to see it: local usage / completion ratio = ...
    return 0
