from collections import namedtuple
import itertools
import logging

import numpy as np
from numpy import float_ as fl
from shapely.geometry import Polygon, box
from shapely.ops import unary_union
from shapely.validation import explain_validity

from ippy.packmath.nfp import nfp_cuninghame_green_with_convex_decomposition, get_ifp_with_rectangular_container
from ippy.packmath.packhelper import get_layout_bounds
from ippy.utils.decorator import deprecated
from ippy.utils.geometry import get_translation_bounds_to_fit_floating_container
from ippy.utils.shapelyextension import GeomWrapper, PolygonWithBoundary, to_size, get_size, PolygonExtended, \
    check_equal, round_coords
from ippy.view.plot import plot_geom_colors

__author__ = 'hs'

# Constraints

# Cache data
# Probably better on solver side, but since we use it on problem side for now we should put it here
# or in a third module, to avoid cyclic import


# ALTERNATIVE to multiple inheritance: each sub-class has its own name, lot attributes
# and the method check_no_overlap_constraint becomes a function with lot passed as first parameter


# REFACTOR: can we create a flexible problem with modules we can select in real-time?

class NestingProblem(object):
    """
    Description of a nesting problem.

    Attributes
        :type container: (str, PieceData)
        :param container:
            A tuple (pieceID, piece data)
        :type lot: dict[string, PieceData]
        :param lot:
             {piece_id: piece data} dictionary of pieces to place in the problem
        metadata
            A dictionary of additional information (derived or heuristics) to ease the search process.
            Both pieces can rotate, although the relative orientation is what affects the shape of the NFP.
            #Only piece B can be rotated, piece A is assumed to have orientation=0.
            #In order to retrieve an NFP for any orientation, you must use the relative orientation
            #orientation_pieceB - orientation_pieceA, then rotate the NFP reference shape by orientation_pieceA.
            {
                'nfps': {
                    ((id_pieceA, id_pieceB), (orientation_pieceA, orientation_pieceB)): {
                        'idComponentsTypes': [(nfpAB_id_polygon1, 1), (nfpAB_id_hole1, -1), ...],
                        'idBoundary': nfpAB_id_boundary
                    },
                    ...
                },
                'ifps': {((id_container, id_polygonA), orientation_pieceA): {[same content as nfps]}}, ...}}
        :type polygons: dict[str, LinearRing]
        :param polygons:
            A dictionary of polygonID - polygon exterior. For piece exterior and holes, NFP exterior and holes.
        boundaries
            A dictionary of Point and Line-like geometries representing individual boundaries for pieces.
            {boundaryID: GeometryCollection/(Multi)Point/(Multi)LineString}
        metadescription
            A dictionary of meta-information on the problem with the following keys:
                name
                    A name to recognize the problem when debugging.
                author
                date
                description
                verticesOrientation
                coordinatesOrigin

    """
    # TODO: a Problem should be linked to a Layout in an intuitive way
    # Therefore, it would be better to expand multiple pieces inside the problem and give them
    # unique names from the beginning.
    # Otherwise, the layout would have to provide a dual key for each placement entry:
    # piece ID and index of the actual instance of the piece to handle multiple pieces with the same shape
    last_id = 0

    def __init__(self, container, lot, metadata, polygons, raw_boundaries, name=None, author='Unknown', date='unknown',
                 description='No description.', verticesOrientation='counterclockwise', coordinatesOrigin='down-left'):
        self.container = container
        self.lot = lot
        self.metadata = metadata
        self.polygons = polygons
        self.raw_boundaries = raw_boundaries
        if name is None:
            name = 'Problem #{}'.format(NestingProblem.last_id)
        self.metadescription = {'name': name,
                                'author': author,
                                'date': date,
                                'description': description,
                                'verticesOrientation': verticesOrientation,
                                'coordinatesOrigin': coordinatesOrigin}
        NestingProblem.last_id += 1

    def __repr__(self):
        return "<NestingProblem {3}: ({0}, {1}, {2})>".format(self.polygons, repr(self.container), self.lot, self.metadescription)

    def __str__(self):
        return "<NestingProblem {3}: ({0}, {1}, {2})>".format(map(str, self.polygons), self.container, self.lot, self.metadescription)

    def __eq__(self, other):
        # test normal equality for primitive types, .equals() equality for Geometry objects
        # use check_equal() to compare non-empty geometries with some tolerance
        # print('Nesting Problem __eq__ test')
        return isinstance(other, type(self)) and \
            self.container[0] == other.container[0] and self.container[1] == other.container[1] and \
            self.lot == other.lot and \
            self.metadata == other.metadata and \
            len(self.polygons.keys()) == len(other.polygons.keys()) and \
            sorted(self.polygons.keys()) == sorted(other.polygons.keys()) and \
            all(check_equal(self.polygons[polygon_id], other.polygons[polygon_id]) for polygon_id in self.polygons.keys()) and \
            self.raw_boundaries == other.raw_boundaries and \
            self.metadescription == other.metadescription

    def __ne__(self, other):
        return not self.__eq__(other)

    # TODO: use a 'raw polygon' for the aggregation data of exterior and hole IDs
    def construct_polygon_with_holes(self, exterior_ids, hole_ids=None):
        """
        Construct a polygon from a list of exterior and hole IDs, and return it.

        :param exterior_ids: list of ID referring to polygons of type "exterior" in the problem
        :param hole_ids: list of IDs referring to polygons of type "hole" in the problem
        :return: Polygon

        """
        # get filled union by taking the union of filled polygons (not exteriors)
        polygons = self.ids_to_polygons(*exterior_ids)
        exterior_union = unary_union(polygons)
        # get all polygon holes by IDs
        hole_union = unary_union(self.ids_to_polygons(*hole_ids))

        polygon_with_holes = exterior_union - hole_union
        return polygon_with_holes

    def construct_boundary(self, boundary_ids):
        """
        Construct a boundary from a list of boundary IDs, and return it.

        :param boundary_ids: list of IDs referring to raw boundary stored in the problem
        :return: BaseGeometry

        """
        return unary_union([self.raw_boundaries[boundary_id].to_geometry() for boundary_id in boundary_ids])

    def construct_nfp(self, nfp_id_data):
        """
        Construct an NFP or an IFP from components and boundary IDs, and return it.

        :param nfp_id_data: dictionary containing IDs of components and boundaries, in the format:
            {
                'idComponentsTypes': [(nfpAB_id_polygon1, 1), (nfpAB_id_hole1, -1), ...],
                'idBoundary': nfpAB_id_boundary
            }
        :return: PolygonWithBoundary

        """
        exterior_ids = []
        hole_ids = []
        for component_id, component_type in nfp_id_data['idComponentsTypes']:

            if component_type == 0:  # exterior
                exterior_ids.append(component_id)
            elif component_type == -1:  # hole
                hole_ids.append(component_id)
            else:
                raise ValueError(
                    'Found component {} of type {}, should be 0 (exterior ring) or -1 (hole)'.format(component_id, component_type))
        closed_interior = self.construct_polygon_with_holes(exterior_ids, hole_ids)
        boundary = self.construct_boundary(nfp_id_data['idBoundaries'])
        # OLD: we do not generate the extra boundary, so please make sure that your XML
        # provides the NFP with *all* the boundaries
        # NEW: to simplify the XMLs, provide only the extra boundary
        # also do this in the XML generation!
        nfp = PolygonWithBoundary(closed_interior, boundary, trivial_boundary_from_interior=True)
        return nfp

    def get_expanded_container(self, fixed_length=None):
        """
        Return the expanded version of the container.
        Used for initialize ExpandedNestingProblem and in unit tests that do not have access to the latter.

        :return: (container piece ID, container Polygon)
        """
        # if fixed length is set, use it to create a rectangular container with the same height
        # as the original container
        original_container_exterior = self.polygons[self.container[1].polygon_id]
        if fixed_length is not None:
            container_geom = box(0, 0, fixed_length, get_size(original_container_exterior)[1])
        else:
            container_geom = Polygon(original_container_exterior)

        # convert LinearRing to Polygon
        return [self.container[0], container_geom]

    def get_expanded_lot(self):
        """
        Return a dictionary of {(pieceID, instanceID): BaseGeometry} by expanding the original lot with quantities.

        Generate geometry of each piece by combining exteriors and interiors refered by the Piece representation
        Since pieces can have multiple quantities, compute the geometry only once,
        then expand if required.

        Use this method to deal with each piece individually instead of having each piece flagged
        with a certain quantity.
        This is especially useful for placement strategies, since most of them consider each piece one by one.

        :rtype: dict[PieceKey, PolygonExtended]

        """
        expanded_lot = {}
        for piece_id, piece in self.lot.iteritems():
            # compute piece geometry, common to all instances (single exterior ring for now)
            piece_geometry = self.construct_polygon_with_holes([piece.polygon_id], piece.hole_ids)

            # set the computed shape as the value for each id-instance key
            for instance_idx in xrange(piece.quantity):
                expanded_lot[PieceKey(piece_id, instance_idx)] = PolygonExtended(piece_geometry)
        return expanded_lot

    def get_piece_dict(self, expanded_lot):
        """
        Return a dictionary of piece ID - Piece pairs

        :type expanded_lot: dict[PieceKey, PolygonExtended]
        :rtype: dict[str, Piece]

        """
        # we use expanded lot not to repeat computations
        # REFACTOR: compute directly the Piece geometries here, and let lot contain information on piece keys only
        # something like:
        # return {piece_id: Piece(qty, orients, self.construct_polygon_with_holes(...)) for piece_id, piece_data in self.lot.iteritems()}
        piece_dict = {}
        for piece_key in expanded_lot.iterkeys():
            piece_data = self.lot[piece_key.id]
            piece_dict[piece_key.id] = Piece(piece_data.quantity, piece_data.orientations, expanded_lot[piece_key])
        return piece_dict

    def get_predefined_nfp(self, piece1_id, piece2_id, orientation1, orientation2):
        """
        If NFP(piece1_id, piece2_id) or NFP(piece2_id, piece1_id) has been predefined with components and boundary IDs,
        for the given orientations of piece 1 and piece 2,
        build it, oppose geometry if necessary and return it.
        Else, return None.

        This only works with an NFP, since IFP are not symmetrical.

        :param piece1_id: string
        :param piece2_id: string
        :param orientation1: orientation of piece1 (degrees)
        :param orientation2: orientation of piece2 (degrees)
        :return: PolygonWithBoundary or None
        """
        # look for existing raw nfp in problem intermediate results first
        nfp_id_data_dict = self.metadata['nfps']
        is_nfp_opposed = False
        nfp_id_data = nfp_id_data_dict.get(((piece1_id, piece2_id), (orientation1, orientation2)))
        if nfp_id_data is None:
            # look for NFP(piece2, piece1) = -NFP(piece1, piece2) instead, with orientations also swapped
            nfp_id_data = nfp_id_data_dict.get(((piece2_id, piece1_id), (orientation2, orientation1)))
            is_nfp_opposed = True

        if nfp_id_data is None:
            # no data on NFP composition found
            # possible improve: check orientation difference to see if there is no NFP shape with that
            # relative orientation, then rotate it to get the actual NFP for our couple of rotations
            # this only works efficiently if the computed all NFPs for relative orientations for
            # rotation of A set to 0 when parsing XML -> NestingProblem
            return None

        nfp = self.construct_nfp(nfp_id_data)
        if is_nfp_opposed:
            nfp.scale(-1)
        return nfp

    @deprecated(get_expanded_lot)
    def get_individual_piece_list(self):
        """
        Return a list of individual polygons as GeomWrapper from the lot. Use this method to deal with each piece
        individually instead of having each piece flagged with a certain quantity. This is especially useful for
        placement strategies, since most of them consider each piece one by one.

        Caution: individual pieces are represented by GeomWrapper, not Piece, which is used for quantified pieces

        :return: list<GeomWrapper>

        """
        individual_piece_list = []
        for piece in self.lot.itervalues():
            for i in xrange(piece.quantity):
                # name the individual piece polygonX_1, polygonX_2, etc.
                # (names are unique as long as no other pieces use polygon X)
                individual_piece = GeomWrapper(self.polygons[piece.polygon_id],
                                               piece.polygon_id,
                                               '{0}_{1}'.format(piece.polygon_id, i))
                individual_piece_list.append(individual_piece)
        return individual_piece_list

    @deprecated(get_expanded_lot)
    def get_piece_instance_dict(self):
        """
        Return a dict of individual polygons *by string ID* for (piece_id, instance_idx) keys.
        Use this method to deal with each piece individually instead of having each piece flagged
        with a certain quantity.
        This is especially useful for placement strategies, since most of them consider each piece one by one.

        Caution: individual pieces are represented by GeomWrapper, not Piece, the latter being is used for quantified pieces

        :return: list<string>

        """
        piece_instance_dict = {}
        for piece_id, piece in self.lot.iteritems():
            for idx in xrange(piece.quantity):
                piece_instance_dict[(piece_id, idx)] = piece.polygon_id
        return piece_instance_dict

    @deprecated(get_predefined_nfp)
    def get_nfp(self, piece1, piece2, nfp_type='nfp'):
        """
        :param piece1: Piece
        :param piece2: Piece
        :param nfp_type: 'nfp' or 'ifp'
        :return: NFP or IFP Geom of piece1 and piece2, depending on type; None if not present
        """
        nfp_id = self.metadata['{}s'.format(nfp_type)].get((piece1.polygon_id, piece2.polygon_id))
        if nfp_id is None:
            return None
        return self.polygons[nfp_id]

    def ids_to_polygons(self, *ids):
        """Build and return a list of polygons upon LinearRings (exterior / holes), given the sequence of their ids"""
        return [Polygon(self.polygons[id]) for id in ids]


class ExpandedNestingProblem(object):
    """
    An expanded version of the Nesting Problem that contains direct references
    to geometric shapes, missing NFP and other additional information created
    in runtime by search strategies.

    It is possible to alter the original problem by passing a fixed_length argument,
    in order to search for a specific container length.

    Attributes
        name
            string
            Name of the problem
        expanded_container
            list(string, BaseGeometry) [containerPieceID, container geometry]
        expanded_lot
            dict<(string, int), BaseGeometry> {(pieceID, #instance): piece geometry}
            Dictionary of actual piece geometry, by ID. BaseGeometry is generally a Polygon.
        :type pieces: dict[str, Piece]
        :param pieces: dictionary of piece id - Piece pairs, containing quantity, orientation and geometry information
        piece_ifps
            dict<((string, string), (float, float)): PolygonWithBoundary>
            Dictionary of ifp between couple of pieces, identified by a couple of IDs and a couple of rotation angles.
            The container ID can be retrieved independently and the container rotation should always be 0.
        piece_nfps
            dict<((string, string), (float, float)): PolygonWithBoundary>
            Dictionary of nfp between couple of pieces, identified by a couple of IDs and a couple of rotation angles.
            Values are actual NFPs, not IDs for faster access and because if a piece is composed of
            multiple polygons, the NFP will not correspond to a single polygon in the problem data.

    """
    def __init__(self, problem, fixed_length=None):
        """
        :type fixed_length: float
        :param fixed_length: if not None, will replace original length

        """
        self.name = problem.metadescription['name']

        # compute expanded versions of the container and lot
        self.expanded_container = problem.get_expanded_container(fixed_length)
        self.expanded_lot = problem.get_expanded_lot()
        self.pieces = problem.get_piece_dict(self.expanded_lot)

        # compute nfp for each couple of piece, using pre-computed nfps when available
        self.piece_nfps = self._generate_piece_nfps(problem)
        # same for ifps
        self.piece_ifps = self._generate_piece_ifps(problem, fixed_length)

    def __repr__(self):
        return "<ExpandedNestingProblem {3}: ({0}, {1}, {2})>".format(self.name, repr(self.expanded_container), self.expanded_lot)

    def __str__(self):
        return "<ExpandedNestingProblem {3}: ({0}, {1}, {2})>".format(self.name, self.expanded_container, self.expanded_lot)

    def _generate_piece_nfps(self, problem):
        """
        Generate and return a dictionary of NFP geometries per couple of piece IDs.

        PRECONDITION:
            self.pieces has already been assigned with problem.get_piece_dict()

        """
        logging.debug('Generating piece NFPs for {}...'.format(self.name))

        # OPTIMIZE? only compute NFPs when required during runtime search
        # However, most search strategies will require nfps as long as 2 pieces can possibly enter the same container
        # so in most cases computing all nfps between different pieces + reflective nfps for pieces with 2+ instances
        # should be profitable
        # Note: if some strategies were not using NFP at all, move this to a special method called by NFP-based
        # strategies only
        piece_nfps = {}

        # non-deterministic: dictionary key iteration order; use sorted(dict.keys()) for deterministic order
        # of course, if there is no bug, the final NFP will be equivalent
        for (piece1_id, piece1), (piece2_id, piece2) in itertools.combinations_with_replacement(self.pieces.iteritems(), 2):

            # nfp is useful for different pieces, or a couple of identical pieces if there are 2+ instances
            if piece1_id != piece2_id or piece1.quantity > 1:

                # for each orientation of A and B, get or compute the NFP(A, B, orientA, orientB)
                for orientation1, orientation2 in itertools.product(piece1.orientations, piece2.orientations):

                    # NOTE: get_predefined_nfp internally reverse an NFP if only NFP(B, A) is found,
                    # so there may be a redundant reversal lower in the code; not very costly and part of
                    # pre-computation though
                    nfp = problem.get_predefined_nfp(piece1_id, piece2_id, orientation1, orientation2)
                    # probably not required but round data obtained from file just in case
                    # (also useful we processed data from file a little)
                    if nfp is not None:
                        nfp = round_coords(nfp, 14)

                    # if nothing found, compute it now (not recommended)
                    else:
                        # compute NFP from scratch (must choose a method here)
                        # OPTIMIZE: replace with burke when complete
                        nfp = nfp_cuninghame_green_with_convex_decomposition(piece1.geom, piece2.geom,
                                                                             orientation1, orientation2)

                    # check NFP closed interior validity
                    if not nfp.closed_interior.is_valid:
                        logging.debug('NFP closed interior: {!s}'.format(nfp.closed_interior))
                        plot_geom_colors((nfp.closed_interior, 'r'))
                        raise ValueError('NFP obtained / produced is invalid! Cause: {}'
                                         .format(explain_validity(nfp.closed_interior)))

                    # add both nfp and reversed nfp
                    piece_nfps[((piece1_id, piece2_id), (orientation1, orientation2))] = nfp
                    reversed_nfp = nfp.scaled(-1)
                    piece_nfps[((piece2_id, piece1_id), (orientation2, orientation1))] = reversed_nfp

        return piece_nfps

    def _generate_piece_ifps(self, problem, fixed_length=None):
        """
        Generate and return a dictionary of IFP geometries per couple of piece IDs.

        Must be called after expanded_container has been set.
        Should not rely on the original problem to ensure stability.

        :type problem: NestingProblem
        :param fixed_length: if not None, we enforce IFP computation since metadata may be invalid
            (exact value of fixed_length does not matter)

        """
        logging.debug('Generating piece IFPs for {}...'.format(self.name))

        piece_ifps = {}
        container_piece_id, container = self.expanded_container
        for piece_id, piece in self.pieces.iteritems():
            # for each orientation of the moving piece, get or compute the IFP with the container
            for orientation in piece.orientations:
                ifp_id_data = None

                if fixed_length is None:
                    # look for existing ifp in problem intermediate results first (container rotation is 0)
                    ifp_id_data = problem.metadata['ifps'].get(((container_piece_id, piece_id), (0, orientation)))
                    # ifp is not symmetrical (container should be first), so don't try IFP(container, piece)
                    if ifp_id_data is not None:
                        ifp = problem.construct_nfp(ifp_id_data)

                if ifp_id_data is None:  # either because fixed length was set or no data could be found
                    # compute IFP by itself (we assume the container is rectangular, bottom-left origin)
                    # polygons contain LinearRings, but since only the bounds matter, it works
                    ifp = get_ifp_with_rectangular_container(piece.geom, container.bounds[2], container.bounds[3],
                                                             orientation=orientation)
                # REFACTOR: you can already consider container id and container rotation=0 as implicit
                # anyway, the last level of problem definition (Constraint Problem) will consider so
                piece_ifps[((container_piece_id, piece_id), (0, orientation))] = ifp  # I checked, all branches define ifp

        return piece_ifps

    def set_container_size(self, width=None, height=None):
        """
        Set the container to a rectangle of size (width, height) and update the IFPs accordingly
        but direct computation.

        :param width:
        :param height:

        """
        original_size = get_size(self.expanded_container[1])
        if width is None:
            width = original_size[0]
        if height is None:
            height = original_size[1]

        self.expanded_container[1] = box(0, 0, width, height)
        for piece_key, _ in self.expanded_lot.iteritems():
            ifp = get_ifp_with_rectangular_container(self.expanded_lot[piece_key], width, height)
            self.piece_ifps[(self.expanded_container[0], piece_key.id)] = ifp

    def get_piece_geoms(self, id_instance_tuple_list):
        """
        Return the list of piece geometries corresponding to id-instance tuples.

        Only IDs are considered since instances of the same ID are identical,
        but this method is useful to work with data structure containing both IDs and
        instance indices, such as the keys of a NestingLayout's placements.

        """
        return [self.expanded_lot[piece_id_instance_tuple] for piece_id_instance_tuple in id_instance_tuple_list]

    @deprecated('InputMinimizationNestingProblem.get_ifp')
    def get_ifp_deprecated(self, piece_id, floating_origin=False, placements=None, override_container_width=None):
        """
        Return inner-fit polygon of a piece inside the problem's container.
        If floating_origin is True, previous placements are considered and the extreme positions
        of the next piece so that all pieces can fit a floating container are used as the boundary
        of a fictive IFP. (Even if, technically, it may be twice as big as a regular IFP.)
        Use override_container_width to indicate a specific container width in OpenDimension problems.

        :param piece_id: id of a piece to place, after placements if floating_origin is True
        :param floating_origin: is it a floating-origin problem?
        :param placements: dictionary of previous placements, if floating_origin is True
        :param override_container_width: if set to a float, will be used instead of the problem's container width
        :rtype: (PolygonWithBoundary | None)

        """
        if not floating_origin:
            # pick the regular IFP from the problem data
            return self.piece_ifps[(self.expanded_container[0], piece_id)]
        else:
            # floating-origin: return fictive IFP
            if not placements:
                # this is the first piece, return None to indicate absence of constraint
                # (actually the ALL set, but cannot be represented in Shapely)
                return None
            else:
                # generate limit domain considering all pieces already placed
                placed_pieces_bounds = get_layout_bounds(self.expanded_lot, placements)
                # since we do not know the instance idx, we use 0 so that we are sure there is one
                # REFACTOR: keep polygons dictionary indexed by piece_id only?
                next_piece_bounds = self.expanded_lot[PieceKey(piece_id, 0)].bounds
                # TODO: does not work for variable width problem
                container_bounds = self.expanded_container[1].bounds
                translation_bounds = get_translation_bounds_to_fit_floating_container(
                    placed_pieces_bounds, next_piece_bounds, to_size(container_bounds))
                # WRONG: see replacement for this deprecated function
                return PolygonWithBoundary(box(*translation_bounds))


    # TODO: make it a static function or put this in a base class of ConstraintSatisfactionProblem
    # because constraintsolver.fit_polygon_vertices_sorter needs it
    # However, we can also provide the problem data and helpers as an instance of ExpandedNestingProblem
    # for easy access to nfps and derived data
    # (this is what we are doing now)
    def get_nfp_with_previous_layout(self, next_piece_id, orientation, placements):
        """
        Return the nfp of piece type #nfp_piece_id with the union of all previously placed pieces in layout,
        when the piece is rotated with orientation.
        The next piece is the orbiting piece.

        :param next_piece_id: id of the piece for which we get the NPF with the previous layout
        :param orientation: orientation of the piece, in degrees
        :param placements:
        :rtype: PolygonWithBoundary
        """
        # OPTIMIZE: use unary_union! -> did not work
        nfp_union = PolygonWithBoundary()

        # translated_nfp_list = []
        for (placed_piece_id, _), placement in placements.iteritems():
            # TODO: if NFP(A,B) is not found, fallback to -NFP(B,A), otherwise compute it in runtime and log it
            # get raw nfp between polygon to place and polygon already place
            # only the polygon shape interests us, hence the ID
            nfp_with_placed_polygon = self.piece_nfps[((placed_piece_id, next_piece_id), (placement.rotation, orientation))]
            # translate this nfp the same amount the placed polygon was translated, to make
            # the collision test meaningful in this context (should be rather cheap)
            translated_nfp = nfp_with_placed_polygon.translated(*placement.translation)

            # current limitations
            if not translated_nfp.closed_interior.type == 'Polygon':
                raise ValueError('NFP closed interior is not a Polygon')

            # translated_nfp_list.append(translated_nfp)

            # PolygonWithBoundary operation
            nfp_union |= translated_nfp

            # only if using unary union
            # if not translated_nfp_list:
            # return PolygonWithBoundary()  # no previous piece, no NFP

        # nfp_union = PolygonWithBoundary.unary_union(translated_nfp_list)
        return nfp_union

    @deprecated('nestingsolver._get_fit_polygon')
    def get_fit_polygon_with_boundary(self, next_piece_id, orientation, placements, floating_origin=False):
        """
        Return PolygonWithBoundary of all positions that fit both previously placed pieces and the container
        of a piece with ID: next_piece_id and for rotation: orientation

        :type next_piece_id: str
        :param next_piece_id: ID of the polygon to place
        :param orientation: orientation of the piece to place
        :type placements: dict[(PieceKey, Placement)]
        :param placements: current layout placements
        :param floating_origin: is it a floating-origin problem?
        :rtype: PolygonWithBoundary
        :return: feasible positions for the next polygon to place

        """
        # get inner fit polygon between polygon to place and container
        # initialize Geometry of feasible positions with this ifp
        ifp = self.get_ifp(next_piece_id, floating_origin, placements)

        # if the IFP is None, it means we are in floating origin and this is the first piece placed
        # so the IFP is all and the NFP is empty, so there are no constraints, that we encode as None (ALL set)
        if ifp is None:
            return None

        nfp_union = self.get_nfp_with_previous_layout(next_piece_id, orientation,
                                                      placements)  # remove union of nfps from the ifp
        feasible_positions_with_boundary = ifp.ifp_sub_nfp(nfp_union)
        return feasible_positions_with_boundary


class PieceData(object):
    """
    Problem piece. Appears in a certain quantity, with certain rotations, and contains polygon components,
    generally one.

    Attributes:
        :type id: str
        :type quantity: int
        :type orientations: list[float]
        :param orientations: list of rotation angles allowed for that piece, in degrees
        :type polygon_id: str
        :type hole_ids: list[str]
        :param hole_ids: Sequence of hole IDs

    """
    def __init__(self, id_, quantity, orientations, polygon_id, hole_ids=None):
        self.id = id_
        self.quantity = quantity
        self.orientations = orientations
        self.polygon_id = polygon_id
        if hole_ids is None:
            hole_ids = []
        self.hole_ids = hole_ids

    def __hash__(self):
        return hash(self.id) ^ hash(self.quantity) ^ hash(self.orientations) ^ hash(self.polygon_id) ^ hash(self.hole_ids) ^ \
            hash((self.id, self.quantity, self.orientations, self.polygon_id, self.hole_ids))

    def __eq__(self, other):
        return isinstance(other, type(self)) and self.id == other.id and \
            self.quantity == other.quantity and self.orientations == other.orientations and \
            self.polygon_id == other.polygon_id and self.hole_ids == other.hole_ids

    def __ne__(self, other):
        return not self.__eq__(other)


class Piece(object):
    """
    Problem piece containing the actual piece geometry.
    Appears in a certain quantity, with certain rotations.

    Used in problem classes that directly manipulate piece geometries, esp. as dictionary value.
    The ID is not self-contained as an attribute and must therefore be indicated as a dict key.

    Attributes:
        :type quantity: int
        :type orientations: list[float]
        :param orientations: list of rotation angles allowed for that piece, in degrees
        :type geom: PolygonExtended
        :param geom: piece geometry

    """
    def __init__(self, quantity, orientations, geom):
        self.quantity = quantity
        self.orientations = orientations
        self.geom = geom

    def __hash__(self):
        return hash(self.quantity) ^ hash(self.orientations) ^ hash(self.geom) ^ \
            hash((self.quantity, self.orientations, self.geom))

    def __eq__(self, other):
        return isinstance(other, type(self)) and self.quantity == other.quantity \
            and self.orientations == other.orientations and self.geom == other.geom

    def __ne__(self, other):
        return not self.__eq__(other)


"""Named tuple for (piece_id, instance_idx) key, indicating a specific instance of piece."""
PieceKey = namedtuple('PieceKey', ['id', 'instance_idx'])
PieceKey.__str__ = lambda x: '{} #{}'.format(x.id, x.instance_idx)


class NestingLayout(object):
    """
    A layout for a packing problem. The layout may be incomplete but valid, incomplete and invalid,
    complete but invalid or complete and valid.

    It also contains the list of all piece keys to place.

    Attributes
        is_complete: bool
            True if all pieces have a placement associated.
            This does not guarantee that the placement is valid.
        is_valid: bool
            True if placement ensures no overlapping between any couple of pieces.
            This does not guarantee that the placement is complete.
            Since the layout does not have access to the container nor the pieces' geometry,
            this attribute must be changed from the exterior.
            Besides, in floating-origin strategies, the placements may not be offset to fit the container yet.
        placements: dict((string, int), Placement)
            A layout for all the pieces in the problem's lot.
            The key is a tuple (piece_id, instance_idx) where piece_id is the name of the piece placed,
            idx is the index of the instance of that piece (from 0 to piece.quantity - 1).
            Most often pieces have a quantity of 1 so the only index is 0.
            The value is the placement of that piece instance.
            If is_complete is True, there should be one key for each piece in the problem lot.
            If is_valid is True, the layout should provide a valid solution to the problem.
        piece_key_list: list<(string, int)>
            List of all the pieces to place. Remains the same during the placements,
            but used to determine whether the layout is complete.

    """
    def __init__(self, piece_key_list, is_valid=True):
        self.is_complete = False
        self.is_valid = is_valid
        self.placements = {}
        self.piece_key_list = piece_key_list

    def __repr__(self):
        return "<Layout: ({0}, {1}, {2})>".format(self.is_complete, self.is_valid, repr(self.placements))

    def __str__(self):
        return "<Layout: ({0}, {1}), {2}>".format(self.is_complete, self.is_valid, self.placements)

    def add_placement(self, piece_id, instance_idx, translation, rotation=0):
        """
        Add placement of PieceKey(piece_id, instance_idx) with given translation and rotation.

        Raise an Exception if the piece key is not expected or the piece is already placed.

        :param piece_id:
        :param instance_idx:
        :param translation:
        :param rotation:

        """
        piece_key = PieceKey(piece_id, instance_idx)

        if piece_key not in self.piece_key_list:
            raise KeyError('Piece key ({}, {}) is not in the list of expected piece keys {}.'
                           .format(piece_id, instance_idx, self.piece_key_list))

        if piece_key in self.placements:
            raise ValueError('Piece with key ({}, {}) has already been placed at {}.'
                             .format(piece_id, instance_idx, self.placements[(piece_id, instance_idx)]))

        self.placements[piece_key] = Placement(translation, rotation)
        if len(self.placements) == len(self.piece_key_list):
            self.is_complete = True


class Placement(object):
    """
    Placement of a polygon. It is not associated to a specific polygon.

    Attributes
        translation
            numpy float[2]: translation of the polygon from its origin
        rotation
            Float: rotation of the polygon around its origin, before being translated, in radians

    """
    def __init__(self, translation, rotation=0):
        self.translation = fl(translation)
        self.rotation = rotation

    def __repr__(self):
        return "<Placement: ({0}, {1})>".format(repr(self.translation), self.rotation)

    def __str__(self):
        return "Placement at ({0}, {1}) (rot={2})".format(self.translation[0], self.translation[1], self.rotation)

    def __eq__(self, other):
        # DEBUG
        print 'Placement __eq__: {} vs {}'.format(self, other)
        print 'Are equal: {}'.format(
            isinstance(other, Placement) and np.allclose(self.translation, other.translation) and \
            self.rotation == other.rotation
        )
        return isinstance(other, Placement) and np.allclose(self.translation, other.translation) and \
            self.rotation == other.rotation

    def __ne__(self, other):
        return not self.__eq__(other)
