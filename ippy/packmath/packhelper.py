import numpy as np
from numpy import float_ as fl

from shapely.affinity import translate
from shapely.geometry.collection import GeometryCollection
from shapely.ops import unary_union
from ippy.utils.shapelyextension import to_bounding_box, get_placed_lot, get_edges, get_edge_length, get_points

__author__ = 'hs'


def get_layout_bounds(lot, placements):
    """
    Return the bounds of the layout, ie the union of all pieces in lot placed with placements.
    Optimized equivalent of get_layout_geometry().bounds, but does not accept empty placements

    :param lot: dict[PieceKey, PolygonExtended]
    :param placements:
    :rtype: (float, float, float, float)

    """
    if not placements:
        raise ValueError('get_lot_bounds_with_placements: cannot compute bounds for empty placements')

    # caution: when supporting rotations, do not compute bounding boxes beforehand: the bounding box
    # change shape with rotation!
    placed_pieces = [lot[piece_key].place_copy(placement) for piece_key, placement in placements.iteritems()]
    # OPTIMIZE? one iteration only (but need to code one's one min/max, so may lose perf too)
    # ALTERNATIVE to optimize: find the appropriate numpy function
    bounding_box_min_x = min(placed_piece.bounds[0] for placed_piece in placed_pieces)
    bounding_box_min_y = min(placed_piece.bounds[1] for placed_piece in placed_pieces)
    bounding_box_max_x = max(placed_piece.bounds[2] for placed_piece in placed_pieces)
    bounding_box_max_y = max(placed_piece.bounds[3] for placed_piece in placed_pieces)
    return bounding_box_min_x, bounding_box_min_y, bounding_box_max_x, bounding_box_max_y

    # piece_bounding_boxes = {piece_key: to_bounding_box(lot[piece_key]) for piece_key
    #                         in placements.iterkeys()}
    # tr_piece_bounding_boxes = [translate(piece_bounding_box, *placements[piece_key].translation)
    #                            for piece_key, piece_bounding_box
    #                            in piece_bounding_boxes.iteritems()]
    # # OPTIMIZE: use update bounds instead of unary union?
    # placement_bounds = unary_union(tr_piece_bounding_boxes).bounds
    # return placement_bounds

def get_lot_bounds(lot):
    """
    Return the bounds of the union of all pieces in the lot,
    assuming pieces have already been translated.

    :param lot:
    :rtype: (float, float, float, float)
    """
    if not lot:
        return GeometryCollection()

    piece_bounding_boxes = [to_bounding_box(lot[piece_key]) for piece_key
                            in lot.iterkeys()]
    # OPTIMIZE: use update bounds instead of unary union?
    placement_bounds = unary_union(piece_bounding_boxes).bounds
    return placement_bounds


def get_layout_geometry(lot, placements):
    """
    Return the union of all the placed pieces as a geometry
    Use this only if you need the complete geometry of the layout, as it is expensive to compute.
    If you only need bounding boxes, use the functions above or get_placed_lot + manual computations

    :param lot:
    :param placements:

    """
    if not placements:
        return GeometryCollection()

    placed_lot = get_placed_lot(lot, placements)

    try:
        layout_geometry = unary_union([placed_piece for placed_piece in placed_lot.itervalues()])
    except ValueError as e:
        print 'unary union detects empty geometry, placed lot is {}, placed pieces are {}' \
            .format(str(placed_lot), [str(placed_piece) for placed_piece in placed_lot.itervalues()])
        raise e

    return layout_geometry


def get_layout_geometry_from_node(node):
    """
    Return the layout geometry for the assignment in node.
    Parameter variant of the above function

    :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode

    """
    return get_layout_geometry(node.problem.cp.lot, node.assignment)

# for regular_picker
def pick_regular_vertices(flatgeom, interval):
    """
    For each LineString or LineString parts in flatgeom,
    pick vertices separated by interval at most.
    For individual points, just pick the point.

    Regularly space the vertices, putting just enough vertices so that the distance between 2 is below interval

    Does not check that some vertices are redundant, currently
    (even using a set, some vertices may be close... we can check that some edges are not too close, though)

    We can also eliminate too close vertices afterward for a complexity of n^2 maybe

    :param flatgeom: 0D - 1D geometry (lines and points)
    :param interval:

    :rtype: list[(float, float)]

    """
    interval = fl(interval)
    edges = get_edges(flatgeom)
    point_coords = [point.coords[0] for point in get_points(flatgeom)]  # convert Point to (x, y-

    regular_vertices = []

    # edges
    for edge in edges:
        # compute minimum number of vertices so that there is a max dist of interval between 2 vertices
        # OPTIMIZE: sqrt is costly, so use squared op if needed
        length = get_edge_length(edge)
        nb_vertices = np.ceil(length / interval) + 1  # nb of vertices = nb of intervals + 1
        # get evenly spaced vertices for both x and y coordinates, and zip to get list of coords
        regular_vertices.extend(zip(*(np.linspace(edge[0][coord], edge[1][coord], nb_vertices) for coord in (0, 1))))

    # points
    regular_vertices.extend(point_coords)

    return regular_vertices
