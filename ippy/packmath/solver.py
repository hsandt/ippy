from abc import ABCMeta, abstractmethod
from copy import deepcopy
import logging

from ippy.packmath.nestingsearch import NFPVNestingSearchProblem, OrderedPlacementState
from ippy.packmath.nfp import get_ifp_with_rectangular_container
from ippy.packmath.packhelper import get_layout_bounds
from ippy.utils.decorator import deprecated
from simpleai.search.traditional import depth_first
from simpleai.search.viewers import ConsoleViewer, WebViewer, BaseViewer

root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)
# root_logger.setLevel(logging.INFO)

import itertools

import numpy as np
from numpy import float_ as fl
from shapely.affinity import translate
from shapely.geometry import box
from ippy.packmath.group import CompositePiece, LeafPiece

from ippy.packmath.problem import NestingLayout, Placement, ExpandedNestingProblem
from ippy.utils.geometry import Position, get_coord_index_for_position, \
    get_sign_for_position, get_translation_bounds_to_fit_floating_container, get_floating_container_limit_bounds
from ippy.utils.shapelyextension import get_unique_coords, PolygonWithBoundary, \
    to_bounding_box, get_bounding_box_area, get_size, is_multi, get_edges_from_coords, get_edge_vector, to_size, \
    bounds_union, get_bounds_area

__author__ = 'hs'


class NestingProblemSolver(object):
    """
    Solver for the packing problem.

    It is a generic class that we can customize during and after initialization
    to define the solving process to use.

    Attributes
        :process:
            The solving process to use.

    """
    def __init__(self, strategy):
        self.strategy = strategy

    def __repr__(self):
        return "<NestingProblemSolver: ({0})>".format(repr(self.strategy))

    def __str__(self):
        return "<NestingProblemSolver: ({0})>".format(self.strategy)

    def search(self, problem):
        """
        Search a solution layout to the problem and return it. If the solver cannot find
        a valid layout, it returns an incomplete layout marked as invalid.

        """
        expanded_problem = ExpandedNestingProblem(problem)
        logging.debug('Running strategy {} on {}'.format(self.strategy, expanded_problem.problem.metadescription['name']))
        return self.strategy.run(expanded_problem)

    def search_variable_width(self, problem, width_span=10):
        """
        Search a solution layout to the problem with a container of variable width,
        and return a tuple (solution layout, container width) for the minimum width for which
        a solution was found.
        To use with a fixed-width strategy.

        The search should always find a solution as long as no piece has a height superior
         to the containers' height (or at least not with all rotations allowed),
         since in worst case, the sum of the width of all pieces should be enough.

         Note that if the strategy is not effective for this problem, the final width will be much larger
         than the actual best width.

        Strategies dedicated to variable width can dynamically modify it.
         Here, however, we use a meta-search that repeats a fixed-width search over
         different widths.
         (Use another method for a search with dynamic width)

        """
        # create the variable width problem: copy the original one, remove IFP references (you can keep IFP geometries)
        # OPTIMIZE: BaseGeometry should copy fine, but it may be costly
        # so if too slow, create you own __deepcopy__ where your copy dictionaries but keep the same geom references
        # FIXME: copying is not good.
        # The Polygon constructor will create a None geometry from a seemingly correct LinearRing (that has a wkt
        # as a LinearString, but closed, by the way), so the unary_union of polygons used to build the expanded lot
        # from polygon parts will fail (even with no union and single-polygon piece it would fail since the geometry
        # is invalid -> Process finished with exit code 139 and tests are missed)

        # DONE in ConstraintOptimizationProblem search
        # Unless this is fixed, I recommend creating a completely separate problem for the variable width problem.
        # It would act as another problem instance for a graph search or any other search actually.
        # It would also be useful to model the search at a higher level. For example, the dichotomy
        # may be a graph search with two nodes (search higher and search lower) and the search gives a higher priority
        # to some nodes depending on the result of the previous sub-search (visiting a node = searching a solution
        # for a given width).
        variable_width_problem = deepcopy(problem)
        variable_width_problem.metadata['ifps'] = {}

        expanded_problem = ExpandedNestingProblem(variable_width_problem)
        logging.info(
            'Running strategy {} on {} repeatedly with variable width'
            .format(self.strategy, expanded_problem.problem.metadescription['name']))

        # start at initial width given by the problem (often very large)
        upper_width, height = fl(get_size(expanded_problem.expanded_container[1]))
        lower_width = fl(0)

        # we assume upper_width is big enough and start from the middle
        # later we may start at upper_width, then 2x if not enough, etc.

        # add a limit to the iterations
        # TODO: copy my code from the Pacman search
        # iter_idx = 0
        # or just use a width precision threshold

        # store last solution, which correspond to the upper bound of the searches
        best_solution = self.strategy.run(expanded_problem)

        assert best_solution.is_complete and best_solution.is_valid, 'Initial container width {} is not enough ' \
                                                                     'to find a solution with the strategy.'

        while upper_width - lower_width > width_span:
            # apply dichotomy [Warning: multiply complexity by the number of dichotomies]
            # ALTERNATIVES: regular intervals (N-chotomy), smooth approach, etc.
            width = (lower_width + upper_width) / 2

            # modify the size of the container to the current search width, then update the generated IFPs
            # caution: the problem still uses the old size; _generate_piece_ifps must only use the expanded problem
            expanded_problem.expanded_container[1] = box(0, 0, width, height)
            # expanded_problem.piece_ifps = expanded_problem._generate_piece_ifps()
            layout = self.strategy.run(expanded_problem)

            # if the layout is valid and complete, we found a solution for this width, which is the new higher bound
            if layout.is_complete and layout.is_valid:
                logging.info('Solution found for width: {}'.format(width))
                upper_width = width
                best_solution = layout
            else:
                logging.info('No solution found for width: {}'.format(width))
                lower_width = width

        # return best solution and best width, which is the current max width
        logging.info('Found solution for width: {} ({})'.format(upper_width, best_solution))
        return best_solution, upper_width

        # TODO: add a method search_from(self, problem, layout) to search
        # from a given layout as a basis; of course, each strategy must
        # implement a way to handle a search half-way


class PackingStrategy(object):
    """
    Abstract class for packing strategies.

    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def run(self, expanded_problem):
        """Run the strategy on the problem and return a layout, valid and complete if any was found."""
        # REFACTOR: avoid returning value on Strategy method and modify layout reference instead?


class SinglePassStrategy(PackingStrategy):
    """
    Abstract class for packing strategies that consist in placing objects
    one after the other and giving up finding a solution if none was found
    on the first run.

    """

# TODO: now that we work with quantities, placement strategies must be adapted by considering multiple times
# the same pieces. It will be easier if we add an intermediate step to transform a lot of pieces with quantities
# to an actual lot of polygons in only 1 exemplary, with some polygons having the same shape, but handled independently
# the only interest of knowing that they are identical to compute NFPs only once and reuse them if the shapes are the same
# this means we should keep track of the polygon ID (provides shape) inside some Polygon Wrapper class or by using dictionary
# keys

class BottomLeftStrategy(SinglePassStrategy):
    """Bottom-left placement strategy with no adjustment"""

    def __init__(self):
        """"""

    def run(self, expanded_problem):
        """"""
        problem = expanded_problem.problem

        # initialize layout, the dictionary of placed polygon ID: placement
        layout = NestingLayout(expanded_problem.expanded_lot.keys())

        # only consider container with one element
        container_piece_id = expanded_problem.problem.container[0]  # container piece id

        piece_instance_dict = problem.get_expanded_lot()

        # pick polygons in order they are given in problem lot
        # for a Python dictionary, this is not reliable but basically
        # means alphabetical order on polygon name
        for piece_id, instance_idx in piece_instance_dict.iterkeys():
        # for geom_wrapper in problem.get_individual_piece_list():

            # compute NFP of this polygon VS container + all previous polygons placed so far
            # if intersection of NFP is empty, no solution, STOP
            # warning: for the container, the NFP is the exterior so available positions are *inside*
            # but for other polygons, the NFP is the interior so available positions are *outside*
            # so substract polygon NFP interior from container NFP interior
            # we assume the offset for the container is always (0, 0)

            # OLD system, leave orientation to 0
            feasible_positions = expanded_problem.get_fit_polygon_with_boundary(piece_id, 0, layout.placements)

            # check if intersection of NFP is empty;
            # if so, it does not mean that there is no solution, just that the bottomleft
            # placement is not enough to find one
            if feasible_positions.is_empty:
                # keep the current layout, it is incomplete by default
                logging.debug('Intersection of complements of NFPs is empty. No solution found.')
                return layout

            # find bottom-left of this intersection of NFP
            # keep track of the last vertex on the bottom bound of the NFP, with the lowest x
            # normally problem orientation does not matter but CCW may be a bit more efficient (fewer update
            # of the bottomleft candidate)
            # TODO: add a graph to illustrate the algorithm in the documentation
            # ALTERNATIVE: for a more intuitive "bottom-left" definition, you could keep the vertex with the minimum
            # vertex.x + vertex.y (*coefficient)

            nfp_intersection_boundary = feasible_positions.boundary
            # TODO: consider cases such as Point
            # see http://stackoverflow.com/questions/21824157/how-to-extract-interior-polygon-coordinates-using-shapely/21922058#21922058
            # for handling Multi-objects

            # logging.debug('Start algo to find bottom-left vertex of ' + str(list(get_unique_coords(nfp_intersection_boundary))))
            bottomleft_vertex = None
            leftmost_x = nfp_intersection_boundary.bounds[2] + 1  # xMax + 1 takes the role of +inf here (+1 to ensure < the 1st time)

            # optional *minor* optimization for Polygons and other looped objects
            # REMOVED because creating a list from an iterator is also slower (if you start with a list, ok)
            coords = get_unique_coords(nfp_intersection_boundary)
            # if len(coords) > 1 and coords[-1] == coords[0]:
            #     coords = coords[0:-1]

            # TODO: reuse get_extreme_vertex_index_value here
            for vertex in coords:
                # logging.debug('Checking vertex ' + str(vertex))
                if vertex[1] == nfp_intersection_boundary.bounds[1]:
                    # logging.debug('Vertex on lower bound of the NFP, compare x')
                    if vertex[0] < leftmost_x:
                        # logging.debug('Vertex more to the left, record it as the best candidate')
                        leftmost_x = vertex[0]
                        bottomleft_vertex = vertex
            if bottomleft_vertex is None:
                logging.debug('Bottom-left algorithm did not work, exit')
                raise Exception('Polygon bounds and boundary coordinates are not consistent, no bottomleft vertex found.')
            # logging.debug('bottomleft_vertex: ' + str(bottomleft_vertex))

            # record the placement in a list
            # TODO: actually translate the polygon to that position and record the translated polygon
            # it would be useful for dynamic algorithms, but for this one we only use NFP from metadata so
            # only storing translations is enough; since our current polygon has (supposedly) no offset, tr = bottomleft vertex's pos
            layout.add_placement(piece_id, instance_idx, bottomleft_vertex)
            logging.debug('Added placement in layout: {} for {}'.format(str(Placement(bottomleft_vertex)), piece_id))

        # if last polygon has been placed, since we used all NFPs, we are sure
        # the polygons are within the container, not intersecting, or just touching each other
        # so this is a solution (layout automatically set to is_complete)
        logging.debug('Valid layout found: ' + str(layout))

        # return it
        # FIXME: prefer modifying out layout on the fly?
        return layout


class NFPVerticesStrategy(PackingStrategy):
    """
    Strategy that consists in testing successively all positions at NFP vertices,
    backtracking to the previous polygon if the current polygon does not fit anywhere,
    until a solution is found.
    Only one order of polygon placement is tried.

    """

    def run(self, expanded_problem):
        """

        :param expanded_problem:
        :return: a layout
        """

        # REFACTOR: factorize with code in other similar strategies

        piece_key_list = expanded_problem.expanded_lot.keys()

        # initialize state with layout
        initial_layout = NestingLayout(piece_key_list)
        initial_state = OrderedPlacementState(piece_key_list, initial_layout, 0)

        # create a console viewer to debug the search
        # viewer = None
        viewer = BaseViewer()
        # viewer = ConsoleViewer(interactive=False)
        # viewer = WebViewer('127.0.0.1')
        # TODO: add a custom viewer that logging.debug the process
        # TODO: add a viewer that creates an animation in Pyplot

        # create a search problem where each action consist in adding a placement to an NFP vertex
        search_problem = NFPVNestingSearchProblem(expanded_problem, initial_state)

        # DFS
        result_node = depth_first(search_problem, viewer=viewer)
        # result_node = depth_first(search_problem)

        # show stats on the problem
        logging.debug('Stats: {}'.format(viewer.stats))
        logging.debug('Events: {}'.format([str(e) for e in viewer.events]))

        # IMPROVE: make search return most advanced node in case no solution is found, so that we can visually debug
        # the partial solution
        if result_node is not None:
        # if layout.is_complete:
        #     assert layout.is_valid, 'Invalid layout found for NFPV strategy, should never happen.'
            logging.debug('Returning complete layout.')  # works on Pycharm Ubuntu
            # logger.debug('Returning complete layout.')
            return result_node.state.layout
        else:
            logging.debug('Returning initial (empty) layout. (to improve, try to return intermediate layout)')
            # logging.debug('Returning incomplete layout.')
            return initial_layout
        # return layout

    def __repr__(self):
        return "<NFPVerticesStrategy>"


class NFPVGStrategy(PackingStrategy):
    """
    No-fit Polygon Vertices with Grouping Strategy.

    Strategy that consists in grouping pairs of pieces if their bounding box waste is low enough,
    then testing successively all positions at NFP vertices,
    backtracking to the previous polygon if the current polygon does not fit anywhere.
    Stop when a complete layout is found.
    Never return an invalid layout.

    TODO: add inter-area / convex hull waste and grouping of 3+ pieces
    TODO: add init parameters to tune the features above

    """
    def __init__(self):
        pass

    def __repr__(self):
        return "<NFPVGStrategy>"

    def run(self, expanded_problem):
        """

        :param expanded_problem:
        :return: a layout
        """
        # find matching pair of pieces and create groups out of them
        self.find_low_bb_waste_pair(expanded_problem)

        # run the usual NFPV strategy
        # DONE as part of CP: backtrack in the grouping if required!
        pass

    @staticmethod
    def find_low_bb_waste_pair(expanded_problem, threshold = 0.1):
        """
        Find matching pairs of pieces by looking at low bounding box waste.
        Return a list of ComponentPieces: CompositePieces for pairs and LeafPieces for single pieces
        (when no matching piece was found, or potential matching pieces were already matched with another one)
        [StC]

        :param expanded_problem:
        :param threshold:
        :return: list<ComponentPiece>
        """
        # consider all possible couples of pieces in the expanded lot
        # (do not mind whether they have already been paired for now)
        # REFACTOR: create all LeafPieces at the beginning, they will be useful anyway

        # list of piece instances that have not been coupled yet (element: tuple (piece_id, instance_idx))
        remaining_piece_instances = expanded_problem.expanded_lot.keys()

        # list of piece instances already grouped (element: tuple (piece_id, instance_idx))
        grouped_piece_instances = []

        # actual groups created
        composite_pieces = []

        # iterate on each couple of piece instance (tuple (piece_id, instance_idx)) with the corresponding geometry
        for (piece_instance1, piece_geometry1), (piece_instance2, piece_geometry2) \
                in itertools.product(expanded_problem.expanded_lot.iteritems(), repeat=2):
            # ignore if twice the same instance of one of the pieces is already grouped
            if piece_instance1 == piece_instance2:
                continue
            if piece_instance1 in grouped_piece_instances or piece_instance2 in grouped_piece_instances:
                continue
            # else, check for the placement with minimum bounding box waste
            # for the NFP, only the piece shape matters, hence the piece ID (index 0)
            # OLD: keep rotations at 0
            nfp = expanded_problem.piece_nfps[((piece_instance1[0], piece_instance2[0]), (0, 0))]
            box1, box2 = to_bounding_box(piece_geometry1), to_bounding_box(piece_geometry2)
            # we will recompute waste from scratch, so no need to store min_area in variable
            min_placement, _ = NFPVGStrategy.get_minimum_bounding_box_translation_along(box1, box2, nfp.boundary)
            assert min_placement is not None, 'No minimum placement found, check that your NFP boundary is not empty.'
            # move geom2 to this position and evaluate bounding box waste
            translated_piece_geometry2 = translate(piece_geometry2, *min_placement)
            bb_waste = NFPVGStrategy.get_bounding_box_waste(piece_geometry1 | translated_piece_geometry2)

            if bb_waste < threshold:
                # waste is low enough, make a group for that couple
                # priority is given to the first acceptable groups found, those pieces cannot be dismantled for another group
                # IMPROVE: instead, continue checking other groups and keep the globally best groups at the end
                # (for instance, by minimizing the sum of bb_waste of all ComponentPieces chosen)
                group = CompositePiece(LeafPiece(piece_instance1[0], piece_instance1[1], piece_geometry1),
                                       LeafPiece(piece_instance2[0], piece_instance2[1], piece_geometry2))
                remaining_piece_instances.remove(piece_instance1)
                remaining_piece_instances.remove(piece_instance2)
                grouped_piece_instances.extend([piece_instance1, piece_instance2])
                composite_pieces.append(group)
            else:
                # waste is too high, but both pieces may fit well with other pieces so do not make them individual for now
                pass

        # create a leaf for remaining pieces
        leaf_pieces = [LeafPiece(piece_id, instance_idx, expanded_problem.expanded_lot[(piece_id, instance_idx)])
                       for piece_id, instance_idx in remaining_piece_instances]

        component_pieces = composite_pieces + leaf_pieces
        return component_pieces

    # OLD: kept for old constraint solver but now use get_min_bb_translations_along for all translations argmin of BB area
    @staticmethod
    # @deprecated('get_min_bb_translations_along')
    def get_minimum_bounding_box_translation_along(geometry1, geometry2, path):
        """
        Search relative translation along path that result in the bounding box of minimum area.
        Return tuple of translation and the corresponding area, or (None, 0) if path is empty.

        We recommend passing bounding boxes as piece_geometry arguments since this is the only information
        required and they are cheaper to manipulate.

        :param geometry1: BaseGeometry
        :param geometry2: BaseGeometry
        :param path: LinePoint-like geometry
        :return: (np.float[2], float) tuple of translation and area
        """
        # OPTIMIZE: use bounding boxes only from the beginning! path is given already
        # for a convex hull, of course, we could not do that, but then we could replace with convex hulls too

        # path must be composed of (Multi)LineString and (Multi)Points
        # Points can be iterated over directly, but for LineStrings we must check that there is no intermediate
        # point with a lower bounding box area, by searching the quadratic root over the translation along the line

        # we have to distinguish points and lines
        # REFACTOR? We could use raw boundaries in order to keep lines and points separate
        # However, we would also need to provide RawBoundary with intersection and union operations,
        # simplifying and moving geometry from multiline to multipoint members as lines intersect into points, etc.

        # during each iteration we also track the lowest bounding box and the corresponding position up to now
        # we recurse on this method by splitting the path in different parts and keeping the minimum area
        # at each node of the recursive tree, in the local variables below
        if path.is_empty:
            logging.warning('Empty path passed in get_minimum_bounding_box_translation_along')
            return  # None, this should not happen in a collection (we could raise an Error too)

        elif is_multi(path):
            min_translation = None
            min_area = float('+inf')
            for part in list(path):
                translation, area = NFPVGStrategy.get_minimum_bounding_box_translation_along(geometry1, geometry2, part)
                # IMPROVE: in case of draw, keep all translations that share the min area
                if area < min_area:
                    min_translation = translation
                    min_area = area
            return fl(min_translation), min_area

        elif path.type == 'Point':
            # translate piece 2 to position indicated by point
            translation = path.coords[0]
            # OPTIMIZE: use a function that directly computes bounds & area with minX/Y and maxX/Y values
            area = NFPVGStrategy.get_union_bb_area_after_translation(geometry1, geometry2, translation)
            return fl(translation), area

        elif path.type == "LineString":
            # create dynamic bounding box bounds, and check for min at each key point,
            # i.e. when the bounds start and stop changing

            key_positions = []

            # consider each segment of the line one by one
            for segment in get_edges_from_coords(path.coords, loop=False):
                initial_translation = fl(segment[0])  # line 1st vertex
                # key_positions.append(initial_translation)  # already in done in check_bounds_variation_keypoint
                translated_piece_geometry2 = translate(geometry2, *initial_translation)
                relative_key_translations = NFPVGStrategy.check_bounds_variation_keypoint(
                    geometry1.bounds,
                    translated_piece_geometry2.bounds,
                    get_edge_vector(segment))
                key_positions.extend(initial_translation + relative_key_translations)

            # append very last vertex of last line in case of open LineString
            if not np.array_equal(segment[1], key_positions[0]):
                # logging.debug('Open LineString provided. Adding last vertex as key position.')
                key_positions.append(segment[1])

            assert key_positions, 'No key positions found, but the path is not empty (LineString).'

            # keep the best translation over all check points
            min_translation = None
            min_area = float('+inf')
            for key_position in key_positions:
                area = NFPVGStrategy.get_union_bb_area_after_translation(geometry1, geometry2, key_position)
                if area < min_area:
                    min_translation = key_position
                    min_area = area

            return fl(min_translation), min_area

    @staticmethod
    def get_union_bb_area_after_translation(geom1, geom2, translation):
        """
        Return the bounding box area of geom1 and geom2 after geom2 has translated.

        :param geom1:
        :param geom2:
        :param translation:
        :return:
        """
        # OPTIMIZE: add function to translate bounds only
        translated_piece_geom2 = translate(geom2, *translation)
        union_bounds = bounds_union(geom1.bounds, translated_piece_geom2.bounds)
        return get_bounds_area(union_bounds)

    @staticmethod
    def get_bounding_box_waste(geometry):
        """Return relative waste of the bounding box of the geometry"""
        return get_bounding_box_area(geometry) - geometry.area

    @staticmethod
    def check_bounds_variation_keypoint(bounds1, bounds2, translation):
        """
        Return a list of keypoints, corresponding to where active bounds are changing during the translation.
        The list does not need to be sorted by translation time, but must contain unique elements.

        Bounds2 must be translated before hand.

        Note that parameters are redundant, since are_geometry1/2_bounds_active can be deduced from bounds1
        and bounds2. They are passed because so that we only have to compute them once.
        In the current structure, however, it could be computed at the beginning of the function without any problems.

        are_geometry1/2_bounds_active are dictionary indexed by int 0-1-2-3 correspond each to
        a Position. Therefore, a 4-array can be used but a dictionary is used in get_min_bb_translations_along
        because it is easier not to rely on iteration order over Position enum

        :type bounds1: (float, float, float, float)
        :type bounds2: (float, float, float, float)
        :type translation: (float, float)
        :rtype: list[(float, float)]

        """
        # REFACTOR: since we use a dictionary for bounds, we could as well use the enum type as key rather
        # than the enum value (int), so that we can just type bounds[position]

        # determine which bound is active in the union sense for each side, i.e. which bound is outermost
        # between the two bounds
        are_geometry1_bounds_active = {}
        are_geometry2_bounds_active = {}
        for position in Position:
            are_geometry1_bounds_active[position.value] = cmp(bounds1[position.value], bounds2[position.value])\
                                                            in (get_sign_for_position(position), 0)
            are_geometry2_bounds_active[position.value] = cmp(bounds1[position.value], bounds2[position.value]) \
                                                            in (-get_sign_for_position(position), 0)

        # always consider initial position as a key position
        # caution: using a set requires a hashable so no numpy float here
        # but you can use a list and manually ensure there are no redundancies
        relative_key_positions_set = {(0, 0)}

        # Follow Table 01 (8 columns)
        for position in Position:  # 4 times
            coord_idx = get_coord_index_for_position(position)
            other_coord_idx = 1 - coord_idx
            translation_coord_for_bound_change = bounds1[position.value] - bounds2[position.value]
            # A.bounds[position] is not active or B.bounds[position] is active (2 columns in the table)
            # equality would mean end of translation in the last comparison, so no need to consider the case
            # (to simplify, line vertices are considered as key points by default, even if the bounding box may
            # not change)
            # in addition, translation main coord should be positive, superior to translation_coord_for_bound_change
            # when bounds1 are inactive on a negative side (bottom, left) or bounds2 is inactive on a positive side
            # (top, right), since it is whether some change may happen.
            if (not are_geometry1_bounds_active[position.value] and
                    np.sign(translation[coord_idx]) == - get_sign_for_position(position) or
                    not are_geometry2_bounds_active[position.value] and
                    np.sign(translation[coord_idx]) == get_sign_for_position(position)) and \
                    abs(translation[coord_idx]) > abs(translation_coord_for_bound_change):
                # translation main abs coordinate is superior to required translation magnitude for change, so not null,
                # can divide to find the ratio and multiply to get other coord
                other_coord = translation[other_coord_idx] / translation[coord_idx] * translation_coord_for_bound_change
                if coord_idx == 0:
                    key_position = (translation_coord_for_bound_change, other_coord)
                else:
                    key_position = (other_coord, translation_coord_for_bound_change)
                relative_key_positions_set.add(key_position)  # no redundancy thanks to set

        # up to 4 changes, since a bound can be active -> inactive and the opposite bound inactive -> active during one
        # translation
        return list(relative_key_positions_set)


class FloatingOriginMinimizeBBStrategy(PackingStrategy):
    """
    Stategy where the total bounding box is minimized at each step.
    The floating origin system is used in order to place pieces without taking care
    about their absolute position in the rectangular container.
    Then, they are placed accordingly in the container.

    """
    def __init__(self):
        pass

    def __repr__(self):
        return "<FloatingOriginMinimizeBBStrategy>"

    def run(self, expanded_problem):
        """

        :param expanded_problem:
        :return: a layout
        """

        # REFACTOR: factorize with code in other similar strategies

        # initialize layout, the dictionary of placed polygon ID: placement
        layout = NestingLayout(expanded_problem.expanded_lot.keys())

        # only consider container with one element
        container_piece_id, container_polygon = expanded_problem.expanded_container

        # REFACTOR: a fringe as used in Graph Search is more flexible
        piece_instance_tuple_list = expanded_problem.expanded_lot.keys()  # [('piece0', 0), ('piece0', 1), ...]
        # all the piece instances unsorted
        # since we do not backtrack in this strategy, it would be nice to start with the bigger shapes:
        # indeed, bigger shapes tend to have more contacts with other shapes, so joining them with other pieces
        # quickly improves odds to obtain a correct layout
        # we can either use the area or the bounding box area to evaluate the size of a piece
        # CHOICE: BB area
        # RATIONALE: if a geom has a high area but low BB area, it means it has many holes and/or exterior diagonal edges
        # so many other pieces can contact this one
        piece_instance_tuple_list.sort(key=lambda piece_instance_tuple:
                                       get_bounding_box_area(expanded_problem.expanded_lot[piece_instance_tuple]),
                                       reverse=True)

        # NFPVerticesStrategy-specific code starting here
        # start at index 0 and increment / backtrack when needed
        self.try_place_polygons_at_min_bb_inside_floating_container_from_idx(0, piece_instance_tuple_list, container_piece_id, layout,
                                                         expanded_problem)

        if layout.is_complete:
            assert layout.is_valid, 'Invalid layout found for FloatingOriginMinimizeBBStrategy, should never happen.'
            logging.debug('Returning complete layout.')  # works on Pycharm Ubuntu
        else:
            logging.debug('Returning incomplete layout.')
        return layout

    def move_conglomerate_into_container(self, conglomerate_bottom_left, layout):
        for piece_key in layout.placements.keys():
            layout.placements[piece_key].translation -= conglomerate_bottom_left
        logging.debug('All placements have been translated of ({}) '
                      'so that the conglomerate bottom-left is at the (0, 0).'
                      .format(conglomerate_bottom_left))

    def try_place_polygons_at_min_bb_inside_floating_container_from_idx(self, next_piece_instance_tuple_list_idx,
                                                                        piece_instance_tuple_list,
                                                                        container_piece_id, layout, expanded_problem):

        """
        Recursive method to place polygons at the minimum bounding box possible inside a floating container.
        We start at the polygon of idx next_piece_instance_tuple_list_idx in the list of individual piece identifiers
        (piece_id - instance_idx tuple), then continue to the next index and backtrack
        to the previous index if the search reaches a dead-end.

        Since we do not work with a free structure such as a fringe and do not leap indices,
        the order of placement is fixed and corresponds to the order in polygon_names.

        :param next_piece_instance_tuple_list_idx: the idx of the next polygon to place, related to polygon_names list
        :param piece_instance_tuple_list: list<(string, int)> ordered list of piece_id - instance_idx
        :param container_piece_id:
        :param layout:
        :param expanded_problem: ExpandedNestingProblem
        :return: layout, valid or invalid

        """
        next_piece_id, next_instance_idx = piece_instance_tuple_list[next_piece_instance_tuple_list_idx]

        # get the NFP(previously placed pieces, next piece)
        # since we work with a floating container, we do not care about the IFP
        nfp = expanded_problem.get_nfp_with_previous_layout(next_piece_id, 999, layout.placements)

        # get placed pieces' shapes and deduce the bounding box of all placed pieces, *after translation*
        # placed_pieces = expanded_problem.get_piece_geoms(layout.placements.keys())
        # tr_placed_pieces = get_translated_polygons(expanded_problem.expanded_lot, layout.placements)

        # if there are no placed pieces yet, we place the first piece at (0, 0) by convention
        if not layout.placements:
            # we still check that this single piece is not too large, but this should not happen
            # unless the problem was ill-formed / obviously unsolvable (for a rectangular container)
            # create a dummy reversed box that is never selected in min/max ops for the move into container operation
            # (conglomerate_bottom_left calculation)
            a_bounds = (float('inf'), float('inf'), float('-inf'), float('-inf'))
            b_geom = expanded_problem.expanded_lot[(next_piece_id, next_instance_idx)]
            b_bounds = b_geom.bounds  # for move into container if it is this end (not probable, would mean only one piece in the pb)
            b_size = get_size(b_geom)
            container_size = get_size(expanded_problem.expanded_container[1])
            if b_size[0] > container_size[0] or b_size[1] > container_size[1]:
                logging.warning('Piece type {} too big for the container. Problem ill-formed. STOP'.format(next_piece_id))
                return
            translation = (0, 0)  # always works for floating origin
        else:
            # compute an intermediate union of multiple boxes (for cheaper computation), then get the final bounds
            # OPTIMIZE: use a function that computes the min_x/y, max_x/y from a sequence of bounds, rather than Polygons
            a_bounds = get_layout_bounds(expanded_problem.expanded_lot, layout.placements)
            next_piece_geom = expanded_problem.expanded_lot[(next_piece_id, next_instance_idx)]
            b_bounds = next_piece_geom.bounds
            container_bounds = expanded_problem.expanded_container[1].bounds
            x_min, y_min, width, height = get_floating_container_limit_bounds(a_bounds, to_size(container_bounds))
            # let function specialized in IFP deduce it, it will handle empty and flat cases correctly
            # OLD: do not mind upgrading for rotations, but new min BB strategies should definitely try all orientations
            ifp = get_ifp_with_rectangular_container(next_piece_geom, width, height, x_min, y_min, orientation=0)

            # compute translation bounds as PolygonWithBoundary (equivalent to an IFP in a larger container)
            translation_bounds_box = ifp
            translation_bounds_ifp = PolygonWithBoundary(translation_bounds_box)

            # generic method: compute IFP - NFP to get all feasible positions inside the floating container
            feasible_positions_with_boundary = translation_bounds_ifp.ifp_sub_nfp(nfp)

            # specific method for strategies looking for a contact with existing pieces:
            # keep only the NFP bounds that are in the IFP, i.e. not fitting the floating container
            contact_positions = nfp.boundary & translation_bounds_box  # BaseGeometry operation

            # for now we will just use contact positions

            if contact_positions.is_empty:
                # the layout is incomplete but at least move it into the container (B not placed, only A matters)
                self.move_conglomerate_into_container(a_bounds[:2], layout)
                logging.debug('No feasible placement space for piece ({}, {}).'
                              .format(next_piece_id, next_instance_idx))
                return

            # try the min BB position (no backtracking)
            b_box = box(*b_bounds)
            # print('a_bounds: '+  str(a_bounds))
            # print('result: '+ str(NFPVGStrategy.get_minimum_bounding_box_translation_along(box(*a_bounds), b_box, contact_positions)))
            translation, _ = NFPVGStrategy.get_minimum_bounding_box_translation_along(box(*a_bounds), b_box, contact_positions)

        # try the translation found
        layout.add_placement(next_piece_id, next_instance_idx, translation)
        logging.debug('Trying: ({}, {}) at {}'.format(next_piece_id, next_instance_idx, translation))

        # END: move onto container
        if next_piece_instance_tuple_list_idx == len(piece_instance_tuple_list) - 1:
            # this was the last polygon in the list
            # readjust the floating container to the conglomerate; move to bottom-left by default
            # since the layout cannot move the container, in practice, move the conglomerate in the opposite direction,
            # i.e. so that its bottom-left is at (0, 0), i.e. alter all placements' translatons to obtain this result
            # we could compute a last union of A and B, but we will just manually compute the bottom-left position here

            tr_b_bottom_left = fl(b_bounds[:2]) + translation
            conglomerate_bottom_left = (min(tr_b_bottom_left[0], a_bounds[0]), min(tr_b_bottom_left[1], a_bounds[1]))

            self.move_conglomerate_into_container(conglomerate_bottom_left, layout)

            logging.debug('Last polygon ({}, {}) has been placed. Layout marked as complete. '
                          'Return with success.'.format(next_piece_id, next_instance_idx))
            return

        # recurse on next polygon, ie call the same function with incremented index
        self.try_place_polygons_at_min_bb_inside_floating_container_from_idx(next_piece_instance_tuple_list_idx + 1,
                                                                             piece_instance_tuple_list, container_piece_id, layout,
                                                                             expanded_problem)
        if layout.is_complete:
            logging.debug(
                'Complete layout found in lower level, return from ({}, {}).'.format(next_piece_id, next_instance_idx))
            return

        # all the feasible vertex positions resulted in an empty set of feasible placement
        # among the next polygons to place
        logging.debug('Min BB placement at fit polygon vertices for ({}, {}) failed.'.format(next_piece_id,
                                                                                             next_instance_idx))
        return
