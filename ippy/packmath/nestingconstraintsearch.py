from copy import copy, deepcopy
from functools import partial
import logging
import math
import numpy as np
from numpy import float_ as fl
from shapely.affinity import translate
from shapely.geometry import box, Point

from shapely.ops import unary_union

from ippy.packmath.constraintsearch import ValuePickingConstraintSearchProblem, AssignmentSearchNode, greedy_vp_search, \
    iterable_domain_value_picker, ordered_master_heuristic, depth_first_heuristic, min_cost_heuristic, \
    value_picking_constraint_graph_search
from ippy.packmath.knapsack1d import MultipleKnapsackProblem1D, ValuePickingMultipleKnapsackProblem1D, \
    MultipleKnapsackDomain
from ippy.packmath.nestingconstraintproblem import OpenDimensionProblem, InputMinimizationNestingProblem
from ippy.packmath.nestingsolver import _get_fit_polygon
from ippy.packmath.packhelper import get_layout_bounds, get_layout_geometry, pick_regular_vertices
from ippy.packmath.problem import Placement, PieceKey
from ippy.packmath.solver import NFPVGStrategy
from ippy.search.constraint import ConstraintOptimizationProblem, forward_check
from ippy.utils.datastructure import PriorityQueueWithFunction
from ippy.utils.decorator import deprecated
from ippy.utils.geometry import Position
from ippy.utils.iterables import flatten
from ippy.utils.packdecorator import null_for_initial_node, random_sampler
from ippy.utils.shapelyextension import get_unique_coords, get_size, rotate_origin, to_bounding_box, get_placed_lot, \
    get_bounding_box_area, get_cumulative_size, get_polygon_diameter, PolygonExtended, get_closed_interior, round_coords, \
    place_geom, is_multi, get_edges_from_coords, get_edge_vector, get_convex_hull_waste, get_min_width_or_height, \
    get_bounding_box_waste

__author__ = 'hs'


class NestingAssignment(object):
    """
    Assignment class for a Nesting Problem
    Behaves as a dictionary, can be hashable, and also has a special transformation
    [(piece_id, [sorted Placements])] or {piece_id: [sorted Placements]} that allows to get rid of
    piece instance swapping issues, by giving the same representation to all equivalent layouts.

    Alternatively, we could use [(piece_id, Placement), (piece_id, Placement), ...] and compute the hash
    as hash((piece_id1, placement1)) ^ hash((piece_id2, placement2)) ^ ... where some piece_ids may be equal.
    This avoids having to choose an arbitrary Placement sorting, while returning the same result
    if we swap 2 instances due to piece instance indices being removed and the xor operation ignoring
    order.
    Of course, this hash is not very robust but will boost search performance a lot. Equality will
    verify if two placements are indeed equivalent, which is very fast to check.

    """
    # TODO
    # for now, we just use a hashable dict for assignments
    # we do not detect equivalent layouts

    # or put this at Node class level


class NestingAssignmentSearchNode(AssignmentSearchNode):
    """
    Subclass of search nodes dedicated to Nesting problems.
    It adds problem-specific information, though most play the role of cache and could be deduced from the basic data
     contained in an AssignmentSearchNode.
    In particular, it stores information specific to constraint problems where variables represent instances of a
     specific variable type. (Here, piece ID - piece instance index)

    Attributes:
        :type next_piece_inst_idx_dict: dict[str, int]
        :param next_piece_inst_idx_dict: dictionary of the next piece instance index, indexed by piece ID

    """
    def __init__(self, assignment=None, parent=None, variable_assigned=None, problem=None, cached_values=None,
                 dynamic=None):
        """
        super parameters:

        :type assignment: hashdict[T, U]
        :param assignment: assignment state located at this node
        :type parent: NestingAssignmentSearchNode
        :param parent: predecessor node (None if initial node) [optional since only the result matters]
        :type variable_assigned: T
        :param variable_assigned: the last variable assigned, that led to this node (None if initial node)
        :type problem: ValuePickingConstraintSearchProblem
        :param problem: problem this node belongs to

        """
        # REFACTOR: move the logic to create node, keep basic construction here -> None values will be corrected beforehand
        # (in tests, don't forget to call create_node instead)
        super(NestingAssignmentSearchNode, self).__init__(assignment, parent, variable_assigned, problem, cached_values,
                                                          dynamic)

        if parent is None:
            # initialize all next piece instances indices at 0 since you pick pieces from 0 to piece.quantity
            # since parent is None, problem is not None, otherwise super() would have raised an error
            self.next_piece_inst_idx_dict = dict.fromkeys(problem.cp.get_piece_ids(), 0)
            # for CSP, use the fixed container size, but for COP start with the min size imaginable (usage 100%)
            # we will stretch the container length later if needed
            # dynamic cost is our dynamic length

            # if dynamic:
            #     # should be a COP, create dynamic domains for dynamic IFPs
            #     # REFACTOR: simply let COP automatically use dynamic=True, CSP dynamic=False
            #     assert problem.cp.open_dim, 'Please use dynamic=False with CSP'
            #     assert isinstance(problem.cp, OpenDimensionProblem)
            #
            #     self.min_cost = self.dynamic_cost = problem.cp.get_min_cost()
            #     ifps = problem.cp.generate_ifps_for_container(problem.cp.lot, problem.cp.pieces,
            #                                                   problem.cp.container_height,
            #                                                   self.dynamic_cost)
            #     self.dynamic_domains = self.problem.cp.create_domains_from_ifps(ifps)
            # else:
            #     assert not problem.cp.open_dim, 'Please use dynamic=True with COP'
            #     # in CSP, dynamic domains are the same are the maximum domains
            #     # BUT dynamic domains may be updated with the dynamic cost so we also
            #     # ensure the dynamic cost is correct
            #     # the best is to always use the domains or the dynamic domains, and update
            #     # as little as possible by checking the nature of the problem
            #     self.min_cost = self.dynamic_cost = get_size(problem.cp.container[1])[0]
            #     self.dynamic_domains = deepcopy(self.domains)

        else:
            # increment instance index for the ID of the last piece placed
            # if it exceeds the quantity of that piece, ie the last piece was placed, remove the entry instead
            inst_idx_dict = copy(parent.next_piece_inst_idx_dict)  # copy is enough for primitive types
            # problem was not passed as parameter, but super set it as attribute, so use self
            if inst_idx_dict[variable_assigned.id] < self.problem.cp.pieces[variable_assigned.id].quantity - 1:
                inst_idx_dict[variable_assigned.id] += 1
            else:
                del inst_idx_dict[variable_assigned.id]
            self.next_piece_inst_idx_dict = inst_idx_dict
            self.min_cost = parent.min_cost
            self.dynamic_cost = parent.dynamic_cost
            # self.dynamic_domains = deepcopy(parent.dynamic_domains)  # DEEP COPY!!

    def get_initial_dynamic_cost(self):
        # for CSP, use the fixed container size, but for COP start with the min size imaginable (usage 100%)
        # we will stretch the container length later if needed (duck-type the COP)
        # dynamic cost is actually our dynamic length
        if not self.problem.cp.open_dim:
            # remark: in a CSP, we will not use these values in practice (use fixed container, not max container)
            return get_size(self.problem.cp.container[1])[0]
        else:
            return self.problem.cp.get_min_cost()

    def get_min_cost_for_non_empty_domain(self, piece_key):
        """Return minimum length for which the domain of variable, in any rotation, is not empty."""
        # algo reference: on paper, ODP: Cost extension
        piece = self.problem.cp.pieces[piece_key.id]
        min_length = float('+inf')
        for orientation in piece.orientations:
            assert not self.domains[piece_key][orientation].is_empty

            # see paper notes, ODP: Cost extension (fixed and floating origin)
            if not self.problem.cp.floating_origin:
                # extension to the right
                # FIXME: this only works if the ref point if on the left of the piece!
                # add an offset to fix that
                domain_x_min = self.domains[piece_key][orientation].bounds[0]
                min_length = min(min_length, domain_x_min + piece.geom.bounds[2])
            else:
                # WIP
                raise AttributeError('get_min_cost_for_non_empty_domain does not support floating origin problems')
                # for floating origin, imagine the extension is in both senses at the same time,
                # until either the left or the right part of the max domain is reached
                layout_bounds = get_layout_bounds(self.problem.cp.lot, self.assignment)
                # TODO: split the domain in two to get the right and left part
                # to do so, check the domain part, offset to match the left / right of the piece,
                # that is just on the left of the layout bounds right, and vice-versa
                # domain_x_min = self.domains[piece_key][orientation].bounds[0]
                # domain_x_max = self.domains[piece_key][orientation].bounds[0]


        return min_length

    # def set_dynamic_cost(self):
    #     pass

    @deprecated('cache update_dynamic_domains')
    def update_dynamic_domain(self, piece_key):
        """
        Update the dynamic domains from the max domains and dynamic cost.
         Useful for COP only.

        :param piece_key:
        """
        # REFACTOR: unary constraint should be a UnaryConstraint with a revise_forward method
        # update dynamic domains would simply use constraint-specific information to update the IFP

        # Dynamic domains: the length may grow too, so in any case compute from max domains not to lose any NFP info
        cp = self.problem.cp
        # OPTIMIZE: compute once per piece ID, copy result for each instance
        for orientation in cp.pieces[piece_key.id].orientations:
            # OPTIMIZE: make this test once, by checking any key with any orientation (one domain is None iff all are)
            if self.dynamic_domains[piece_key][orientation] is None:  # FIXME? self.domains??
                return
            # OPTIMIZE: if intersection is too slow, try difference with the complement (right to infinity part of max IFP)
            self.dynamic_domains[piece_key][orientation] = self.domains[piece_key][orientation] & \
                self.cache.get_ifp(piece_key.id, orientation, dynamic=True)

    def get_previous_layout_geometry(self):
        """Return union of the placed pieces"""
        if self.parent is None:
            raise AttributeError('Node {} has no parent, cannot get previous layout geometry'.format(str(self)))
        return get_layout_geometry(self.problem.cp.lot, self.parent.assignment)

    def get_current_layout_geometry(self):
        """Return union of the placed pieces"""
        return get_layout_geometry(self.problem.cp.lot, self.assignment)

    @property
    def remaining_piece_ids(self):
        """
        Return the list of remaining piece IDs for this assignment

        :rtype: list[str]

        """
        return self.problem.cp.get_remaining_piece_ids(self.assignment)


class ValuePickingNestingSearchProblem(ValuePickingConstraintSearchProblem):
    """
    Concrete class for Nesting search problems by value picking.

    Attributes:
        :type cp: (ippy.search.constraint.ConstraintProblem | ippy.packmath.nestingconstraintproblem.InputMinimizationNestingProblem)
        :param cp: Constraint problem associated to this search problem

    """
    def __init__(self, cp):
        super(ValuePickingNestingSearchProblem, self).__init__(cp)

    def create_node(self, assignment=None, parent=None, variable_assigned=None, problem=None, cached_values=None,
                    dynamic=None):
        """
        Create an AssignmentSearchNode specific to the problem subclass

        :rtype: AssignmentSearchNode

        """
        return NestingAssignmentSearchNode(assignment, parent, variable_assigned, problem, cached_values, dynamic)

    def pick_variables(self, node):
        """
        Return a list of variables (piece keys) to assign.
        Only pick the first instance of each piece type.
        Use the dictionary of next instance indices contained in the node.

        :type node: NestingAssignmentSearchNode
        :rtype: list

        """
        next_piece_ids, next_piece_inst_indices = zip(*node.next_piece_inst_idx_dict.iteritems())
        return map(PieceKey, next_piece_ids, next_piece_inst_indices)

        # next_idx_variables = []
        # unassigned_variables = self.cp.get_unassigned_variables(node)
        # remaining_piece_ids = self.cp.get_remaining_piece_ids(node)
        # for remaining_piece_id in remaining_piece_ids:
        #     # get piece keys with this ID
        #     piece_keys = [piece_key for piece_key in unassigned_variables if piece_key.id == remaining_piece_id]
        #     # keep the piece key with the smallest instance index
        #     next_idx_variables.append(min(piece_keys, key=lambda x: x.instance_idx))
        #
        # return next_idx_variables


# -- cache filtering --

def analyze_free_space(node, viewer=None):
    """
    Analyze living and dead regions in free space, estimate the *extra* cost required to have a completable
    layout.
    If the current dynamic cost is enough, return 0.
    In a CSP, any non-None value will be interpreted as "there is no solution".

    :type node: NestingAssignmentSearchNode
    :return:

    """
    living_space_area_margin = get_living_space_area_margin(node, viewer)
    # threshold?
    if living_space_area_margin >= 0:
        return 0  # we cannot decide whether the layout is completable or not (at that level of analysis)
    else:
        # divide overflowing area by container height to get minimum extra length required
        # IMPROVE: as written in the Thesis, a too small container length extension will create
        # a new dead region! so at least extend so that the smallest piece can fit in the worst case,
        # otherwise if there is some space on the right of the container already, we may need less...
        # it is like empty domain extension, we only need one point in the domain of the piece
        # except it has to be on the right of the container, unlocked by the extension...
        # computation is not trivial and may not be worth it if Knapsack takes care of it
        extra_length = -living_space_area_margin / node.problem.cp.get_container_height()
        return extra_length

def get_living_space_area_margin(node, viewer=None):
    """
    Return the area of the living space, minus the area of all the remaining pieces

    This correspond to the remaining area located in regions considered as living for the current node,
    after all pieces have been placed.
    If this value is null, any complete placement that did not extend
    the length will have a usage of 100%.
    If this value is negative, the placement cannot be completed without extending the length.

    :type node: NestingAssignmentSearchNode
    :param viewer:
    :return:

    """
    if node.cache.get('dynamic_free_space') is None:
        # floating-origin, initial node: no restriction so living space supposed infinite, no extra cost required
        return 0

    cp = node.problem.cp
    living_space_area = get_living_space_area(node, viewer)
    remaining_pieces_area = cp.get_sum_area_remaining_pieces(node.assignment)
    # remaining_pieces_area = sum(cp.pieces[piece_key.id].geom.area
    #                             for piece_key in cp.get_unassigned_variables(node.assignment))
    return living_space_area - remaining_pieces_area

def get_living_space_area(node, viewer=None):
    """
    Return the area of the free space, minus the area of the dead regions.

    :type node: NestingAssignmentSearchNode
    :return:

    """
    free_space_area = node.cache.get('dynamic_free_space').area
    dead_regions_area = sum(region.area for region in get_dead_regions(node, viewer))
    return free_space_area - dead_regions_area

def get_dead_regions(node, viewer=None):
    """
    Return the list of regions where no pieces can fit, whatever their orientation.
    (alternatively, the list of indices of those regions, where the indices correspond to the order
    of the regions returned by region fit polygons (dictionary keys are stable per Python implementation but be careful))

    :type node: NestingAssignmentSearchNode
    :rtype: list[PolygonExtended]

    """
    dead_regions = []

    # ROUNDING: apply round_coords to regions? We get .0000...1 and 0.999...
    free_regions = node.cache.get('dynamic_free_space')  # use dynamic free space!
    region_fitting_pieces = get_region_fitting_pieces(node, free_regions)
    for region_idx, fitting_piece_ids in region_fitting_pieces.iteritems():
        # logging.debug('Region {} -> fitting piece ids: {}'.format(str(region), fitting_piece_ids))
        if not fitting_piece_ids:
            # no piece can fit in that region, you can gray it out: it is a wasted region
            dead_regions.append(free_regions[region_idx])
        # DEBUG
        # if viewer.stats['iterations'] == 5:
        #     logging.debug('Region {} -> fitting piece ids: {}'.format(str(region), fitting_piece_ids))

    if dead_regions and viewer is not None:
        # logging.debug('Wasted regions: ' + ', '.join(map(str, wasted_regions)))
        viewer.event('dead_region', dead_regions)

    return dead_regions


def analyze_free_region_knapsack(node, viewer=None):
    """
    Apply a 1D knapsack problem to the free regions, where stocks are free orders, orders are pieces
    to place, and lengths are areas.

    :type node: NestingAssignmentSearchNode
    :return:

    """
    free_regions = node.cache.get('dynamic_free_space')
    # regions, _ = zip(*region_fitting_pieces)
    # PRECISION: round areas? (square is 7.99999999...8 at 1e-15)
    region_areas = [free_region.area for free_region in free_regions]  # ensure regions are listed in the same order everywhere

    # caution: we must take *all* remaining piece instances into account, although we studied fitting pieces
    # by ID since one shape and orientation gives the same result in terms of fitting
    # hence we compute the array of repeated IDs for each instance, such as ['square', 'smallTriangle', 'smallTriangle']
    remaining_piece_instance_nb = node.problem.cp.get_remaining_piece_instance_counter(node.assignment)
    piece_ids = remaining_piece_instance_nb.keys()  # order does not matter, will be used as dict keys
    piece_repeated_ids = list(flatten([[piece_id] * nb for piece_id, nb in remaining_piece_instance_nb.iteritems()]))
    piece_areas = [node.problem.cp.pieces[piece_id].geom.area for piece_id in piece_repeated_ids]

    region_fitting_pieces = get_region_fitting_pieces(node, free_regions)
    piece_fitting_region_dict = to_piece_fitting_region_dict(piece_ids, region_fitting_pieces)

    # the 1D Knapsack problem does not care about multiple piece instances:
    # ['square', 'smallTriangle', 'smallTriangle'] -> variables (order indices) [0, 1, 2]
    # so for each order index, find the corresponding piece ID and then the list of region indices it can fit in
    domains = {
        order_idx: MultipleKnapsackDomain(piece_fitting_region_dict[piece_repeated_ids[order_idx]])
        for order_idx in xrange(len(piece_repeated_ids))
    }
    # FIXME: identify free region and remove it or place it in last position no matter what

    # REFACTOR: as for Packing problem, use ID - instance index system to reduce redundancy
    knapsack1d_problem = MultipleKnapsackProblem1D(region_areas, piece_areas, domains)
    vp_knapsack1d_problem = ValuePickingMultipleKnapsackProblem1D(knapsack1d_problem)

    # solve the 1D knapsack problem by applying any complete and optimal search
    # even BFS would make it, but if you prefer quickly find a solution and then shrink
    # the max cost, use greedy dynamic
    # with the new test, we can really grow the cost, but a global search with infinite cost (no restriction
    # on orders placed in infinite stock) should be enough for small problems)
    # OPTIMIZE: use optimize/dynamic=True, give a measure of domain size that excludes infinite stock,
    # maybe start with smaller orders instead of bigger orders
    # for now, COP only works with optimize and dynamic set to True, and dynamic=optimize
    # later, maybe distinguish both so that we can find better and better solutions without changing the max cost
    # (need exhaustive search, costly in general, but we ignore worse nodes)
    heuristic=ordered_master_heuristic(depth_first_heuristic, min_cost_heuristic)
    pq_factory = partial(PriorityQueueWithFunction, heuristic, viewer)

    best_node = value_picking_constraint_graph_search(
        vp_knapsack1d_problem,
        pq_factory,
        optimize=True,
        value_picker=iterable_domain_value_picker,
        # dynamic_cost_fun=get_current_dynamic_cost,
        # viewer=viewer,  # uncomment if you want you see what happens inside the Knapsack search
        # max_iterations=300,
        cached_values=['domains', 'dynamic domains'],
        # cache_filter_fun=None,
        # multistart=1
    )

    assert best_node is not None, 'No solution found for the 1D Knapsack problem, but there is an infinite stock!'

    # IMPROVE: do not optimize on area, which only gives a very optimistic lower bound on the width overflow (all pieces
    # are liquid)
    # instead, optimize on min(max(min(width on all orientations) on all overflowing orders) on all solutions)
    # or whatever measure we use to actually find the extra length required
    # note that this is not enough, as a node with a too optimistic dynamic cost will have to extend many times
    # but that is a good indication if we want to sample extension and make estimations for next steps
    # as with free region, also note that if the container has some space on the right already, a lower
    # extension may be enough
    min_overflow_area = knapsack1d_problem.get_cost(best_node.assignment)  # total area in infinite stock
    min_overflow_width = float(min_overflow_area) / node.problem.cp.get_container_height()
    return min_overflow_width  # extra length (0 if no overflow)

def to_piece_fitting_region_dict(piece_ids, region_fitting_pieces):
    """
    Convert a sequence of (region, fitting_piece_ids) to a dictionary of {piece_id: fitting_regions}
    It is basically an inversion of keys and values relationship.
    >>> sorted(to_piece_fitting_region_dict(['square', 'triangle'], {0: ['square'], 1: ['square', 'triangle']}).items())
    [('square', [0, 1]), ('triangle', [1])]

    :param piece_ids:
    :param region_fitting_pieces:
    :return:

    """
    # REFACTOR: use set instead of lists, for more consistency with usage as domains in Knapsack
    # do not use defaultdict(list), since you want to initialize entries for piece IDs with no fitting regions too
    piece_fitting_region_dict = {piece_id: [] for piece_id in piece_ids}
    for region_idx, fitting_piece_ids in region_fitting_pieces.iteritems():
        for fitting_piece_id in fitting_piece_ids:
            piece_fitting_region_dict[fitting_piece_id].append(region_idx)
    return piece_fitting_region_dict

def get_region_fitting_pieces(node, free_regions=None):
    """
    Analyze which free regions can be occupied by which remaining pieces, and return a dictionary
    {region index: list of piece IDs that can fit}.

    Region indices are based on free region if passed, otherwise
    the order in which the cache's free space MultiPolygon iterates over the regions

    The analysis can reuse result from the parent node, if any, if some regions are similar.

    :type node: NestingAssignmentSearchNode
    :param node: node on which the analysis is done
    :type free_regions: (list[Polygon] | None)
    :param free_regions: list of free regions, obtained from cache's free space (passed for convenience, ensuring order)
    :rtype: dict[int, list[str])]

    """
    # OPTIMIZE: reuse information from parent node for similar regions of if you recognize the parent region that
    # gave birth to ours
    cp = node.problem.cp

    if free_regions is None:
        free_regions = node.cache.get('free_space')  # for convenience in tests and other quick evaluations

    # first, compute all actual point domains per piece-orientation, ie the domains translated to represent the feasible
    # positions of an arbitrary point inside the piece (tD)
    actual_point_domains = {}
    for piece_id, instance_idx in node.next_piece_inst_idx_dict.iteritems():
        piece = cp.pieces[piece_id]
        for orientation in piece.orientations:
            # get domain of the piece for this orientation
            # since the domain may be a singleton for placed instances, pick it for the next piece (any remaining ok)
            # OPTIMIZE: cache all pieces in all orientations at the beginning of the search, or at the problem creation
            rotated_piece = rotate_origin(piece.geom, orientation)
            # OPTIMIZE: do not round coords or optimize round coords... for now, rotation clearly add garbage floating points
            # should be let rotate round by default?
            # for now, deactivated: indeed, we will just use a representative point which is taken at more than 1e-14
            # of margin, so should be okay
            # rotated_piece = round_coords(rotated_piece)
            # compute tD = D + v
            # REFACTOR: domains should be computer per piece ID, then copied for each remaining piece
            # FIXED: use dynamic domains instead of domains; we can see the difference on the
            # front edge of the container: sometimes a piece *could* fit on the right but needs to protrude
            # from the dynamic container (for clearly dead regions surrounded by other pieces or the left of
            # the container in fixed origin, it will be detected even with max domains)
            translation_domain = node.dynamic_domains[PieceKey(piece_id, instance_idx)][orientation]  # D
            assert translation_domain is not None, 'translation domain is None; do not call get_region_fitting pieces ' \
                                                   'on initial node of floating-origin problem'
            # translate D using a representative poitn in the *rotated* geometry!
            v = rotated_piece.representative_point().coords[0]  # for (a) version below
            actual_point_domain = translation_domain.translated(*v)  # tD
            actual_point_domains[(piece_id, orientation)] = actual_point_domain

    # second, for each Polygon region of the MultiPolygon free space, find which pieces can fit there
    # init dict {region idx: []} (do not use dict.fromkeys(), it clones list default value (same reference)
    region_fit_polygons = {region_idx: [] for region_idx in xrange(len(free_regions))}
    for region_idx, region in enumerate(free_regions):
        region = PolygonExtended(region)  # to compare more easily in tests
        for piece_id in node.next_piece_inst_idx_dict.iterkeys():
            for orientation in cp.pieces[piece_id].orientations:
                # as we are not considered each part of the domain Dk separately, we cannot apply an inside test,
                # (see bug square #34) but we can test if:
                # a - a representative point of the piece, ie a point *within* the piece can move in the region
                #     to do this, translate the domains of a vector from the reference point (origin) to that
                #     representative point, and check if the resulting set has an intersection with the region
                #     in addition, this intersection can only be within the region, in theory
                # b - place the piece geometry to any domain part Dk, and check if this placed geom is inside (touch ok)
                #     the region (very intuitive)

                # (a) version
                # if a point in the closed interior is inside the region, a point in the boundary too,
                # since the actual point domain parts cannot protrude from each region, so checking the boundary is enough
                intersection_with_region = actual_point_domains[(piece_id, orientation)].boundary & region
                if not intersection_with_region.is_empty:
                    # the piece can fit in the free region with one of the orientations, this is enough
                    region_fit_polygons[region_idx].append(piece_id)  # append to list of piece IDs, 2nd elt of last tuple
                    break

    return region_fit_polygons


# -- value pickers --

@random_sampler
def fpv_value_picker(piece_key, node, cp, dynamic):
    """
    Return the list of Fit-Polygon vertex positions, for any available rotations.

    :type piece_key: PieceKey
    :param piece_key: piece for which we consider the FPV
    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
    :param node: node containing the assignment and other information to compute the FPV
    :type cp: (ippy.packmath.constraintproblem.ConstraintProblem | ippy.packmath.nestingconstraintproblem.InputMinimizationNestingProblem)
    :param cp: Constraint Problem, can be useful to get more information on the problem
    :type cached_values: list[str]
    :param cached_values: list of values to cache
    :rtype: list[(float, float)]

    """
    placements = node.assignment

    picked_placements = []

    for orientation in cp.pieces[piece_key.id].orientations:
        # compute fit polygon; if cache is not used, it is set to None
        fit_polygon_with_boundary = _get_fit_polygon(piece_key.id, orientation, placements, cp, cache=node.cache)

        if fit_polygon_with_boundary is None:
            # first piece in floating-origin, choose arbitrary origin position, for any rotations allowed
            picked_placements.append(Placement((0, 0), orientation))

        else:
            fit_polygon_vertices = get_unique_coords(fit_polygon_with_boundary.boundary)  # may be empty
            picked_placements.extend([Placement(vertex, orientation) for vertex in fit_polygon_vertices])

    return picked_placements


@random_sampler
def rdv_value_picker(piece_key, node, cp, dynamic):
    """
    Return the list of Fit-Polygon vertex positions, for any available rotations,
    using exclusively the remaining domains stored in the assignment node.

    If FC is correct, is technically the same as the Fit-Polygon Vertex picker (FPV).
    We call this the Remaining Domain Vertex picker (RDV) to differenciate performance.

    CAUTION: If Forward Check or a stronger filtering is not activated on this search,
    this value picker will start returning invalid values (violate constraints) after a few iterations.

    Since domain is used cache is not used here. Unless used somewhere else, we recommend
    disabling it for this search to reduce computations.

    :type piece_key: PieceKey
    :param piece_key: piece for which we consider the FPV
    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
    :param node: node containing the assignment and other information to compute the FPV
    :type cp: (ippy.packmath.constraintproblem.ConstraintProblem | ippy.packmath.nestingconstraintproblem.InputMinimizationNestingProblem)
    :param cp: Constraint Problem, can be useful to get more information on the problem
    :param dynamic: should we pick values from the dynamic domains?
    :rtype: list[(float, float)]

    """
    picked_placements = []
    domains = node.dynamic_domains if dynamic else node.domains

    for orientation in cp.pieces[piece_key.id].orientations:
        # we are expanding the node so the domain has been filtered just before, if FC or more is active
        fit_polygon_with_boundary = domains[piece_key][orientation]

        if fit_polygon_with_boundary is None:
            # first piece in floating-origin, choose arbitrary origin position, for any rotations allowed
            picked_placements.append(Placement((0, 0), orientation))

        else:
            fit_polygon_vertices = get_unique_coords(fit_polygon_with_boundary.boundary)  # may be empty
            picked_placements.extend([Placement(vertex, orientation) for vertex in fit_polygon_vertices])

    return picked_placements


@random_sampler
def regular_picker(piece_key, node, cp, dynamic):
    """
    Return placements regularly picked on the NFP boundaries,
    for each orientation

    Space interval?

    Corners outside NFP not supported

    :type piece_key: PieceKey
    :param piece_key: piece for which we consider the FPV
    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
    :param node: node containing the assignment and other information to compute the FPV
    :type cp: (ippy.packmath.constraintproblem.ConstraintProblem | ippy.packmath.nestingconstraintproblem.InputMinimizationNestingProblem)
    :param cp: Constraint Problem, can be useful to get more information on the problem
    :param dynamic: should we pick values from the dynamic domains?
    :rtype: list[(float, float)]

    """
    placements = node.assignment

    # list of placements sharing the same depth in the NFP convex hull
    regular_placements = []

    for orientation in node.problem.cp.pieces[piece_key.id].orientations:
        ifp = node.cache.get_ifp(piece_key.id, orientation, dynamic)

        # handle no restriction case
        if ifp is None:
            # floating origin and no pieces placed before, arbitrarily choose (0, 0)
            regular_placements.append(Placement((0, 0), orientation))
            continue

        # handle full restriction (unlikely as long as piece size and allowed orientations make sense, even in dynamic mode)
        if ifp.is_empty:
            logging.warning('IFP of {} is empty for orientation {}'.format(piece_key, orientation))
            continue  # no valid positions

        if not placements:
            # fixed origin but no placements yet
            # since the problem has a fixed origin, it is worth trying all the corners
            regular_placements.extend((Placement(vertex, orientation) for vertex in get_unique_coords(ifp.boundary)))
            continue

        # get vertices of NFP inside IFP
        nfp_union = node.cache.get_nfp(piece_key.id, orientation)

        # get the contact positions that are also inside the container
        # we use a trick to automate intersection computation but we could also compute
        # nfp_union & ifp.closed_interior | nfp_union & ifp.boundary (the 2nd term is redundant for rectangular IFPs)
        contact_position_geom = (nfp_union.get_boundary_as_pwb() & ifp).boundary

        if contact_position_geom.is_empty:
            continue  # no valid positions for this orientation (should be valid at least for some other orientations if FC is used)

        # here we compute the interval as the min width/height among all pieces, divided by 5
        # OPTIMIZE: compute this before hand in a cache or a special heuristic structure,
        # or as a cache inside the function giving this result
        # we can also use an interval based on the piece key to place
        # IMPROVE: we do not take rotations into account: create another function that this time works
        # on pieces and find the min size for any rotations
        # REFACTOR: let interval be a parameter and use partial to use this heuristic but tuned?
        # after all, for the chocolate puzzle for instance we know that the optimal interval is 1...
        interval = get_min_width_or_height([piece.geom for piece in node.problem.cp.pieces.itervalues()]) / 5

        # pick vertices at regular intervals on each linestring on contact_position_geom
        regular_vertices = pick_regular_vertices(contact_position_geom, interval)

        regular_placements.extend((Placement(regular_vertex, orientation)
                                   for regular_vertex in regular_vertices))

    return regular_placements


@random_sampler
def min_bb_area_placement_picker(piece_key, node, cp, dynamic):
    """
    Value picker:

    Return placements for which the bounding box of all pieces, including the one to place,
    is the minimum possible, in area
    If multiple placements share the minimum, they will all be returned in a list. Otherwise,
    the unique placement will be returned in a 1-element list.
    If no pieces placed yet, choose (0, 0) for a floating origin and the 4 ifp corners for a fixed origin

    Supports:
    - fixed & floating origin single bin size

    :type piece_key: PieceKey
    :param piece_key: piece for which we consider the FPV
    :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
    :param node: node containing the assignment and other information to compute the FPV
    :type cp: (ippy.packmath.constraintproblem.ConstraintProblem | ippy.packmath.nestingconstraintproblem.InputMinimizationNestingProblem)
    :param cp: Constraint Problem, can be useful to get more information on the problem
    :type cached_values: list[str]
    :param cached_values: list of values to cache
    :param dynamic: should we pick values from the NFP boundary parts inside the dynamic IFPs?
    :rtype: list[Placement]

    """
    placements = node.assignment
    cache = node.cache

    min_bb_area_placements = []

    for orientation in node.problem.cp.pieces[piece_key.id].orientations:
        # get the ifp depending on the problem type
        ifp = node.cache.get_ifp(piece_key.id, orientation, dynamic)

        # handle no restriction case
        if ifp is None:
            # floating origin and no pieces placed before, arbitrarily choose (0, 0)
            min_bb_area_placements.append(Placement((0, 0), orientation))
            continue

        # handle full restriction (unlikely as long as piece size and allowed orientations make sense)
        if ifp.is_empty:
            logging.warning('IFP of {} is empty for orientation {}'.format(piece_key, orientation))
            continue  # no valid positions

        if not placements:
            # fixed origin but no placements yet
            # since the problem has a fixed origin, it is worth trying all the corners:
            # any position gives the same BB but as in FPV, some may lead to a solution, others to a failure
            min_bb_area_placements.extend((Placement(vertex, orientation) for vertex in get_unique_coords(ifp.boundary)))
            continue

        # compute the union of NFPs instead of directly the fit polygon,
        # because the min BB positions are located
        # on the NFP boundary, whereas the fit polygon has a larger boundary with uninteresting
        # positions on the edge of the container
        # at the same time, there are at most 4 corners added in the domain compared to NFP inter IFP,
        # and those corners have a very high BB area so they won't be chosen; the edges linking to them
        # must also give a high BB because they lack contact with previous pieces
        # (this also means that even in Forward Checking, using the domains directly is difficult,
        # OPTIMIZE: redundant with internal calculation in get_ifp, reuse domains as above?
        # For now, we cache the nfps and recompute the domains (ifp - nfp) when required
        nfp_union = node.cache.get_nfp(piece_key.id, orientation)

        # get the contact positions that are also inside the container
        # we use a trick to automate intersection computation but we could also compute
        # nfp_union & ifp.closed_interior | nfp_union & ifp.boundary (the 2nd term is redundant for rectangular IFPs)
        contact_position_geom = (nfp_union.get_boundary_as_pwb() & ifp).boundary

        if contact_position_geom.is_empty:
            continue  # no valid positions for this orientation (should be valid at least for some other orientations if FC is used)

        placed_pieces_bounds = get_layout_bounds(cp.lot, placements)
        # rotate piece to place so that they are ready for the function that minimizes the evaluation, and find the argmins
        rotated_next_piece = rotate_origin(cp.lot[piece_key], orientation)
        min_bb_translations, _ = get_min_bb_translations_along(
            box(*placed_pieces_bounds), to_bounding_box(rotated_next_piece), contact_position_geom)

        min_bb_area_placements.extend((Placement(min_bb_translation, orientation)
                                       for min_bb_translation in min_bb_translations))

    return min_bb_area_placements


@random_sampler
def max_bb_overlap_placement_picker(piece_key, node, cp, dynamic):
    """
    Value picker:

    Return placements for which the overlap of the bounding box of the new piece with
    the previous pieces (do not count overlap with each previous piece individually).

    """
    # TODO

# TODO: add simple all_contact_vertex_pickers (similar to RDV but without inefficient IFP corners)


@random_sampler
def dominant_point_picker(piece_key, node, cp, dynamic):
    """
    Value picker:

    Select the dominant point, or the dominant points that share the same distance,
    i.e. the point on the NFP the farther from the boundary of the convex hull of the NFP.
    Intuitively, for that point B touches A and thanks to shape matching it could enter
    some crevice or so inside A.

    See Cuckoo 6. Clustering for more information

    :param piece_key:
    :param node:
    :param cp:
    :param dynamic:
    :return:
    """
    placements = node.assignment

    # TODO: support dominant point for each island with contact graph
    # WARNING: NFP with previous layout is always global. If you want to minimize computations
    # to get the convex hull of the NFP with each island, you have to cache the islands IDs from the contact graph
    # and also cache the union of NFPs toward each island (very advanced)

    # list of placements sharing the same depth in the NFP convex hull
    dominant_point_placements = []

    for orientation in node.problem.cp.pieces[piece_key.id].orientations:
        # get the ifp depending on the problem type (cache not used, but dynamic ifp need to be recomputed anyway)
        ifp = node.cache.get_ifp(piece_key.id, orientation, dynamic)

        # handle no restriction case
        if ifp is None:
            # floating origin and no pieces placed before, arbitrarily choose (0, 0)
            dominant_point_placements.append(Placement((0, 0), orientation))
            continue

        # handle full restriction (unlikely as long as piece size and allowed orientations make sense, even in dynamic mode)
        if ifp.is_empty:
            logging.warning('IFP of {} is empty for orientation {}'.format(piece_key, orientation))
            continue  # no valid positions

        if not placements:
            # fixed origin but no placements yet
            # since the problem has a fixed origin, it is worth trying all the corners
            dominant_point_placements.extend((Placement(vertex, orientation) for vertex in ifp.boundary.coords))
            continue

        # get vertices of NFP inside IFP
        nfp_union = node.cache.get_nfp(piece_key.id, orientation)
        # get the convex hull boundary from the NFP, *before* intersecting with the IFP
        hull_boundary = nfp_union.convex_hull.boundary

        # get the contact positions that are also inside the container
        # we use a trick to automate intersection computation but we could also compute
        # nfp_union & ifp.closed_interior | nfp_union & ifp.boundary (the 2nd term is redundant for rectangular IFPs)
        contact_position_geom = (nfp_union.get_boundary_as_pwb() & ifp).boundary

        if contact_position_geom.is_empty:
            continue  # no valid positions for this orientation (should be valid at least for some other orientations if FC is used)

        # get points of max depth in convex hull of contact positions
        # since all vertices are inside or on the convex hull, we simply check the distance to the hull
        contact_vertices = get_unique_coords(contact_position_geom)

        max_depth = -1  # distances are positive
        max_depth_vertices = []

        for contact_vertex in contact_vertices:
            # FIXME: should compute depth with *opposite* side as in [Cuckoo]
            # otherwise, if the point is deep but close to an adjacent or opposide side of the real opposite side,
            # the point will be considered shallow
            depth = hull_boundary.distance(Point(contact_vertex))
            if depth > max_depth:
                # better depth found, reset list of vertices with this one
                max_depth = depth
                max_depth_vertices = [contact_vertex]
            elif depth == max_depth:
                max_depth_vertices.append(contact_vertex)

        dominant_point_placements.extend((Placement(max_depth_vertex, orientation)
                                          for max_depth_vertex in max_depth_vertices))

    return dominant_point_placements


# - value picker helpers -

def get_min_bb_translations_along(geometry1, geometry2, path):
    """
    Search relative translations along path that result in the bounding box of minimum area.
    Return tuple of list of translations and the corresponding area, or (None, 0) if path is empty.
    At least one translation should be found, but multiple translations may correspond to the min area.

    We recommend passing bounding boxes as piece_geometry arguments since this is the only information
    required and they are cheaper to manipulate.

    :type geometry1: BaseGeometry
    :type geometry2: BaseGeometry
    :param path: LinePoint-like geometry

    :rtype: (list[(float, float)], float)
    :return: tuple of list of translations, and bounding box area for any of these

    """
    # OPTIMIZE: use bounding boxes only from the beginning! path is given already
    # for a convex hull, of course, we could not do that, but then we could replace with convex hulls too

    # path must be composed of (Multi)LineString and (Multi)Points
    # Points can be iterated over directly, but for LineStrings we must check that there is no intermediate
    # point with a lower bounding box area, by searching the quadratic root over the translation along the line

    # we have to distinguish points and lines
    # REFACTOR? We could use raw boundaries in order to keep lines and points separate
    # However, we would also need to provide RawBoundary with intersection and union operations,
    # simplifying and moving geometry from multiline to multipoint members as lines intersect into points, etc.

    # list of ex-aequo translations
    min_translations = []
    min_area = float('+inf')

    # during each iteration we also track the lowest bounding box and the corresponding position up to now
    # we recurse on this method by splitting the path in different parts and keeping the minimum area
    # at each node of the recursive tree, in the local variables below
    if path.is_empty:
        logging.warning('Empty path passed in get_minimum_bounding_box_translation_along')
        return  # None, this should not happen in a collection (we could raise an Error too)

    elif is_multi(path):
        for part in list(path):
            translations, area = get_min_bb_translations_along(geometry1, geometry2, part)
            # IMPROVE: in case of draw, keep all translations that share the min area
            if area < min_area:
                # discard all previous translations, this is the new standard (note that we change the
                # reference of the list object but we could fill it with translations' elements too)
                min_translations = translations
                min_area = area
            elif area == min_area:
                # PRECISION: more tolerance? allow custom threshold not to miss quite good low BB area?
                # we found new translations with the best area on that part, add them to the list
                min_translations.extend(translations)

    elif path.type == 'Point':
        # translate piece 2 to position indicated by point
        translation = path.coords[0]
        min_translations.append(translation)
        # OPTIMIZE: use a function that directly computes bounds & area with minX/Y and maxX/Y values
        min_area = NFPVGStrategy.get_union_bb_area_after_translation(geometry1, geometry2, translation)

    elif path.type == "LineString":
        # create dynamic bounding box bounds, and check for min at each key point,
        # i.e. when the bounds start and stop changing

        key_positions = []

        # consider each segment of the line one by one
        segments = get_edges_from_coords(path.coords, loop=False)
        assert segments, 'No segments returned from coords: ' + str(list(path.coords))
        for segment in segments:
            initial_translation = fl(segment[0])  # line 1st vertex
            # key_positions.append(initial_translation)  # already in done in check_bounds_variation_keypoint
            translated_piece_geometry2 = translate(geometry2, *initial_translation)
            # REFACTOR: move static function check_bounds_variation_keypoint in newer module
            relative_key_translations = NFPVGStrategy.check_bounds_variation_keypoint(
                geometry1.bounds,
                translated_piece_geometry2.bounds,
                get_edge_vector(segment))
            # special numpy operation: add translation to *each* vector of the second array
            key_positions.extend(initial_translation + fl(relative_key_translations))

        # append very last vertex of last line in case of open LineString
        if not np.array_equal(segments[-1], key_positions[0]):
            # logging.debug('Open LineString provided. Adding last vertex as key position.')
            # last vertex is second vertex of last edge (segments[-1] == last value of local var segment)
            key_positions.append(segments[-1][1])

        assert key_positions, 'No key positions found, but the path is not empty (LineString).'

        # keep the best translation over all check points
        for key_position in key_positions:
            area = NFPVGStrategy.get_union_bb_area_after_translation(geometry1, geometry2, key_position)
            if area < min_area:
                # discard all previous translations, this is the new standard (note that we change the
                # reference of the list object but we could fill it with translations' elements too)
                min_translations[:] = [key_position]
                min_area = area
            elif area == min_area:
                # PRECISION: more tolerance? allow custom threshold not to miss quite good low BB area?
                # we found a new translation with the best area on that part, add it to the list
                min_translations.append(key_position)

    assert min_translations and not math.isinf(min_area), 'No translations / min area found'
    return min_translations, min_area


def get_max_bb_overlap_translations_along(geometry1, geometry2, path):
    """
    Search relative translations along path that result in the maximum bounding box overlap area.
    Return tuple of list of translations and the corresponding area, or (None, 0) if path is empty.
    At least one translation should be found, but multiple translations may correspond to the max overlap.

    We recommend passing bounding boxes as piece_geometry arguments since this is the only information
    required and they are cheaper to manipulate.

    :type geometry1: BaseGeometry
    :type geometry2: BaseGeometry
    :param path: LinePoint-like geometry

    :rtype: (list[(float, float)], float)
    :return: tuple of list of translations, and bounding box area for any of these

    """
    # REFACTOR: pass bounds rather than geometries? but less generic signature, since other
    # evaluation optimization method may not use only the bounding box

    # TODO

# -- node heuristics --

# - heuristic helpers -

def get_domain_size_orientation_average(domain):
    """
    Return an arbitrary measurement of the domain size, as a tuple
    (total area, total line length, point cardinal), averaged over the possible orientations.

    The total line length includes both the trivial and the extra boundary.

    Can be used as a comparable value for heuristics

    :type domain: ippy.packmath.nestingconstraintproblem.NestingDomain
    :param domain:
    :rtype: (float, float, int)

    """
    # if there is one None in domain, probably all values are None (first placement in floating-origin)
    # you can return anything, technically the domain is infinite though
    if None in domain.itervalues():
        return float('+inf')
    return np.mean(np.array([(translation_domain.area, translation_domain.length, translation_domain.points_cardinal)
                            for translation_domain in domain.itervalues()]), axis=0)

# - variable-based heuristics -

@null_for_initial_node
def minimum_domain_area_heuristic(node):
    """
    Variable-based heuristic
    CSP - Variable ordering - MDS - Measurement 1

    Return the area of the previous domain of the piece placed during this node's assignment.
    The lower the domain area, the more the piece was restricted before placing it.

    Inspired from the minimum remaining value (MRV) selection used for problems
    with discrete domains.

    The area does not take the boundaries of the fit-polygon into account, so it cannot be used
    to compare pieces for which the fit-polygons are reduced to lines and points.
    Use an advanced function for this purpose.

    Heuristic category: variable-based

    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
    :param node:
    :rtype: float

    """
    # OPTIMIZE: the domain area before placement is independent from the value chosen
    # could be computed at a higher value once and for all the nodes with the same variable assigned

    # we do not mind about giving higher priority to complete assignments, use deeper_node_heuristic_wrapper for this

    # we consider the previous domain just in case, but since this is evaluated when node is pushed
    # in the fringe, the domain is not reduced to a Point yet so node.domains would work as well
    remaining_domain = node.parent.domains[node.variable_assigned][node.value_assigned.rotation]
    if remaining_domain is None:
        # floating origin, all domains are None before 1st piece is placed
        # we can consider the area is infinite, or for more information consider
        # - the area of the NFP instead
        # IMPROVE: consider -area of NFP, so that pieces with bigger NFPs are placed first
        return float('+inf')
    # IMPROVE: consider - area of NFP intersection IFP in Open Dimension,
    # so that we don't have to compare huge areas
    return remaining_domain.area


@null_for_initial_node
def minimum_domain_area_length_heuristic(node):
    """
    Variable-based heuristic
    CSP - Variable ordering - MDS - Measurement 2

    :type node: NestingAssignmentSearchNode
    :return:

    """
    remaining_domain = node.parent.domains[node.variable_assigned][node.value_assigned.rotation]
    if remaining_domain is None:
        return float('+inf')

    return remaining_domain.area, remaining_domain.length

@null_for_initial_node
def minimum_domain_area_length_points_heuristic(node):
    """
    Variable-based heuristic
    CSP - Variable ordering - MDS - Measurement 3

    :type node: NestingAssignmentSearchNode
    :return:

    """
    remaining_domain = node.parent.domains[node.variable_assigned][node.value_assigned.rotation]

    # IMPROVE: consider piece by size here
    if remaining_domain is None:
        return float('+inf')

    # IMPROVE: length - closed_interior_length to get only the extra length boundary, more meaningful?
    return remaining_domain.area, remaining_domain.length, remaining_domain.points_cardinal

@null_for_initial_node
def minimum_domain_width_height_heuristic(node):
    """
    Variable-based heuristic
    CSP - Variable ordering - MDS - Measurement 4

    :type node: NestingAssignmentSearchNode
    :return:

    """
    remaining_domain = node.parent.domains[node.variable_assigned][node.value_assigned.rotation]
    if remaining_domain is None:
        return float('+inf')

    closed_interior_cumulative_size = get_cumulative_size(remaining_domain.closed_interior)
    # redundant if LineString split into individual segments
    boundary_cumulative_size = get_cumulative_size(remaining_domain.boundary)

    return sum(closed_interior_cumulative_size + boundary_cumulative_size)


@null_for_initial_node
def minimum_domain_diameter_heuristic(node):
    """
    Variable-based heuristic
    CSP - Variable ordering - MDS - Measurement 5

    :type node: NestingAssignmentSearchNode
    :return:

    """
    remaining_domain = node.parent.domains[node.variable_assigned][node.value_assigned.rotation]
    if remaining_domain is None:
        return float('+inf')

    # closed interior are gradually converted to MultiPolygon but some direct initialization may still use Polygons
    if remaining_domain.closed_interior.type == 'Polygon':
        return get_polygon_diameter(remaining_domain.closed_interior)

    return sum(get_polygon_diameter(polygon) for polygon in remaining_domain.closed_interior)


@null_for_initial_node
def minimum_domain_boundary_length_heuristic(node):
    """
    Variable-based heuristic
    CSP - Variable ordering - MDS - Measurement 6

    :type node: NestingAssignmentSearchNode
    :return:

    """
    remaining_domain = node.parent.domains[node.variable_assigned][node.value_assigned.rotation]
    if remaining_domain is None:
        return float('+inf')

    return remaining_domain.boundary.length, remaining_domain.points_cardinal


# - value-based heuristics -

@null_for_initial_node
def left_vertex_heuristic(node):
    """
    Value-based heuristic

    Return lower values for nodes that place a piece more to the left
    Note that each piece has a different width and reference point, which also change with orientation,
    so this heuristic only makes sense to comparing nodes that assign the same variable,
    with the same orientation.

    Actually this heuristic is mainly implemented to remove undeterministic issues and compare
    value picking methods that should lead to equivalent searches, such as FPV and RDV.

    If you are interested in minimizing the length of the container, prefer a variable-value cross-heuristic
    such as lower xMax on the placed piece's bounds or lower xMax on the new layout's bounds.

    Heuristic category: variable-based

    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
    :param node:
    :rtype: float

    """
    return node.value_assigned.translation[0]


@null_for_initial_node
def length_heuristic(node):
    """
    Value-based heuristic

    Return lower values for layouts with lower length

    Heuristic category: variable-based

    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
    :param node:
    :rtype: float

    """
    return node.problem.cp.get_length(node.assignment)


@null_for_initial_node
def bigger_piece_bb_area(node):
    """Bigger piece first, in BB area"""
    return - get_bounding_box_area(node.problem.cp.lot[node.variable_assigned])


@null_for_initial_node
def bigger_piece_area(node):
    """Bigger piece first, in area"""
    return - node.problem.cp.lot[node.variable_assigned].area


@null_for_initial_node
def exact_fit(node):
    """
    Value-based heuristic

    Prefer exact fit > exact slide > placement in domain closed interior

    Return 0 for exact fit, i.e. placement to an exterior point
    Return 1 for exact slide, i.e. placement on an exterior line
    Return 2 for others, i.e. placement in closed interior (including its boundary)

    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
    :rtype: float

    """
    piece_key = node.variable_assigned
    placement = node.value_assigned

    translation_domain = node.parent.dynamic_domains[piece_key][placement.rotation]

    # for first placement in floating-origin, cannot say
    # (actually we could say the big triangle in Tangram for example is in exact slide but
    # not so easy... count on MRD? but need to tune it to sort pieces by size...)
    if translation_domain is None:
        return float('+inf')

    # OPTIMIZE: add method to get extra boundary only, then test extra lines and extra points instead of polygon containment?
    # PRECISION: but keep a margin threshold just in case?
    # use only closed interior here
    # REFACTOR: improve PWB adding a method for this
    translation_point = Point(placement.translation)
    if not translation_domain.closed_interior.contains(translation_point) and \
            not translation_domain.closed_interior.touches(translation_point):
        # extra line or point
        # PRECISION: add threshold?
        if translation_domain.get_boundary_multipoint().contains(Point(placement.translation)):
            # extra point: exact fit
            return 0
        else:
            # extra line: exact slide
            return 1

    return 2


@null_for_initial_node
def max_contact_previous_layout(node):
    """
    Value-based heuristic

    Return the *opposite* of the number of contacts of the last piece placed with the placed pieces

    Maximize the number of contacts with pieces previously placed

    Requires: contact_graph cache

    :type node: NestingAssignmentSearchNode
    :return:

    """
    return - node.cache.get('contact_graph').get_nb_edges(node.variable_assigned)

# -- local evaluations --
# kind of variable-based heuristic that takes the whole layout into account, and sometimes its evolution
# during the last placement
# there are used in BoundedPriorityQueue for now so they take only one argument, a Node

# normalized variants of min_cost_heuristic, with only node argument (problem-specific, this time)

# L1: incentive length in Beam Search by Bennell et al.
@null_for_initial_node
def length_incentive(node):
    cp = node.problem.cp
    assert isinstance(cp, ConstraintOptimizationProblem)
    piece_key = node.variable_assigned
    # get length for the rotation with which it was placed
    # OPTIMIZE: precompute all rotated pieces at problem setup
    rotated_piece = rotate_origin(cp.pieces[piece_key.id].geom, node.value_assigned.rotation)
    piece_length = get_size(rotated_piece)[0]
    return cp.get_cost(node.assignment) / piece_length

# L2: balance length in Beam Search by Bennell et al.
@null_for_initial_node
def length_balance(node):
    cp = node.problem.cp
    assert isinstance(cp, ConstraintOptimizationProblem)
    piece_key = node.variable_assigned
    # get length for the rotation with which it was placed
    # OPTIMIZE: precompute all rotated pieces at problem setup
    rotated_piece = rotate_origin(cp.pieces[piece_key.id].geom, node.value_assigned.rotation)
    piece_length = get_size(rotated_piece)[0]
    old_length = cp.get_cost(node.parent.assignment) if node.parent is not None else 0.
    new_length = cp.get_cost(node.assignment)
    return (new_length - old_length) / piece_length


# TODO: overlap, rectangular enclosure (caution: we want to maximize overlap so reverse the value)
# O1: incentive overlap in Beam Search by Bennell et al.
# caution: reverse the value for overlap maximization!
@null_for_initial_node
def overlap_incentive(node):
    """
    CSP and COP

    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode

    """
    cp = node.problem.cp
    piece_key = node.variable_assigned
    # get area of intersection of bounding box with bounding boxes of other pieces
    # the paper does not tells clearly if we should count the area coming from the
    # intersection with different boxes twice, so we decide not to count it twice to get an actual
    # area we could represent at once on the plot
    placed_lot = get_placed_lot(cp.lot, node.assignment)  # this includes the last piece

    # ALTERNATIVE: compute the area of the intersection with each box individually, then remove the area
    # for each intersection of a couple of boxes

    # get the last piece placed itself
    piece_bb = to_bounding_box(placed_lot[piece_key])
    # get the union of the boxes of all other pieces, since we decided to count them as a whole
    placed_pieces_bb_union = unary_union([to_bounding_box(placed_piece) for placed_piece_key, placed_piece in placed_lot.iteritems()
                 if placed_piece_key != piece_key])
    intersection = piece_bb & placed_pieces_bb_union
    overlap_area = intersection.area

    return -overlap_area  # maximization


@null_for_initial_node
def overlap_balance(node):
    """
    CSP and COP

    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode

    """
    cp = node.problem.cp
    piece_key = node.variable_assigned

    # get piece bounding box area and use it to normalize overlap area, provided by overlap incentive
    # REFACTOR? one function to get overlap, and both overlap_incentive and overlap_balance use it

    piece_bb_area = get_bounding_box_area(cp.lot[piece_key])
    return overlap_incentive(node) / piece_bb_area

# TODO: add rectangular enclosure


def get_remaining_pieces_average_domain_size(node):
    """
    Helper for domain size based heuristics

    Return the average domain size over orientations of all remaining pieces

    :param node:
    :return:

    """
    total_domain_size_orientation_average = sum(
        (get_domain_size_orientation_average(node.domains[piece_key])
         for piece_key in node.unassigned_variables),
        fl([0, 0, 0])  # ensures that the result is a 3-array even if there are no remaining pieces
    )
    return total_domain_size_orientation_average

# FIXME: domain is only relevant AFTER filtering, but heuristics is computed BEFORE
# with the current fringe system, you would need to update the domain of the successors early to make this measurement
# meaningful; which is costly if you do that for all successors
def max_domain_value_heuristic(node):
    """
    Value-based absolute heuristic

    Return the opposite of the sum of the sizes of the domain of the remaining pieces (counting multiple times for
    multiple instances), averaged over all their orientations, in terms of area, line length and point cardinal.
    Here we consider the size of the domains after assignment

    Since we want the *least* constraining value, we prefer *bigger* domains, hence the opposition.

    The size must be Python tuples, not numpy arrays, because numpy arrays compare element-wise, not in order.

    Effect: placements that leave a lot of possibilities of further placements will be chosen first.
    Since we average over the orientations, pieces with many orientations will not have a higher weight,
    which is fine since pieces with more orientations are also more flexible.

    :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
    :rtype: (float, float, float)

    """
    # IMPROVE: add dynamic parameter to study dynamic domains instead? (you will have to use the partials
    # of this method as search parameter)
    # use numpy to compute average and sum element-wise
    total_domain_size_orientation_average = get_remaining_pieces_average_domain_size(node)
    # return Python tuple for ordered comparison
    return tuple(-total_domain_size_orientation_average)


@null_for_initial_node
def minimize_bounding_box_waste(node):
    """
    Value-based absolute heuristic

    Minimize the non-used space inside the bounding box of the layout.
    There is no guarantee some parts will not be actually used later.

    Equivalent to maximizing bounding box usage

    :type node: NestingAssignmentSearchNode
    :return:

    """
    # get bounding box of current layout
    # OPTIMIZE: only compute layout bounds, then remove sum of placed pieces area, etc.
    layout_geometry = node.get_current_layout_geometry()
    return get_bounding_box_waste(layout_geometry)


@null_for_initial_node
def minimize_convex_hull_waste(node):
    """
    Value-based absolute heuristic

    Minimize the non-used space inside the convex hull of the layout.
    There is no guarantee some parts will not be actually used later.

    Waste is relative, ie we maximize usage.
    So big layouts will be considered "the same" as small layouts
    (the measurement is stable for successive placements that keep around the same density)

    No support for islands yet

    Since relative computation uses division by zero, we just return 0 for the initial node,
    that has an empty layout

    :param node:
    :return:

    """
    # get convex hull of current layout
    layout_geometry = node.get_current_layout_geometry()
    return get_convex_hull_waste(layout_geometry)


# - domain-based evaluation -
def min_dead_regions_area(node):
    """
    Value/domain-based heuristic

    Minimize the areas of the dead regions

    Caution: since opening a free region make it merge with the rest and reduce its chance to become a dead region,
    the unwanted behavior may be that the pieces just avoid cutting regions.

    To counter that, we can instead compute the average area per dead region or so.

    :param node:
    :rtype: float

    """
    return sum(dead_region.area for dead_region in get_dead_regions(node))


# -- relative evaluations --
# value-based evaluations that compare previous and new values, a bit like what balance heuristics do in Bennell

@null_for_initial_node
def least_domain_constraining_value_heuristic(node):
    """
    Value-based relative heuristic
    Relative version of max_domain_value_heuristic

    Minimize the loss of domain, average on orientation, summed over all pieces.

    We consider the difference of size due to the assignment.

    Effect: same as max_domain_value_heuristic, but successors that reduce the domains the least will
    be chosen, even if the resulting domain is small compared to nodes from other branches

    :param node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
    :rtype: (float, float, float)

    :param node:
    :return:
    """
    # return Python tuple for ordered comparison
    # contains the loss of domain for the placed piece too, so contains a kind or MRV averaged
    return tuple(get_remaining_pieces_average_domain_size(node.parent) - get_remaining_pieces_average_domain_size(node))

# -- global evaluations -- (for beam search and as an alternative to depth heuristic, predicts final cost)
# mainly useful for COP but may be adapted to CSP

def final_length_upper_bound(node):
    """
    COP global evaluation by final cost estimation

    Fixed container problems consider the length from x=0 no matter what, whereas floating-origin problems
    only care about the bounding box the of placed pieces,
    so we delegate the computation of the cost = length to the problem.

    Rough computation of an upper bound for the final length from the partial assignment of a node.
    It adds the sum of the maximum width of all the remaining pieces among their possible orientations
    to the current width of the placement, which we would obtain by brutally placing all the next pieces
    in the lengthiest orientation possible.
    This is really an exaggerated estimation. Pieces can probably be set in any orientation without
    exceeding the container's height (otherwise, the orientation should not have been allowed in the first place),
    so the minimum of the lengths of a piece among their possible orientations would be a better evaluation already.

    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
    :param node: node from which we evaluate the upper bound of the final length
    :rtype: float
    :return: an upper bound of the final length of the placement

    """
    # REFACTOR: should we pass the problem as parameter as in heuristics or access it from the node?
    # but Priority Queues compare elements directly, so difficult... or we need to forget about
    # priority queues and just sort manually on each iteration
    cp = node.problem.cp
    piece_geoms = [cp.pieces[piece_key.id].geom for piece_key in cp.get_unassigned_variables(node.assignment)]
    # return sum of length (width) of all remaining pieces (or compute length once per type and multiple by instance nb)
    return sum(get_size(piece_geom)[0] for piece_geom in piece_geoms)


def fast_layout_completion(value_picker, heuristic, filter_fun=forward_check, cached_values=None):
    """
    Heuristic wrapper: the wrapper heuristic is used to complete the layout in node with a DFS without backtracking
    (single-pass)
    For COP, the cost of the completed layout is then returned.
    If no cost is defined (CSP), 0 is returned if a layout is found, 1/None else.
    IMPROVE: for CSP, return a higher value if the search stops earlier, to show that the partial layout must be even
    worse.

    The value returned by the inner function an upper bound for the final length of the layout.

    :param value_picker: value picker
    :param heuristic: heuristic used to sort nodes (as in graph search, but only one node picked per level)
    :param filter_fun: filter function to use (identical to graph search)
    :param cached_values: list of value names to cache
    :rtype: float

    """
    def global_heuristic(node):
        """
        Complete the layout with the passed heuristic with a single-pass based on the heuristic passed by the wrapper.
        Return the cost of the final length of the layout, or 0 if no cost is defined, if a solution is found.
        Else, return 1/None. (TBD... in COP, there is always a solution anyway)

        The body of this function is a simplified graph search with absolute priority given to depth
        (fringe is a PriorityQueueWithFunction as in greedy search).

        :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
        :param node: node from which we evaluate the upper bound of the final length
        :rtype: float

        """
        problem = node.problem

        # start with the node passed as initial node
        fringe = PriorityQueueWithFunction(heuristic)
        fringe.push(node)

        while fringe:
            # pop next node according to fringe data structure priority
            # we have not studied the node until now so domains and cache are still identical to the parent node's
            last_node = fringe.pop()
            assignment = last_node.assignment
            logging.debug('single-pass global evaluation: iteration')

            # in single-pass you must not try other nodes in the same level: clear the fringe
            fringe.clear()

            # OPTIONAL
            # - singleton domain - (non-initial node only)
            if last_node.variable_assigned is not None:
                last_node.domains[last_node.variable_assigned] = problem.cp.create_domain_singleton(
                    last_node.variable_assigned, last_node.value_assigned)

            # if goal node i.e. assignment is complete, stop and return that node
            if problem.is_complete(assignment):
                if hasattr(problem, 'get_cost'):
                    return problem.get_cost(last_node)  # COP
                else:
                    return 0  # CSP

            # - filtering - (non-initial node only)
            # since we have just popped the node, the current domains in the node are still clones of
            # the parent node's domains; we should reduce them in-place, so that get_successors benefit from it
            if last_node.variable_assigned is not None and filter_fun is not None:
                empty_domain_tail = filter_fun(last_node.variable_assigned, assignment, last_node.domains, problem.cp)
                if empty_domain_tail is not None:
                    # the domain of empty_domain_tail was reduced to the empty set, this assignment is not completable
                    continue

            # - cache - (non-initial node only)
            # update cache in-place (no need to filter before, this does not rely on domains)
            # if at least one value is cached
            # if last_node.parent is not None and cached_values:
            #     problem.cp.update_cache(last_node)  # OLD

            successors = problem.get_successors(last_node, value_picker)
            for successor in successors:
                # push successor to fringe; if a priority queue, will be sorted *now* not later!
                fringe.push(successor)

        return float('+inf')
        # return None

    return global_heuristic


# also used as dynamic cost updater
def area_based_final_length_estimation(node):
    """
    Global heuristic for COP

    Estimation of the final length by using the ratio of the area of placed pieces
    on the area of all the pieces.

    We want to minimize the final length so this can be used as is as a COP heuristic.

    """
    cp = node.problem.cp

    assert isinstance(cp, ConstraintOptimizationProblem), 'area_based_final_length_estimation heuristic does not ' \
                                                          'support CSP. Please add get_cost() -> length method if you prefer'
    assert isinstance(cp, InputMinimizationNestingProblem)

    # if no assignment yet, cannot estimate length... but initial node only so value does not matter
    # still use the min length because this function is also used for dynamic cost reevaluation
    if not node.assignment:
        return cp.get_min_cost()

    length = node.cost
    sum_area_placed_pieces = cp.get_sum_area_placed_pieces(node.assignment)
    sum_area_all_pieces = cp.get_sum_area_all_pieces()
    return length / sum_area_placed_pieces * sum_area_all_pieces


def current_usage_based_final_length_estimation(node):
    """
    Global heuristic for COP

    Estimation of the final length by using the usage ratio inside the convex hull of the
    current placement

    We want to minimize the final length so this can be used as is as a COP heuristic.

    """
    # if no assignment yet, cannot estimate length... but initial node only so value does not matter
    if not node.assignment:
        return 0

    cp = node.problem.cp
    assert isinstance(cp, ConstraintOptimizationProblem), 'area_based_final_length_estimation heuristic does not ' \
                                                          'support CSP. Please add get_cost() -> length method if you prefer'
    assert isinstance(cp, InputMinimizationNestingProblem)

    # arbitrary definition of a hull containing the current layout, where new pieces will probably not
    # be inserted later
    # we choose the conex hull here
    layout_geometry = get_layout_geometry(cp.lot, node.assignment)
    # TODO: add variants with different convex hulls
    layout_hull = layout_geometry.convex_hull
    container_height = cp.get_container_height()

    sum_area_placed_pieces = cp.get_sum_area_placed_pieces(node.assignment)
    sum_area_all_pieces = cp.get_sum_area_all_pieces()

    current_usage = sum_area_placed_pieces / layout_hull.area

    # sum of all piece areas / container height / (sum of area of placed pieces / layout hull area)
    return sum_area_all_pieces / container_height / current_usage


# to maximize usage, either use current_usage_based_final_length_estimation
# since this estimated length is inversely proportional to usage, or
# one of the get_(relative_)waste functions (different hulls available)
