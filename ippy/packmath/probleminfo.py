from ippy.packmath.problemfactory import create_expanded_packing_problem_from_file, \
    create_fixed_origin_single_bin_size_problem
from ippy.utils.files import get_full_path

__author__ = 'hs'


def get_sum_piece_areas_for_problem(xml_filepath):
    expanded_problem = create_expanded_packing_problem_from_file(xml_filepath)
    fixed_origin_single_bin_size_problem = create_fixed_origin_single_bin_size_problem(expanded_problem)
    return fixed_origin_single_bin_size_problem.get_sum_area_all_pieces()


def main():
    # xml_filepath = get_full_path('benchmark', 'improved_xml', 'shirts_improved.xml')
    # xml_filepath = get_full_path('benchmark', 'improved_xml', 'blaz_cleared.xml')
    # xml_filepath = get_full_path('benchmark', 'improved_xml', 'dagli_cleared.xml')
    # xml_filepath = get_full_path('benchmark', 'improved_xml', 'albano_cleared.xml')
    # xml_filepath = get_full_path('benchmark', 'improved_xml', 'swim_improved.xml')
    # xml_filepath = get_full_path('benchmark', 'improved_xml', 'trousers_improved.xml')
    # xml_filepath = get_full_path('benchmark', 'improved_xml', 'fu_cleared.xml')
    # xml_filepath = get_full_path('benchmark', 'improved_xml', 'marques_improved.xml')
    # xml_filepath = get_full_path('benchmark', 'improved_xml', 'jakobs1_cleared.xml')
    # xml_filepath = get_full_path('benchmark', 'improved_xml', 'jakobs2_cleared.xml')
    # xml_filepath = get_full_path('benchmark', 'improved_xml', 'dighe2_improved.xml')
    # xml_filepath = get_full_path('benchmark', 'improved_xml', 'dighe1_improved.xml')
    xml_filepath = get_full_path('benchmark', 'Personal', 'tangram', 'problem_tangram.xml')
    expanded_problem = create_expanded_packing_problem_from_file(xml_filepath)
    fixed_origin_single_bin_size_problem = create_fixed_origin_single_bin_size_problem(expanded_problem)
    cp = fixed_origin_single_bin_size_problem
    sum_area = cp.get_sum_area_all_pieces()
    container_height = cp.get_container_height()
    total_vertices = cp.get_total_vertices()
    print 'total piece area: ' + str(sum_area)
    print 'container height: ' + str(container_height)
    print 'min cost: ' + str(sum_area / container_height)
    print 'total vertices: ' + str(total_vertices)


if __name__ == '__main__':
    main()
