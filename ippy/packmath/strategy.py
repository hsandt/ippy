from collections import OrderedDict
from ippy.config.config import ExecutionLevel, RoundingLevel
from ippy.packmath.constraintsearch import null_heuristic, depth_first_heuristic
from ippy.packmath.dynamiclength import get_current_dynamic_cost
from ippy.packmath.dynamiclength import get_min_length_for_non_empty_domain
from ippy.packmath.nestingconstraintsearch import rdv_value_picker, regular_picker, min_bb_area_placement_picker, \
    dominant_point_picker, analyze_free_space, analyze_free_region_knapsack, minimum_domain_area_length_points_heuristic, \
    left_vertex_heuristic, length_heuristic, bigger_piece_bb_area, bigger_piece_area, overlap_incentive, overlap_balance, \
    max_domain_value_heuristic, least_domain_constraining_value_heuristic, minimize_convex_hull_waste, exact_fit, \
    min_dead_regions_area, area_based_final_length_estimation, max_contact_previous_layout

__author__ = 'hs'


#
# fringe_name_class_tuples = [
#     ('normal', PriorityQueueWithFunction),
#     ('bounded', BoundedPriorityQueueWithFunction),
#     ('random pop', PriorityQueueWithFunctionRandomizedPop),
# ]

fringe_names = [
    'normal',
    'bounded',
    'random pop'
]

picker_functions = (
    ('rdv', rdv_value_picker),
    ('regular', regular_picker),
    ('min bb', min_bb_area_placement_picker),
    # ('max overlap', max_bb_overlap_placement_picker()),
    ('dominant point', dominant_point_picker),
)

filter_functions = (
    ('none', None),     # use None to indicate no function (nil not good, will return None instead of float)
    ('dead region', analyze_free_space),
    ('area knapsack', analyze_free_region_knapsack)
)

heuristic_functions = (
    ('none', null_heuristic),
    ('depth', depth_first_heuristic),
    ('mrd', minimum_domain_area_length_points_heuristic),
    ('left', left_vertex_heuristic),
    ('length', length_heuristic),
    ('bigger bb area', bigger_piece_bb_area),
    ('bigger area', bigger_piece_area),
    ('overlap I', overlap_incentive),
    ('overlap B', overlap_balance),
    ('max domain', max_domain_value_heuristic),
    ('least constraint', least_domain_constraining_value_heuristic),
    ('min ch waste', minimize_convex_hull_waste),
    ('exact', exact_fit),
    ('min dead area', min_dead_regions_area),
    ('max contact', max_contact_previous_layout),
)

cache_values = [
    'ifps',
    'dynamic_ifps',
    'nfps',
    'free_space',
    'dynamic_free_space',
    'contact_graph',
    'domains',
    'dynamic_domains'
]

dynamic_cost_functions = (
    ('none', get_current_dynamic_cost),  # dyna cost will not change
    ('non empty domain', get_min_length_for_non_empty_domain),
    ('area completion ratio ', area_based_final_length_estimation),
)

execution_level_value_tuples = (
    ('debug', ExecutionLevel.debug),
    ('semidebug', ExecutionLevel.semidebug),
    ('release', ExecutionLevel.release),
)

rounding_level_value_tuples = (
    ('raw', RoundingLevel.raw),
    ('weak', RoundingLevel.weak),
    ('strong', RoundingLevel.strong),
)

class Strategy(object):
    """Container for a search strategy"""

    def __init__(self, fringe_idx, picker_idx, filter_idx, heuristic_indices, cached_values, max_iterations,
                 bounded_fringe_limit, random_span, picker_sampling_active, picker_sampling, multistart,
                 force_fixed_length_value, fixed_length_value, dynamic_cost_fun_idx, execution_level, rounding_level):
        self.fringe_idx = fringe_idx
        self.picker_idx = picker_idx
        self.filter_idx = filter_idx
        self.heuristic_indices = heuristic_indices
        self.cached_values = cached_values
        self.max_iterations = max_iterations
        self.bounded_fringe_limit = bounded_fringe_limit
        self.random_span = random_span
        self.picker_sampling_active = picker_sampling_active
        self.picker_sampling = picker_sampling
        self.multistart = multistart
        self.force_fixed_length_value = force_fixed_length_value
        self.fixed_length_value = fixed_length_value
        self.dynamic_cost_fun_idx = dynamic_cost_fun_idx
        self.execution_level = execution_level
        self.rounding_level = rounding_level

    def get_settings_dict(self):
        """Return the settings as a dictionary of human-readable strings"""
        return OrderedDict([
            ('Fringe', fringe_names[self.fringe_idx]),
            ('Picker', picker_functions[self.picker_idx][0]),
            ('Filter', filter_functions[self.filter_idx][0]),
            ('1st heuristic', heuristic_functions[self.heuristic_indices[0]][0]),
            ('2nd heuristic', heuristic_functions[self.heuristic_indices[1]][0]),
            ('3rd heuristic', heuristic_functions[self.heuristic_indices[2]][0]),
            ('4th heuristic', heuristic_functions[self.heuristic_indices[3]][0]),
            ('Cached values', ', '.join(self.cached_values)),
            ('Max iterations', self.max_iterations),
            ('Bounded fringe limit', self.bounded_fringe_limit),
            ('Random span', self.random_span),
            ('Picker sampling', self.picker_sampling if self.picker_sampling_active else 'No'),
            ('Multistart', self.multistart),
            ('Force fixed length', self.force_fixed_length_value),
            ('Fixed length', self.fixed_length_value),
            ('Dynamic cost function', self.dynamic_cost_fun_idx),
            ('Execution level', self.execution_level.name),
            ('Rounding level', self.rounding_level.name)
        ])

    def __repr__(self):
        return '<Strategy>'

    def __str__(self):
        return '<Strategy>'
