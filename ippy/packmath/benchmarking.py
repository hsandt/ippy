from ippy.packmath.problemfactory import create_expanded_packing_problem_from_file, \
    create_fixed_origin_single_bin_size_problem, create_fixed_origin_open_dimension_problem, \
    create_floating_origin_single_bin_size_problem, create_floating_origin_open_dimension_problem

__author__ = 'hs'
#
# def load_with_options(problem_path):
#
# def solve_with_options(problem_path, )

create_problem_functions = {
    ('fixed', 'single'): create_fixed_origin_single_bin_size_problem,
    ('fixed', 'open'): create_fixed_origin_open_dimension_problem,
    ('floating', 'single'): create_floating_origin_single_bin_size_problem,
    ('floating', 'open'): create_floating_origin_open_dimension_problem,
}

# class ProblemConfig(object):
#     """Benchmark configuration for a given problem"""
#     def __init__(self, problem_filepath, ):
#         self.

def main():
    """
    Benchmark various problems with various strategies and print the result in a TXT file

    """
    # config
    # as with Sublime Text JSON, nested config apply to the specific problem variant,
    # same level config apply to all (override by nested configs?)
    # need to separate origins rather than passing a tuple ('fixed', 'floating')?
    # accept both?
    # how to nest?
    config = [
        {
            'problem_filepath': 'tangram.xml',
            'origin': ('fixed', 'floating'),
            'dimensions': ('single', 'open'),
            # 'origin': 'fixed',
            # 'single heuristic': ,  # heuristics for single size
            # 'open heuristic': ,

        }
    ]

    # or just use the same heuristics for every single size problem,
    # and then another set of heuristics for every open dim problem

    problem_filepaths = []

    # in order to be efficient, load the problem once, build variants once,
    # solve with each strategy once
    for problem_filepath in problem_filepaths:
        expanded_problem = create_expanded_packing_problem_from_file(problem_filepath)
        origin_modes = ['fixed', 'floating']
        for origin_mode in origin_modes:
            dimension_modes = ['single', 'open']
            for dimension_mode in dimension_modes:
                problem = create_problem_functions[(origin_mode, dimension_mode)](expanded_problem)


if __name__ == '__main__':
    main()
