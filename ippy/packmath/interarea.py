__author__ = 'hs'


def interarea(polygon1, polygon2):
    """
    Compute interarea between both polygons

    Currently:
    - no concavity tolerance
    - classic shapely Polygon


    """
    # take the convex hull of the union
    # intersection = polygon1 & polygon2
    union = polygon1 | polygon2
    union_hull = union.convex_hull
    area = union_hull.area - union.area
    # as a first approx.
    # TODO: complete computation
    return area
