__author__ = 'hs'


class PhiPolygon(object):
    """
    Connected phi-polygon, bounded or unbounded.

    Attributes
        outside_boundary: list[PhiLinearRing]
            A list of phi-polygons.

        inside_boundaries: list[LinearRing]
            A list of phi-polygons.
    """

    def __init__(self, outside_boundary, inside_boundaries):
        self.outside_boundary = outside_boundary
        self.inside_boundaries = inside_boundaries

    def __repr__(self):
        return "<PhiPolygon> ({0}, {1})".format(self.outside_boundary, self.inside_boundaries)

class PhiLinearRing(object):
    """
    Linear ring, bounded or unbounded.

    Attributes
        broken_lines: list[LineString]
            A list of line strings that represent the phi-linear ring.
    Either is_bounded is True and there is only one broken line in the list, which is a LinearRing (LinearRing derives from LineString),
    either is_bounded is False and there are at least 2 broken lines, and all the broken lines are unbounded, i.e. the first and the last edges are considered to extend to the infinite (they are actually rays).
    Later, we can create a specific class BrokenLine that would contain a LineString (sometimes a LinearRing) and an extra attribute rays that would give separately the extreme rays that extend the LineString to infinity (instead of points indicators).

        is_bounded: bool
            True if the PhiLinearRing contains a single LinearRing (bounded), False else (contains a list of unbounded LineStrings).
    """

    def __init__(self, broken_lines):
        self.broken_lines = broken_lines
        is_bounded = len(broken_lines) == 1

    def __repr__(self):
        return "<PhiLinearRing> ({0}, {1})".format(self.broken_lines, self.is_bounded)
