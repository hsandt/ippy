from collections import OrderedDict
import os

from Tkconstants import TOP, BOTH, BOTTOM
from Tkinter import Tk, Frame, Button, IntVar, Entry, StringVar, Label
from abc import abstractmethod, ABCMeta
import logging
import time
from time import strftime
from datetime import datetime

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure

from ippy.config import platform
from ippy.packmath.packhelper import get_layout_bounds
from ippy.search.constraint import ConstraintOptimizationProblem
from ippy.utils.files import get_full_path
from ippy.utils.iterables import flatten
from ippy.utils.shapelyextension import update_bounds, get_placed_lot, place_geom, get_size, to_size
from ippy.utils.visual import uniform_key_to_rgb, change_hsv_value
from ippy.view.matplotlibhelper import full_extent
from ippy.view.plot import get_uniform_dimension_bounds, to_patch_collection
from ippy.view.tkinterextension import GridFrameManager

__author__ = 'hs'

class LogEvent(object):
    def __init__(self, name, description, loglevel):
        self.name = name
        self.description = description
        self.loglevel = loglevel  # 10 by default, ie logging.DEBUG value

    def __str__(self):
        return self.name


class BaseConstraintSearchViewer(object):
    """
    Abstract base class for constraint search viewer

    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def event(self, name, *params):
        """Handle event called name with free params"""


class EmptyConstraintSearchViewer(BaseConstraintSearchViewer):
    """
    Convenience class that does nothing, only here to implement methods
    and avoid putting many if conditions in the code.

    """
    def __init__(self):
        super(EmptyConstraintSearchViewer, self).__init__()

    def event(self, name, *params):
        pass


class EventConstraintSearchViewer(BaseConstraintSearchViewer):
    """
    Constraint search viewer that records all events during the search

    :type cp: (ippy.search.constraint.ConstraintProblem | ippy.packmath.nestingconstraintproblem.InputMinimizationNestingProblem)
    :param cp: constraint problem observed, used by some viewers to show problem-specific information
    :param event_handler_factories: a list of callable that return an event handler, use to build the event handlers
        that handle any received event in the same order they are in the list. Typically a list of classes.
        Event handlers must implement an event method
    :param log_result: should we log the results in a file?
    :param problem_filepath: XML problem full filepath
    :type last_event: LogEvent
    :param last_event: last event logged, for easier access than getting the last element of events
    :param events: list of all events logged
    :param stats: dictionary of information on the running of the constraint problem search, updated in real-time

    """

    def __init__(self, cp, event_handler_factories, log_result=False, strategy=None, problem_filepath=None):
        super(EventConstraintSearchViewer, self).__init__()
        # self.successor_color = '#DD4814'
        # self.fringe_color = '#20a0c0'
        # self.solution_color = '#adeba8'
        # self.font_size = 11

        # REFACTOR: cp in BaseConstraintSearchViewer init
        self.cp = cp
        # instantiate event handlers from passed classes
        self.event_handlers = [event_handler_factory(self) for event_handler_factory in event_handler_factories]
        self.log_result = log_result
        self.strategy = strategy
        self.problem_filepath = problem_filepath

        self.last_event = None
        self.events = []

        self.stats = OrderedDict([
            # backtracking currently uses variable selection only so fringe has just the length of the list
            # returned by variable selectors, or 0/1 if it returns an iterator (no need to remember anything,
            # but in practice you probably store some vertices somewhere)
            ('best cost', 0),
            ('max assignment', 0),  # maximum number of var assignments reached after a pop (assumed valid)
            ('iterations', 0),
            ('expanded', 0),  # how many times have we expanded a node, after successfully passing filtering
            ('search time', 0),  # search time in sec, without setup
            ('max fringe size', 0),
            ('best cost iter', 0),      # iter at which the best cost was found
            ('best cost exp', 0),       # number of expanded nodes when the best cost was found
            ('best cost time', 0),      # time at which the best code was found
            ('forward check empty', 0),  # how many times an assignment was dropped before being logged thanks to FC
            ('filter2', 0),  # how many times advanced filtering dropped a node? (region analysis, etc.)
            ('optimize drop', 0),        # how many nodes were dropped because the best cost possible from this node was not better than the best cost found
            ('old pops', 0),
            ('new pops', 0),
        ])

        # self.clear_nodes_data()
        self.start_time = None  # clock time (to assess search speed)
        self.finished_time = None  # python datetime (to archive search result)

    def event_to_str(self):
        event_description = 'Events:\n'
        for event in self.events:
            event_description += '{}:\t{}\n'.format(event.name, event.description)
        return event_description

    def event(self, name, *params):
        # log the event with a different behavior based on its name
        # we could also name them handle_, but we call them log_ to insist on what they do,
        # and doing it first is required for some event handlers to work on top of that
        getattr(self, 'log_' + name)(*params)

        # delegate event handling for each event handler in the list, in order
        # this allows to combine plotting and console output for example, while ensuring which one comes first
        for event_handler in self.event_handlers:
            event_handler.event(name, *params)

    def log_event(self, name, description, loglevel=20):
        """
        Log the event with a name, a description and a logging level.

        The logging level is used by the console viewer.

        :param name:
        :param description:
        :param loglevel:
        :return:
        """
        self.last_event = LogEvent(name, description, loglevel)
        self.events.append(self.last_event)

    def log_started(self):
        self.log_event('started', 'Algorithm just started.')
        self.start_time = time.clock()

    def log_new_iteration(self, iteration_count, fringe_size):
        """
        Log event: a new iterations started

        Record number of total iterations of the viewer, and also log the number of iterations internally
        recorded by the search (in case of multi-search, the numbers are different). If no such number is
        provided, only the total number is shown.

        :type iteration_count: int
        :param iteration_count: number of iterations as recorded by the search
        :param fringe_size: size of the current fringe
        :return:
        """
        # self.current_fringe = fringe
        self.stats['max fringe size'] = max(self.stats['max fringe size'], fringe_size)
        self.stats['iterations'] += 1

        self.log_event('new iteration', '#{} [fringe size: {}]'.format(iteration_count, fringe_size))

    def log_old_popped(self, node):
        """
        Log event: a node has been popped, but it was already in the closed set: ignore it

        Useful to check if not visiting the same layouts really improves performance.

        :type node: ippy.packmath.contraintsearch.AssignmentNode

        """
        self.stats['old pops'] += 1
        self.log_event('old popped', '{0} was already visited'.format(str(node)))

    def log_new_popped(self, node):
        """
        Log event: the popped node was not in the closed set (most common).

        It corresponds to a new assignment but it is still a mere node exploration.

        :type node: ippy.packmath.contraintsearch.AssignmentNode

        """
        self.stats['new pops'] += 1
        self.log_event('new popped', '[{1} vars assigned] {0}'.format(str(node), len(node.assignment)))
        self.stats['max assignment'] = max(self.stats['max assignment'], len(node.assignment))

    def log_optimize_drop(self, last_node, best_cost):
        """
        Log event: the popped node was dropped because its dynamic cost was not inferior to the cost
        of the best solution found up to now

        :type last_node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
        :param last_node: dropped node
        :param best_cost: best cost that made us drop the node

        """
        self.stats['optimize drop'] += 1
        self.log_event('optimize drop', 'Drop node because dynamic cost {0} >= best cost {1}'
                       .format(str(last_node.dynamic_cost), best_cost))

    def log_dead_region(self, dead_regions):
        """
        Log event: found at least one dead region

        :type dead_regions: list[Polygon]

        """
        self.log_event('dead region', 'found dead regions: ' + ', '.join(map(str, dead_regions)))

    def log_expanded(self, node, successors, search_idx=0):
        """

        :param expanded_count:
        Log event: Forward Check caused empty domain to appear, backtrack.

        :param node: expanded node
        :param successors: list of nodes resulting from expansion
        :param search_idx: search branch on which expansion occurred (0 for single-search)

        """
        # self.last_expandeds, self.last_successors = nodes, successors

        # TODO: add comment for search branch? But in dependent-search, not so meaningful...
        self.stats['expanded'] += 1
        description = '#{2} -> {1} successors'.format(node, len(successors), self.stats['expanded'])
        self.log_event('expanded', description)

    def log_push(self, node, priority=0):
        """
        Log event: a node was pushed to the fringe
        If the fringe is a priority queue, the priority is also passed.

        :param node:
        :param priority:
        :return:

        """
        description = 'Node pushed to fringe with priority {1}: {0}'.format(node, priority)
        self.log_event('push', description, loglevel=10)

    def log_forward_check_empty(self, variable, value, empty_domain_tails):
        """
        Log event: Forward Check caused empty domain to appear, backtrack.

        :param variable: variable assigned, head of the forward check
        :param value: value assigned to the variable
        :param empty_domain_tails: variables whose domain was reduced to empty

        """
        self.stats['forward check empty'] += 1
        self.log_event('forward check empty',
                       'Forward check dropped {0} <- {1} (empty domain tails {2})'
                       .format(variable, value, empty_domain_tails))

    def log_filter2(self, node):
        """
        Log event: Advanced filtering

        :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode

        """
        self.stats['filter2'] += 1
        self.log_event('filter2',
                       'Assignment cannot be completed after assignment {0} <- {1}'
                       .format(node.variable_assigned, node.value_assigned))

    def log_finished(self, node, reason, error=None):
        """
        Log event: search stopped (successful or failed) with an assignment for a reason

        :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
        :param node: last node visited before the search stopped
        :type reason: str
        :param reason: reason for stopping the search among 'solution found', 'no solution found',
            'max iterations', 'cost span', 'topological error', 'exception'
        :param debug_geom: debug geometry (special Nesting problem, replace with *args
        if you create an independent module) -> not used here

        """
        stop_time = time.clock()
        self.finished_time = datetime.now()

        search_time = stop_time - self.start_time
        self.stats['search time'] = search_time

        # for CSP, we would like to have the final cost too
        if reason == 'solution found':
            self.stats['best cost'] = node.cost

        description = 'Finished algorithm for reason: "{}"'.format(reason)
        if node is not None and node.assignment:
            description += '\nLast assignment: {}'.format(node.assignment)

        self.log_event('finished', description)

        # if log_result active, log that to a file named after the benchmark
        if self.log_result:
            self.log_search_result(node, reason)

    def log_search_result(self, node, finished_reason):
        if self.problem_filepath is None:
            problem_basename = 'all_results.txt'
        else:
            problem_basename = os.path.splitext(os.path.basename(self.problem_filepath))[0]

        with open(get_full_path('benchmark', 'Results', problem_basename + '.txt'), 'a') as f:
            f.write('=========\n')
            f.write('Benchmark\t' + self.cp.name + '\n')
            f.write('File\t' + self.problem_filepath + '\n')
            finished_time_str = self.finished_time.strftime("%Y-%m-%d %H:%M:%S")
            f.write('Time\t' + finished_time_str + '\n')
            f.write('Platform\t' + '{}, {}'.format(platform.OS, platform.PROCESSOR) + '\n')
            f.write('\n')

            f.write('Strategy parameters\n')
            f.write('\n')

            settings = self.strategy.get_settings_dict()
            for setting, value in settings.iteritems():
                f.write(setting + '\t' + str(value) + '\n')
            f.write('\n')

            f.write('Finished reason\t' + finished_reason + '\n')
            f.write('\n')

            f.write('Stats\n')
            f.write('\n')
            # add special line for usage, which can be deduced from best cost
            # WARNING: packing problem only! remove when cleaning up your modules!
            # that said, usage = min cost / best cost so this is a value < 1
            # that has a meaning in any problem when a min cost can be estimated
            # best usage = (total area / container width) / best length
            # the first factor is get_min_cost for a COP
            if isinstance(self.cp, ConstraintOptimizationProblem):
                best_usage = self.cp.get_min_cost() / self.stats['best cost']
                f.write('Best usage' + '\t' + str(best_usage) + '\n')
            else:
                # for CSP write the fixed cost / actual cost
                # warning: really special packing this time!
                container = self.cp.get_current_container(node)
                if container is not None:
                    size = get_size(container)
                else:
                    size = 'infinity'
                f.write('Fixed cost' + '\t' + str(size) + '\n')

                bounding_box_size = to_size(get_layout_bounds(self.cp.lot, node.assignment))
                actual_length = bounding_box_size[0]

                f.write('Actual final cost' + '\t' + str(actual_length) + '\n')

            for stat, value in self.stats.iteritems():
                if stat == 'max assignment':
                    # show how many vars had to be assigned
                    message = '{!s} / {!s}'.format(value, len(self.cp.variables))
                else:
                    message = str(value)
                f.write(stat + '\t' + message + '\n')
            f.write('\n\n')

    def log_better_solution(self, node, cost, search_idx=0):
        """
        Log event: found a node with an assignment with a lower cost

        :param node: node with a better assignment
        :param cost: cost associated to that node

        """
        current_time = time.clock()
        best_cost_time = current_time - self.start_time

        self.stats['best cost'] = cost

        # record stats for when the best code was found
        self.stats['best cost iter'] = self.stats['iterations']
        self.stats['best cost exp'] = self.stats['expanded']
        self.stats['best cost time'] = best_cost_time

        description = 'Found a better cost {} with {}'.format(cost, node)
        self.log_event('better_solution', description)

    def log_found_model(self, node, search_idx=0):
        """
        Log event: found a model node

        Does not say whether the node was better or worse than others.
        Useful for beam search that does makes searches in parallel.

        :param node: node with a better assignment
        :param search_idx: index of the search branch that led to this model node

        """
        if hasattr(self.cp, 'get_cost'):
            description = 'Found model node on branch {1} with cost {2}: {0}'.format(node, search_idx, self.cp.get_cost(node.assignment))
        else:
            description = 'Found model node on branch {1}: {0}'.format(node, search_idx)
        self.log_event('found_model', description)

    def log_increase_dynamic_cost(self, previous_cost, new_cost, search_idx=0):
        """
        Log event: increase dynamic cost because filtering reduced one of the variable domains to empty

        :param previous_cost: cost before increase
        :param new_cost: cost after increase
        :param search_idx: index of the search branch that led to this model node

        """
        description = 'Incompletable layout, increase dynamic cost {} -> {}'.format(previous_cost, new_cost)
        self.log_event('increase_dynamic_cost', description)


class ConstraintSearchEventHandler(object):
    """
    Abstract base class for constraint search event handlers

    :type viewer: EventConstraintSearchViewer
    :param viewer: event viewer using this event handler

    """
    __metaclass__ = ABCMeta

    def __init__(self, viewer):
        self.viewer = viewer

    @abstractmethod
    def event(self, name, *params):
        """Handle event passed from viewer"""


# REFACTOR: dependent on Nesting problem, put that in a module on nesting side
class NestingPlotCSEventHandler(ConstraintSearchEventHandler):
    """
    Constraint search view that displays an animated plot of the search process.
    Only works with a Nesting problem.

    Attributes
        :param fig: plotting figure containing the animation
        :type axis: matplotlib.axes.Axes
        :param axis: axis to plot on (occupies all the figure)
        :param geoms: list of geometries to plot sequentially
        :param artists_sequence: sequence of list of artists to draw on each step of the animation
        :param piece_patch_collections: list of patches of placed pieces up to now
        :type piece_ids: list[str]
        :type cmap: (int) -> Color
        :type nb_axes: int
        :param nb_axes: number of axes in the figure (number of simultaneous searches)
        #:type interval: float
        #:param interval: time interval between two plots in the animation sequence, in ms
        :param notext:
        :param save_plot:

    """
    def __init__(self, viewer, nb_axes=1, notext=False, save_plot=False):
        super(NestingPlotCSEventHandler, self).__init__(viewer)
        # self.interval = interval

        self.fig = None
        self.nb_axes = nb_axes
        self.axes = []  # REFACTOR: do not initialize twice with handle_started
        # initialize bounds with infinite values in the opposite direction to be replaced on 1st update (extra axis too)
        self.axes_bounds = [[float('+inf'), float('+inf'), float('-inf'), float('-inf')] for i in xrange(nb_axes + 1)]

        self.last_artists_lists = [[] for i in xrange(nb_axes)]  # list of the list of artists added on a branch, indexed by search branch index
        self.artists_sequence = []  # unique sequence for all axes (choose how you update each)
        self.start_artists_idx = 0  # where to start showing the animation from
        self.active_artists_idx = 0  # current animation step shown / next animation step to draw
        self.best_result_subplot_idx = 0  # subplot to save

        self.notext = notext
        self.save_plot = save_plot

        # filtering data
        self.dead_regions = []

        # prepare color mapping on all pieces (nuance on different instances)
        piece_ids = self.viewer.cp.get_piece_ids()  # useful for later to find index of ID in that sorted list

        # count number of instances for each piece (we do not have access to piece quantity directly)
        instance_counter = self.viewer.cp.get_all_piece_instance_counter()

        # build colormap with hue based on ID, saturation based on instance idx
        self.colormap = {
            piece_key: uniform_key_to_rgb(piece_ids.index(piece_key.id), piece_key.instance_idx,
                                          len(piece_ids), instance_counter[piece_key.id])
            for piece_key in self.viewer.cp.lot.iterkeys()
        }

    def event(self, name, *params):
        # event received from view is in turn redirected to a specialized method
        if hasattr(self, 'handle_' + name):
            getattr(self, 'handle_' + name, None)(*params)

    def handle_started(self):
        """Handle event: search started, no iterations yet"""
        # add one axis per subplot (search branch), plus a bonus axis for meta-information (next piece to place)
        total_nb_axes = self.nb_axes + 1
        self.fig = Figure(figsize=(10 * total_nb_axes, 10))  # we create canvas for Tkinter later
        self.axes = [self.fig.add_subplot(1, total_nb_axes, i) for i in xrange(1, total_nb_axes + 1)]

    def handle_new_iteration(self, iteration_count, fringe_size):
        """
        Handle event: a new iterations started

        :type iteration_count: int
        :param iteration_count: number of iterations as recorded by the search
        :param fringe_size: size of the current fringe

        """
        # purge data used during single iteration only
        del self.dead_regions[:]

    def handle_old_popped(self, node):
        """
        Handle event: popped node was already in closed set, ignored.

        :type node: ippy.packmath.constraintsearch.AssignmentSearchNode

        """

    def handle_new_popped(self, node, search_idx=0):
        """
        Handle event: a node has been popped and was not in the closed set.
        It corresponds to a new assignment but it is still a mere node exploration.

        :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
        :type search_idx: int
        :param search_idx: search in which expansion occurred (0 in single-search)

        """
        # uncomment this if you want to see each and every step of the search
        # also comment the same line in expanded event to avoid plotting the same layout twice
        self.add_animation_state(node, search_idx)

    def handle_dead_region(self, dead_regions):
        """
        Handle event: found at least one dead region

        :type dead_regions: list[Polygon]

        """
        # store dead regions for use in handle_expanded (reset them on new iteration to avoid
        # showing them later, on a later iteration; happens if filtering skip node and no new dead region found)
        self.dead_regions = dead_regions

    def handle_expanded(self, node, successors, search_idx=0):
        """

        :param search_idx:
        Handle event: a node has been expanded.

        :type node: ippy.packmath.contraintsearch.AssignmentNode
        :param node: expanded node
        :type successors: list[ippy.packmath.contraintsearch.AssignmentNode]
        :param successors: list of successors obtained on expansion
        :type search_idx: int
        :param search_idx: search in which expansion occurred (0 in single-search)

        """
        # if we draw in expanded, we will not represent layouts dropped by FC
        # self.add_animation_state(node, search_idx)

    def handle_push(self, node, priority=0):
        """
        Handle event: a node was pushed to the fringe
        If the fringe is a priority queue, the priority is also passed.

        :param node:
        :param priority:
        :return:

        """

    def handle_forward_check_empty(self, variable, value, empty_domain_tail):
        """
        Handle event: Forward Check caused empty domain to appear, backtrack.

        :param variable: variable assigned, head of the forward check
        :param value: value assigned to the variable
        :param empty_domain_tail: variable whose domain was reduced to empty

        """
        pass

    # TODO: also pass remaining fringe as parameter, as in simpleai
    def handle_finished(self, node, reason, error=None):
        """
        Handle event: search stopped (successful or failed) with an assignment for a reason

        :param node: last node visited before the search stopped
        :type reason: str
        :param reason: reason for stopping the search among 'solution found', 'no solution found',
            'max iterations', 'cost span'
        :param error:

        """
        if self.fig is None or not self.axes:
            raise Exception("NestingPlotConstraintSearchViewer's figure/axis is None, is 'finished' event called before 'started' event?")

        # TODO: check that at least one non-empty geom was added

        # plot the search process if a solution was found (to show how we reached it)
        # OR no solution was found (to show how we reached a dead-end)
        # OR the max number of iterations were reached (to show where we were slowing down)
        # OR no better solution was found
        # 'cost span' is sent at the meta level of the dichotomy so no need to plot again, sub-problems did

        # if reason not in ('solution found', 'no solution found', 'max iterations', 'no better solution found',
        #               'fringe empty', 'exception', 'topological error'):
        #     logging.warn('[PLOT VIEW] Unknown finished reasion, not plotting the final result')
        #     return

        # if an exception was raised, plot the last layout (previous layout may have been skipped
        # because of filtering)
        if reason in ('exception', 'topological error'):
            self.add_animation_state(node, error=error)  # use axis_idx=0 by default, ok for debug

        for axis_idx in xrange(self.nb_axes + 1):  # apply to extra axis too
            # provide xy limits to show all pieces + a small margin at a uniform scale
            uniform_bounds = get_uniform_dimension_bounds(self.axes_bounds[axis_idx])
            self.axes[axis_idx].set_xlim(uniform_bounds[0] - 1, uniform_bounds[2] + 1)
            self.axes[axis_idx].set_ylim(uniform_bounds[1] - 1, uniform_bounds[3] + 1)

        # if normal mode, start with only the first step visible
        # in record mode, start with the best step
        for artist in flatten(self.artists_sequence):
            artist.set_visible(False)

        self.active_artists_idx = 0
        if self.save_plot:
            if reason == 'solution found':
                # CSP solution, last step
                self.active_artists_idx = len(self.artists_sequence) - 1
            elif reason in ('max iterations', 'no better solution found', 'fringe empty', 'exception', 'topological error'):
                # COP, maybe a solution
                # if best cost iter is 0, no COP solution
                if self.viewer.stats['best cost iter'] == 0:
                    # do not save plot
                    self.save_plot = False
                else:
                    # we assume we draw 2 steps per iteration
                    self.active_artists_idx = self.start_artists_idx
            else:
                # CSP no solution found and other future cases: do not save
                self.save_plot = False

        for artist in self.artists_sequence[self.active_artists_idx]:
            artist.set_visible(True)

        # logging.debug([repr(artist.get_figure()) for artist_collection in self.artists for artist in artist_collection])
        # anim = animation.ArtistAnimation(self.fig, self.artists_sequence, interval=self.interval, repeat=False)

        # reduce margins as much as possible and keep equal ratio
        self.fig.set_tight_layout(True)
        for axis in self.axes:  # includes extra axis
            axis.set_aspect('equal', 'datalim')

        # plt.show()

        # Tkinter app
        root = Tk()
        app = SearchViewApplication(self, master=root)

        # create canvas
        canvas = app.create_canvas(self.fig)

        # create toolbar
        app.create_toolbar(canvas)

        # if save option is active, save the best layout
        if self.save_plot:
            problem_basename = os.path.splitext(os.path.basename(self.viewer.problem_filepath))[0]
            finished_time_filestr = self.viewer.finished_time.strftime("%Y-%m-%d %H_%M_%S")
            best_cost_str = self.viewer.stats['best cost']
            image_filename = '{0} w{2} {1}'.format(problem_basename, finished_time_filestr, best_cost_str)

            # Save just the portion _inside_ the second axis's boundaries
            extent = full_extent(self.axes[self.best_result_subplot_idx]).transformed(self.fig.dpi_scale_trans.inverted())
            # Alternatively,
            # extent = ax.get_tightbbox(fig.canvas.renderer).transformed(fig.dpi_scale_trans.inverted())
            self.fig.savefig(get_full_path('benchmark', 'Results', 'plot', image_filename + '.svg'), bbox_inches=extent)
            self.fig.savefig(get_full_path('benchmark', 'Results', 'plot', image_filename + '.pdf'), bbox_inches=extent)

        # bind keyboard event to see placements step by step, using the canvas dedicated to Tkinter
        canvas.mpl_connect('key_press_event', self.on_keyboard)

        app.mainloop()
        root.destroy()

    def handle_found_model(self, node, search_idx=0):
        """
        Handle event: found a model node

        :param search_idx: index of the search branch that led to this model node

        """
        self.add_animation_state(node, search_idx)

    def handle_better_solution(self, node, cost, search_idx=0):
        """
        Handle event: found a node with an assignment with a lower cost

        :param node: node with a better assignment
        :param cost: cost associated to that node

        """
        # just record the current artist idx to plot it later, if not overriden by even better
        # the corresponding animation step has been called in handle_found_model
        # active_artists_idx is the prepared idx for next step so -1 to get 2nd step of last step
        self.start_artists_idx = self.active_artists_idx - 1
        self.best_result_subplot_idx = search_idx  # can be any subplot but the last, reserved to next piece

    def handle_increase_dynamic_cost(self, previous_cost, new_cost, search_idx=0):
        """
        Handle event: found a model node

        """

    def get_container_patch_collections(self, node, axis_idx):
        """
        Return list of patch collections representing the fixed / floating container given the current
        placements. Only floating mode requires the placements arguments.

        The list often contains only one patch collection, but it is easier to manipulate and concatenate lists.

        :param node: node containing the current placements
        :param axis: index of the axis on which to draw the container
        :return: list[PatchCollection]

        """
        if node.dynamic:
            container = self.viewer.cp.get_current_container(node)
        else:
            container = self.viewer.cp.get_maximum_container(node.assignment)

        # before first placement in floating origin problem, no container is defined; just ignore it
        if container is None:
            container_patch_collections = []
        else:
            # make container thinner to see domains better
            container_patch_collection = to_patch_collection(container, {'facecolor': 'w', 'lw': 1})
            # let the artist live in our axis, or it cannot be animated (we don't provide the axis later)
            self.axes[axis_idx].add_collection(container_patch_collection)
            container_patch_collections = [container_patch_collection]
        return container_patch_collections

    def add_animation_state(self, node, axis_idx=0, error=None):
        """
        Add one animation state = two animation steps, one to show the domain on which we are placing the last piece
        in node and one where we actually place it.

        :type node: ippy.packmath.nestingconstraintsearch.NestingAssignmentSearchNode
        :param axis_idx: index of the axis on which to draw the step. Useful for multi-searches
        :param error: contains extra geometry to plot as a debug information, in a special color

        """
        # main axis
        axis = self.axes[axis_idx]
        axis_bounds = self.axes_bounds[axis_idx]

        # extra axis to show next piece
        extra_axis = self.axes[-1]
        extra_axis_bounds = self.axes_bounds[-1]

        # COMMON PLOT
        # plots of other branches / axes (just freeze them to their previous state)
        other_branch_artist_collections = list(flatten([self.last_artists_lists[search_idx] for search_idx in xrange(self.nb_axes)
                                               if search_idx != axis_idx]))

        # text for this axis
        # update iteration number label with popped counter or iteration counter
        text_artists = []

        if not self.notext:
            iteration_text = axis.text(0.5, -0.6, 'Iteration #{}'.format(self.viewer.stats['iterations']),
                                       size='xx-large')
            expanded_text = axis.text(5.5, -0.6, 'Node expanded #{}'.format(self.viewer.stats['expanded']),
                                      size='xx-large')
            dynamic_cost_text = axis.text(1.0, -1.1, 'Dyna/Best cost {:.2f}/{:.2f}'.format(node.dynamic_cost,
                                                                                   self.viewer.stats['best cost']),
                                          size='x-large')

            text_artists.extend([iteration_text, expanded_text, dynamic_cost_text])

            # if node is a model, add a message 'Model'
            if node.is_complete:
                model_text = axis.text(8.0, -1.1, 'Model', size='x-large')
                text_artists.append(model_text)

        # if initial node, nothing to plot this time (the next node will draw the previous state anyway)
        # but iteration 0 (even the container is not displayed because meaningless in floating mode, but you can get
        # it as below if you prefer)
        if node.parent is None:
            self.artists_sequence.append(text_artists)
            # prepare index for next artist collection (used to track animation step)
            self.active_artists_idx += 1  # only 1 step
            return

        lot = self.viewer.cp.lot
        last_piece_key = node.variable_assigned  # not used, but useful later to put last piece placed in highlight
        new_placement = node.assignment[last_piece_key]

        # STEP 1: we show the previous layout, with the previous fixed/floating container,
        # already placed pieces and the remaining domain for the next piece we place,
        # as well as the next piece

        # PREVIOUS CONTAINER
        previous_placements = node.parent.assignment

        # the container may be floating so we delegate definition of the container to the problem
        previous_container_patch_collections = self.get_container_patch_collections(node.parent, axis_idx)
        # TODO: update bounds with container bounds, but in Open Dimension problem
        # do not update the width to the max width or the result plot will be too small

        # ALL PIECES PREVIOUSLY PLACED
        if previous_placements:
            placed_lot = get_placed_lot(lot, previous_placements)

            # OPTIMIZE: if a bottleneck or memory waste, cache patch collection for each piece
            #   with only rotation set, then translate the patches at the last moment with pyplot
            #   then when needed find the ID string back
            # trick: add_collection() returns the collection, so attach the patch to the axis in list comprehension
            previous_pieces_patch_collections = [
                axis.add_collection(
                    to_patch_collection(placed_piece, {
                        'facecolor': self.colormap[piece_key]
                    })
                )
                for piece_key, placed_piece in placed_lot.iteritems()
                ]

            # update plot bounds to contain all the previously placed pieces
            for placed_piece in placed_lot.itervalues():
                update_bounds(axis_bounds, placed_piece.bounds)

        else:
            # it is the previous placements, no previous pieces to plot
            previous_pieces_patch_collections = []

        # DOMAIN FOR THE NEW PIECE PLACED
        # get the patch collection for the remaining domain for the last piece before being added,
        # for the rotation that was chosen in the end

        # FIXED? MultineLineString back-and-forth issue in FPV can cause empty domain to appear, replaced by singleton...

        # if translation domain is None (everything) due to floating-origin on first iteration, do not draw it at all
        last_piece_domain_patch_collections = []
        translation_domain = node.parent.domains[last_piece_key][new_placement.rotation]
        if translation_domain is not None:

            if translation_domain.is_empty:
                raise ValueError(
                    'Domain empty for piece {} on parent node, should not be able to place it on {}!'.format(
                        last_piece_key, new_placement
                    ))

            last_piece_domain_patch_collection = to_patch_collection(
                node.parent.domains[last_piece_key][new_placement.rotation], {
                    'facecolor': self.colormap[last_piece_key],  # color for domain closed interior
                    'edgecolor': change_hsv_value(self.colormap[last_piece_key]),
                    'lw': 4
                    # color for boundary and closed interior boundary
                },
            )
            # domain are shown in transparency
            last_piece_domain_patch_collection.set_alpha(0.7)
            # associate to current axis
            axis.add_collection(last_piece_domain_patch_collection)
            # only one element in list but more flexible to work with empty list
            last_piece_domain_patch_collections.append(last_piece_domain_patch_collection)

        # DEBUG for now: show free space (otherwise in white by default) if cached
        last_free_space_patch_collections = []

        if 'free_space' in node.parent.cache.cache_dict:
            free_space = node.parent.cache.get('free_space')
            if free_space is not None:  # don't draw anything if free space is all set (initial node, floating only)
                if free_space.is_empty:
                    raise ValueError(
                        'Cached free space empty for piece {} on parent node, should not be able to place it on {}!'.format(
                            last_piece_key, new_placement
                        ))

                last_free_space_patch_collection = to_patch_collection(
                    free_space, {
                        'facecolor': 'orange',  # color for free space
                        'edgecolor': 'pink'  # you need humor be a programmer
                    }
                )
                # domain are shown in transparency
                last_free_space_patch_collection.set_alpha(0.1)
                # associate to current axis
                axis.add_collection(last_free_space_patch_collection)
                last_free_space_patch_collections.append(last_free_space_patch_collection)

        # show next piece to place (the last piece in this node)
        last_piece_geom = lot[last_piece_key]
        last_piece_patch_collection = extra_axis.add_collection(
            to_patch_collection(last_piece_geom, {
                'facecolor': self.colormap[last_piece_key]
            })
        )

        # update axis for next piece (so that we can see all pieces proportionally in the end)
        update_bounds(extra_axis_bounds, last_piece_geom.bounds)


        # EXTRA DEBUG GEOMETRY (GeometryException)
        extra_debug_collections = []
        if error is not None:
            debug_geom_patch_collection = to_patch_collection(
                error.geometry, {
                    'facecolor': 'red',  # color for free space
                }
            )
            debug_geom_patch_collection.set_alpha(0.5)
            axis.add_collection(debug_geom_patch_collection)
            extra_debug_collections.append(debug_geom_patch_collection)

            # also add a message showing this is an extra step for debug
            extra_debug_text = axis.text(-1.0, -0.8, 'ERROR: {}'.format(error.msg))
            text_artists.append(extra_debug_text)


        # add step to artists sequence
        artist_collections = previous_container_patch_collections + previous_pieces_patch_collections + \
            last_piece_domain_patch_collections + last_free_space_patch_collections + \
            text_artists + [last_piece_patch_collection] + extra_debug_collections

        self.artists_sequence.append(other_branch_artist_collections + artist_collections)

        # STEP 2: we display the container + all previous placed pieces + the last piece

        # NEW PIECE PLACED
        last_placed_piece = place_geom(lot[last_piece_key], new_placement)
        last_placed_piece_patch_collection = axis.add_collection(
            to_patch_collection(last_placed_piece, {
                'facecolor': self.colormap[last_piece_key]
            })
        )

        # update axis bounds for the last piece
        update_bounds(axis_bounds, last_placed_piece.bounds)

        # DEAD REGION DEBUG
        dead_region_patch_collections = []
        # REFACTOR: put dead region analysis and dynamic length before in code so that even initial node has it
        # anyway, here draw the dead region for *this* node
        if self.dead_regions:
            for region in self.dead_regions:
                region_patch_collection = to_patch_collection(
                    region, {
                        'facecolor': 'purple',  # color for free space
                    }
                )
                # domain are shown in transparency
                region_patch_collection.set_alpha(0.2)
                # associate to current axis
                axis.add_collection(region_patch_collection)
                dead_region_patch_collections.append(region_patch_collection)


        # prepare artist collections specific to this axis
        artist_collections = previous_container_patch_collections + previous_pieces_patch_collections + \
                             [last_placed_piece_patch_collection] + \
                             dead_region_patch_collections + \
                             text_artists + \
                             extra_debug_collections

        # add step to artists sequence
        self.artists_sequence.append(other_branch_artist_collections + artist_collections)

        # update last artist collection for this axis so that other branches draw it in its last state
        self.last_artists_lists[axis_idx] = artist_collections

        # prepare index for next artist collection (used to track animation step)
        self.active_artists_idx += 2  # 2 steps

    def on_keyboard(self, event):
        # print('you pressed', event.key, event.xdata, event.ydata)

        # if event.key not in ('left', 'right', 'down', 'up'):
        #     return

        # go to previous/next index with left/right, jump 10 with up/down
        if event.key == 'left':
            self.previous_step()
        elif event.key == 'right':
            self.next_step()
        elif event.key == 'down':
            # do not loop when jumping so that you can reach the start properly
            self.goto_frame(-10, relative=True, loop=False)
            # print self.active_artists_idx
        elif event.key == 'up':
            # do not loop when jumping so that you can reach the start properly
            self.goto_frame(+10, relative=True, loop=False)
        elif event.key == 'o':
            self.previous_state()
        elif event.key == 'p':
            self.next_state()

    # @refresh_artist
    def goto_frame(self, frame, relative=False, loop=False):
        """
        Go to a given frame

        :param frame: frame to go to (1 frame per step)
        :param relative: if True, the frame number is added to the current one
        :param loop: if True, the frame will loop at the beginning and at the end
            Note that loop set to True really makes sense when relative is True too

        """
        # logging.debug('goto frame: {} (relative: {})'.format(frame, relative))

        # hide current step
        for artist in self.artists_sequence[self.active_artists_idx]:
            artist.set_visible(False)

        # modify current frame index
        if relative:
            frame += self.active_artists_idx
        self.active_artists_idx = frame

        if self.active_artists_idx < 0:
            if loop:
                self.active_artists_idx = len(self.artists_sequence) - 1
            else:
                self.active_artists_idx = 0

        elif self.active_artists_idx >= len(self.artists_sequence):
            if loop:
                self.active_artists_idx = 0
            else:
                self.active_artists_idx = len(self.artists_sequence) - 1

        # show next step
        for artist in self.artists_sequence[self.active_artists_idx]:
            artist.set_visible(True)

        # flush (required)
        self.fig.canvas.draw()

    def previous_step(self, loop=True):
        """
        Go to the previous visualization step

        """
        self.goto_frame(-1, True, loop)

    def next_step(self, loop=True):
        """
        Go to the next visualization step

        """
        self.goto_frame(+1, True, loop)


    def previous_state(self, loop=True):
        """
        Go to the next visualization state (2 steps)

        """
        self.goto_frame(-2, True, loop)

    def next_state(self, loop=True):
        """
        Go to the next visualization state (2 steps)

        """
        self.goto_frame(+2, True, loop)


# now put directly in goto_frame method

# def refresh_artist(inner_fun):
#     """
#     Wrapper function that hides previous artist before
#     and show new artist after the inner function call.
#     Also flush the canvas.
#
#     """
#     def outer_fun(self, *args):
#
#         # hide current step
#         for artist in self.artists_sequence[self.active_artists_idx]:
#             artist.set_visible(False)
#
#         inner_fun(self, *args)
#
#         # show next step
#         for artist in self.artists_sequence[self.active_artists_idx]:
#             artist.set_visible(True)
#
#         # flush (required)
#         self.fig.canvas.draw()
#
#     return outer_fun

class ConsoleCSEventHandler(ConstraintSearchEventHandler):
    """
    Constraint search event handler that outputs the last event registered
    by the parent view.

    :param min_loglevel: minimum level of events to print (imitates Python logging level)

    """
    def __init__(self, viewer, min_loglevel=10):
        super(ConsoleCSEventHandler, self).__init__(viewer)
        self.min_loglevel = min_loglevel

    def event(self, name, *params):
        # log before plotting, because the plot is shown asynchronously and we want to know
        # immediately whether we stopped with a solution and failed to find one
        last_event = self.viewer.last_event
        self.output('EVENT {}: {}'.format(last_event.name, last_event.description), last_event.loglevel)

    def output(self, text, loglevel):
        if loglevel >= self.min_loglevel:
            logging.debug('Viewer: ' + text)


class SearchViewApplication(Frame):
    """
    Main application to execute and visualize packing

    :type plot_event_handler: NestingPlotCSEventHandler
    :param plot_event_handler:

    """
    def __init__(self, plot_event_handler, master=None):
        Frame.__init__(self, master)

        self.plot_event_handler = plot_event_handler

        # bind quit event to root (or bind_all on this frame)
        self.master.bind("<Escape>", self.quit_callback)
        self.master.protocol("WM_DELETE_WINDOW", self._quit)

        self.goto_target_state = IntVar(value=50)  # frame to go to (actually 2 steps by "frame")

        self.create_widgets()
        self.pack()

    def create_widgets(self):
        """
        Create all widgets

        """
        gf = GridFrameManager(self)

        # -- NAVIGATION FRAME --
        f = gf.new_frame(side=BOTTOM)

        gf.new_row()
        # gf.int_field("to", self.goto_target_state, width=3)
        # In this app, the entry value does not update automatically, so we get the value from the
        # entry widget instead
        label = Label(f, text="to")
        self.goto_entry = Entry(f, textvariable=self.goto_target_state)
        gf.next_column(label)
        gf.next_column(self.goto_entry)
        gf.next_column(Button(f, text='GO!!', command=self.go))
        # self.a.trace('w', self.validate)
        # self.a.bind('<FocusIn>', self.focus_in_entry)
        # self.a.bind('<FocusOut>', self.focus_out_entry)

        # -- APPLICATION ACTION FRAME --
        gf.new_frame()
        gf.new_row()

        quit_button = Button(f, text='QUIT', command=self._quit)
        gf.next_column(quit_button)

    # def validate(self, *dummy):
    #     print 'VALID: ' + self.a.get()
    #
    # def focus_in_entry(self, event):
    #     print 'IN'
    #     print event.widget.get()
    #
    # def focus_out_entry(self, event):
    #     print 'OUT'
    #     print event.widget.get()
    #     self.a.set(event.widget.get())

    def create_canvas(self, fig):
        """
        Create and return a canvas for a figure

        :param fig: figure that should contain the canvas
        :return: figure canvas

        """
        canvas = FigureCanvasTkAgg(fig, master=self)
        canvas.show()
        # canvas.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)
        return canvas

    def create_toolbar(self, canvas):
        """
        Create and return a toolbar for a canvas (a tk.DrawingArea)

        :param canvas: canvas for the toolbar
        :return: toolbar

        """
        toolbar = NavigationToolbar2TkAgg(canvas, self.master)  # self.master is the root window
        toolbar.update()
        canvas._tkcanvas.pack(side=TOP, fill=BOTH, expand=1)

    def go(self):
        self.plot_event_handler.goto_frame(2 * int(self.goto_entry.get()), relative=False)
        # self.plot_event_handler.goto_frame(2 * self.goto_target_state.get(), relative=False)
        # self.plot_event_handler.goto_frame(self.a.get(), relative=False)

    def quit_callback(self, event):
        self._quit()

    def _quit(self):
        self.quit()     # stop mainloop
        # self.destroy()  # this is necessary on Windows to prevent
                        # Fatal Python Error: PyEval_RestoreThread: NULL tstate
