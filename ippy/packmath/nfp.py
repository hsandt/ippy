import itertools
import math

import enum
import numpy as np
from numpy import float_ as fl, arctan2
from numpy.linalg import norm
from numpy.testing import assert_allclose
from shapely.affinity import translate
from shapely.geometry import Polygon, LinearRing, LineString, box, Point

from shapely.validation import explain_validity

from ippy.geometry.polytri import earclip_shapely_polygon
from ippy.utils.decorator import deprecated
from ippy.utils.geometry import Position, RelativePosition, get_edge_relative_position, Progression, get_points_orientation, \
    Orientation, check_if_point_inside_segment, get_vector_relative_position, get_relative_position_vector
from ippy.utils.iterables import flatten
from ippy.utils.shapelyextension import count_edge, PolygonWithBoundary, get_extreme_vertex_index_value, get_edges_from_coords, \
    get_edge_vectors, get_edge_vector, rotate_origin, check_equal, assert_validity, \
    round_coords, get_unique_coords, get_edges, raycast_polygon

__author__ = 'hs'


def get_ifp_with_rectangular_container(geom, width, height, min_x=0, min_y=0, orientation=0):
    """
    Return the inner fit polygon of a Geometry object rotated of orientation with a rectangular container
    of size (width, height), bottom-left at the (0, 0), as a PolygonWithBoundary.

    :param geom: Geometry object
    :param width: width of the rectangular container
    :param height: height of the rectangular container
    :param min_x: bottom-left corner x coord of the container
    :param min_y: bottom-left corner y coord of the container
    :param orientation: rotation angle of the polygon to fit, in degrees
    :return: inner fit polygon as PolygonWithBoundary
    """
    # rotate geometry according to orientation parameter
    rotated_geom = rotate_origin(geom, orientation)

    # rotation may have caused floating errors to appear, esp. if we rotated from an inexact value
    # to a value that should be exact (ex: in Tangram, medium triangle defined with sqrt(2) but rotated 45
    # gives 3.99999 -> ...9e-15 instead of 4)
    ifp_min_x = np.round(min_x - rotated_geom.bounds[0], 14)
    ifp_min_y = np.round(min_y - rotated_geom.bounds[1], 14)
    ifp_max_x = np.round(width - rotated_geom.bounds[2], 14)
    ifp_max_y = np.round(height - rotated_geom.bounds[3], 14)
    # box will allow min coords superior to max coords, simply returning the Polygon
    # so deal with empty ifp manually
    if ifp_min_x > ifp_max_x or ifp_min_y > ifp_max_y:
        # no place to fit the geometry at all
        return PolygonWithBoundary()
    elif ifp_min_x == ifp_max_x and ifp_min_y == ifp_max_y:
        # geom just fits into the container; probably at (0,0), but in case
        # the geom's origin is not at the bottom-left of its bounding box, use ifp_min_x/y
        # Note: Point case is handled separately, but a LineString reduced to 2 equal points would work too
        return PolygonWithBoundary(boundary=Point(ifp_min_x, ifp_min_y))
    elif ifp_min_x == ifp_max_x or ifp_min_y == ifp_max_y:
        # one size of geom just fits into the container but the other is smaller,
        # so the ifp is an axis-aligned LineString (geom can only slide inside the container)
        # in both cases, the diagonal formula works
        # (a flat Polygon gives unexpected results on intersection (similar to empty geometry)
        # so we use LineString in this case)
        return PolygonWithBoundary(boundary=LineString([(ifp_min_x, ifp_min_y), (ifp_max_x, ifp_max_y)]))
    else:
        # the geom can move around in the container, hitting the borders of the rectangle at some point
        return PolygonWithBoundary(box(ifp_min_x, ifp_min_y, ifp_max_x, ifp_max_y))


def nfp_cuninghame_green_with_convex_decomposition(polygon1, polygon2, orientation1=0, orientation2=0):
    """
    Return the NFP of two polygons by sorting the edges by orientation,
    as a PolygonWithBoundary.
    If necessary, Polygons are decomposed into convex polygons and the union of intermediate NFP is returned.
    Performance note: expect low performance, due to tessellation

    :param polygon1: a polygon
    :param polygon2: a polygon
    :param orientation1: orientation of polygon1
    :param orientation2: orientation of polygon2
    :return: NFP of the two POLYGONS as a PolygonWithBoundary

    """
    if not isinstance(polygon1, Polygon) or polygon1.is_empty:
        raise ValueError('polygon1 is not a non-empty Polygon')

    if not isinstance(polygon2, Polygon) or polygon2.is_empty:
        raise ValueError('polygon2 is not a non-empty Polygon')

    is_polygon1_convex = polygon1.convex_hull.equals(polygon1)
    is_polygon2_convex = polygon2.convex_hull.equals(polygon2)

    # DEBUG assertions to check if approximate equality is preferable
    assert polygon1.convex_hull.equals(polygon1) or not check_equal(polygon1.convex_hull, polygon1), \
        'Polygon1 only equal to its convex hull in approximation test; if acceptable to apply Cuninghame-Green,' \
        'apply approximate test instead of exact test'
    assert polygon2.convex_hull.equals(polygon2) or not check_equal(polygon2.convex_hull, polygon2), \
        'Polygon2 only equal to its convex hull in approximation test; if acceptable to apply Cuninghame-Green,' \
        'apply approximate test instead of exact test'

    if is_polygon1_convex and is_polygon2_convex:
        # no need for convex decomposition, use normal algorithm for convex - convex case
        # ALTERNATIVE: you can rotate polygons here instead of passing orientation parameters
        return nfp_cuninghame_green(polygon1, polygon2, orientation1, orientation2)

    # rotate both polygons according to orientation parameters
    # ALTERNATIVE: only rotate one polygon of the relative orientation, then rotate the obtained NFP
    # with one of the orientations
    rotated_polygon1 = rotate_origin(polygon1, orientation1)
    rotated_polygon2 = rotate_origin(polygon2, orientation2)

    # decompose polygons in pieces

    # both polygons are bounded
    if not is_polygon1_convex:
        # CAUTION: does not work with polygons with holes. Use a better decomposition for that
        if rotated_polygon1.interiors:
            raise ValueError('Polygon with hole is not supported. Please provide manual convex decomposition, '
                             'a tessellation algorithm that supports holes, '
                             'use Burke NFP method or and advanced Minkowski sum for polygon with holes')
        tesselated_polygon1 = list(earclip_shapely_polygon(rotated_polygon1))
    else:
        # do not tessellate if the shape is already convex! simply take one component equal to the polygon
        tesselated_polygon1 = [list(rotated_polygon1.exterior.coords)]

    # same with polygon2
    # REFACTOR: factorize with polygon1
    if not is_polygon2_convex:
        if rotated_polygon2.interiors:
            raise ValueError('Polygon with hole is not supported. Please provide manual convex decomposition, '
                             'a tessellation algorithm that supports holes, '
                             'use Burke NFP method or and advanced Minkowski sum for polygon with holes')
        tesselated_polygon2 = list(earclip_shapely_polygon(rotated_polygon2))
    else:
        tesselated_polygon2 = [list(rotated_polygon2.exterior.coords)]

    # OPTIMIZE: use a better tessellation algorithm (eventually we will use CGAL anyway)
    # OPTIMIZE: just precompute tessellation for your test shapes, it will be much more efficient and correct
    # earclip preserves CCW, but just in case you can check
    # either use a custom CCW check (sign of the algebraic area) or convert to LineRing and check is_ccw
    # there is a small chance that LineString modifies the order of the coordinates, in which case you
    #   must get back the new coords from the LineString, else just use the original coords (as we do here)
    # we reverse the iterators if needed to obtain CCW

    # REFACTOR: better to pass Geom rather than coords in convex_part?
    tesselated_polygon1_list = [convex_part if LinearRing(convex_part).is_ccw else convex_part[::-1] for convex_part in tesselated_polygon1]
    tesselated_polygon2_list = [convex_part if LinearRing(convex_part).is_ccw else convex_part[::-1] for convex_part in tesselated_polygon2]

    # compute NFP individually for each possible couple, and union incrementally
    # also store nfp boundary to handle LineString and Point feasible positions
    nfp = PolygonWithBoundary()
    for convex_part1, convex_part2 in itertools.product(tesselated_polygon1_list,
                                                        tesselated_polygon2_list):
        nfp_part = nfp_cuninghame_green(Polygon(convex_part1), Polygon(convex_part2))

        # NFP (not IFP) is never empty
        assert not nfp_part.closed_interior.is_empty, 'NFP part {} is empty'.format(nfp_part)
        # in this case, NFP should be a true Polygon
        assert nfp_part.closed_interior.type == 'Polygon', 'NFP part {} has type {}'.format(nfp_part, nfp_part.type)

        # special PolygonWithBoundary operation
        nfp = nfp | nfp_part

    # ultimate high-precision rounding to eliminate 1e-16 instead of 0, etc.
    nfp = round_coords(nfp, 14)

    # check NFP validity
    if not nfp.closed_interior.is_valid or not nfp.boundary.is_valid:
        raise ValueError('NFP produced by Cuninghame-Green with convex decomposition is invalid!'
                         'closed interior: {} - bounday: {}'
                         .format(explain_validity(nfp.closed_interior)), explain_validity(nfp.boundary))

    return nfp


# TODO: once CGAL-bindings are installed, try Minkowski difference instead
# Or just compute Minkowski in Python with one of the algorithms
# it should give the same result (it basically uses the same algorithm)
def nfp_cuninghame_green(convex_polygon1, convex_polygon2, orientation1=0, orientation2=0):
    """
    Return the NFP of two convex polygons by sorting the edges by orientation,
    when rotated by orientation parameters, as a Polygon

    :param convex_polygon1: a convex polygon
    :param convex_polygon2: a convex polygon
    :param orientation1: orientation of polygon1
    :param orientation2: orientation of polygon2
    :return: NFP of the two polygons (convex too) as PolygonWithBoundary

    """
    if not isinstance(convex_polygon1, Polygon) or convex_polygon1.is_empty:
        raise ValueError('convex_polygon1 is not a non-empty Polygon')
    if not check_equal(convex_polygon1.convex_hull, convex_polygon1):
        raise ValueError('convex_polygon1 is not a convex Polygon, please use nfp_cuninghame_green_with_convex_decomposition')

    if not isinstance(convex_polygon2, Polygon) or convex_polygon1.is_empty:
        raise ValueError('convex_polygon2 is not a non-empty Polygon')
    if not check_equal(convex_polygon2.convex_hull, convex_polygon2):
        raise ValueError('convex_polygon2 is not a convex Polygon, please use nfp_cuninghame_green_with_convex_decomposition')

    # rotate both polygons according to orientation parameters
    # ALTERNATIVE: only rotate one polygon of the relative orientation, then rotate the obtained NFP
    # with one of the orientations
    rotated_polygon1 = rotate_origin(convex_polygon1, orientation1)
    rotated_polygon2 = rotate_origin(convex_polygon2, orientation2)

    # quick fix for extreme vertex and merge issues: rounding coords
    # keep max precision for square roots, but remove 1e-15 dummy ...0001
    # ISSUE: "top-direction edges at the end of the sort are merged but not put back
    # at the beginning of the chain vectors" that gave us this idea, is fixed now but we can still use it
    rotated_polygon1 = round_coords(rotated_polygon1, decimals=14)
    rotated_polygon2 = round_coords(rotated_polygon2, decimals=14)

    # match leftmost vertex of polygon 2 with rightmost vertex of polygon 1 for initial nfp position
    # sensible to small variation of coordinates, but in the end the final NFP will be close to expectations
    polygon1_rightmost_bottom_idx, polygon1_rightmost_bottom_vertex = get_extreme_vertex_index_value(
        rotated_polygon1.exterior, Position.right, Position.bottom)
    polygon2_leftmost_top_idx, polygon2_leftmost_top_vertex = get_extreme_vertex_index_value(
        rotated_polygon2.exterior, Position.left, Position.top)
    initial_position = polygon1_rightmost_bottom_vertex - polygon2_leftmost_top_vertex

    # get edge vectors from each polygon
    edge_vectors1 = list(get_edge_vectors(rotated_polygon1.exterior.coords))
    edge_vectors2 = list(get_edge_vectors(rotated_polygon2.exterior.coords))

    # ONLY IF MERGING IS NOT USED

    # the first edge along which polygon 2 will orbit can be deduced from one of the two
    # edges that start (0, 0) at the vertices found above, resp. of polygon 1 and 2

    # get_potential_translation_vector is deprecated but much more convenient for this calculation,
    # since there is no real translation so no real edges for now, only abstract vectors
    # first_translation = get_potential_translation_vector(edge_vectors1[polygon1_rightmost_bottom_idx],
    #                                                      edge_vectors2[polygon2_leftmost_top_idx],
    #                                                      0, 0)

    # more complicated to use get_potential_translation_vector_from_edges... translate yourself the polygon to cause contact
    # tr_polygon2 = translate(rotated_polygon2, *polygon2_leftmost_top_vertex)
    #
    # # OPTIMIZE: if you really use this, combine with edge vector calculation above
    # edges1 = get_edges_from_coords(rotated_polygon1.exterior.coords)
    # edges2 = get_edges_from_coords(tr_polygon2.exterior.coords)
    #
    # first_translation = get_potential_translation_vector_from_edges(edges1[polygon1_rightmost_bottom_idx],
    #                                                                 edges2[polygon2_leftmost_top_idx])
    #
    # if first_translation is None:
    #     raise Exception('Both edge vectors are touching at Start yet no potential vector was found')
    #
    # # determine whether the potential translation vector was deduced from the stationary or orbiting edge
    # # (we do this manually, since it is only useful for the convex-convex case; otherwise, returning
    # # this information altogether with the vector, e.g. with a tuple of info, would be more appropriate)
    # polygon_idx = (1, polygon1_rightmost_bottom_idx) if array_equal(first_translation, edge_vectors1[polygon1_rightmost_bottom_idx]) else (2, polygon2_leftmost_top_idx)

    # END - ONLY IF MERGING IS NOT USED

    # build a list of edge vectors with an identified origin, as tuples: (#polygon, #edge, edge)
    # reverse sense of orbiting polygon edges for correct sorting and translations
    # if not using the cross-product, also store arctan value for sorting and merging later
    edge_vector_angles1 = ((edge_vector, arctan2(edge_vector[1], edge_vector[0])) for idx, edge_vector in enumerate(edge_vectors1))
    edge_vector_angles2 = ((-edge_vector, arctan2(-edge_vector[1], -edge_vector[0])) for idx, edge_vector in enumerate(edge_vectors2))
    np_edge_vector_angles = np.array(list(itertools.chain(edge_vector_angles1, edge_vector_angles2)),
                                     dtype=[('translation', np.float64, 2), ('angle', np.float64)])

    # in order to start at pi/2, you need to translate the sorted list, modulo 2*pi and translate back
    # work on angles only, ie the second column
    np_edge_vector_angles['angle'] = (np_edge_vector_angles['angle'] - math.pi / 2) % (2 * math.pi) - 3 * math.pi / 2

    # OPTIMIZE: use cross-product instead of arctan2 (but only allows relative comparison, so more computations; compare!)
    # if you use cross-product, you cannot rely on arctan2 for merging and must reuse cross-product == 0 to check if parallel
    # however, pure cross-product does not give a total order: it is cyclic! need to cut at pi/2 (top direction)
    np_edge_vector_angles.sort(order='angle')  # numpy sort, on last column (angle) by default

    # FIXED: to solve the extreme vertex offset floating error + merge angle tolerance issue,
    # we decided to use approximations every where, and keep the same initial position.
    # IMPROVE: a more robust implementation would be to detect when a merging contains edges in first and last
    # positions; if so, move the initial position to the start point of the merged edge (the start point
    # of the 1st edge in the merging if we had to chain them; not easy to retrieve though)
    # -> even if the initial position was wrong, you can have the right one at any time

    # merge vectors that have the same direction and sense (same angle modulo 2*pi or null cross-product)
    # no need to keep track of indices, just start from the top and iterate CCW to build the NFP

    # TEMP: disable edge merging

    # MERGING

    merged_vectors = []
    top_direction_vector = fl([0, 0])  # all top-direction vectors summed here
    last_angle = None  # to ensure the first equality is False
    for edge_vector, angle in np_edge_vector_angles:
        # after a rotation floating errors appear on angles, always compare with isclose
        # FIXED: finding extreme vertices is sensible to small coordinate changes,
        # which was contradictory with np.isclose comparison on angles
        # Buggy example: tangram medium triangle originally hypotenuse at the bottom, rotation 45,
        # has its top-left vertex on the bottom-left due to a small offset
        # However, the left side is almost vertical so it merges with the vertical side of the parallelogram (rot=90)
        # So NFP(parallelogram, mediumTriangle, 90, 45) is wrong (only in that sense, due to extreme vertices test)

        # all top-directed edges are merged in a special container because some may be
        # at the start of the sorted array, some at the end
        if np.isclose(angle % (2 * np.pi), np.pi / 2):  # pi/2 = top direction
            top_direction_vector += edge_vector
        elif last_angle is not None and np.isclose(angle, last_angle):
            # current edge vector belongs to the same class as the previous edge,
            # merge vector to current class by summing
            # DEBUG: if hinders performance, remove it
            assert np.isclose(np.cross(edge_vector, merged_vectors[-1]), 0),\
                'Edge has the same angle as the previous edge, but cross product is not close to zero'

            merged_vectors[-1] += edge_vector
        else:
            # a new class of edge vectors start here
            merged_vectors.append(edge_vector)
            last_angle = angle

    chain_vectors = [top_direction_vector] + merged_vectors

    # END MERGING

    # initialize nfp
    nfp_coords = [initial_position]

    # ONLY IF MERGING IS NOT USED
    #
    # # get index of first translation in list of all sorted edge vectors
    # first_translation_global_id = next(global_id for global_id, (edge_vector, angle) in enumerate(np_edge_vector_angles) if id_edge_vector[:2] == polygon_idx)
    #
    # chain_vectors = [edge_vector for edge_vector, angle in np_edge_vector_angles]
    #
    # # from this index, append last vertex + translation iteratively, going back to the beginning
    # # if you reached the end of the list, until you completed the circle
    # # this way, we can obtain the NFP for convex polygons by translating successively along polygon edges
    # for idx in xrange(first_translation_global_id, first_translation_global_id + len(np_edge_vector_angles)):
    #     nfp_coords.append(nfp_coords[-1] + np_edge_vector_angles[idx % len(np_edge_vector_angles)][2])
    #
    # END - ONLY IF MERGING IS NOT USED

    # append vectors one by one, ignoring the angle
    for edge_vector in chain_vectors:
        nfp_coords.append(nfp_coords[-1] + edge_vector)

    # build the final nfp from the coordinates
    # *ignore* the last coordinate and let shapely's constructor loop by itself
    # so that our method is robust again accumulated floating point error during the looped translations
    # just in case, send a warning if the last coordinate did not match the first coord (at 1e-16 or so)
    # FIXME: support degenerated cases
    # moderately sensitive test (most errors at 1e-14)
    assert_allclose(nfp_coords[-1], nfp_coords[0], rtol=0, atol=1e-7)
    # round at 14 decimals by default to have some precision but clean dummy floating errors
    nfp_coords = np.round(nfp_coords, decimals=14)
    nfp = PolygonWithBoundary(Polygon(nfp_coords[:-1]))  # remove last coordinate to avoid bowtie

    # check NFP validity
    assert_validity(nfp)

    return nfp


class Toucher(object):
    """Toucher structure"""

    def __init__(self, static_edge, moving_edge, intersection=None, translation=None):
        # touching point position (maybe vertex index and #polygon are more useful)
        self.static_edge = static_edge
        self.static_edge_vector = get_edge_vector(static_edge)  # derived field
        self.moving_edge = moving_edge
        self.moving_edge_vector = get_edge_vector(moving_edge)  # derived field

        # normally intersection is precomputed, but tests give a minimum of information
        # so at least for tests, compute intersection here if it is None
        if intersection is not None:
            self.intersection = intersection
            assert not intersection.is_empty, 'Intersection provided to initializer is empty.'
        else:
            # okay to convert to LineString for tests but if too costly, convert
            # edges before calling this constructor
            self.intersection = LineString(static_edge) & LineString(moving_edge)
            assert not self.intersection.is_empty, 'Intersection of static and moving edges is empty.'

        # compute intersection point ratio
        # static_edge_touch_position_ratio = norm(point - static_edge[0]) / norm(self.static_edge_vector)
        # moving_edge_touch_position_ratio = norm(point - moving_edge[0]) / norm(self.moving_edge_vector)

        # if a translation was given, use it (experimental)
        if translation is not None:
            self.translation = translation
        else:
            self.translation = get_potential_translation_vector_from_edges(static_edge, moving_edge, intersection)

    def __eq__(self, other):
        # compare core attributes (ignore derivated attributes since they are redundant)
        # use check_equal() method to compare shapely geometry objects with a small tolerance
        return type(self) == type(other) and check_equal(self.intersection, other.intersection) and \
            np.array_equal(self.static_edge, other.static_edge) and \
            np.array_equal(self.moving_edge, other.moving_edge) and \
            np.array_equal(self.translation, other.translation)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return "<Toucher: ({0}, {1}, {2}, {3})>".format(self.intersection, self.static_edge, self.moving_edge, self.translation)


# HIATUS: shapely can create acceptable geometries by using buffer(0) on a Polygon with hole
# completely described by an exterior that goes back and forth and rotates around holes in CW
# so we will just orbit around the piece as described in Burke

# of course, the graph is the most efficient but requires to analyze the structure to detect holes
# and remove interior boundaries manually in order to build the closed interior of the PWB
class Graph(object):
    """
    Graph class is used for the connected_component

    Attributes
        nodes : Node[]
            All nodes discovered in the graph so far. Also contains unexplored fringe.
    """
    def __init__(self):
        self.nodes = []

    def __repr__(self):
        return "<Graph: ({0})>".format(self.nodes)


class Node(object):
    """
    Node class contains edges and corresponding successors

    Attributes
        position : float[2]
            (x, y) position
        translationToNodeDict : dict<float[2], Node>
            dictionary associating an edge Vector2 translation to a successor node

    """
    def __init__(self, x, y):
        # TODO: use numpy.float array instead, but use an edge class with a custom hash method to use as key in the dict
        self.position = (x, y)

    def __repr__(self):
        return "<Node: ({0}, {1})>".format(self.position, [(translation, str(node)) \
            for translation, node in self.translationToNodeDict.iteritems()])


def make_toucher(param):
    pass



def find_next_start_point(polygon1, polygon2):
    """

    """
    polygon1_edge_count = count_edge(polygon1)
    polygon2_edge_count = count_edge(polygon2)

    # begin

    for i in xrange(polygon1_edge_count):
        if is_edge_marked(polygon1, i):
            continue
        else:
            static_edge = get_edges_from_coords(fl(polygon1.exterior.coords))[i]
        for j in xrange(polygon2_edge_count):
            moving_edge = get_edges_from_coords(fl(polygon2.exterior.coords))[i]

            # translate polygon 2 so that moving_edge start is on the start of the static edge
            current_polygon2 = translate(polygon2, *(moving_edge[0] - static_edge[0]))

            is_edge_finished = False
            intersection_test = current_polygon2.intersects(polygon1)

            while intersection_test and not is_edge_finished:
                # edge slide until not intersecting or end of static edge reached
                current_toucher = make_toucher(static_edge[0])
                trimmed_toucher = trim(current_toucher, polygon1, polygon2)
                current_polygon2 = translate(polygon2, *trimmed_toucher.translation)

                # FIXME: it is an overlap test
                intersection_test = current_polygon2.intersects(polygon1)

                if intersection_test:
                    if moving_edge[0] == static_edge[1]:
                        # end of static edge reached (for moving edge, only start matters)
                        is_edge_finished = True
                # mark the traversed edge as seen (whether edge start point found or not)

            # FIXME: if an edge is only traversed partially when a non-intersection position is found,
            # it will be marked as traversed and next time we may miss the remaining part of the edge!
            # -> mark the last position so that you can finish the study later
            # if you prefer, you can continue now to mark all free positions (it is a collection of intervals
            # so actually mark beginning and end positions of the intervals), then start from each free positions
            # found, ignoring free positions that have been explored during the orbiting from one of the previous
            # free positions
            mark(static_edge, True)

            if not intersection_test:
                # set the references to the points passed in to be the next start point
                next_start_point = moving_edge[0]
                polygon2_ref_point = moving_edge[0]
                return True, next_start_point, polygon2_ref_point

    # no start point found anymore, the algo will end
    return False, None, None

def nfp_burke(polygon1, polygon2, orientation1=0, orientation2=0):
    """Return the NFP of two polygons with Burke's method

    :param polygon1:
    :param polygon2:
    :param orientation1: orientation of polygon1
    :param orientation2: orientation of polygon2
    :rtype: PolygonWithBoundary
    :return: NFP of the two polygons

    """
    assert isinstance(polygon1, Polygon)
    assert isinstance(polygon2, Polygon)

    # rotate both polygons according to orientation parameters
    # ALTERNATIVE: only rotate one polygon of the relative orientation, then rotate the obtained NFP
    # with one of the orientations
    polygon1 = rotate_origin(polygon1, orientation1)
    polygon2 = rotate_origin(polygon2, orientation2)

    # match leftmost vertex of polygon 2 with rightmost vertex of polygon 1 for initial nfp position
    idx_rightmost1, rightmost1 = get_extreme_vertex_index_value(polygon1.exterior, Position.right)
    idx_leftmost2, leftmost2 = get_extreme_vertex_index_value(polygon2.exterior, Position.left)
    initial_feasible_position = rightmost1 - leftmost2

    # loop condition: are there still starting positions to try?
    is_start_point_available = True

    nfp_coords = []  # first element is list of exterior coords, following are lists of interior coords
    loop_count = 0

    # -- BEGIN --

    # place polygons at initial feasible position
    current_polygon2 = translate(polygon2, *initial_feasible_position)

    # set start and moving reference points, to test later if we got back to our start position
    nfp_loop_start_ref_point = polygon2_ref_point = leftmost2

    # store last translation to always turn on the left with the tighest angle
    last_translation = None

    while is_start_point_available:
        # if all edges of both polygons have been flagged as traversed, algorithm will end
        is_start_point_available = False

        # find touching points & segments touching those points, generate touching structures
        # TODO: optimize by memorizing touching points from the last trimming test
        toucher_structures = find_touchers(polygon1, current_polygon2)

        # TODO: add view to visualize the touchers and the NFP

        # not sure whether touching structures should contain potential translation vectors, or if trim should use them as temp
        # it seems better to me to add a step to consider potential translation vectors

        # eliminate non-feasible touches, ones that cause immediate intersection
        feasible_touchers = filter_can_move(polygon1, current_polygon2, toucher_structures)

        if not feasible_touchers:
            # no feasible touchers at all; this can only happen when trapped in a hole
            # (single fitting point on NFP boundary)
            is_start_point_available = True  # ??
            # TODO. prepare next iteration
            continue

        closest_turnleft_feasible_toucher = get_closest_turnleft_feasible_toucher(feasible_touchers, last_translation)

        # TODO: detect lines and points while orbiting! in holes, the polygons can be reduced to that...
        # buffer(0) may help

        # trim chosen translation/toucher against polygon 1 and 2
        # IMPROVE: do not always trim to the shortest translation if we can go on with no risk to miss a branching
        # nor collide
        trimmed_translation = trim(closest_turnleft_feasible_toucher, polygon1, current_polygon2, toucher_structures)

        # sort trimmed translations by length (only if we got several translations)
        # length_sorted_translations = sorted(trimmed_translations, key=len)

        # translate polygon 2 along longest feasible translation vector
        # current_polygon2 = translate(current_polygon2, *length_sorted_translations[0])
        current_polygon2 = translate(current_polygon2, *trimmed_translation)
        # update polygon 2 ref point: a - translate point the same amount or b - reuse idx_leftmost2 to get to new coord
        # chosen: b to ensure coord is consistent with translated polygon (a probably works well too)
        # polygon2_ref_point += trimmed_translation  # a [WARNING: requires numpy array!]
        polygon2_ref_point = current_polygon2.exterior.coords[idx_leftmost2]  # b

        # add translation to nfp_edges and mark traversed edge on static
        # each array entry corresponds to an NFP component (outer path or one of the inner paths)
        # TODO: in Python, [-1] should work as well
        # nfp_coords[loop_count].append(length_sorted_translations[0].static_edge_id)
        nfp_coords[loop_count].append(trimmed_translation)

        # IMPROVE: prefer numpy array
        if np.allclose(nfp_loop_start_ref_point, polygon2_ref_point):
            # we completed an NFP loop
            # find next feasible start point and reset polygon2_ref_point to relevant point
            # if find_next_start_point returns False in tuple 1st position here, the algo will end (nothing to explore)
            is_start_point_available, next_start_point, polygon2_ref_point = find_next_start_point(polygon1, polygon2)

            if is_start_point_available:
                # translate polygon 2 to next_start_point
                # we translate directly from the original position to avoid floating error accumulation
                # but we could also translate from the current position if it had any memory advantage (not for us)
                current_polygon2 = translate(polygon2, *next_start_point)
                polygon2_ref_point = current_polygon2.exterior.coords[idx_leftmost2]  # b

                nfp_loop_start_ref_point = next_start_point
                loop_count += 1

            else:
                # allow edge traversal to continue
                is_start_point_available = True

    # build the NFP from coordinates
    # since we started with leftmost/rightmost matching, the first path is always the outer path,
    # the others are inner paths ie holes

    # IMPROVE: verify looping as in Cuninghame-Green and remove last vertex to avoid bowtie!
    # TODO: add boundary
    nfp = PolygonWithBoundary(Polygon(nfp_coords[0], holes=nfp_coords[1:]))

    # check NFP validity
    if not nfp.closed_interior.is_valid or not nfp.boundary.is_valid:
        raise ValueError('NFP produced by Cuninghame-Green with convex decomposition is invalid!'
                         'closed interior: {} - bounday: {}'
                         .format(explain_validity(nfp.closed_interior)), explain_validity(nfp.boundary))

    return nfp


# HIATUS: really useful for multi-component pieces and to optimize exploration if many branchings
# for a problem with many convex pieces and non-convex pieces but without holes, this will not improve performance
def explore_connected_component(fringe, polygonA, polygonB):
    """
    Return the connect component containing all the nodes in the fringe.
    The fringe generally contains only one node, from which all connected nodes are explored.

    This is an alternative to Burke's orbiting method.
    We still need to seek starting positions and apply this function from each of these position.
    Alternatively, we can store in the fringe all root positions from which we can discover all
    the NFP boundary (1 for the exterior and 1 per hole), but in practice it is difficult to know
    if two positions are not connected in some way before completing the connected component.

    polygonA and polygonB are the polygons in their original position (reference point at the origin)

    """
    # initialize connected component
    connected_component = Graph()

    while not fringe.is_empty():
        node = fringe.pop()  # we recommend DFS, i.e. to use a Stack for the fringe
        position = node.position
        polygon1, polygon2 = polygonA, translate(polygonB, *position)

        # find touching points & segments touching those points, generate touching structures
        # TODO: optimize by memorizing touching points from the last trimming test
        toucher_structures = find_touchers(polygon1, polygon2)

        # TODO (DEBUG): add view to visualize the touchers and the NFP

        # eliminate non-feasible touchers, those that cause immediate intersection
        feasible_translations = filter_can_move(polygon1, polygon2, toucher_structures)

        # trim feasible translations against polygon 1 and 2
        trimmed_translations = trim(feasible_translations, polygon1, polygon2)

        for translation in trimmed_translations:
            # find the position of the successor bound to this translation
            successor_position = position + translation
            # identify the node by its position (instead of using a closed set as in traditional search)
            successor_node = next((node for node in connected_component if node.position == successor_position), None)

            # if the position is a new one, create a node at this position
            # and add it to the graph, as well as in the fringe
            # it means every new node is added once and only once in the fringe,
            # ensuring exactly N explorations for N nodes in the graph at the end
            if successor_node is Node:
                successor_node = Node(successor_position)
                connected_component.nodes.append(successor_node)
                fringe.push(position + translation)

            # link the current node to the successor node (either created or found)
            # through the translation
            node.translationToNodeDict[translation] = successor_node

    return connected_component  # it only contains references to nodes, so all edge-related information is inside nodes


def find_touchers(polygon1, polygon2):
    """Return list of touching structures for two polygons in their current positions"""
    # for each pair of edge of polygon 1 and 2 resp., test edge intersection
    # the Toucher constructor will verify that we have a touch point, not a cross intersection
    # TODO: optimization: middle touch info is redundant, only one is enough
    touchers = []
    for edge1 in get_edges_from_coords(polygon1.exterior.coords):
        for edge2 in get_edges_from_coords(polygon2.exterior.coords):
            # I think shapely is fast enough for segment intersection, but conversion may take some time
            # (besides, shapely may be slower than
            # algorithms in numpy so for small geometrical structures it may be appropriate to choose numpy)
            intersection = LineString(edge1) & LineString(edge2)
            if intersection.is_empty:
                continue
            toucher = Toucher(edge1, edge2, intersection)
            # we could ignore touchers with 0 translation, but we keep them for now
            # since they give additional information and will never be chosen
            # as long as there is a longer translation elsewhere
            touchers.append(toucher)
    return touchers


def check_feasible_range(toucher):
    """
    Return a tuple or 2 booleans, True if translations are feasible on the right of the static and moving edges,
    respectively.

    """
    # note: * are theoretically impossible cases, but can be encountered during hole search,
    # in which case the best direction to move to may vary

    # discriminate the colinear case first
    if toucher.intersection.type == 'LineString':
        # cases 1*, 19*, 26*, 32*, 7=13, 28=30: right of static, left of moving (no edge reversal)
        return True, False

    # then consider edges touching the other edge in the middle
    assert toucher.intersection.type == 'Point', "The toucher intersection is neither a LineString nor a Point."
    intersection_point = toucher.intersection.coords[0]

    static_edge_progression, moving_edge_progression = get_edge_progressions(toucher.static_edge, toucher.moving_edge,
                                                                             intersection_point)

    if static_edge_progression == Progression.middle:
        # cases 25*, 27: right of static only, the 2nd condition is ignored
        return True, None
    elif moving_edge_progression == Progression.middle:
        # cases 29, 31*: left of moving only, the 1st condition is ignored
        return None, False

    # cases 2-6, 8-12, 14-18, 20-24
    # re-orientate the edges:
    # reverse edges that touch at their ends, so that we can work with only edges starting from the intersection
    # use the edge *vector* to reverse easily; we assume vectors intersect at their start so relative vectors are enough
    starting_static_edge_vector = toucher.static_edge_vector * (-1 if static_edge_progression == Progression.end else 1)
    starting_moving_edge_vector = toucher.moving_edge_vector * (-1 if moving_edge_progression == Progression.end else 1)

    # now discriminate the remaining classes (representative cases 2, 3, 4, 5, 6)
    # here, everything is put at the origin so vectors represent their extremity
    orientation = get_points_orientation((0, 0), starting_static_edge_vector, starting_moving_edge_vector)
    if orientation in (Orientation.counter_clockwise, Orientation.aligned):
        # cases 2, 3, 4
        return False, False
    elif orientation == Orientation.clockwise:
        # cases 5, 6
        return True, True


def filter_can_move(current_polygon1, current_polygon2, toucher_structures):
    """Return the list of touching structures that are feasible, ie can move without immediate intersection"""
    # initialize feasible touchers (in practice, only the translations interest us, but knowing where the
    # translation comes from allow us to do some optimization by ignoring adjacent edges)
    feasible_touchers = []

    # for each toucher
    # OPTIMIZE: only consider unique translations, ignore redundant translations among touchers
    for toucher in toucher_structures:
        # test suggested translation against all touching couple of edges

        # ignore toucher if null translation
        if not np.any(toucher.translation):
            continue

        is_feasible = True

        # OPTIMIZE: since the suggested translation works for the current toucher,
        # only test feasibility for *other* touchers (need an __eq__ or an ID to check equality)
        for other_toucher in toucher_structures:

            # if one other toucher makes the translation fail, ignore this translation and continue
            # else, append it to the list of feasible touchers
            feasible_on_static_right, feasible_on_moving_right = check_feasible_range(other_toucher)

            # we use the relative position check method here
            # if you need to cache for performance, compute angles with atan2 instead
            # OPTIMIZE: do not compute all the terms if one value to test against is None, since it will not matter
            translation_relative_pos_to_static = get_vector_relative_position(other_toucher.static_edge_vector, toucher.translation)
            translation_relative_pos_to_moving = get_vector_relative_position(other_toucher.moving_edge_vector, toucher.translation)

            # if the translation is parallel, or on the feasible side of either the static or moving edge,
            # it is feasible for this other edge (but continue iteration to check remaining edges)
            # else, it is not feasible, pass this toucher
            if not ((translation_relative_pos_to_static == RelativePosition.right) == feasible_on_static_right or
                            (translation_relative_pos_to_moving == RelativePosition.right) == feasible_on_moving_right or
                            translation_relative_pos_to_static == RelativePosition.parallel or
                            translation_relative_pos_to_moving == RelativePosition.parallel):
                is_feasible = False
                break

        if is_feasible:
            feasible_touchers.append(toucher)

    return feasible_touchers


def get_closest_turnleft_feasible_toucher(feasible_touchers, last_translation):
    """
    Return the toucher that has the translation that makes the tightest turn to the left with the
    last translation (the first one in the list if no previous translation)

    :type feasible_touchers: list[Toucher]
    :type last_translation: (float, float)

    :return:
    """
    # select the translation that corresponds to the edge the closest from the previous edge
    # since edges may belong to different polygons, you can see this as the steepest angle toward CCW
    # this procedure is only needed if there are 2+ feasible touchers and a last translation defined
    # (they may be 2 touchers with the same translation but to test that we would need
    # to get the angle or some equivalent, so we will not test so far)
    if len(feasible_touchers) >= 2 and last_translation is not None:
        # first rotate mentally all vectors so that the last translation has an angle 0, then modulo 2*pi,
        # finally select the vector with the max angle (in the limit case, 2*pi-eps; you can add a threshold on eps
        # to avoid selecting a vector that comes back too soon); no need to store angles nor rotating all angles back

        # for now, pick shortest translation for more safety if multiple translations with same orientation
        # OPTIMIZATION: small optimization: in case there are several vectors with the same orientation, choose the lengthiest;
        # it may be trimmed later but you will not miss a longer translation (would happen next step anyway2)

        # since we are selecting a max, we can just search a max by key = angle with offset (A),
        # but we can also use numpy (B)

        # add this threshold to the offset so that vectors only slightly on the left get the smallest angles
        angle_threshold = 1e-5

        last_angle = arctan2(last_translation[1], last_translation[0])

        # A
        closest_turnleft_feasible_toucher = max(feasible_touchers,
                                                key=lambda x: (arctan2(x.translation[1], x.translation[0])
                                                               - last_angle + angle_threshold, -norm(x.translation)))

        # B
        feasible_touchers_relative_angles = np.array(((toucher, arctan2(toucher.translation[1], toucher.translation[0])
                                                       + last_angle, -norm(toucher.translation))
                                                      for toucher in feasible_touchers),
                                                     dtype=[('toucher', np.object, 2),
                                                            ('angle', np.float64), ('opposite_norm', np.float64)])

        # sorting is less efficient than max but numpy max does not allow to compare a specific axis only
        # ALTERNATIVE: we could still use argmax on the unpacked angles (in the same order)
        # and then retrieve the touchers by reusing the returned indices
        sorted_feasible_touchers_relative_angles = np.sort(feasible_touchers_relative_angles,
                                                           order=['angle', 'opposite_norm'])
        closest_turnleft_feasible_toucher = sorted_feasible_touchers_relative_angles[-1][0]

    elif len(feasible_touchers) == 1:
        closest_turnleft_feasible_toucher = feasible_touchers[0]

    elif last_translation is None and len(feasible_touchers) >= 1:
        # this is the first translation: choose any toucher
        closest_turnleft_feasible_toucher = feasible_touchers[0]

    else:
        raise ValueError('No touchers in the list passed as parameter')

    return closest_turnleft_feasible_toucher

def trim(toucher, current_polygon1, current_polygon2, toucher_structures):
    """
    Return the trimmed toucher, considering the translation of polygon2 around polygon1,
    and ignoring any contact edges ie edges refered by all toucher_structures found

    Even if the translation does not cross but only touches a non-parallel edge, stop by default.
    See paper notes on NFP Burke: Trimming

    :type toucher: Toucher
    :type current_polygon1: Polygon
    :type current_polygon2: Polygon
    :type toucher_structures: list[Toucher]
    :param toucher_structures: all toucher structures found between polygon 1 and 2

    """
    # verify intersection of vertices of B translated according to toucher translation with any part of A
    # to do this, raycast from each vertex of B with the translation VS edges of A
    # as suggested by Burke, trim vectors immediately so that in the following tests we need less and less to trim

    # first, create a geometry where edges referred by *any* toucher have been removed
    # indeed, feasible translations, are, by definition, compatible with such edges

    # OPTIMIZE: identify edges by ID, no value comparison required?
    # CAUTION: this comparison works for tuple, not for numpy arrays
    # ALTERNATIVE: test if toucher.intersection touches or is within edge
    ignored_edges = flatten([(toucher.static_edge, toucher.moving_edge) for toucher in toucher_structures])
    trimming_edges = [edge for edge in get_edges(current_polygon1)
                      if edge not in ignored_edges]

    trimmed_translation = toucher.translation
    for vertex2 in get_unique_coords(current_polygon2.boundary):
        # OPTIMIZE: if intersection against whole polygon boundary is too slow (unlikely), prefer checking signed distance
        # to each edge of the polygon, and trim toucher translation at each step
        # OPTIMIZE: ignoring parallel edges is optional, as there will always be a secant edge when there is a parallel
        # edge, so you can remove that extraneous operation
        pt = raycast_polygon(vertex2, trimmed_translation, current_polygon1)
        if pt is not None:
            # trim translation until that point (will be kept for raycast from next vertex)
            # PRECISION: should not be a problem, but the translation may slightly change direction each time
            trimmed_translation = pt - vertex2

    # do the reverse test, raycasting from A's vertices, in case B is stopped by a vertex of A
    for vertex1 in get_unique_coords(current_polygon1.boundary):
        pt = raycast_polygon(vertex1, trimmed_translation, current_polygon2)
        if pt is not None:
            trimmed_translation = pt - vertex1

    return trimmed_translation

def mark(static_edge, True):
    pass

def is_edge_marked(polygon1, i):
    pass


class TranslationMethod(enum.Enum):
    """Enumeration indicating from which edge(s) a translation vector should be derived"""
    non_applicable = 0
    from_stationary_edge = 1
    from_orbiting_edge = 2
    from_both_edges = 3


def get_edge_progressions(stationary_edge, orbiting_edge, intersection_point):
    """
    Determine whether intersection happens at start, middle or end of each edge and
    return couple of progressions for orbiting ans stationary edge respectively.
    Can only be used with an intersection point with the shape of the 2-array.

    The progression is based on the location of the intersection point on each edge.
    For instance, stationary_edge_progression is 'middle' when it *is* touched in the middle by the other edge.

    """
    # numpy note: if using numpy arrays, use np.allclose() instead
    if intersection_point == stationary_edge[0]:
        stationary_edge_progression = Progression.start
    elif intersection_point == stationary_edge[1]:
        stationary_edge_progression = Progression.end
    else:
        # check if the point is really on the edge, else raise an Error
        # numpy / shapely note: use convenience methods if available
        # note: the second ValueError will not be raised, so gather all error messages for the end if needed
        if not check_if_point_inside_segment(intersection_point, stationary_edge):
            raise ValueError('Provided intersection point does not belong to the stationary edge.')
        stationary_edge_progression = Progression.middle
    if intersection_point == orbiting_edge[0]:
        orbiting_edge_progression = Progression.start
    elif intersection_point == orbiting_edge[1]:
        orbiting_edge_progression = Progression.end
    else:
        # check if the point is really on the edge, else raise an Error
        # numpy / shapely note: use convenience methods if available
        if not check_if_point_inside_segment(intersection_point, orbiting_edge):
            raise ValueError('Provided intersection point does not belong to the stationary edge.')
        orbiting_edge_progression = Progression.middle

    return stationary_edge_progression, orbiting_edge_progression


def get_potential_translation_vector_from_edges(stationary_edge, orbiting_edge, intersection=None):
    """
    Return the potential translation vector associated to a couple of touching edges.
    If Polygons are oriented CCW (their holes CW), then the returned translation correspond
    to an orbiting movement CCW on the exterior, and CW in the interior.
    (if the orbiting polygon has a hole and is orbiting around the static polygon, the latter inside
    this hole, then the orbiting polygon has a moves CCW globally, so the static polygon appears to move CW
    relatively)


    If the edges are not touching at all, or cross-intersecting, an exception is raised.

    :param stationary_edge: edge of the stationary polygon
    :param orbiting_edge: edge of the orbiting polygon
    :param intersection: the intersection between the two edges (pre-computed)
    :return: potential translation vector
    """
    # if the intersection is lazily evaluated, compute it now (redundant)
    if intersection is None:
        intersection = LineString(stationary_edge) & LineString(orbiting_edge)

    if intersection.is_empty:
        raise ValueError('The pre-computed or computed intersection is empty.')

    relative_position = get_edge_relative_position(stationary_edge, orbiting_edge)

    intersection_point = None  # we will set it only if the intersection is a Point

    if relative_position == RelativePosition.parallel:
        # case 8 & 14 (14 if edge vectors have the *same* directions)
        stationary_edge_vector = get_edge_vector(stationary_edge)
        orbiting_edge_vector = get_edge_vector(orbiting_edge)
        if np.array_equal(np.sign(stationary_edge_vector), np.sign(orbiting_edge_vector)):
            # case 14
            derivation_method = TranslationMethod.non_applicable
        else:
            # case 8
            derivation_method = TranslationMethod.from_both_edges

    else:
        assert intersection.type == 'Point', "The edges are intersecting and not parallel," \
                                             "yet the intersection is not a Point."

        intersection_point = intersection.coords[0]

        stationary_edge_progression, orbiting_edge_progression = get_edge_progressions(stationary_edge, orbiting_edge,
                                                                                       intersection_point)

        if stationary_edge_progression == Progression.middle and orbiting_edge_progression == Progression.middle:
            raise ValueError('The edges are cross-intersecting (case 13).')

        # case 9 to 12: a vertex of one edge touches the middle of the other edge
        if stationary_edge_progression == Progression.middle:
            # case 10 & 11
            derivation_method = TranslationMethod.from_stationary_edge
        elif orbiting_edge_progression == Progression.middle:
            # case 9 & 12
            derivation_method = TranslationMethod.from_orbiting_edge
        else:
            assert not relative_position == RelativePosition.crossed, \
                'The edges are touching at their vertices, yet the relative position of the moving edge is crossed.'

            # case 1 to 7
            # note: we could factorize cases where the translation vector expression is the same and result to 0
            # but it would require more conditions and computations, so we chose not to do that
            if stationary_edge_progression == Progression.start and relative_position == RelativePosition.right:
                # case 2 & 4
                derivation_method = TranslationMethod.from_stationary_edge
            elif orbiting_edge_progression == Progression.start and \
                    (stationary_edge_progression == Progression.start and relative_position == RelativePosition.left or
                     stationary_edge_progression == Progression.end and relative_position == RelativePosition.right):
                # case 1 & 6
                derivation_method = TranslationMethod.from_orbiting_edge
            else:
                # case 3, 5 & 7
                derivation_method = TranslationMethod.non_applicable

    if derivation_method == TranslationMethod.from_stationary_edge:
        assert intersection_point is not None
        return fl(stationary_edge[1]) - fl(intersection_point)
    elif derivation_method == TranslationMethod.from_orbiting_edge:
        assert intersection_point is not None
        return fl(intersection_point) - orbiting_edge[1]
    elif derivation_method == TranslationMethod.from_both_edges:
        return fl(stationary_edge[1]) - fl(orbiting_edge[1])
    else:
        return fl([0, 0])


@deprecated(get_potential_translation_vector_from_edges)
def get_potential_translation_vector(stationary_vector, orbiting_vector,
                                     stationary_touch_position_ratio, orbiting_touch_position_ratio):
    """
    Return the potential translation vector associated to a couple of touching edge vertices
    at given position ratios.
    At least one of the touch position ratios must be equal to 0 or 1.

    :param stationary_vector: edge Vector of the stationary polygon
    :param orbiting_vector: edge Vector of the orbiting polygon
    :param stationary_touch_position_ratio: where is the touch on the edge of the stationary polygon,
                                            between 0 (Start) and 1 (End)
    :param orbiting_touch_position_ratio: where is the touch on the edge of the orbiting polygon,
                                            between 0 (Start) and 1 (End)
    :return: a potential translation vector
    """
    # TODO: handle parallel case by replacing ratio parameters with intersection parameters
    # (or even edge parameters and recompute the intersection)
    # you can recompute the ratio here or directly use the intersection coordinates

    # Alternative: only compute the direction of the translation. Then, at trimming time, also trim
    # when reaching the end of the edge on which the orbiting polygon is sliding.
    # Advantage: no need to handle the more complex parallel case with the correct translation magnitude.
    # Disadvantage: need to do more clever trimming
    # TODO: replace this with a proper exception
    assert 0 <= stationary_touch_position_ratio <= 1 and 0 <= orbiting_touch_position_ratio <= 1, \
        "Touch position ratio is not between 0 and 1."

    # by default, no potential translation found
    potential_translation_vector = None

    # case ii and iii: a vertex of one edge touches the middle of the other edge
    # we assume we are not evading collision (but could work too)
    if stationary_touch_position_ratio != 0 and stationary_touch_position_ratio != 1:
        potential_translation_vector = (1 - stationary_touch_position_ratio) * stationary_vector
    elif orbiting_touch_position_ratio != 0 and orbiting_touch_position_ratio != 1:
        potential_translation_vector = - orbiting_vector * (1 - orbiting_touch_position_ratio)
    # case i
    else:
        relative_position = get_relative_position_vector(stationary_vector, orbiting_vector, orbiting_touch_position_ratio)
        if relative_position == RelativePosition.parallel:
            # FIXME: we should check more things about whether contacts are Start or End
            # we assume stationary and orbiting vectors are opposite so neither stat or -orb should work
            potential_translation_vector = stationary_vector
        elif stationary_touch_position_ratio == 0:
            if relative_position == RelativePosition.right:
                potential_translation_vector = stationary_vector
            elif relative_position == RelativePosition.left:
                if orbiting_touch_position_ratio == 0:
                    potential_translation_vector = - orbiting_vector
        else:
            # stationary edge touch at its End
            if orbiting_touch_position_ratio == 0 and relative_position == RelativePosition.right:
                potential_translation_vector = - orbiting_vector
    return potential_translation_vector


