from abc import abstractmethod
from ippy.packmath import nestingimprovedxml
from ippy.packmath.nfp import get_ifp_with_rectangular_container
from ippy.packmath.problem import ExpandedNestingProblem, PieceKey
from ippy.packmath.nestingconstraintproblem import SingleBinSizeBinPackingProblem, FixedOriginSingleBinSizeBinPackingProblem, \
    FloatingOriginSingleBinSizeBinPackingProblem, OpenDimensionProblem, FixedOriginOpenDimensionProblem, \
    FloatingOriginOpenDimensionProblem
from ippy.parser.xmlproblem import nesting_root_to_problem
from ippy.utils.decorator import abstractclassmethod
from ippy.utils.shapelyextension import PolygonExtended, get_size

__author__ = 'huulong'

# not used a lot but useful if you do not reuse the expanded problem multiple times
def create_bin_packing_problem_from_file(filepath, factory_function):
    """
    Create an Input Minimization Nesting Problem subclass from
    a problem XML filepath, and return it.

    :param filepath: path to an XML problem
    :type factory_function: (ExpandedNestingProblem) -> ippy.packmath.nestingconstraintproblem.InputMinimizationNestingProblem
    :param factory_function:
    :rtype: (ippy.packmath.nestingconstraintproblem.InputMinimizationNestingProblem | ippy.packmath.constraint.ConstraintProblem)
    :return: InputMinimizationNestingProblem concrete class instance

    """
    expanded_problem = create_expanded_packing_problem_from_file(filepath)
    return factory_function(expanded_problem)


def create_expanded_packing_problem_from_file(filepath, fixed_length=None):
    """
    Create an Expanded Nesting Problem from a problem XML filepath, and return it.
    This is a convenience function used in tests because it is faster to generate
    multiple constraint problems from one Expanded Nesting Problem, rather than reparsing a file multiple times.

    :param filepath: path to an XML problem
    :param fixed_length: set it to a float value to alter the original container width (useful for single size only)
    :rtype: ippy.packmath.problem.ExpandedNestingProblem

    """
    nesting_root = nestingimprovedxml.parse(filepath, silence=True)
    problem = nesting_root_to_problem(nesting_root)
    return ExpandedNestingProblem(problem, fixed_length)


# REFACTOR: a lot to factorize between all these factories

def get_lot_pieces_container_nfps_from_expanded(expanded_problem):
    """
    Return tuple of (lot, pieces, container, nfps) from an expanded problem

    :type expanded_problem: ExpandedNestingProblem
    :return:

    """
    # TEMP: convert polygons in lot to PolygonExtended, until done in ExpandedNestingProblem
    lot = {PieceKey(*piece_key): PolygonExtended(polygon) for piece_key, polygon in
           expanded_problem.expanded_lot.iteritems()}
    pieces = expanded_problem.pieces
    container = (expanded_problem.expanded_container[0], PolygonExtended(expanded_problem.expanded_container[1]))
    nfps = expanded_problem.piece_nfps
    return lot, pieces, container, nfps

def create_fixed_origin_single_bin_size_problem(expanded_problem):
    """
    Create a problem of type FixedOriginSingleBinSizeBinPackingProblem and return it.

    :type expanded_problem: ExpandedNestingProblem
    :param expanded_problem: problem data with geometries

    """
    lot, pieces, container, nfps = get_lot_pieces_container_nfps_from_expanded(expanded_problem)
    # the expanded problem indexes ifps by (container_id, piece_id), but in single bin packing
    # we only need one container, so we reduce the key to piece_id and its orientation
    ifps = {(piece_id, orientation): ifp for ((container_id, piece_id), (_, orientation)), ifp in expanded_problem.piece_ifps.iteritems()}
    return FixedOriginSingleBinSizeBinPackingProblem(expanded_problem, expanded_problem.name + ' (Single Size)', lot, pieces, nfps, container, ifps)


def create_floating_origin_single_bin_size_problem(expanded_problem):
    """
    Create a problem of type FixedOriginSingleBinSizeBinPackingProblem and return it.

    :param ExpandedNestingProblem expanded_problem: problem data with geometries

    """
    lot, pieces, container, nfps = get_lot_pieces_container_nfps_from_expanded(expanded_problem)
    return FloatingOriginSingleBinSizeBinPackingProblem(expanded_problem, expanded_problem.name + ' (Single Size FO)', lot, pieces, nfps, container)


def create_fixed_origin_open_dimension_problem(expanded_problem):
    """
    Create a problem of type FixedOriginOpenDimensionProblem and return it.

    :param ExpandedNestingProblem expanded_problem: problem data with geometries

    """
    # TEMP: convert polygons in lot to PolygonExtended, until done in ExpandedNestingProblem
    lot = {PieceKey(*piece_key): PolygonExtended(polygon) for piece_key, polygon in expanded_problem.expanded_lot.iteritems()}
    pieces = expanded_problem.pieces
    container_size = get_size(expanded_problem.expanded_container[1])  # use original problem container for the height
    nfps = expanded_problem.piece_nfps

    # use the max width = sum of width of all pieces to start with a very big ifp, to play the role of an unbounded domain
    # OPTIMIZE: compute width once per piece_id and multiply by number of instances
    max_container_width = sum(get_size(piece_geom)[0] for piece_geom in expanded_problem.expanded_lot.itervalues())

    return FixedOriginOpenDimensionProblem(expanded_problem, expanded_problem.name + ' (Open Dimension)',
                                           lot, pieces, nfps, max_container_width, container_size[1])


def create_floating_origin_open_dimension_problem(expanded_problem):
    """
    Create a problem of type FloatingOriginOpenDimensionProblem and return it.

    :param ExpandedNestingProblem expanded_problem: problem data with geometries

    """
    # TEMP: convert polygons in lot to PolygonExtended, until done in ExpandedNestingProblem
    lot = {PieceKey(*piece_key): PolygonExtended(polygon) for piece_key, polygon in expanded_problem.expanded_lot.iteritems()}
    pieces = expanded_problem.pieces
    container_size = get_size(expanded_problem.expanded_container[1])
    nfps = expanded_problem.piece_nfps

    # OPTIMIZE: compute width once per piece_id and multiply by number of instances
    max_container_width = sum(get_size(piece_geom)[0] for piece_geom in expanded_problem.expanded_lot.itervalues())

    return FloatingOriginOpenDimensionProblem(expanded_problem, expanded_problem.name + ' (Open Dimension FO)', lot, pieces,
                                              nfps, max_container_width, container_size[1])
