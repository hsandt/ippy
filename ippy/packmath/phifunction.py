import itertools
from shapely.affinity import translate
from shapely.geometry import LinearRing, Point
from ippy.geometry.polytri import earclip_shapely_polygon
from ippy.packmath.nfp import nfp_burke

from ippy.utils.geometry import get_equation_params, get_signed_distance
from ippy.utils.shapelyextension import get_edges_from_coords
from ippy.utils.iterables import overlap_pairwise

__author__ = 'huulong'


# dependence on NFP, beware of cyclic dependencies!
def phi_nfp(polygon1, polygon2, translation, nfp=None, is_polygon1_container=False):
    """
    Phi-function of two polygons (shapely objects).
    You must pass the translation separately, as the nfp is computed from the ORIGINAL
    polygons (or at least pass the reference or the meta-nfp so that we can find back
    the original polygon)

    OLD: reference is always (0, 0) from original polygon2
    polygon2_reference is the reference point of the polygon2 *after translation*.
    To minimize the number of parameters, prefer a Polygon wrapper with all this data.

    If nfp is not None, its value is directly used to avoid computations

    If is_polygon1_container is True,
    polygon1 is considered as a container ie it is substituted with its complement (closed).

    """

    # if polygon2_reference is None:
    #     # as fallback, use the bottom-left (not always true!)
    #     polygon2_reference = polygon2.bounds[0:2]

    translated_polygon2 = translate(polygon2, *translation)

    if nfp is None:
        # not supported! (except for convex case)
        raise Exception('Runtime NFP computation is not supported yet!')
        nfp = nfp_burke(polygon1, polygon2)

    # reference_point = Point(polygon2_reference)
    tr_reference_point = Point(translation)  # (0, 0) translated gives translation coords

    absolute_distance = tr_reference_point.distance(nfp.boundary)

    # TODO: handle container case
    # reuse NFP to check intersection but beware of edge cases
    # if polygons are touching, the distance is 0 so the sign does not matter
    # (in practice there may be floating errors so within may be wrong, but the absolute distance
    # will be very small anyway so tests using phi_nfp at 1e-7 should still work)
    do_polygons_intersect = tr_reference_point.within(nfp.closed_interior)
    # or ask Shapely to check intersection, but this would be an operation on polygons so more expensive
    # do_polygons_intersect = polygon1.intersects(translated_polygon2)
    intersection_sign = -1 if do_polygons_intersect else 1
    return intersection_sign * absolute_distance


def phi2(phi_polygon1, phi_polygon2):
    """
    Phi-function for two phi-polygons.

    Translation must be applied beforehand.
    """
    if phi_polygon1.outside_boundary:
        pass
        # phi-polygon 1 is a container
    elif phi_polygon2.outside_boundary:
        pass
        # phi-polygon 2 is a container
    else:
        # both phi-polygons are not containers
        if not phi_polygon1.outside_boundary.is_bounded or not phi_polygon2.outside_boundary.is_bounded:
            raise ValueError('Unbounded non-container polygons are not supported yet.')
        else:
            # both polygons are bounded
            if phi_polygon1.inside_boundaries or phi_polygon2.inside_boundaries:
                raise ValueError('Inside Boundaries (holes) are not supported yet.')
            else:
                # both polygons are bounded and without holes
                tesselated_polygon1 = earclip_shapely_polygon(phi_polygon1.outside_boundary.broken_lines[0])
                tesselated_polygon2 = earclip_shapely_polygon(phi_polygon2.outside_boundary.broken_lines[0])

                # earclip preserves CCW, but just in case you can check
                # either use a custom CCW check (sign of the algebraic area) or convert to LineRing and check is_ccw
                # there is a small chance that LineString modifies the order of the coordinates, in which case you
                #   must get back the new coords from the LineString, else just use the original coords
                # we reverse the iterators if needed to obtain CCW
                tesselated_polygon1 = [triangle if LinearRing(triangle).is_ccw else triangle[::-1] for triangle in tesselated_polygon1]
                tesselated_polygon2 = [triangle if LinearRing(triangle).is_ccw else triangle[::-1] for triangle in tesselated_polygon2]

                min_phi = float('inf')
                for convex_part1, convex_part2 in itertools.product(tesselated_polygon1,
                                                                    tesselated_polygon2):
                    min_phi = min(min_phi, phi_convex_convex(convex_part1, convex_part2))
                return min_phi

    raise ValueError('Phi-polygons passed are not supported yet.')


def phi(polygon1, polygon2, translation=(0, 0), rotation=0, is_polygon1_cw=False, is_polygon2_cw=False):
    """
    Phi-function for two polygons with no interiors

    Tessellate then applies the default phi-function to convex pieces.
    The result depends on the phi-function used for convex pieces.
    The current default function does not necessarily return the euclidian distance
    between objects.
    Because of this, applying tessellation before applying tessellation on a convex
    polygon may alter the result.

    If is_polygon_ccw is True, then polygonX's coordinates are considered clockwise
    (also used for holes and containers described CCW, to correct the direction of the edge normals)
    hence reversed.

    Be careful, this does not mean that the polygon is actually cw or ccw. For that, shapely's
    is_ccw method works. But it means that, providing the polygon is ccw for shapely,
    it is actually a hole / container in our mind.

    TODO: use a subclass or wrapper of Polygon containing the information: CW or CCW.
    Holes are already included in shapely Polygons but it will be useful for containers.

    Params
        polygon1: shapely Polygon
        polygon2: shapely Polygon
        translation: (float, float)
        rotation: float
        is_polygonX_cw: should polygon be considered as clockwise (hole or container)

    """
    # if not satisfied by the default phi-function, may add a "phi-function type" parameter
    # to enable the user to use different phi-functions
    # TODO: triangulate only once and store result for later computations
    # then translate when required

    translated_polygon2 = translate(polygon2, *translation)
    # TODO: apply rotation

    # apply convex tessellation (for now, only ear-clipping) and
    # return the minimum of phi-values between all possible pairs of convex parts
    # taken respectively from both polygons
    # TODO: earclip is not efficient in general and since this code is even applied to
    # already convex polygons, this is a clear loss of efficiency; prefer better triangulation
    # algorithm, or even a tessellation into convex parts directly
    # TODO: detect convex shapes and only tessellate when needed
    tesselated_polygon1 = earclip_shapely_polygon(polygon1)
    tesselated_polygon2 = earclip_shapely_polygon(translated_polygon2)

    # first verifiy that all polygons have been created CCW. Normally it is true
    # the way we created our files and parsed them, but if it is not we should
    # reverse where we don't and not reverse where we do.
    # TODO: there are two few verifications of CW and CCW. Plus, even if during creation
    # polygons are CCW, after any shaeply operation (e.g. intersection) it may not be true
    # so add more verifications and create a routine to automatically reverse vertex order
    # when required
    assert polygon1.exterior.is_ccw, "Polygon1 is not CCW, adapt phi function computation."
    assert polygon2.exterior.is_ccw, "Polygon2 is not CCW, adapt phi function computation."

    # OOPS, after tesselation this does not make sense, it is too late
    # if we wrongly tesselated the interior of a container... but I don't know
    # how to compute phi with a container with is technically concave
    # TODO. find the computation of phi for a concave shape, if the shape is
    # completely concave it is probably symmetrical to the convex case,
    # and if it is partially concave, tesselate the "dual" to get concave shapes
    # (see my drawings with how to split an object into hats, horns and convex shapes
    # in the case of the container)

    # reverse coordinates if cw flag
    if is_polygon1_cw:
        tesselated_polygon1 = reversed(tesselated_polygon1)
    if is_polygon2_cw:
        tesselated_polygon2 = reversed(tesselated_polygon2)

    min_phi = float('inf')
    for convex_part1, convex_part2 in itertools.product(tesselated_polygon1,
                                                        tesselated_polygon2):
        min_phi = min(min_phi, phi_convex_convex(convex_part1, convex_part2))
    return min_phi


def phi_convex_convex(coords1, coords2):
    """
    Phi-function for two convex polygons suggested by Phi[1]

    This does *not* return the distance between two convex objects,
    but the function follows the criteria of a phi-function i.e.
    it is positive when no intersection, null on contact and else negative,
    and increases when objects are going away from each other.

    Counter-example: Sample1b, t=(11,1) for polygon 2 // polygon 1
    Since the closest points are vertices, and we compute a signed distance
    vertex/edge, we can never obtain the distance vertex-vertex
    which gives 0.8... instead of 1
    When the smallest distance is vertex/edge (or edge/edge, it implies the former),
    then the returned value is equal to the euclidian distance between the objects.

    Params
        coords1
            Coordinate sequence of the convex polygon 1, in CCW
        coords2
            Coordinate sequence of the convex polygon 2, in CCW

    """
    # get the parameters of the half-plane associated to each vertex of
    # polygon 1 and 2
    # for polygon1 and polygon2
    # consider each edge of one polygon, vertex of the other one
    # take the min of the distance between the vertices and one edge
    # then the max over all the edges
    # then do the same reverting the role of both polygons
    # and return the max of the 2 values
    max1 = float('-inf')
    for coords, other_coords in ((coords1, coords2), (coords2, coords1)):
        max2 = float('-inf')
        for edge in get_edges_from_coords(coords):
            min3 = float('inf')
            for vertex in other_coords:
                dist3 = get_signed_distance(vertex, *get_equation_params(edge))
                min3 = min(min3, dist3)
            max2 = max(max2, min3)
        max1 = max(max1, max2)
    return max1

    # return max(max(min(get_signed_distance(vertex, *get_equation_params(edge))
    #              for vertex in other_coords)
    #         for edge in get_edges_from_coords(coords))
    #     for coords, other_coords in ((coords1, coords2), (coords2, coords1)))


def phi_convex_concave(coords1, coords2):
    """
    Phi-function for one convex polygon and one concave polygon.
    The concave polygon coordinates should always be the passed as the second parameter.

    This does *not* return the distance between the two objects,
    but the function follows the criteria of a phi-function i.e.
    it is positive when no intersection, null on contact and else negative,
    and increases when objects are going away from each other.

    Params
        coords1
            Coordinate sequence of the convex polygon, in CCW
        coords2
            Coordinate sequence of the concave polygon, in CCW

    """
    # the formula is similar to the convex-convex case, but when edges of the concave
    # polygon are considered, we should take the *minimum* distance since *all*
    # signed distances must be positive for a point to be inside a concavity

    # Max-min for convex edges
    max2 = float('-inf')
    for edge in get_edges_from_coords(coords1):
        min3 = float('inf')
        for vertex in coords2:
            dist3 = get_signed_distance(vertex, *get_equation_params(edge))
            min3 = min(min3, dist3)
        max2 = max(max2, min3)

    # min-min for concave edges
    min2 = float('+inf')
    for edge in get_edges_from_coords(coords2):
        min3 = float('inf')
        for vertex in coords1:
            dist3 = get_signed_distance(vertex, *get_equation_params(edge))
            min3 = min(min3, dist3)
        min2 = min(min2, min3)

    max1 = max(max2, min2)

    return max1
