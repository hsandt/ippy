from abc import abstractmethod, ABCMeta, abstractproperty
from copy import deepcopy
import itertools
import logging
from ippy.utils.algorithm import IterationCounter, MaxIterationsException

root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)

from numpy import float_ as fl


__author__ = 'hs'


class ConstraintProblem(object):
    """
    Base class for all constraint problems.

    Attributes
        :type variables: list<variable type>
            - list of variables. Float values are assigned to them
        :type domains: dict<variable type, NestingDomain>
            - dictionary of initial domains per variable. The domain type is problem-specific
                and must be interpreted by the solver
            The ConstraintProblem must implement create_domain_singleton to create a domain singleton.
        :type unary_constraints: list[(variable type, (variable type) -> bool)]
            - list of constraint functions that return True if the unary constraint is satisfied
        :type binary_constraints: list[((variable type, variable type), BinaryConstraint)]
            - list of binary constraint objects
        :type global_constraints: list[(assignment) -> bool]
            - list of constraint functions that returns True if the complete constraint associated
                to this global constraint may be satisfied when the partial assignment is completed

    """
    __metaclass__ = ABCMeta
    __hash__ = None

    def __init__(self, variables, domains, unary_constraints=None, binary_constraints=None, global_constraints=None):
        self.variables = variables
        self.domains = domains

        # REFACTOR: passing constraint methods directly to the initializer is difficult because of
        # subclassing on multiple levels, add register constraint methods for individual or
        # a list of constraints (instead of only a uniform constraint on all variables)
        # so that the user can add constraints more easily outside initialization
        if unary_constraints is None:
            self.unary_constraints = []
        else:
            self.unary_constraints = unary_constraints

        if binary_constraints is None:
            self.binary_constraints = []
        else:
            self.binary_constraints = unary_constraints

        if global_constraints is None:
            self.global_constraints = []
        else:
            self.global_constraints = global_constraints

    def __repr__(self):
        return '<ConstraintProblem: variables {0}, unary constraints {1}, binary constraints {2}>'\
            .format(self.variables, self.unary_constraints, self.binary_constraints)

    def register_unary_constraint_all_vars(self, constraint_method):
        """
        Add the constraint method as a unary constraint for each variable.
        The method must be bound to the problem instance.
        This is a helper to apply after initialization.

        :type constraint_method: (variable type, value type) -> bool

        """
        self.unary_constraints.extend([(variable, constraint_method) for variable in self.variables])

    def register_binary_constraint_all_vars(self, binary_constraint):
        """
        Add the constraint as a binary constraint for each couple of variables.
        The method must be bound to the problem instance.
        This is a helper to apply after initialization.

        :type binary_constraint: BinaryConstraint

        """
        # you must register constraint for each couple of different variables, in both senses
        # so either use itertools.combinations and manually provide the reversed, tuple,
        # or use itertools.product but remove reflective tuples (as here)
        self.binary_constraints.extend([((variable1, variable2), binary_constraint)
                                        for variable1, variable2 in itertools.product(self.variables, repeat=2)
                                        if variable1 != variable2])

    def register_global_constraint(self, constraint_method):
        """
        Add the constraint method as a global constraint for any assignment.
        The method must be bound to the problem instance.
        This is a helper to apply after initialization.

        :type constraint_method: (assignment) -> bool

        """
        self.global_constraints.append(constraint_method)

    def get_unassigned_variables(self, assignment):
        """
        Return variables still unassigned in assignment

        :param assignment:
        :rtype: list[T]

        """
        # OPTIMIZE: if it is a bottleneck, store assigned/unassigned variables somewhere (probably okay)
        return [variable for variable in self.variables if variable not in assignment]

    @abstractmethod
    def create_domain_singleton(self, variable, value):
        """
        Return a singleton containing value, in the problem-specific domain format.
        May depend on the variable whose value as been assigned to that singleton's value.

        :param variable: variable for which the singleton is generated
        :param value: unique value of the singleton
        :return: singleton in the domain format
        """

    # REFACTOR: having to pass the initial node is not really good...
    @abstractmethod
    def get_initial_cache(self, initial_node, cached_values):
        """
        Return a initial cache value to start the search

        """

    @abstractmethod
    def update_cache(self, node):
        """
        Update the cache in-place

        :param node:

        """

    @abstractmethod
    def get_cost(self, assignment):
        """
        Return the cost of the assignment.
        For CSP, return 0, but we prefer using a common interface

        :rtype: float

        """

class ConstraintSatisfactionProblem(ConstraintProblem):
    """
    CSP problem class for continuous numerical value assignments.
    They are no continuous variable domains, instead of numerical ranges are given by
    unary constraints. In other words, all domains are assumed to be ]-inf, +inf[.

    """
    def __init__(self, variables, domains, unary_constraints=None, binary_constraints=None, global_constraints=None):
        super(ConstraintSatisfactionProblem, self).__init__(variables, domains, unary_constraints, binary_constraints, global_constraints)

    def __repr__(self):
        return '<ConstraintSatisfactionProblem: variables {0}, unary constraints {1}, binary constraints {2}>'\
            .format(self.variables, self.unary_constraints, self.binary_constraints)

    def __str__(self):
        return 'ConstraintSatisfactionProblem'.format()

    def __eq__(self, other):
        """
        Return True if other is a ConstraintSatisfactionProblem with the same variables
        and constraint function references.

        """
        return isinstance(other, ConstraintSatisfactionProblem) and \
            self.variables == other.variables and \
            self.unary_constraints == other.unary_constraints and \
            self.binary_constraints == other.binary_constraints

    def __ne__(self, other):
        return not self.__eq__(other)

    def get_cost(self, assignment):
        """
        For CSP, return 0

        :rtype: float

        """
        return 0


class ConstraintOptimizationProblem(ConstraintProblem):
    """
    Constraint optimization problem class.
    Domains are expected to be compound types of floats, but ranges are enforced by
    unary constraints only.

    In addition, the class provides a function to compute the cost of an assignment,
    cost that must be minimized.

    """
    __metaclass__ = ABCMeta
    __hash__ = None

    def __init__(self, variables, domains, unary_constraints=None, binary_constraints=None, global_constraints=None):
        super(ConstraintOptimizationProblem, self).__init__(variables, domains, unary_constraints, binary_constraints, global_constraints)

    def __repr__(self):
        return '<ConstraintSatisfactionProblem: variables {0}, unary constraints {1}, binary constraints {2}>'\
            .format(self.variables, self.unary_constraints, self.binary_constraints)

    def __str__(self):
        return 'ConstraintSatisfactionProblem'.format()

    def __eq__(self, other):
        """
        Return True if other is a ConstraintSatisfactionProblem with the same variables
        and constraint function references.

        """
        return isinstance(other, ConstraintSatisfactionProblem) and \
            self.variables == other.variables and \
            self.unary_constraints == other.unary_constraints and \
            self.binary_constraints == other.binary_constraints

    def __ne__(self, other):
        return not self.__eq__(other)

    @abstractmethod
    def get_cost(self, assignment):
        """
        Return the cost of an assignment on this problem.

        :param assignment: dict<variable type, value type>
        :return: float - cost of the assignment

        """

    @abstractmethod
    def get_min_cost(self):
        """
        Return the minimum possible cost of an assignment on this problem.
        If it cannot be evaluated, returning None is an option.
        Otherwise, you can return 0 if the cost is always positive.

        :rtype: (float | None)

        """

    # @abstractmethod
    def update_data_on_cost(self, max_cost):
        """
        Update the problem data, typically the domains, so that the search
        does not get over the max cost.

        """

class Domain(object):
    """
    Abstract base class for domains

    Should support shallow copy without issues
    Implement __copy__ if required
    (or we can use deepcopy and implement __deepcopy__)

    """
    __metaclass__ = ABCMeta

    @abstractproperty
    def is_empty(self):
        """
        Return True if the domain is empty from the point of view of constraint solving.

        :rtype: bool

        """
    @abstractmethod
    def contains(self, value):
        """
        Return True if the domain contains the value.

        :rtype: bool

        """

    # REFACTOR: abstract singleton construction method?


class BinaryConstraint(object):
    """
    Abstract base class for binary constraints

    Attributes
        :type cp: ConstraintProblem
        constraint problem the constraint is applied to (required to query NFPs)

    """
    __metaclass__ = ABCMeta

    def __init__(self, cp):
        self.cp = cp

    @abstractmethod
    def check_if_consistent(self, variables, values):
        """Return True if the 2 values assigned to the 2 variables satisfy the constraint"""

    @abstractmethod
    def revise_forward(self, node, tail):
        """
        Filter tail's domain based on value assigned to head
        Value assigned to head must be retrieved as placements[head].
        We keep the whole assignment as a parameter because some implementations use them.

        :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
        :type tail: T

        """


class GlobalConstraint(object):
    """
    Abstract base class for global constraints

    Attributes
        :type cp: ConstraintProblem
        constraint problem the constraint is applied to (required to query NFPs)

    """
    __metaclass__ = ABCMeta

    def __init__(self, cp):
        self.cp = cp

    @abstractmethod
    def check_if_consistent(self, assignment):
        """
        :type assignment: dict[T, U]

        Return True if the partial assignment currently satisfies the constraint

        """

    @abstractmethod
    def revise_forward(self, node, tail):
        """
        Filter tail's domain based on value assigned to head
        Value assigned to head must be retrieved as placements[head].
        We keep the whole assignment as a parameter because some implementations use them.

        :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
        :type tail: T

        """


def backtracking_search(csp, variable_selector=None, value_sorter=None, forward_checking=False, check_conflicts=True,
                        max_iterations=1000,
                        viewer=None):
    """
    Apply backtracking search to a ConstraintSatisfactionProblem and
    return the complete assignment, or None (failure) if no solutions were found.

    :param ConstraintSatisfactionProblem csp:
    :param variable_selector: variable selector function: (unassigned vars, csp) -> next variable to assign
    :param value_sorter: value sorter function: (variable, assignment, csp) -> ordered list of values to try
    :param forward_checking: if set to True, forward checking will be applied after each assignment, before recursion
    :param viewer: handle events during the search process for debugging output
    :return: dict<variable type, value type> - complete assignment or None

    """
    domains = deepcopy(csp.domains)
    if variable_selector is None:
        variable_selector = basic_variable_selector
    if value_sorter is None:
        raise ValueError('No default value sorter on continuous domains.')
        # value_sorter = basic_value_sorter

    # VIEWER
    if viewer:
        viewer.event('started')

    # prepare search cache
    initial_cache = csp.get_initial_cache()  # OLD

    # iteration counter
    iteration_counter = IterationCounter(max_iterations)

    try:
        result = _recursive_backtracking(csp, {}, domains, variable_selector, value_sorter, initial_cache,
                                         forward_checking=forward_checking,
                                         check_conflicts=check_conflicts, iteration_counter=iteration_counter,
                                         viewer=viewer)
    except MaxIterationsException as e:
        result = None
        logging.info('Max iterations {} exceeded, must stop search.'.format(e.max_iterations))

    return result


def backtracking_search_min_cost(cop, cost_span, initial_cost_upper_bound, variable_selector=None, value_sorter=None,
                                 forward_checking=False, max_iterations=1000, viewer_class=None):
    """
    Apply a search method on a COP, by repeating a CSP search with different parameter values,
    and returning the assignment with the best value, accompanied by this value.

    The parameter is the cost associated to a soft constraint and must be minimized.

    :type cop: ConstraintOptimizationProblem
    :param initial_cost_upper_bound: initial upper bound of the cost, problem-informed value
    :param cost_span: maximum interval width between the lower and upper bound of the cost to find
    :param variable_selector:
    :param value_sorter:
    :param forward_checking: if set to True, forward checking will be applied after each assignment, before recursion
    :param viewer_class: subclass BaseConstraintViewer we use to instantiate all viewers (any callable factory works)
    :rtype: (dict[variable type, value type], float)
    :type: (best assignment found, associated parameter) tuple

    """
    domains = deepcopy(cop.domains)
    if variable_selector is None:
        variable_selector = basic_variable_selector
    if value_sorter is None:
        raise ValueError('No default value sorter on continuous domains.')
        # value_sorter = basic_value_sorter

    # prepare search cache
    initial_cache = cop.get_initial_cache()

    # start search for initial cost provided (problem-informed value)
    cost_upper_bound = fl(initial_cost_upper_bound)
    cost_lower_bound = fl(cop.get_min_cost())
    logging.debug('initial search between cost {} and {}'.format(cost_lower_bound, cost_upper_bound))

    # important: modify data according to cost for initial search
    # FIXME: create a new CSP (or another solution I've written) each time
    # to avoid modifying the original problem's data
    cop.update_data_on_cost(cost_upper_bound)
    # we could use max_cost=None for a free search, but we still need initial_cost_upper_bound
    # to update data; for a cost in general, unrelated to the data, it would not be required (REFACTOR)

    # iteration counter
    iteration_counter = IterationCounter(max_iterations)

    # VIEWER: create one viewer instance from the viewer class provided
    # IMPROVE: because of this, we cannot get the instances of viewers from the upper process
    # give a way to access at least the viewers for the most interesting solutions
    # and stats for searches that failed, esp. stats because plotting does not show them directly
    if viewer_class is not None:
        viewer = viewer_class(cop)
        viewer.event('started')
    else:
        viewer = None

    # REFACTOR: refactor initial test with loop test
    try:
        best_assignment = _recursive_backtracking(cop, {}, domains, variable_selector, value_sorter, initial_cache,
                                              forward_checking=forward_checking,
                                              max_cost=cost_upper_bound,
                                              iteration_counter=iteration_counter,
                                              viewer=viewer)
    except MaxIterationsException as e:
        # we continue as if the search completed but no solution was found
        # since we cannot explore all the search space, 'no solutions' always mean 'nothing found in time' anyway
        best_assignment = None
        logging.info('Max iterations {} exceeded, must stop search.'.format(e.max_iterations))

    if best_assignment is None:
        logging.warning('Initial cost upper bound {} is not enough to find a solution with value sorter {}, '
                        'abandon here.'.format(cost_upper_bound, value_sorter))
        return None, 0  # no assignment, dummy best cost value
    else:
        actual_cost = cop.get_cost(best_assignment)
        logging.info('Initial solution found with cost {} for max cost {}'.format(actual_cost, cost_upper_bound))
        cost_upper_bound = actual_cost

    # search until cost precision span is reached
    while cost_upper_bound - cost_lower_bound > cost_span:
        # apply dichotomy [Warning: multiply complexity by the number of dichotomies]
        # ALTERNATIVES: regular intervals (N-chotomy), smooth approach, etc.
        max_cost = (cost_lower_bound + cost_upper_bound) / 2

        # modify data so that the max cost is not overstepped during the search
        cop.update_data_on_cost(max_cost)

        # iteration counter
        iteration_counter = IterationCounter(max_iterations)

        if viewer_class is not None:
            # the viewer uses the original COP so the new width cannot be seen
            # generating a new CSP each time should fix this
            viewer = viewer_class(cop)
            viewer.event('started')
        else:
            viewer = None

        try:
            # TODO: create a CSP based on the COP? Prefer focusing on dynamic width search instead?
            result = _recursive_backtracking(cop, {}, domains, variable_selector, value_sorter, initial_cache,
                                         forward_checking=forward_checking,
                                         max_cost=max_cost,
                                         iteration_counter=iteration_counter,
                                         viewer=viewer)
        except MaxIterationsException as e:
            result = None
            logging.info('Max iterations {} exceeded, must stop search.'.format(e.max_iterations))

        # If a solution, could be found, try a lower cost next time. Otherwise, try a higher one.
        if result is not None:
            # get the actual cost of the assignment, since it is probably at least a little lower
            # than the max cost required
            actual_cost = cop.get_cost(result)
            logging.info('Solution found with cost {} for max cost {}'.format(actual_cost, max_cost))
            cost_upper_bound = actual_cost
            best_assignment = result
        else:
            logging.info('No solution found for max cost {}'.format(max_cost))
            cost_lower_bound = max_cost

    # VIEWER: cannot use with viewer class instantiation
    # IMPROVE: also provide a meta-level viewer to see how dichotomy progresses
    # (instead of using logging only)
    # However, in new versions we will use dynamic width or simply cost-based search so things
    # will be more flat and one viewer should be enough
    # if viewer_class:
    #     viewer.event('finished', best_assignment, 'cost span')

    # return best solution and optimal cost, which is the current actual cost
    logging.info('Best solution for cost {} ({})'.format(actual_cost, best_assignment))
    return best_assignment, actual_cost  # should not be (None, 0) if initial cost was high enough


# REFACTOR: too many search parameters passed recursively, prefer a Strategy object, and if you want a Search object too
def _recursive_backtracking(cp, assignment, domains, variable_selector, value_sorter, cache=None,
                            forward_checking=False, check_conflicts=True, max_cost=None, iteration_counter=None, viewer=None):
    """
    Recursive part of the backtracking algorithm.
    For an incomplete assignment, return the same assignment plus one variable assigned.
    For a complete assignment, return the same assignment.
    In addition, update the domain based on the filtering strategy.

    :param forward_checking:
    :param cache:
    :param ConstraintProblem csp:
    :param dict<variable type, value type> assignment: partial assignment
    :param dict<variable type, domain type> domains: domains remaining after filtering
    :param variable_selector: variable selector function: (unassigned vars, domains, csp) -> next variable to assign
    :param value_sorter: value sorter function: (variable, domains, assignment, csp, cache) -> ordered list of values to try
    :param cache: problem-specific cache data to reduce computation cost (often a dedicated class)
    :param forward_checking: if set to True, forward checking will be applied after each assignment, before recursion
    :param check_conflicts: if set to False, constraint violations will not be checked when selecting a value.
        Useful for methods that apply domain filtering or valid value selection
    :type max_cost: (float | dtype | None)
    :param max_cost: maximum cost allowed, in COP
    :type iteration_counter: IterationCounter
    :param iteration_counter: iteration counter independent from the viewer, used to stop search when needed
    :param viewer: handle events during the search process for debugging output

    """
    # check if assignment is complete at the beginning
    # (classical recursion structure that allows chained return to climb the recursion,
    # and return correct result if we call this method directly on a complete assignment)
    if len(assignment) == len(cp.variables):
        if viewer:
            viewer.event('finished', assignment, 'solution found')
        return assignment

    # TODO: add an iteration counter to stop after limit is reached
    # when stopping, send a finish event with the cause
    # also useful to stop running and display the sequence of placements (Plot debug) until now
    if iteration_counter is not None:
        if iteration_counter.increment_and_check_exceed():
            # raise Exception to catch immediately on upper level instead of going up all the recursion
            if viewer is not None:
                viewer.event('finished', assignment, 'max_iterations')
            raise MaxIterationsException(iteration_counter.max_iterations)
            # send finished event with cause

    # VIEWER
    if viewer:
        iteration = iteration_counter.iteration if iteration_counter is not None else None
        viewer.event('new_iteration', iteration)

    unassigned_variables = cp.get_unassigned_variables(assignment)
    variable = variable_selector(unassigned_variables, domains, cp)
    # TODO: value sorter can only pick a finite number of values;
    # use something more flexible such as grid partitioning to allow infinite and informed
    # value selection, but limited to a certain number of iterations / precision to avoid
    # being stuck to one variable, even when backtracking

    # OPTIMIZATION: bottleneck here
    for value in value_sorter(variable, domains, assignment, cp, cache):
        # OPTIMIZE: instead of creating a new copy every time, just remove the last assigned value
        # at the end of the loop
        new_assignment = deepcopy(assignment)
        new_assignment[variable] = value

        # check for conflicts in new assignment
        # OPTIMIZE: provide the possibility to pass the constraint test
        # if we ensured the remaining values never violate a constraint
        # e.g. by filtering with Forward Checking,
        # OPTIMIZATION: bottleneck here
        if check_conflicts:
            if check_if_conflicts(new_assignment, cp):
                continue

        # TODO: DEBUG, log special event if max cost is exceeded, so that we can see the efficiency of each test
        # same for check conflicts
        # verify if max cost is crossed by this placement
        # OPTIMIZE: this is very uneffective, just as check_conflicts in general
        # it is better to ensure cost limitation by limiting the domain with Forward Checking
        # of course we also to minimize cost even lower than the max cost as much as possible but we
        # cannot completely reduce the domain because it is really a soft constraint and it would
        # correspond to reducing the domain in a fuzzy way;
        # but instead we can mimic fuzzy theory by giving probability or priority to the domain
        # depending on the cost on the value (since the assignment would be partial, we can only guess cost bounds)!
        if max_cost is not None:
            # REFACTOR: get_cost is only defined for COP
            # check_cost is set to True for COP only, so this is fine, but in C++ for instance
            # this would not be accepted, to unify CSP and COP classes
            if cp.get_cost(new_assignment) > max_cost:
                continue

        # selection reduces domain to one element; let the problem define its own singleton format
        new_domains = deepcopy(domains)
        new_domains[variable] = cp.create_domain_singleton(variable, value)

        # - filtering -
        # forward checking
        if forward_checking:
            empty_domain_tail = forward_check(variable)
            if empty_domain_tail is not None:
                # the domain of empty_domain_tail was reduced to empty set, pass that last assignment and log this
                if viewer:
                    viewer.event('forward_check_empty', variable, value, empty_domain_tail)
                continue

        # prepare cache
        if cache is not None:
            new_cache = deepcopy(cache)
            cp.update_cache_constraint(new_cache)  # uses OLD version
        else:
            new_cache = None

        # VIEWER
        if viewer:
            viewer.event('assigned', variable, value, new_assignment)

        result = _recursive_backtracking(cp, new_assignment, new_domains, variable_selector, value_sorter, new_cache,
                                         forward_checking, check_conflicts,
                                         max_cost=max_cost, iteration_counter=iteration_counter,
                                         viewer=viewer)
        if result is not None:
            # the complete assignment test (first line) immediately returned,
            # from here return the complete assignment, climbing back all the recursion
            return result

    # all values failed at some point deeper in the assignment tree

    # VIEWER
    if viewer:
        # if assignment is empty, the first variable assignment failed: no solution found
        # else, it is a simple backtrack
        if assignment:
            viewer.event('backtracked', variable, assignment)
        else:
            viewer.event('finished', {}, 'no solution found')

    return None  # failure


# REFACTOR: make it a method of ConstraintProblem?
def check_if_conflicts(assignment, cp):
    """

    Return True if there is at least one conflict in the assignment for the constraint problem.

    :type cp: ConstraintProblem
    :type assignment: dict[variable type, value type]

    """
    # check unary constraints
    # only check assigned variables, since we assume unassigned variables have acceptable domains
    # for variable in assignment.iterkeys():
    # IMPROVE: keep a record of constraints related to a variable in a dict as in simpleAI
    # to provided easier access to such constraints (but avoid redundancy?)
    for variable, unary_constraint in cp.unary_constraints:
        # only check constraint if the variable have an assigned value
        if variable not in assignment:
            continue
        if not unary_constraint(variable, assignment[variable]):
            return True

    # check binary constraints
    for (variable1, variable2), binary_constraint in cp.binary_constraints:
        # only check constraint if both variables have an assigned value
        if not {variable1, variable2} <= set(assignment):
            continue
        if not binary_constraint.check_if_consistent((variable1, variable2), (assignment[variable1], assignment[variable2])):
            return True

    for global_constraint in cp.global_constraints:
        if not global_constraint(assignment):
            return True

    return False


# Filtering

def forward_check(node, revised_node=None):
    """
    Apply Forward Checking to the constraint problem, using binary and global constraints
    to reduce search domains of unassigned variables.
    If any tail domain gets reduced to the empty set, immediately stop checking and return that tail.

    We pass the whole assignment until now, not only the value assigned to the head,
    because some methods may use previous assignments too.

    :type node: ippy.packmath.constraintsearch.AssignmentSearchNode
    :param node: node containing the domains to filter and the head of the forward checking as last variable assigned
    :type dynamic: bool
    :param dynamic: should we modify the cost limit allowed on this problem (set to True for COP)
    :param revised_node: extra parameter used for domain update in cache with backward compatibility
    If left to None, revised_node will be node. Else, the cached domain of revised node will be revised
    instead, but the last placement will come from node, the "next node"
    :rtype: (T | None)
    :return: None if no domains were reduced to the empty set,
        the tail variable for which the domain got empty otherwise (iteration stops there, so we do not know
        for other tails)

    """
    if revised_node is None:
        revised_node = node

    head = node.variable_assigned
    cp = node.problem.cp

    # OPTIMIZE: record constraints involving head during problem constraint definition for faster access

    if node is None:
        # initial node, do nothing
        return None

    unassigned_variables = node.unassigned_variables

    for binary_constraint_tuple in cp.binary_constraints:
        # only consider binary constraint from an unassigned variable to the last variable assigned
        if binary_constraint_tuple[0][0] != head:  # since binary constraints are symmetrical, head can be idx 0 or 1
            continue
        tail = binary_constraint_tuple[0][1]
        if tail not in unassigned_variables:
            # domain is already a singleton (or at least not relevant), no need to update
            continue
        binary_constraint = binary_constraint_tuple[1]
        # let the constraint reduce domain of tail as it is informed of the problem
        # OPTIMIZE: since we pass each tail separately, problem-specific operations cannot optimize
        # by factorizing similar computations (e.g. for Nesting problem, piece key -> piece id same result)
        # find a way to let revise_forward do more higher level computations (delegate more, cache results, etc.)
        # revise_forward returns a normal domain for a CSP and a dynamic domain for a COP, since domains are rarely
        # empty for COPs
        binary_constraint.revise_forward(node, tail, revised_node)

    #     if not dynamic:
    #         # if the domain was reduced to empty in a CSP, the partial assignment can not be completed
    #         # REFACTOR: use dynamic domains everywhere and put condition on top of the tree?
    #         if revised_node.domains[tail].is_empty:
    #             return tail
    #
    # return None

# Variable selectors

def basic_variable_selector(variables, domains, csp):
    """
    Select the next variable in order

    :param variables: unassigned variables
    :type domains: dict[T, Domain]
    :param csp: problem
    :return: first variable in the list of variables
    """
    return variables[0]

# def basic_value_sorter(variable, domains, assignment, csp):
#     """
#     Return the values
#
#     :param variable: variable to assign a value to
#     :param assignment: partial assignment
#     :param csp: problem
#     :return: first value in the list of filtered values
#     """
#     return variables[0]

