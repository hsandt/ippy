__author__ = 'hs'

from enum import Enum

class ExecutionLevel(Enum):
    debug = 1           # run all assertions
    semidebug = 2       # only verify major statements
    release = 3         # only stop on critical errors

class RoundingLevel(Enum):
    raw = 1             # no rounding
    weak = 2            # minimum rounding to avoid critical errors
    strong = 3          # rounding after any complex operation

class Config(object):
    """Config for the run"""
    def __init__(self):
        self.execution_level = ExecutionLevel.release
        self.rounding_level = RoundingLevel.weak

    def set_execution_level(self, level):
        self.execution_level = level

    def set_rounding_level(self, rounding):
        self.rounding_level = rounding

    @property
    def debug(self):
        return self.execution_level.value <= ExecutionLevel.debug.value

    @property
    def semidebug(self):
        return self.execution_level.value <= ExecutionLevel.semidebug.value

    @property
    def release(self):
        return self.execution_level.value <= ExecutionLevel.release.value

    @property
    def no_rounding(self):
        return self.rounding_level.value >= RoundingLevel.raw.value

    @property
    def weak_rounding(self):
        return self.rounding_level.value >= RoundingLevel.weak.value

    @property
    def strong_rounding(self):
        return self.rounding_level.value >= RoundingLevel.strong.value

    def __str__(self):
        return "Config: {!s}, {!s}".format(self.execution_level, self.rounding_level)


# change this enum value manually
# execution_level = ExecutionLevel.debug
# execution_level = ExecutionLevel.semidebug
execution_level = ExecutionLevel.release

rounding_level = RoundingLevel.weak

config = Config()
config.set_execution_level(execution_level)
config.set_rounding_level(rounding_level)

# usage:
# from config import config  # import "singleton" ("global" access)
# if config.debug: ...
# if config.semidebug: ...
# if config.release: ...
