from collections import OrderedDict
import logging

from lxml import etree
from shapely import wkt
from shapely.geometry import LinearRing

from ippy.packmath.problem import NestingProblem, PieceData
from ippy.parser.converterror import ProblemXMLFormatError
from ippy.parser.xmlproblem import output_problem_to_xml, \
    parse_xml_to_problem
from ippy.utils.decorator import deprecated
from ippy.utils.iterables import pairwise
from ippy.utils.shapelyextension import get_exterior_coords, RawBoundary, update_bounds, get_edges_from_coords

__author__ = 'huulong'


def to_wkt_file(esicup_filepath, output_filepath):
    """Read a file in ESICUP format and writes the equivalent WKT file at the output path"""
    _to_file(_to_wkt(esicup_filepath), output_filepath)


def _to_wkt(esicup_filepath):
    """Read a file and generates the corresponding WKT"""
    with open(esicup_filepath, 'rU') as esicup_file:
        line_idx = 0
        for line in esicup_file:
            # ignore the first line (number of polygons) and the second line (canvas size)
            # (we could also perform a check on the number of polygons at the end)
            # warning: this will count empty lines instead of skipping them
            if line_idx < 2:
                line_idx += 1
                continue
            # trim the line, especially the newline at the end
            line = line.strip()
            # if nothing left on this line, skip
            if not line:
                continue
            # split [nb_poly] [x1] [y1] ... into a list and ignore the 1st element
            # (we could also perform a check on the number of vertices)
            coord_list = line.split(' ')
            coord_list.pop(0)
            # > [x1, y1, x2, y2, ...]
            # put isolate coordinates x and y into tuples (x, y)
            coords_tuple_list = list(pairwise(coord_list))
            # > [(x1, y1), (x2, y2), ...]
            # replace each tuple (x ,y) with a representative string "x y"
            coords_string_list = map(' '.join, coords_tuple_list)
            # > ['x1 y1', 'x2 y2', ...]
            # repeat the first element at the end of the list to loop the coords
            coords_string_list.append(coords_string_list[0])
            # > ['x1 y1', 'x2 y2', ..., 'xn yn', 'x1 y1']
            # join them all
            coords_string = ", ".join(coords_string_list)
            # > 'x1 y1, x2 y2, ... xn, yn, x1, y1'
            # add the POLYGON keyword
            wkt_line = 'POLYGON ((' + coords_string + '))'
            # > 'POLYGON ((x1 y1, x2 y2, ...))'
            yield wkt_line


def _to_file(line_iterator, output_filepath):
    """
    Writes a file at the output path where each line is yielded by a generator,
    or any iterator or iterable,
    adding a newline at the end of the file

    """
    # open a temp file in truncating write mode
    with open(output_filepath, 'w') as f:
        # write in this file with the generator, one yield per line
        for line in line_iterator:
            f.write(line + "\n")


def _get_polygon_id(polygon_name, prefix_len=0):
    """Return the ID at the end of a string 'polygon[ID]'
    For nfpPolygon[ID] or ifpPolygon[ID], set prefix_len=3"""
    # TODO: for a more generic approach, prefer a dictionary
    # with polygon names instead of a list with polygon IDs
    return int(polygon_name[prefix_len+7:])


@deprecated('generateDS _parse_nfps_from_nesting_root')
def _parse_nfps(nfp_type, xml_problem_root):
    """
    Stores metadata or type nfp_type (NFP or IFP) in dictionary nfps {(polygonA_id, polygonB_id): NFP(A,B)_id}
    from XML problem

    """
    nfps = {}
    ns = {'g': xml_problem_root.nsmap[None]}
    # nfps or ifps node always contain *nfp* node
    # in case the author has made a mistake and is using <ifp> instead if <nfp>, add a safeguard
    # because of this, we prefer findall over iterfind because we can test the result
    # OPTIMIZE: for a large number of NFPs, prefer iterfind and find a workaround to check if there are <ifp>s
    xml_nfps = xml_problem_root.findall('./g:{0}s/g:nfp'.format(nfp_type), namespaces=ns)
    if nfp_type == 'ifp' and not xml_nfps:
        # no <nfp> found, maybe the author used <ifp> instead
        logging.warning('No <nfp> found in <ifps>, looking for <ifp> instead. Please fix the XML file.')
        xml_nfps = xml_problem_root.findall('./g:ifps/g:ifp', namespaces=ns)
    for xml_nfp in xml_nfps:
        # nesting_improved ONLY: use NFP(piece1, piece2) instead of NFP(polygon1, polygon2)
        static_piece = xml_nfp.find('./g:staticPiece', namespaces=ns)
        if static_piece is None:
            # the old XML format for NFP definition is used; we could find out which couple of pieces corresponds
            # to the couple of polygons passed, but for now we just pass
            continue
        static_piece_id = static_piece.get('idPiece')
        orbiting_piece_id = xml_nfp.find('./g:orbitingPiece', namespaces=ns).get('idPiece')
        resulting_polygon_with_boundary_node = xml_nfp.find('./g:resultingPolygonWithBoundary', namespaces=ns)

        # FALLBACK
        # if there is no such node, it means the old XML NFP format is used; fall back to parsing NFP without boundaries
        if resulting_polygon_with_boundary_node is None:
            xml_resulting_polygon_without_boundary = xml_nfp.find('./g:resultingPolygon', namespaces=ns)
            id_components_types = [(xml_resulting_polygon_without_boundary.get('idPolygon'), 0)]  # always filled here
            id_boundaries = []

        else:
            # stack up all components with their types (0 filled, -1 hole) in tuples + the boundary, with IDs
            # TODO: add support for x/yOffset
            xml_components = resulting_polygon_with_boundary_node.findall('./g:component', namespaces=ns)
            id_components_types = [(xml_component.get('idPolygon'), int(xml_component.get('type'))) for xml_component in xml_components]
            # TODO: add support for x/yOffset
            xml_boundary_components = resulting_polygon_with_boundary_node.findall('./g:boundaryComponent', namespaces=ns)
            id_boundaries = [xml_boundary_component.get('idBoundary') for xml_boundary_component in xml_boundary_components]

        # OLD: deprecated so keep null angles
        nfps[((static_piece_id, orbiting_piece_id), (0, 0))] = {
            'idComponentsTypes': id_components_types,
            'idBoundaries': id_boundaries
        }
    return nfps


@deprecated('generateDS _parse_nfps_from_nesting_root')
def _parse_nfps_old_xml_schema(nfp_type, xml_problem_root):
    """Stores metadata or type nfp_type (NFP or IFP) in dictionary nfps {(polygonA_id, polygonB_id): NFP(A,B)_id}"""
    nfps = {}
    ns = {'g': xml_problem_root.nsmap[None]}
    # nfps or ifps node always contain *nfp* node
    # in case the author has made a mistake and is using <ifp> instead if <nfp>, add a safeguard
    # because of this, we prefer findall over iterfind because we can test the result
    # OPTIMIZE: for a large number of NFPs, prefer iterfind and find a workaround to check if there are <ifp>s
    xml_nfps = xml_problem_root.findall('./g:{0}s/g:nfp'.format(nfp_type), namespaces=ns)
    if nfp_type == 'ifp' and not xml_nfps:
        # no <nfp> found, maybe the author used <ifp> instead
        logging.warning('No <nfp> found in <ifps>, looking for <ifp> instead. Please fix the XML file.')
        xml_nfps = xml_problem_root.findall('./g:ifps/g:ifp', namespaces=ns)
    for xml_nfp in xml_nfps:
        static_polygon_id = xml_nfp.find('./g:staticPolygon', namespaces=ns).get('idPolygon')
        orbiting_polygon_id = xml_nfp.find('./g:orbitingPolygon', namespaces=ns).get('idPolygon')
        resulting_polygon_id = xml_nfp.find('./g:resultingPiece', namespaces=ns).get('idPolygon')
        # OLD: keep null angles
        nfps[((static_polygon_id, orbiting_polygon_id), (0, 0))] = resulting_polygon_id  # store ID only !
    return nfps


@deprecated(parse_xml_to_problem)
def xml_to_problem(xml_filepath):
    """
    Parse an XML file following ESICUP's XMLSchema for packing problems,
    and return a packing problem instance.

    """
    # TODO: refactor in multiple sub-functions

    # parsing
    tree = etree.parse(xml_filepath)

    # level 0: nesting
    root = tree.getroot()
    ns = {'g': root.nsmap[None]}  # store namespace

    # level 1: name, author, etc.
    metadescription = {}
    for key in ['name', 'author', 'date', 'description',
                'verticesOrientation', 'coordinatesOrigin']:
        metadescription[key] = root.findtext('./g:{}'.format(key), namespaces=ns)
    # note: we could reverse all coordinates if the orientation is up-left
    # for now, we don't mind; since this is a mere question of point of view,
    # we could simply reverse the vertical exis when plotting too

    # level 1: polygons
    # (last in the XML but first here so that we can check if IDs referenced later are correct)
    # for now, assume the polygons are named polygon0, polygon1... in order in the XML
    # so you can use a list; later, use a dictionary (or a list but sort it accordingly)
    polygons = {}
    xml_polygons = root.iterfind('./g:polygons/g:polygon', namespaces=ns)
    for xml_polygon in xml_polygons:
        # assume the polygon has no holes: only consider lines
        # trust nVertices attribute for the number of vertices
        # TODO: use custom BaseGeometry wrapper that stores object ID from XML
        polygon_id = xml_polygon.get('id')
        nVertices = int(xml_polygon.get('nVertices'))

        if nVertices == 0:
            raise ProblemXMLFormatError('polygon {} with nVertices == 0'.format(xml_polygon.get('id')), xml_filepath)

        # XML Schema contains arcs, but we only handle problems with segments
        # conversion will fail for problems with arcs!
        xml_segments = xml_polygon.iterfind('./g:lines/g:segment', namespaces=ns)
        coords = [(float(xml_segment.get('x0')), float(xml_segment.get('y0'))) for xml_segment in xml_segments]
        assert nVertices == len(coords)

        # despite the name, we use LinearRings internally to stress that holes are not allowed
        polygons[polygon_id] = LinearRing(coords)

    # level 1: boundaries
    raw_boundaries = {}
    xml_boundaries = root.iterfind('./g:boundaries/g:boundary', namespaces=ns)
    for xml_boundary in xml_boundaries:
        boundary_id = xml_boundary.get('id')

        xml_segments = xml_boundary.iterfind('./g:lines/g:segment', namespaces=ns)
        # a priori, all lines are independent so store them separately
        # we can also merge them to optimize data storage, but to keep problem representation integrity we do not here
        # note that MultiGeometries may keep order but there is no strict guarantee
        # in other words, a conversion XML -> problem -> XML is not guaranteed to be identical,
        # but will give an equivalent problem XML. The full conversion/reconversion is idempotent, though.
        # Problem -> XML -> Problem shoud be consistent too.
        lines_coords = [[tuple(float(xml_segment.get(coord_idx)) for coord_idx in coord_idx_tuple)
                   for coord_idx_tuple in (('x0', 'y0'), ('x1', 'y1'))] for xml_segment in xml_segments]

        xml_points = xml_boundary.iterfind('./g:points/g:point', namespaces=ns)
        points_coords = [tuple(float(xml_point.get(coord_idx)) for coord_idx in ('x0', 'y0')) for xml_point in xml_points]

        raw_boundaries[boundary_id] = RawBoundary(lines_coords, points_coords)

    # level 1: problem
    # store problem element reference
    xml_problem = root.find('./g:problem', namespaces=ns)

    # level 2: boards
    # reference polygon container
    # we assume the container has only one piece with one component
    xml_piece = xml_problem.find('./g:boards/g:piece', namespaces=ns)
    piece_id = xml_piece.get('id')
    xml_piece_cmpnt = xml_piece.find('./g:component', namespaces=ns)
    # since we use a list instead of a dict, just extract the ID and get the reference by index
    # we assume the list has the correct size
    polygon_id = xml_piece_cmpnt.get('idPolygon')
    if polygon_id not in polygons:
        raise ProblemXMLFormatError('component refers to idPolygon {} '
                                    'but no corresponding element was found.'.format(polygon_id), xml_filepath)
    container = (piece_id, PieceData(piece_id, 1, [0], polygon_id))  # keep polygon container ID

    # level 2: lot
    # in order to find the corresponding NFPs later, keep the polygonID
    # you could also use a dictionary polygonID: Polygon object to have a quick access
    # to both; later, use a complete Piece object with quantities
    lot = {}
    xml_pieces = xml_problem.iterfind('./g:lot/g:piece', namespaces=ns)
    # we assume the piece has only one component
    for xml_piece in xml_pieces:
        piece_id = xml_piece.get('id')
        quantity = int(xml_piece.get('quantity'))
        xml_piece_cmpnts = xml_piece.iterfind('./g:component', namespaces=ns)

        polygon_ids = []
        hole_ids = []
        for xml_piece_cmpnt in xml_piece_cmpnts:
            component_id = xml_piece_cmpnt.get('idPolygon')
            component_type = int(xml_piece_cmpnt.get('type'))
            if component_type == 0:
                # we have a polygon
                polygon_ids.append(component_id)

                # OPTIONAL check to see if polygon exists
                # since we work with ids now, it does not matter much and is impossible
                # to check if polygons are parsed later anyway
                if component_id not in polygons:
                    # TODO: factorize with above
                    raise ProblemXMLFormatError('component refers to idPolygon {} '
                                                'but no corresponding element was found.'.format(polygon_id),
                                                xml_filepath)
                # END OPTIONAL
            elif component_type == -1:
                # we have a hole
                hole_ids.append(component_id)
            else:
                raise ValueError('Unknown type {1} for component with idPolygon {0}'.format(component_id, component_type))

        # TODO: support multiple polygons per piece
        lot[piece_id] = PieceData(piece_id, quantity, [0], polygon_ids[0], hole_ids)

    # level 1: nfps & ifps
    metadata = {
        'nfps': _parse_nfps('nfp', root),
        'ifps': _parse_nfps('ifp', root)
    }

    # note: old version used basename(xml_filepath) as name
    return NestingProblem(container, lot, metadata, polygons, raw_boundaries, **metadescription)


def format_coord(coord):
    """Format a x or y coordinate for a problem XML file."""
    return format(coord, '.1f')


@deprecated(output_problem_to_xml)
def problem_to_xml(problem, output_file_path, schema='http://globalnest.fe.up.pt/nesting'):
    """
    Convert a Problem instance to an XML file following ESICUP's XMLSchema for packing problems,
    and return the corresponding lxml XML representation object.

    :type problem: NestingProblem
    :type output_file_path: str
    :type schema: str
    """
    # ** create a lxml tree element **

    # level 0: nesting
    xml_nesting = etree.Element('nesting', xmlns=schema)

    # level 1: name, author, etc.
    for key in ['name', 'author', 'date', 'description',
                'verticesOrientation', 'coordinatesOrigin']:
        etree.SubElement(xml_nesting, key).text = problem.metadescription[key]

    # level 1: problem
    xml_problem = etree.SubElement(xml_nesting, 'problem')

    # level 2: boards and lots
    xml_boards = etree.SubElement(xml_problem, 'boards')
    # support for one piece and fixed name only (not critical for container)
    xml_piece = etree.SubElement(xml_boards, 'piece', id='board0', quantity='1')
    # only support one container component
    # TODO: support for rotations, mirroring and other pieceTypes
    # offset 0 by default, so basically polygon coordinates define everything
    etree.SubElement(xml_piece, 'component', idPolygon=problem.container[1].polygon_id, type='0', xOffset='0', yOffset='0')
    xml_lot = etree.SubElement(xml_problem, 'lot')
    # add one piece per polygon
    for piece_id, piece in problem.lot.iteritems():
        # TODO: add support for custom piece ID
        # TODO: add support for multiple quantities

        # WARNING: with incremental piece ID, since dict order is not reliable
        # test on back-and-forth conversion may simply fail!
        xml_piece = etree.SubElement(xml_lot, 'piece', id=piece_id, quantity=str(piece.quantity))

        orientation_node = etree.SubElement(xml_piece, 'orientation')
        # for each allowed rotation, generate an enumeration value
        for angle in piece.orientations:
            etree.SubElement(orientation_node, 'enumeration', angle=str(angle))

        # support for only one filled polygon component
        etree.SubElement(xml_piece, 'component', idPolygon=piece.polygon_id, type='0', xOffset='0', yOffset='0')

        # add hole components
        for hole_id in piece.hole_ids:
            etree.SubElement(xml_piece, 'component', idPolygon=hole_id, type='-1', xOffset='0', yOffset='0')


    # level 1: nfps
    xml_nesting.append(_generate_nfps_nodes('nfp', problem))

    # level 1: ifps
    xml_nesting.append(_generate_nfps_nodes('ifp', problem))

    # level 1: polygons
    # we could optimize by creating polygons in the lot iteration above,
    # but it would not work anymore when using the independent piece-polygon system
    xml_polygons = etree.SubElement(xml_nesting, 'polygons')
    # note: dict is unordered so polygons may not be dumped in order of ID
    # IMPROVE: use a dictionary alphabetically ordered (or an indexed list and you recreate the names 'polygon' + id manually)
    for polygon_id, polygon in problem.polygons.iteritems():
        # in case polygon is actually a LineString or a Point (common for IFP), use a type-agnostic coords getter
        # TODO: add support for PolygonWithBoundary (both senses conversion)
        exterior_edges = list(get_edges_from_coords(get_exterior_coords(polygon)))  # compatible with Polygon and LinearRing
        # we prefer using a generic formula for the number of vertices that works for both Polygons and Lines/Points,
        # so we close the coords through get_edges_from_coords() and then deduce the number of vertices as the number of edges
        xml_polygon = etree.SubElement(xml_polygons, 'polygon', id=polygon_id, nVertices=str(len(exterior_edges)))
        lines = etree.SubElement(xml_polygon, 'lines')
        # get_edges_from_coords will automatically close a LineString segment, as suggested for the XML
        # similarly, a Point will be transformed into an edge with starting and ending vertices at the same position
        # (shirts_2007-05-15/shirts.xml has NFP of 2 segments, but there are just wrong. Only IFP can do that)
        for idx, edge in enumerate(exterior_edges):
            # WARNING: support 1 decimal only! remove or adapt format if you need to support more
            # for readability, use an OrderedDict to define attribute order
            etree.SubElement(lines, 'segment',
                             OrderedDict([('n', str(idx + 1)), ('x0', format_coord(edge[0][0])), ('y0', format_coord(edge[0][1])),
                                          ('x1', format_coord(edge[1][0])), ('y1', format_coord(edge[1][1]))])
                             )
        etree.SubElement(xml_polygon, 'xMin').text = str(polygon.bounds[0])
        etree.SubElement(xml_polygon, 'xMax').text = str(polygon.bounds[2])
        etree.SubElement(xml_polygon, 'yMin').text = str(polygon.bounds[1])
        etree.SubElement(xml_polygon, 'yMax').text = str(polygon.bounds[3])
        # TODO: support holes

    # level 1: boundaries
    xml_boundaries = etree.SubElement(xml_nesting, 'boundaries')
    for raw_boundary_id, raw_boundary in problem.raw_boundaries.iteritems():
        assert not raw_boundary.is_empty, 'Cannot construct XML from empty raw boundary'

        xml_boundary = etree.SubElement(xml_boundaries, 'boundary', id=raw_boundary_id)


        # intialize [xMin, yMin, xMax, yMax] with opposite infinite values
        # we will track the bounds of this boundary
        # ALTERNATIVE: create a union Geometry and get the bounds
        boundary_bounds = [float('+inf'), float('+inf'), float('-inf'), float('-inf')]

        if not raw_boundary.multiline.is_empty:
            xml_lines = etree.SubElement(xml_boundary, 'lines')
            for idx, line in enumerate(raw_boundary.multiline):
                # for readability, we use an OrderedDict to define attribute order
                # WARNING: format_coord currently supports 1 decimal only!
                etree.SubElement(xml_lines, 'segment',
                                 OrderedDict([
                                     ('n', str(idx + 1)),
                                     ('x0', format_coord(line.coords[0][0])),
                                     ('y0', format_coord(line.coords[0][1])),
                                     ('x1', format_coord(line.coords[1][0])),
                                     ('y1', format_coord(line.coords[1][1]))
                                     ])
                                 )
            # update extreme coords
            update_bounds(boundary_bounds, raw_boundary.multiline.bounds)

        if not raw_boundary.multipoint.is_empty:
            xml_points = etree.SubElement(xml_boundary, 'points')
            for idx, point in enumerate(raw_boundary.multipoint):
                etree.SubElement(xml_points, 'point',
                                 OrderedDict([
                                     ('n', str(idx + 1)),
                                     ('x0', format_coord(point.coords[0][0])),
                                     ('y0', format_coord(point.coords[0][1]))
                                 ])
                                 )
            update_bounds(boundary_bounds, raw_boundary.multipoint.bounds)


    # level 1: sequences (?)

    # level 1: solutions [not supported]
    # TODO

    # if you prefer the with format, try out http://www.yattag.org/
    # lxml has also some print limitations such as indent to 2 spaces only
    # (you can reformat as post-processing if needed)

    # ** create a tree from the root element, and output it into a file **

    tree = etree.ElementTree(xml_nesting)
    tree.write(output_file_path, encoding='ISO-8859-1', pretty_print=True, standalone=False)


@deprecated('generateDS _generate_xml_nfps')
def _generate_nfps_nodes(nfp_type, problem):
    """Return XML node with metadata of type nfp_type (NFP or IFP) from the given problem"""
    plural_type = '{}s'.format(nfp_type)  # 'nfps' or 'ifps'
    nfps_node = etree.Element(plural_type)
    # if no nfps found at all, return an empty node
    if plural_type in problem.metadata:
        for piece_id_couple, id_components_boundary_dict in problem.metadata[plural_type].iteritems():
            nfp_node = etree.SubElement(nfps_node, 'nfp')
            # only null angles supported, no mirrors
            etree.SubElement(nfp_node, 'staticPiece', angle="0", idPiece=piece_id_couple[0], mirror="none")
            etree.SubElement(nfp_node, 'orbitingPiece', angle="0", idPiece=piece_id_couple[1], mirror="none")
            resulting_polygon_with_boundary_node = etree.SubElement(nfp_node, 'resultingPolygonWithBoundary')
            for id_component, component_type in id_components_boundary_dict['idComponentsTypes']:
                etree.SubElement(resulting_polygon_with_boundary_node, 'component', idPolygon=id_component,
                                 type=str(component_type), xOffset="0", yOffset="0")
            for id_boundary in id_components_boundary_dict['idBoundaries']:
                etree.SubElement(resulting_polygon_with_boundary_node, 'boundaryComponent', idBoundary=id_boundary,
                                 xOffset="0", yOffset="0")
    return nfps_node


def to_shapely_object_list(wkt_filepath):
    """Return a list of Shapely geometric objects, one per line in the WKT file"""
    shapely_object_list = []
    with open(wkt_filepath, 'rU') as wkt_file:
        # iterate over each line and load the string
        # alternative: use load (without s) on the file pointer and decompose the MultiGeometry obtained
        for line in wkt_file:
            # process the line if it is not blank
            if line.rstrip():
                # append the object described on this line to the list to return
                shapely_object_list.append(wkt.loads(line))
    return shapely_object_list

def geom_list_to_wkt(geom_list, wkt_filepath):
    """Output the content of a list of Shapely geometries to a WKT file, one line per geometry"""
    _to_file((geom.wkt for geom in geom_list), wkt_filepath)
