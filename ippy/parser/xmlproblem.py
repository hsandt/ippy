import logging
from shapely.geometry import LinearRing
from ippy.packmath import nestingimprovedxml
from ippy.packmath.nestingimprovedxml import nesting, problemType, pieceContainerType, pieceType, componentType, \
    orientationType, enumerationType, nfpsType, nfpType, nfpPieceType, resultingPolygonWithBoundaryType, \
    boundaryComponentType, polygonsType, polygonType, LinesType, segmentType, boundariesType, boundaryType, pointType, \
    PointsType
# CAUTION: import all types from nestingimprovedxml for new problems!!
from ippy.packmath.problem import NestingProblem, PieceData
from ippy.parser.converterror import ProblemXMLFormatError
from ippy.utils.shapelyextension import RawBoundary, get_exterior_coords, get_edges_from_coords, update_bounds

__author__ = 'hs'

def parse_xml_to_problem(input_filepath):
    """
    Parse the XML problem file at input_filepath and return the associated NestingProblem.
    The file must follow the Schema http://globalnest.fe.up.pt/nesting_improved

    :rtype input_filepath: str
    :rtype: NestingProblem

    """
    nesting_root = nestingimprovedxml.parse(input_filepath, silence=True)
    return nesting_root_to_problem(nesting_root)


def output_problem_to_xml(problem, output_filepath):
    """
    Generate an XML generateDS root from problem, and export it to output_filepath.
    The output file should follow the Schema http://globalnest.fe.up.pt/nesting_improved

    :rtype problem: NestingProblem
    :rtype output_filepath: str

    """
    nesting_root = problem_to_nesting_root(problem)
    with open(output_filepath, 'w') as output_file:
        output_file.write('<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>\n')
        nesting_root.export(output_file, 0, namespacedef_='xmlns="http://globalnest.fe.up.pt/nesting_improved"')


def nesting_root_to_problem(root):
    """
    Convert a nesting root obtained with generateDS into a Nesting Problem.
    Use parse_xml_to_problem for a direct conversion from a file.

    :type root: nes
    :rtype: NestingProblem

    """

    name = root.get_name()
    author = root.get_author()
    date = root.get_date()
    description = root.get_description()
    vertices_orientation = root.get_verticesOrientation()
    coordinates_origin = root.get_coordinatesOrigin()

    # level 1: polygons
    polygons = {}

    xml_polygons = root.get_polygons().get_polygon()
    for xml_polygon in xml_polygons:
        polygon_id = xml_polygon.get_id()
        nVertices = xml_polygon.get_nVertices()
        if nVertices == 0:
            raise ProblemXMLFormatError('polygon {} with nVertices == 0'.format(polygon_id),
                                        'file associated to {}'.format(name))
        # TODO? support arcs
        xml_segments = xml_polygon.get_lines().get_segment()
        coords = [(xml_segment.get_x0(), xml_segment.get_y0()) for xml_segment in xml_segments]
        assert nVertices == len(coords), 'nVertices = {} but {} coords found.'.format(nVertices, len(coords))

        # despite the name, we use LinearRings internally to stress that holes are not allowed
        polygons[polygon_id] = LinearRing(coords)

    # level 1: boundaries
    raw_boundaries = {}

    xml_boundaries_parent = root.get_boundaries()
    # we tolerate that the boundaries XML block is not present at all
    if xml_boundaries_parent is not None:
        xml_boundaries = xml_boundaries_parent.get_boundary()
        for xml_boundary in xml_boundaries:
            boundary_id = xml_boundary.get_id()

            # some boundaries are only made of lines or only made of points, so check existence
            xml_lines = xml_boundary.get_lines()
            if xml_lines is not None:
                xml_segments = xml_lines.get_segment()
                # prefer direct attribute access here to avoid repeating get_x0, get_y0, etc.
                lines_coords = [[tuple(getattr(xml_segment, coord_idx) for coord_idx in coord_idx_tuple)
                       for coord_idx_tuple in (('x0', 'y0'), ('x1', 'y1'))] for xml_segment in xml_segments]
            else:
                lines_coords = []

            xml_point_parent = xml_boundary.get_points()
            if xml_point_parent is not None:
                xml_points = xml_point_parent.get_point()
                points_coords = [tuple(getattr(xml_point, coord_idx) for coord_idx in ('x0', 'y0')) for xml_point in xml_points]
            else:
                points_coords = []

            raw_boundaries[boundary_id] = RawBoundary(lines_coords, points_coords)

    # level 1: problem
    # store problem element reference
    xml_problem = root.get_problem()

    # level 2: boards
    # reference polygon container
    # we assume the container has only one piece with one component
    xml_piece = xml_problem.get_boards().get_piece()[0]
    piece_id = xml_piece.get_id()
    xml_piece_cmpnt = xml_piece.get_component()[0]
    # since we use a list instead of a dict, just extract the ID and get the reference by index
    # we assume the list has the correct size
    polygon_id = xml_piece_cmpnt.get_idPolygon()
    if polygon_id not in polygons:
        raise ProblemXMLFormatError('component refers to idPolygon {} '
                                    'but no corresponding element was found.'.format(polygon_id),
                                    'file associated to {}'.format(name))
    # assume the container cannot rotate (only angle allowed: 0)
    assert xml_piece.get_orientation() is None or \
        (len(xml_piece.get_orientation().get_enumeration()) == 1 and
         xml_piece.get_orientation().get_enumeration()[0] == 0),\
        'Container has an orientation enumeration, with values other than 0.'
    container = (piece_id, PieceData(piece_id, 1, [0], polygon_id))  # keep polygon container ID

    # level 2: lot
    # in order to find the corresponding NFPs later, keep the polygonID
    # you could also use a dictionary polygonID: Polygon object to have a quick access
    # to both; later, use a complete Piece object with quantities
    lot = {}
    xml_pieces = xml_problem.get_lot().get_piece()
    # we assume the piece has only one component
    for xml_piece in xml_pieces:
        piece_id = xml_piece.get_id()
        quantity = xml_piece.get_quantity()
        if quantity <= 0:
            # null quantity supported because when creating problem variants, it is convenient
            # to just set a quantity to 0 to remove a piece
            continue

        xml_orientation = xml_piece.get_orientation()
        xml_piece_cmpnts = xml_piece.get_component()




        # obtain polygon and holes IDs
        polygon_ids = []
        hole_ids = []
        for xml_piece_cmpnt in xml_piece_cmpnts:
            component_id = xml_piece_cmpnt.get_idPolygon()
            component_type = xml_piece_cmpnt.get_type()
            if component_type == 0:
                # we have a polygon
                polygon_ids.append(component_id)

                # OPTIONAL check to see if polygon exists
                # since we work with ids now, it does not matter much and is impossible
                # to check if polygons are parsed later anyway
                if component_id not in polygons:
                    # TODO: factorize with above
                    raise ProblemXMLFormatError('component refers to idPolygon {} '
                                                'but no corresponding element was found.'.format(polygon_id),
                                                'file associated to {}'.format(name))
            elif component_type == -1:
                # we have a hole
                hole_ids.append(component_id)
            else:
                raise ValueError('Unknown type {1} for component with idPolygon {0}'.format(component_id, component_type))

        # find all allowed angles
        if xml_orientation is not None:
            xml_angles = xml_orientation.get_enumeration()
            angles = [xml_angle.get_angle() for xml_angle in xml_angles]
        else:
            # explicitly mentioning rotations of 0 is preferred, but if it is not we assume no rotations
            angles = [0]

        # TODO? support multiple polygons per piece (rare, unlike holes)
        lot[piece_id] = PieceData(piece_id, quantity, angles, polygon_ids[0], hole_ids)

    # level 1: nfps & ifps
    metadata = {
        'nfps': _parse_nfps_from_nesting_root('nfp', root),
        'ifps': _parse_nfps_from_nesting_root('ifp', root)
    }

    # note: old version used basename(xml_filepath) as name
    return NestingProblem(container, lot, metadata, polygons, raw_boundaries,
                          name, author, date, description, vertices_orientation, coordinates_origin)


def _parse_nfps_from_nesting_root(nfp_type, root):
    """
    Stores metadata or type nfp_type (NFP or IFP) in dictionary nfps {(polygonA_id, polygonB_id): NFP(A,B)_id}
    from XML problem

    """
    nfps = {}
    # nfps or ifps node always contain *nfp* node
    # in case the author has made a mistake and is using <ifp> instead if <nfp>, add a safeguard
    # because of this, we prefer findall over iterfind because we can test the result
    if nfp_type == 'nfp':
        xml_nfp_parent = root.get_nfps()
    else:  # ifp
        xml_nfp_parent = root.get_ifps()

    if xml_nfp_parent is not None:
        xml_nfps = xml_nfp_parent.get_nfp()  # nesting.xsd and nesting_improved.xsd stipulate it
    else:
        xml_nfps = []  # there may be no NFP/IFP root at all

    for xml_nfp in xml_nfps:
        # nesting_improved ONLY: use NFP(piece1, piece2) instead of NFP(polygon1, polygon2)
        if getattr(xml_nfp, 'staticPiece') is None:
            logging.warning('Old XML schema nesting.xsd used -> NFP IGNORED (staticPolygon instead of staticPiece is still used)')
            continue
        static_piece = xml_nfp.get_staticPiece()
        static_piece_id = static_piece.get_idPiece()
        orbiting_piece = xml_nfp.get_orbitingPiece()
        orbiting_piece_id = orbiting_piece.get_idPiece()

        # FALLBACK
        # if there is no such node, it means the old XML NFP format is used; fall back to parsing NFP without boundaries
        if getattr(xml_nfp, 'resultingPolygonWithBoundary') is None:
            xml_resulting_polygon_without_boundary = xml_nfp.get_resultingPolygon()
            id_components_types = [(xml_resulting_polygon_without_boundary.get_idPolygon(), 0)]  # always filled here
            id_boundaries = []

        else:
            resulting_polygon_with_boundary_node = xml_nfp.get_resultingPolygonWithBoundary()

            # stack up all components with their types (0 filled, -1 hole) in tuples + the boundary, with IDs
            # TODO: add support for x/yOffset
            xml_components = resulting_polygon_with_boundary_node.get_component()
            id_components_types = [(xml_component.get_idPolygon(), xml_component.get_type()) for xml_component in xml_components]
            xml_boundary_components = resulting_polygon_with_boundary_node.get_boundaryComponent()
            id_boundaries = [xml_boundary_component.get_idBoundary() for xml_boundary_component in xml_boundary_components]

        nfps[(static_piece_id, orbiting_piece_id), (static_piece.get_angle(), orbiting_piece.get_angle())] = {
            'idComponentsTypes': id_components_types,
            'idBoundaries': id_boundaries
        }
    return nfps


def problem_to_nesting_root(problem):
    """
    Convert a NestingProblem to a nesting root.
    Use output_problem_to_xml for a direct file generation.

    :param problem: NestingProblem
    :rtype: nesting

    """
    # we build from top to bottom
    # so we need to attach a newly created child to its parent each time, with either set_ or add_ methods

    # level 0: nesting tree root
    nesting_root = nesting(
        problem.metadescription['name'],
        problem.metadescription['author'],
        problem.metadescription['date'],
        problem.metadescription['description'],
        problem.metadescription['verticesOrientation'],
        problem.metadescription['coordinatesOrigin']
    )

    # level 1: problem
    xml_problem = problemType()
    nesting_root.set_problem(xml_problem)

    # level 2: boards
    xml_boards = pieceContainerType()
    xml_problem.set_boards(xml_boards)

    # fill boards
    container_id, container_piece = problem.container
    xml_piece = pieceType(container_id, 1)  # we assume there is only one container, with no rotations allowed
    xml_boards.add_piece(xml_piece)

    # only support one container component
    # offset 0 by default, so basically polygon coordinates define everything
    xml_piece.add_component(componentType(idPolygon=container_piece.polygon_id, type_=0, xOffset=0, yOffset=0))

    # level 2: lot
    xml_lot = pieceContainerType()
    xml_problem.set_lot(xml_lot)

    # fill lot: add each piece from the problem
    for piece_id, piece in problem.lot.iteritems():
        # TODO: support for mirroring and other pieceTypes
        # LIMITATION: we do not support angle intervals, having no specific solver for this
        xml_piece = pieceType(piece_id, piece.quantity)
        xml_lot.add_piece(xml_piece)

        xml_orientation = orientationType()
        xml_piece.set_orientation(xml_orientation)

        # by default, no rotation allowed
        for angle in piece.orientations:
            xml_orientation.add_enumeration(enumerationType(angle))

        # support for only one filled polygon component (all pieces are built from polygons with no offset
        # in the problem data structure)
        xml_piece.add_component(componentType(piece.polygon_id, type_=0, xOffset=0, yOffset=0))

        # add hole components (no offset)
        for hole_id in piece.hole_ids:
            xml_piece.add_component(componentType(hole_id, type_=-1, xOffset=0, yOffset=0))

    # level 1: nfps
    nesting_root.set_nfps(_generate_xml_nfps('nfp', problem))

    # level 1: ifps
    nesting_root.set_ifps(_generate_xml_nfps('ifp', problem))

    # level 1: polygons
    xml_polygons = polygonsType()
    nesting_root.set_polygons(xml_polygons)
    # note: dict is unordered so polygons may not be dumped in order of ID
    # IMPROVE: use a dictionary alphabetically ordered
    for polygon_id, polygon in problem.polygons.iteritems():
        assert not polygon.is_empty, 'Cannot construct XML from empty polygon'

        # currently all polygon filled or hole components are stored as LinearRings,
        # but for a broader compatibility we use get_exterior_coords that works with Polygons too
        # note that get_edges_from_coords close the coords loop by default so even a LineString would make it
        exterior_edges = list(get_edges_from_coords(get_exterior_coords(polygon)))
        xml_polygon = polygonType(id=polygon_id, nVertices=len(exterior_edges))
        xml_polygons.add_polygon(xml_polygon)

        xml_lines = LinesType()
        xml_polygon.set_lines(xml_lines)
        for idx, edge in enumerate(exterior_edges, 1):
            # WARNING: support 1 decimal only! remove or adapt format if you need to support more
            # for readability
            # support segments but not arcs, for which we have no algorithms for computations such as NFPs
            xml_lines.add_segment(segmentType(n=idx, x0=edge[0][0], y0=edge[0][1], x1=edge[1][0], y1=edge[1][1]))
        xml_polygon.set_xMin(polygon.bounds[0])
        xml_polygon.set_xMax(polygon.bounds[2])
        xml_polygon.set_yMin(polygon.bounds[1])
        xml_polygon.set_yMax(polygon.bounds[3])

        # note: we could also add perimeter and area, but most geometrical libraries offer quick access to those

    # level 1: boundaries
    xml_boundaries = boundariesType()
    nesting_root.set_boundaries(xml_boundaries)
    for raw_boundary_id, raw_boundary in problem.raw_boundaries.iteritems():
        assert not raw_boundary.is_empty, 'Cannot construct XML from empty raw boundary'

        xml_boundary = boundaryType(raw_boundary_id)
        xml_boundaries.add_boundary(xml_boundary)

        # intialize [xMin, yMin, xMax, yMax] with opposite infinite values
        # we will track the bounds of this boundary (optional information in the XML)
        # ALTERNATIVE: create a union Geometry and get the bounds
        boundary_bounds = [float('+inf'), float('+inf'), float('-inf'), float('-inf')]

        if not raw_boundary.multiline.is_empty:
            xml_lines = LinesType()
            xml_boundary.set_lines(xml_lines)

            for idx, line in enumerate(raw_boundary.multiline, 1):
                xml_lines.add_segment(segmentType(n=idx,
                                                  x0=line.coords[0][0], y0=line.coords[0][1],
                                                  x1=line.coords[1][0], y1=line.coords[1][1]))

            # update extreme coords
            update_bounds(boundary_bounds, raw_boundary.multiline.bounds)

        if not raw_boundary.multipoint.is_empty:
            xml_points = PointsType()
            xml_boundary.set_points(xml_points)
            for idx, point in enumerate(raw_boundary.multipoint, 1):
                xml_points.add_point(pointType(n=idx, x0=point.coords[0][0], y0=point.coords[0][1]))
            update_bounds(boundary_bounds, raw_boundary.multipoint.bounds)

        xml_boundary.set_xMin(boundary_bounds[0])
        xml_boundary.set_xMax(boundary_bounds[2])
        xml_boundary.set_yMin(boundary_bounds[1])
        xml_boundary.set_yMax(boundary_bounds[3])

    # level 1: sequences (?)

    # level 1: solutions [not supported]
    # TODO

    return nesting_root


def _generate_xml_nfps(nfp_type, problem):
    """Return generateDS node data with metadata of type nfp_type (NFP or IFP) from the given problem"""
    plural_nfp_type = '{}s'.format(nfp_type)  # 'nfps' or 'ifps'
    xml_nfps = nfpsType()
    # if no nfps found at all, return an empty node
    # else add a child for each nfp
    if plural_nfp_type in problem.metadata:
        for (piece_id_couple, orientation_couple), id_components_boundary_dict in problem.metadata[plural_nfp_type].iteritems():
            # only null angles supported, no mirrors
            # TODO: add support for nfp for each angle of static and orbiting piece
            #   in practice, we will not write such NFP by hand so generation will be automated in
            #   ExpandedNestingProblem or with an option when converting Problem -> XML
            xml_nfp = nfpType(staticPiece=nfpPieceType(piece_id_couple[0], angle=orientation_couple[0], mirror='none'),
                              orbitingPiece=nfpPieceType(piece_id_couple[1], angle=orientation_couple[1], mirror="none"))
            xml_nfps.add_nfp(xml_nfp)

            xml_resulting_polygon_with_boundary = resultingPolygonWithBoundaryType()
            xml_nfp.set_resultingPolygonWithBoundary(xml_resulting_polygon_with_boundary)
            for id_component, component_type in id_components_boundary_dict['idComponentsTypes']:
                xml_resulting_polygon_with_boundary.add_component(
                    componentType(id_component, type_=component_type, xOffset=0, yOffset=0)
                )
            for id_boundary in id_components_boundary_dict['idBoundaries']:
                xml_resulting_polygon_with_boundary.add_boundaryComponent(
                    boundaryComponentType(idBoundary=id_boundary, xOffset=0, yOffset=0)
                )
    return xml_nfps
