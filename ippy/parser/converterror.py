__author__ = 'huulong'


class ProblemXMLFormatError(Exception):
    """Error raised when parsing a Problem XML with incorrect structure or ill-formed content"""
    def __init__(self, message, xml_file_path):
        super(ProblemXMLFormatError, self).__init__('in {}, {}'.format(xml_file_path, message))
