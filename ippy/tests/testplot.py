from shapely.geometry import Point, Polygon, MultiLineString
from ippy.view.plot import plot_linestring_colors, plot_geom_colors, get_uniform_dimension_bounds

__author__ = 'hs'

import unittest

import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib import patches
import numpy as np


class PlotTestCase(unittest.TestCase):
    """Test plotting a polygon"""

    @classmethod
    def setUpClass(cls):
        cls.polygon_with_hole = Polygon([(0, 0), (10, 0), (10, 10), (0, 10)], holes=[
            [(4, 4), (8, 4), (8, 8), (4, 8)],
            [(2, 2), (2, 3), (3, 3), (3, 1)]
        ])
        cls.multiline = MultiLineString([[(0, 0), (1, 0)], [(2, 0), (2, 1)]])

    def test_human(self):
        plot_geom_colors(
            (self.polygon_with_hole, 'red'),
            (self.multiline, 'blue'),
            (Point(0, 0), 'green'),
        )

    def test_human_to_patch_collection(self):
        # TODO
        pass

        # def test_plot_polygon(self):
    #     verts = [
    #         (0., 0.),  # left, bottom
    #         (0., 1.),  # left, top
    #         (1., 1.),  # right, top
    #         (1., 0.),  # right, bottom
    #         (0., 0.),  # ignored
    #         ]
    #
    #     codes = [
    #         Path.MOVETO,
    #         Path.LINETO,
    #         Path.LINETO,
    #         Path.LINETO,
    #         Path.CLOSEPOLY,
    #         ]
    #
    #     path = Path(verts, codes)
    #
    #     fig = plt.figure()
    #     ax = fig.add_subplot(111)
    #     patch = patches.PathPatch(path, facecolor='orange', lw=2)
    #     ax.add_patch(patch)
    #     ax.set_xlim(-2, 2)
    #     ax.set_ylim(-2, 2)
    #     plt.show()

    def test_get_uniform_dimension_bounds_rectangle(self):
        """Test get_uniform_dimension_bounds on rectangle"""
        rectangle_bounds = 0, 0, 4, 6
        self.assertEqual(get_uniform_dimension_bounds(rectangle_bounds), (0, 0, 6, 6))

    def test_get_uniform_dimension_bounds_square(self):
        """Test get_uniform_dimension_bounds on square (nothing should change)"""
        square_bounds = -2, 0, 4, 6
        self.assertEqual(get_uniform_dimension_bounds(square_bounds), (-2, 0, 4, 6))

if __name__ == '__main__':
    unittest.main()
