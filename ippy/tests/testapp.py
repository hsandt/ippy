from ippy.view.app import Application

__author__ = 'hs'

import unittest

import matplotlib
matplotlib.use('TkAgg')

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

import Tkinter
from Tkinter import Tk

from shapely.geometry import Polygon
from ippy.view.plot import to_patch_collection


class MyTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    def test_app(self):
        fig = Figure(figsize=(5, 4), dpi=100)
        axis = fig.add_subplot(111)

        p = Polygon([(0, 0), (10, 0), (10.54, 10), (0, 10)], holes=[[(1, 1.578), (2, 1), (2, 2), (1, 2)],
                                                                    [(3, 3), (5, 3.56897), (5, 5)]])

        p_patch_collections = [axis.add_collection(to_patch_collection(p))]
        # p_patch_collections[0].set_visible(False)
        artists_sequence = [p_patch_collections]

        axis.set_xlim(0, 20)
        axis.set_ylim(0, 20)

        root = Tk()
        app = Application(master=root)

        # a tk.DrawingArea
        canvas = FigureCanvasTkAgg(fig, master=app)
        canvas.show()
        canvas.get_tk_widget().pack(side=Tkinter.TOP, fill=Tkinter.BOTH, expand=1)

        # bind keyboard event to see placements step by step
        fig.canvas.mpl_connect('key_press_event', self.on_keyboard)

        app.mainloop()
        root.destroy()

    def on_keyboard(self, event):
        print('you pressed', event.key, event.xdata, event.ydata)



if __name__ == '__main__':
    unittest.main()
