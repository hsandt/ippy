from shapely import geometry
from shapely.geometry import box, Polygon

from ippy.packmath.nfp import nfp_cuninghame_green, get_ifp_with_rectangular_container, \
    nfp_cuninghame_green_with_convex_decomposition
from ippy.packmath.problem import NestingProblem, PieceData
from ippy.parser.convert import problem_to_xml, to_shapely_object_list, geom_list_to_wkt
from ippy.parser.xmlproblem import output_problem_to_xml
from ippy.utils.files import get_full_path
from ippy.utils.shapelyextension import rotate_translate

__author__ = 'hs'


def generate_problem1():
    """Generate problem1: BLS fails, NFPVS succeeds"""

    # generate problem partially from a WKT file (polygons) and partially manually (container and rotations)
    # TODO: when XML <-> problem conversion supports rotation, directly set rotation data in problem instead
    wkt_filepath = get_full_path('benchmark', 'Personal', 'sample0.wkt')
    output_filepath = get_full_path('benchmark', 'Personal', 'problem_bls_fails_nfpvs_succeeds.xml')

    # create polygons
    polygon0 = geometry.box(0, 0, 10, 15)  # container polygon

    # polygons to place, rotate them 90 degrees clockwise
    polygon_list = to_shapely_object_list(wkt_filepath)
    polygon7, polygon8 = rotate_translate(polygon_list[7 - 1], -90, (0, 10)), \
        rotate_translate(polygon_list[8 - 1], -90, (0, 10))

    # compute ifp with rectangular container of size (10, 15)
    ifp_polygon0 = get_ifp_with_rectangular_container(polygon7, 10, 15)
    ifp_polygon1 = get_ifp_with_rectangular_container(polygon8, 10, 15)

    # both polygons are convex so cuninghame-green method enough to obtain NFP
    # (NFP computed on rotated polygons)
    nfp_polygon0 = nfp_cuninghame_green(polygon7, polygon8)

    container = ('board0', PieceData('board0', 1, [0], 'polygon0'))
    lot = {'piece0': PieceData('piece0', 1, [0], 'polygon7'), 'piece1': PieceData('piece1', 1, [0], 'polygon8')}
    # for NFP metadata, note that we don't need reflective NFPs since there is only one piece of each
    # as long as the search strategy does not ask for it, the program should work
    metadata = {'nfps': {('polygon7', 'polygon8'): 'nfpPolygon0'},
                'ifps': {('polygon0', 'polygon7'): 'ifpPolygon0', ('polygon0', 'polygon8'): 'ifpPolygon1'}}
    polygons = {'polygon0': polygon0, 'polygon7': polygon7, 'polygon8': polygon8,
                'ifpPolygon0': ifp_polygon0, 'ifpPolygon1': ifp_polygon1,
                'nfpPolygon0': nfp_polygon0}
    name = 'BLS fails, NFPVS succeeds Problem'
    author = 'huulong'
    date = '2015/04/19'
    description = 'In this problem, no rotations are allowed on 2 triangles so that BLS fails but NFPVS succeeds.'
    vertices_orientation = 'counterclockwise'
    coordinates_origin = 'down-left'
    problem = NestingProblem(container, lot, metadata, polygons, {}, name, author, date, description,
                             vertices_orientation, coordinates_origin)

    problem_to_xml(problem, output_filepath)


def generate_problem2():
    """Generate NFPVS fails problem; generated manually and not converted to XML for now"""

    # generate problem partially from a WKT file (polygons) and partially manually (container and rotations)
    wkt_filepath = get_full_path('benchmark', 'Personal', 'sample3.wkt')
    output_filepath = get_full_path('benchmark', 'Personal', 'problem2_bls_fails_nfpvs_fails.xml')

    # create polygons
    polygon0 = geometry.box(0, 0, 18, 6)  # container polygon

    # polygons to place, rotate them 90 degrees clockwise
    polygon_list = to_shapely_object_list(wkt_filepath)
    polygon2, polygon3 = polygon_list[2 - 1], polygon_list[3 - 1]

    # compute ifps and nfps (optional, but makes search faster by avoid runtime computation)

    # compute ifp with rectangular container of size (10, 15)
    # TODO: use a PolygonWithBoundary instead, and adapt XML
    ifp_polygon0 = get_ifp_with_rectangular_container(polygon2, 18, 6)
    ifp_polygon1 = get_ifp_with_rectangular_container(polygon3, 18, 6)

    # both polygons are convex so cuninghame-green method enough to obtain NFP
    # (NFP computed on rotated polygons)
    nfp_polygon0 = nfp_cuninghame_green(polygon2, polygon2)
    nfp_polygon1 = nfp_cuninghame_green_with_convex_decomposition(polygon2, polygon3)

    container = ('board0', PieceData('board0', 1, [0], 'polygon0'))
    lot = {'piece0': PieceData('piece0', 2, [0], 'polygon2'), 'piece1': PieceData('piece0', 1, [0], 'polygon3')}
    # no NFP until the converter is fixed (nfp will be converted in runtime as PolygonWithBoundary)
    metadata = {
        # 'nfps': {('polygon2', 'polygon2'): 'nfpPolygon0', ('polygon2', 'polygon3'): 'nfpPolygon1'},
        'nfps': {},
        # 'ifps': {('polygon0', 'polygon2'): 'ifpPolygon0', ('polygon0', 'polygon3'): 'ifpPolygon1'}
        'ifps': {}
    }
    polygons = {'polygon0': polygon0, 'polygon2': polygon2, 'polygon3': polygon3,
                # 'ifpPolygon0': ifp_polygon0, 'ifpPolygon1': ifp_polygon1,
                # 'nfpPolygon0': nfp_polygon0, 'nfpPolygon1': nfp_polygon1
                }
    name = 'BLS fails, NFPVS fails Problem'
    author = 'huulong'
    date = '2015/04/21'
    description = 'In this problem, both BLS and NFPVS fail.'
    vertices_orientation = 'counterclockwise'
    coordinates_origin = 'down-left'
    problem = NestingProblem(container, lot, metadata, polygons, {}, name, author, date, description,
                             vertices_orientation, coordinates_origin)

    problem_to_xml(problem, output_filepath)

def generate_problem_tangram():
    """Generate Tangram problem; generated manually and not converted to XML for now"""

    # generate problem partially from a WKT file (polygons) and partially manually (container and rotations)
    wkt_filepath = get_full_path('benchmark', 'Personal', 'tangram', 'tangram.wkt')
    output_filepath = get_full_path('benchmark', 'Personal', 'tangram', 'problem_tangram.xml')

    # create polygons
    square_container = geometry.box(0, 0, 8, 8)  # container polygon

    # polygons to place, rotate them 90 degrees clockwise
    polygon_list = to_shapely_object_list(wkt_filepath)
    big_triangle, medium_triangle, small_triangle, square, parallelogram = polygon_list

    # IFP and NFP computation should be completely automated in the new Problem -> XML converter
    # (but you can override some values if you want)

    container = ('board0', PieceData('board0', 1, [0], 'squareContainer'))
    lot = {
        'bigTriangle': PieceData('bigTriangle', 2, [0, 45, 90, 135, 180, -135, -90, -45], 'bigTrianglePolygon'),
        'mediumTriangle': PieceData('mediumTriangle', 1, [0, 45, 90, 135, 180, -135, -90, -45], 'mediumTrianglePolygon'),
        'smallTriangle': PieceData('smallTriangle', 2, [0, 45, 90, 135, 180, -135, -90, -45], 'smallTrianglePolygon'),
        'square': PieceData('square', 1, [0, 45], 'squarePolygon'),
        'parallelogram': PieceData('parallelogram', 1, [0, 45, 90, 135], 'parallelogramPolygon'),  # TODO: allow mirroring (optional)
    }

    # no NFP until the converter is fixed (nfp will be converted in runtime as PolygonWithBoundary)
    metadata = {
        # 'nfps': {('polygon2', 'polygon2'): 'nfpPolygon0', ('polygon2', 'polygon3'): 'nfpPolygon1'},
        'nfps': {},
        # 'ifps': {('polygon0', 'polygon2'): 'ifpPolygon0', ('polygon0', 'polygon3'): 'ifpPolygon1'}
        'ifps': {}
    }

    polygons = {'squareContainer': square_container,
                'bigTrianglePolygon': big_triangle,
                'mediumTrianglePolygon': medium_triangle,
                'smallTrianglePolygon': small_triangle,
                'squarePolygon': square,
                'parallelogramPolygon': parallelogram,
                }
    name = 'Tangram Problem'
    author = 'huulong'
    date = '2015/05/30'
    description = 'Tangram problem where pieces can rotate by increments of 45 or 90 degrees.'
    vertices_orientation = 'counterclockwise'
    coordinates_origin = 'down-left'
    problem = NestingProblem(container, lot, metadata, polygons, {}, name, author, date, description,
                             vertices_orientation, coordinates_origin)

    output_problem_to_xml(problem, output_filepath)


def generate_problem_chocolate():
    """Generate chocolate fails problem"""

    # generate both WKT (for use in unit tests) and XML problem
    wkt_filepath = get_full_path('benchmark', 'Personal', 'chocolate', 'chocolate.wkt')
    output_filepath = get_full_path('benchmark', 'Personal', 'chocolate', 'problem_chocolate.xml')

    # create polygons
    container_polygon = box(0, 0, 9, 5)  # container polygon

    # pieces
    o = box(0, 0, 2, 2)
    elbow = Polygon([(0, 0), (2, 0), (2, 2), (1, 2), (1, 1), (0, 1)])
    i3_h = box(0, 0, 3, 1)
    i3_v = box(0, 0, 1, 3)
    i4_h = box(0, 0, 4, 1)
    i4_v = box(0, 0, 1, 4)
    s_h = Polygon([(0, 0), (2, 0), (2, 1), (3, 1), (3, 2), (1, 2), (1, 1), (0, 1)])
    z_v = Polygon([(0, 0), (1, 0), (1, 1), (2, 1), (2, 3), (1, 3), (1, 2), (0, 2)])
    j_h = Polygon([(0, 0), (3, 0), (3, 1), (1, 1), (1, 2), (0, 2)])
    j_v = Polygon([(0, 0), (2, 0), (2, 3), (1, 3), (1, 1), (0, 1)])
    t_h = Polygon([(0, 0), (3, 0), (3, 1), (2, 1), (2, 2), (1, 2), (1, 1), (0, 1)])
    t_v = Polygon([(0, 0), (1, 0), (1, 1), (2, 1), (2, 2), (1, 2), (1, 3), (0, 3)])

    # IFP and NFP computation should be completely automated in the new Problem -> XML converter
    # (but you can override some values if you want)

    container = ('board0', PieceData('board0', 1, [0], 'containerPolygon'))
    lot = {
        'o': PieceData('o', 1, [0], 'oPolygon'),
        'elbow': PieceData('elbow', 1, [0, 180], 'elbowPolygon'),
        'i3_h': PieceData('i3_h', 1, [0], 'i3_hPolygon'),
        'i3_v': PieceData('i3_v', 1, [0], 'i3_vPolygon'),
        'i4_h': PieceData('i4_h', 1, [0], 'i4_hPolygon'),
        'i4_v': PieceData('i4_v', 1, [0], 'i4_vPolygon'),
        's_h': PieceData('s_h', 1, [0], 's_hPolygon'),
        'z_v': PieceData('z_v', 1, [0], 'z_vPolygon'),
        'j_h': PieceData('j_h', 1, [0, 180], 'j_hPolygon'),
        'j_v': PieceData('j_v', 1, [0, 180], 'j_vPolygon'),
        't_h': PieceData('t_h', 1, [0, 180], 't_hPolygon'),
        't_v': PieceData('t_v', 1, [0, 180], 't_vPolygon'),
    }

    # no NFP until the converter is fixed (nfp will be converted in runtime as PolygonWithBoundary)
    metadata = {
        # 'nfps': {('polygon2', 'polygon2'): 'nfpPolygon0', ('polygon2', 'polygon3'): 'nfpPolygon1'},
        'nfps': {},
        # 'ifps': {('polygon0', 'polygon2'): 'ifpPolygon0', ('polygon0', 'polygon3'): 'ifpPolygon1'}
        'ifps': {}
    }

    polygons = {'containerPolygon': container_polygon,
                'oPolygon': o,
                'elbowPolygon': elbow,
                'i3_hPolygon': i3_h,
                'i3_vPolygon': i3_v,
                'i4_hPolygon': i4_h,
                'i4_vPolygon': i4_v,
                's_hPolygon': s_h,
                'z_vPolygon': z_v,
                'j_hPolygon': j_h,
                'j_vPolygon': j_v,
                't_hPolygon': t_h,
                't_vPolygon': t_v
                }
    name = 'Chocolate Problem'
    author = 'Hanayama & meiji'
    date = '2015/07/04'
    description = 'Chocolate puzzle from Hanayama, easy, X and Y tile size are not compatible (only 180 degrees rotation)'
    vertices_orientation = 'counterclockwise'
    coordinates_origin = 'down-left'
    problem = NestingProblem(container, lot, metadata, polygons, {}, name, author, date, description,
                             vertices_orientation, coordinates_origin)

    output_problem_to_xml(problem, output_filepath)

    # also create WKT
    geom_list_to_wkt([container_polygon, o, elbow, i3_h, i3_v, i4_h, i4_v, s_h, z_v, j_h, j_v, t_h, t_v], wkt_filepath)

if __name__ == '__main__':
    generate_problem_chocolate()

