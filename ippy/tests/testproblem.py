from shapely.geometry import box
from ippy.packmath.problem import NestingProblem, PieceData, ExpandedNestingProblem
from ippy.utils.shapelyextension import GeomWrapper, ComparableBaseGeometry
from ippy.utils.unittestextension import assertKeyEqual

__author__ = 'huulong'

import unittest


class SingleBinSizeBinPackingProblemTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    def test_something(self):
        pass

class MyProblemTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # create polygons
        cls.polygon0 = box(0, 0, 10, 15)
        cls.polygon1, cls.polygon2 = box(0, 0, 5, 5), box(0, 0, 10, 10)

        container = ('polygon0', PieceData('board0', 1, [0], 'polygon0'))
        lot = {'polygon1': PieceData('piece0', 1, [0], 'polygon1'), 'polygon2': PieceData('piece1', 2, [0], 'polygon2')}
        metadata = {'nfps': {}, 'ifps': {}}
        polygons = {'polygon0': cls.polygon0, 'polygon1': cls.polygon1, 'polygon2': cls.polygon2}
        boundaries = {}
        cls.problem = NestingProblem(container, lot, metadata, polygons, boundaries)
        cls.expanded_problem = ExpandedNestingProblem(cls.problem)

    def test_get_individual_piece_list(self):

        golden_individual_piece_list = [
            GeomWrapper(self.polygon1, 'polygon1', 'polygon1_0'),
            GeomWrapper(self.polygon2, 'polygon2', 'polygon2_0'),
            GeomWrapper(self.polygon2, 'polygon2', 'polygon2_1'),
        ]

        # use assertItemsEqual to compare ignoring item order
        # alternative: use a list instead of a dict for the lot; pieces contain their own IDs anyway
        self.assertItemsEqual(self.problem.get_individual_piece_list(), golden_individual_piece_list)


    def test_expanded_nesting_problem_expanded_container(self):
        """Test initialization of an ExpandedNestingProblem's expanded container"""
        self.assertEqual(self.expanded_problem.expanded_container[0], 'polygon0')
        assertKeyEqual(self, self.expanded_problem.expanded_container[1], self.polygon0, key=ComparableBaseGeometry)

    def test_expanded_nesting_problem_expanded_lot(self):
        """Test initialization of an ExpandedNestingProblem's expanded container"""
        self.assertItemsEqual(self.expanded_problem.expanded_lot.keys(), [('polygon1', 0), ('polygon2', 0), ('polygon2', 1)])
        for piece_id, instance_idx in self.expanded_problem.expanded_lot.keys():
            polygon = self.polygon1 if piece_id == 'polygon1' else self.polygon2
            assertKeyEqual(self, self.expanded_problem.expanded_lot[(piece_id, instance_idx)], polygon, key=ComparableBaseGeometry)

    def test_expanded_nesting_problem_piece_nfps(self):
        """Test initialization of an ExpandedNestingProblem's piece NFPs"""
        # TODO

    def test_expanded_nesting_problem_piece_ifps(self):
        """Test initialization of an ExpandedNestingProblem's piece IFPs"""
        # TODO


if __name__ == '__main__':
    unittest.main()
