import unittest

import os
from ippy.packmath.problem import NestingProblem, PieceData

from ippy.parser.convert import _to_wkt, _to_file, to_shapely_object_list, xml_to_problem, problem_to_xml, \
    geom_list_to_wkt
from ippy.utils.files import get_full_path
from ippy.utils.shapelyextension import ComparableBaseGeometry, PolygonWithBoundary, RawBoundary
from ippy.utils.unittestextension import assertKeyEqual, assertSequenceKeyEqual, assertDictKeyEqual, assertTupleKeyEqual

from shapely.geometry import Polygon, box, Point, GeometryCollection, LineString

__author__ = 'huulong'


# TODO: find how to solve the problem of multiple polygons with multiple holes in the XML
# (each hole must belong to a Polygon, test manually if hole is inside polygon, remove geometry
# manually in Shapely and see what happens instead of building a clean Polygon with a hole, etc.)
# TODO 2: see if the component definition of a piece in the XML is useful enough for an NFP
# to make it a schema type itself and refer all NFP/IFP to that component structure (+ boundary)

class ConvertTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Terashima input file path
        cls.esicup_filepath = get_full_path('benchmark', 'Personal', 'polygon_5_terashima_sample.txt')
        # reference wkt file path
        cls.wkt_filepath = get_full_path('benchmark', 'Personal', 'polygon_5_wkt_sample.wkt')

        # XML input file path
        cls.xml_filepath = get_full_path('benchmark', 'Personal', 'xml_sample1.xml')

        # test problem (corresponds to golden_problem.xml)
        polygon0 = box(0, 0, 10, 10)
        # polygon1 = Polygon([(0.0, 0.0), (10.0, 0.0), (10.0, 10.0), (0.0, 10.0), (0.0, 0.0)],
        #                    holes=[[(1.0, 1.0), (2.0, 1.0), (2.0, 2.0), (1.0, 2.0), (1.0, 1.0)]])
        polygon1 = Polygon([(0.0, 0.0), (10.0, 0.0), (10.0, 2.0), (5.0, 2.0), (5.0, 4.0), (10.0, 4.0),
                            (10.0, 10.0), (0.0, 10.0), (0.0, 0.0)],
                           holes=[[(2.0, 6.0), (4.0, 6.0), (4.0, 8.0), (2.0, 8.0), (2.0, 6.0)],
                                  [(5.0, 5.0), (9.0, 5.0), (9.0, 8.0), (5.0, 8.0), (5.0, 5.0)]])

        polygon2 = box(0, 0, 2, 2)

        ifp_raw_boundary1 = RawBoundary(points=[Point(0, 0)])

        ifp_exterior2 = box(0, 0, 8, 8).exterior

        nfp0_polygon_with_boundary = PolygonWithBoundary(Polygon([(10, -2), (10, 10), (-2, 10), (-2, -2)],
                                                         holes=[[(5, 5), (7, 5), (7, 6), (5, 6)]]),
                                                         # boundary=GeometryCollection([LineString([(5, 2), (10, 2)]),
                                                         #                              Point(2, 6)])
                                                         )
        # it is difficult to extract a raw boundary from a boundary, although not impossible
        # for now, just input the raw boundary separately
        (nfp0_exterior, ), (nfp0_hole, ), _  = nfp0_polygon_with_boundary.extract_exteriors_holes_boundary_tuple()
        nfp0_raw_boundary = RawBoundary([LineString([(5, 2), (10, 2)])])

        # should match golden_problem.xml
        container = ('board0', PieceData('board0', 1, [0], 'polygon0'))
        lot = {'piece1': PieceData('piece1', 1, [0], 'polygon1', ['hole0', 'hole1']),
               'piece2': PieceData('piece2', 1, [0, 180], 'polygon2')}
        metadata = {
            'nfps': {
                ('piece1', 'piece2'): {
                    'idComponentsTypes': [('nfpPolygon0', 0), ('nfpHole0', -1)],
                    'idBoundaries': ['nfpBoundary0']
                    }
                },
            'ifps': {
                ('board0', 'piece1'): {
                    'idComponentsTypes': [],
                    'idBoundaries': ['ifpBoundary1']
                },
                ('board0', 'piece2'): {
                    'idComponentsTypes': [('ifpPolygon2', 0)],
                    'idBoundaries': []
                }
            }
        }
        polygons = {
            'polygon0': polygon0.exterior,
            'polygon1': polygon1.exterior, 'hole0': polygon1.interiors[0], 'hole1': polygon1.interiors[1],
            'polygon2': polygon2.exterior,
            'ifpPolygon2': ifp_exterior2,
            'nfpPolygon0': nfp0_exterior, 'nfpHole0': nfp0_hole
        }
        raw_boundaries = {
            'nfpBoundary0': nfp0_raw_boundary,
            'ifpBoundary1': ifp_raw_boundary1
        }
        name = 'ConvertTestCaseProblem'
        author = 'Me'
        date = '2015/04/28'
        description = 'A problem with NFP/IFP with boundaries.'
        vertices_orientation = 'counterclockwise'
        coordinates_origin = 'down-left'
        cls.problem = NestingProblem(container, lot, metadata, polygons, raw_boundaries, name, author, date, description,
                                     vertices_orientation, coordinates_origin)

    @classmethod
    def tearDownClass(cls):
        pass

    def test_to_wkt(self):
        """Test conversion from ESICUP to WKT"""
        # compare the WKT lines generated and the lines of the golden file
        with open(self.wkt_filepath, 'r') as wkt_file:
            golden_lines = [line.strip() for line in list(wkt_file)]
            output_lines = list(_to_wkt(self.esicup_filepath))
            self.assertEqual(output_lines, golden_lines)

    @unittest.skip('xml_to_problem is deprecated, use nesting_root_to_problem instead')
    def test_xml_to_problem_container(self):
        """Test conversion from nesting XML to PackingProblem: container"""
        # golden container
        golden_container = PieceData('board0', 1, [0], 'polygon0')
        # convert XML to PackingProblem
        problem = xml_to_problem(self.xml_filepath)
        # compare the PackingProblem generated and the expected problem
        self.assertEqual(problem.container[1], golden_container)

    @unittest.skip('xml_to_problem is deprecated, use nesting_root_to_problem instead')
    def test_xml_to_problem_lot(self):
        """Test conversion from nesting XML to PackingProblem: lot"""
        # golden lot polygons (not string IDs)
        # golden_lot = {
        #     'polygon1': Polygon([(0., 0.), (10., 2.), (8., 5.), (4., 4.)]),
        #     'polygon2': Polygon([(0., 0.), (7., 0.), (5., 7.)]),
        #     'polygon3': Polygon([(0., 1.), (3., 0.), (7., 2.), (4., 5.), (1., 3.)]),
        #     'polygon4': Polygon([(2., 3.), (6., 0.), (7., 7.), (0., 10.)])
        # }
        golden_lot = {
            'piece1': PieceData('piece1', 1, [0], 'polygon1'),
            'piece2': PieceData('piece2', 1, [0], 'polygon2'),
            'piece3': PieceData('piece3', 1, [0], 'polygon3'),
            'piece4': PieceData('piece4', 1, [0], 'polygon4'),
        }
        # convert XML to PackingProblem
        problem = xml_to_problem(self.xml_filepath)
        # compare the PackingProblem generated and the expected problem
        self.assertDictEqual(problem.lot, golden_lot)

    @unittest.skip('xml_to_problem is deprecated, use nesting_root_to_problem instead')
    def test_xml_to_problem_nfp(self):
        """Test conversion from nesting XML to PackingProblem: metadata NFP"""
        # new convention is to store NFP polygon id only, to reflect the XML better
        golden_nfp_component_boundary_descriptions = {
            ('piece1', 'piece1'): {
                'idComponentsTypes': [('nfpPolygon0', 0)],
                'idBoundaries': []
            },
            ('piece1', 'piece2'): {
                'idComponentsTypes': [('nfpPolygon1', 0)],
                'idBoundaries': []
            }
        }

        # convert XML to PackingProblem
        problem = xml_to_problem(self.xml_filepath)
        # compare the PackingProblem generated and the expected problem
        self.maxDiff = None
        # just compare the first elements, there are too many of them...
        iter1 = golden_nfp_component_boundary_descriptions.iteritems()
        for x in xrange(2):
            polygon_tuple, golden_nfp_id = iter1.next()
            nfp_component_boundary_description = problem.metadata['nfps'][polygon_tuple]  # ensure you use NFP of the same polygons

            self.assertEqual(nfp_component_boundary_description, golden_nfp_component_boundary_descriptions[polygon_tuple])

    @unittest.skip('xml_to_problem is deprecated, use nesting_root_to_problem instead')
    def test_xml_to_problem(self):
        """Test conversion from nesting XML to PackingProblem: all data"""
        xml_filepath = get_full_path('tests', 'goldenfiles', 'golden_problem.xml')
        problem = xml_to_problem(xml_filepath)
        # compare the PackingProblem generated and the expected problem
        self.assertEqual(problem, self.problem)

    # INVALID test
    # due to difference of XML formatting
    @unittest.skip('XML formatting is not completely ensured')
    def test_problem_to_xml(self):
        """ Test conversion from a PackingProblem to an XML file following ESICUP's XMLSchema"""
        golden_filepath = get_full_path('tests', 'goldenfiles', 'golden_problem.xml')
        # REFACTOR: use a mocking library
        mock_filepath = get_full_path('temp', 'mock_file.xml')
        problem_to_xml(self.problem, mock_filepath, schema='http://globalnest.fe.up.pt/nesting_improved')

        with open(golden_filepath, 'r') as golden_file, open(mock_filepath, 'r') as mock_file:
            # OPTIMIZE: prefer generator if list generation is too long, and adapt assertion test
            golden_lines = list(golden_file)  # ensure there is a newline at the end of the file
            output_lines = list(mock_file)
            self.assertEqual(output_lines, golden_lines)
            # clean-up mock file (usually on Exception finally, here it is deliberately
            # prevented if assertion fails to debug the output file afterward)
            os.remove(mock_filepath)

    @unittest.skip('xml_to_problem is deprecated, use nesting_root_to_problem instead')
    def test_problem_to_xml_to_problem(self):
        """Test conversion back and forth to a problem is conservative"""
        mock_filepath = get_full_path('temp', 'mock_file.xml')
        problem_to_xml(self.problem, mock_filepath)
        self.assertEqual(xml_to_problem(mock_filepath), self.problem)


    # INVALID test
    # due to difference of XML formatting
    @unittest.skip('XML formatting is not completely ensured')
    def test_xml_to_problem_to_xml(self):
        """Test conversion back and forth to a problem XML file is conservative"""
        # this test cannot pass for now, because of lxml 2-space indentation...
        # we could write a more permissive test with space trimming, though

        # here, we will use a golden file semi-generated with lxml, so with 2-space indent
        golden_filepath = get_full_path('tests', 'goldenfiles', 'golden_problem.xml')

        # REFACTOR: use a mocking library
        mock_filepath = get_full_path('temp', 'mock_file.xml')
        problem_to_xml(xml_to_problem(golden_filepath), mock_filepath, schema='http://globalnest.fe.up.pt/nesting_improved')

        with open(golden_filepath, 'r') as golden_file, open(mock_filepath, 'r') as mock_file:
            # OPTIMIZE: prefer generator if list generation is too long, and adapt assertion test
            golden_lines = list(golden_file)  # ensure there is a newline at the end of the file
            output_lines = list(mock_file)
            # add trim line for all lines if needed
            self.assertEqual(output_lines, golden_lines)
            # clean-up mock file (usually on Exception finally, here it is deliberately
            # prevented if assertion fails to debug the output file afterward)
            os.remove(mock_filepath)

    def test_to_file(self):
        """Test writing from line generator to file"""
        # golden file path
        golden_filepath = get_full_path('benchmark', 'Personal', 'golden_yielded_lines.txt')
        # provide a mock generator as the input to test
        mock_file_generator = (x for x in ("abc", "123", "___"))
        # apply the function to the mock generator
        # TODO: use Text Fixtures or another Test library for a sandbox
        mock_filepath = get_full_path('temp', 'mock_file.txt')
        _to_file(mock_file_generator, mock_filepath)
        # compare the lines of the file built from the generator and of the golden file
        with open(golden_filepath, 'r') as golden_file, open(mock_filepath, 'r') as mock_file:
            golden_lines = list(golden_file)  # newline at the end of the file
            output_lines = list(mock_file)
            self.assertEqual(output_lines, golden_lines)
            # clean-up mock_filepath (or use fixtures, or create file in setUp and
            # remove file in tearDown)
            os.remove(mock_filepath)

    def test_to_shapely_object_list(self):
        """Test conversion from WKT file to list of Shapely geometrical objects"""
        # create golden list of Shapely objects manually
        golden_shapely_object_list = [
            Polygon([(2, 0), (12, 0), (12, 14), (0, 14), (8, 10), (8, 4)]),
            Polygon([(0, 0), (2, 4), (10, 10), (0, 10)]),
            Polygon([(0, 0), (13, 0), (13, 3), (1, 3)]),
            Polygon([(0, 0), (8, 0), (0, 7)]),
            Polygon([(8, 0), (8, 3), (0, 7)])
        ]
        # apply conversion to Shapely object list
        shapely_object_list = to_shapely_object_list(self.wkt_filepath)
        # compare both using Shapely's equals thanks to keying
        assertSequenceKeyEqual(self, shapely_object_list, golden_shapely_object_list, ComparableBaseGeometry)

    def test_geom_list_to_wkt(self):
        """Test conversion from list of Shapely geometrical objects to WKT file"""
        # geometries to dump individually
        # correspond to golden file self.wkt_filepath
        shapely_object_list = [
            Polygon([(2, 0), (12, 0), (12, 14), (0, 14), (8, 10), (8, 4)]),
            Polygon([(0, 0), (2, 4), (10, 10), (0, 10)]),
            Polygon([(0, 0), (13, 0), (13, 3), (1, 3)]),
            Polygon([(0, 0), (8, 0), (0, 7)]),
            Polygon([(8, 0), (8, 3), (0, 7)])
        ]
        # apply conversion to Shapely object list
        mock_filepath = get_full_path('temp', 'mock_file.txt')
        geom_list_to_wkt(shapely_object_list, mock_filepath)
        # compare obtained and golden files
        with open(self.wkt_filepath, 'r') as golden_file, open(mock_filepath, 'r') as mock_file:
            golden_lines = list(golden_file)  # newline at the end of the file
            output_lines = list(mock_file)
            self.assertEqual(output_lines, golden_lines)
            # clean-up mock_filepath (or use fixtures, or create file in setUp and
            # remove file in tearDown)
            os.remove(mock_filepath)


if __name__ == '__main__':
    unittest.main()
