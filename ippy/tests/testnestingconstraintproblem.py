from reportlab.lib.utils import _FmtSelfDict
from shapely.geometry import box, MultiPolygon
from ippy.packmath.nestingconstraintproblem import NestingCache, InputMinimizationNestingProblem, \
    SingleBinSizeBinPackingProblem
from ippy.packmath.nestingconstraintsearch import ValuePickingNestingSearchProblem, NestingAssignmentSearchNode
from ippy.packmath.problem import Piece, Placement, PieceKey
from ippy.packmath.problemfactory import create_expanded_packing_problem_from_file, \
    create_fixed_origin_single_bin_size_problem
from ippy.utils.files import get_full_path
from ippy.utils.shapelyextension import PolygonExtended, check_equal
import mock
from mock import patch
from ippy.view.plot import plot_geom_colors

__author__ = 'hs'

import unittest


class NestingConstraintProblemTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    def test_something(self):
        # TODO: more tests for nestingconstraintproblem package
        self.assertEqual(True, False)

    @patch.multiple(InputMinimizationNestingProblem, __abstractmethods__=set())
    def test_update_cache_free_space(self):
        # TODO: quite hard, I need to recreate a big part of the problem... or simply
        # load an existing problem as in MockNestingConstraintProblemTestCase

        # mock input (if too heavy, prefer pure static functions and test them as pure units)
        # not sure how to use Mock with partial attributes, for now put None of unused attributes

        # create rectangular piece with a square hole
        piece_geom = PolygonExtended(box(0, 0, 10, 5) - box(2, 2, 4, 4))
        pieces = {'piece1': Piece(1, [90], piece_geom)}
        # IMPROVE: create a mock class instance for each init parameter (that uses a class)
        input_min_pb = SingleBinSizeBinPackingProblem(None, None, False, False, None, pieces, None, None)

        cached_values = ['nfps', 'ifps', 'free_space']
        cache_dict = dict(nfps={}, ifps={}, free_space=box(0, 0, 20, 10))
        cache = NestingCache(cp=None, cache_dict=cache_dict)
        placements = {PieceKey('piece1', 0): Placement((5, 0), 90)}
        # use value picker not cp here...
        node = NestingAssignmentSearchNode(placements, problem=input_min_pb, cached_values=cached_values, dynamic=False)
        node.cache = cache
        input_min_pb.update_cache_free_space(node)  # OLD

        # golden remaining free space
        golden_free_space = MultiPolygon([box(1, 2, 3, 4), box(5, 0, 20, 10)])

        # plot_geom_colors((cache.free_space, 'b'))
        # plot_geom_colors((cache.free_space, 'b'), (golden_free_space, 'y'))
        # plot_geom_colors((golden_free_space, 'y'))
        self.assertTrue(check_equal(cache.free_space, golden_free_space))


class MockNestingConstraintProblemTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # REFACTOR: too many ops for a unit test; mock that as much as you can
        # better, use pure unit functions as much as possible
        xml_filepath = get_full_path('benchmark', 'Personal', 'tangram', 'problem_tangram.xml')
        cls.expanded_problem = create_expanded_packing_problem_from_file(xml_filepath)
        cls.fixed_origin_single_bin_size_problem = create_fixed_origin_single_bin_size_problem(cls.expanded_problem)
        cls.value_picking_nsp = ValuePickingNestingSearchProblem(cls.fixed_origin_single_bin_size_problem)

        cls.two_piece_placements = {
            PieceKey('mediumTriangle', 0): Placement((0, 4), 45),
            PieceKey('smallTriangle', 0): Placement((0, 8), -135),
        }

    def test_get_piece_ids(self):
        golden_piece_ids = ['bigTriangle', 'mediumTriangle', 'smallTriangle', 'parallelogram', 'square']
        piece_ids = self.value_picking_nsp.cp.get_piece_ids()
        self.assertItemsEqual(golden_piece_ids, piece_ids)

    def test_get_remaining_piece_ids(self):
        placements = {
            PieceKey('bigTriangle', 0): Placement((0, 4), 45),
            PieceKey('mediumTriangle', 0): Placement((0, 4), 45),
            PieceKey('smallTriangle', 0): Placement((0, 8), -135),
            PieceKey('smallTriangle', 1): Placement((5, 8), 0),
            PieceKey('parallelogram', 0): Placement((6, 8), 0)
        }

        golden_remaining_piece_ids = ['bigTriangle', 'square']
        remaining_piece_ids = self.value_picking_nsp.cp.get_remaining_piece_ids(placements)
        self.assertItemsEqual(golden_remaining_piece_ids, remaining_piece_ids)

    def test_get_remaining_piece_instance_counter(self):
        golden_remaining_piece_instance_nb = {
            'bigTriangle': 2,
            'smallTriangle': 1,
            'parallelogram': 1,
            'square': 1
        }

        remaining_piece_instance_nb = self.value_picking_nsp.cp.get_remaining_piece_instance_counter(self.two_piece_placements)
        self.assertEqual(remaining_piece_instance_nb, golden_remaining_piece_instance_nb)

    def test_get_sum_area_remaining_pieces(self):
        self.assertEqual(self.value_picking_nsp.cp.get_sum_area_remaining_pieces(self.two_piece_placements), 52)

    def test_get_all_piece_instance_counter(self):
        golden_piece_instance_counter = {
            'bigTriangle': 2,
            'mediumTriangle': 1,
            'smallTriangle': 2,
            'parallelogram': 1,
            'square': 1
        }
        piece_instance_counter = self.value_picking_nsp.cp.get_all_piece_instance_counter()
        self.assertEqual(piece_instance_counter, golden_piece_instance_counter)

    def test_placed_piece_instance_counter(self):
        placements = {
            PieceKey('mediumTriangle', 0): Placement((0, 4), 45),
            PieceKey('smallTriangle', 0): Placement((0, 8), -135),
            PieceKey('smallTriangle', 1): Placement((4, 8), 0),
        }
        golden_placed_piece_instance_counter = {
            'mediumTriangle': 1,
            'smallTriangle': 2
        }
        piece_instance_counter = self.value_picking_nsp.cp.get_placed_piece_instance_counter(placements)
        self.assertEqual(piece_instance_counter, golden_placed_piece_instance_counter)

    # TODO: could add other tests on piece area sum

if __name__ == '__main__':
    unittest.main()
