from functools import partial
import logging
from ippy.config.config import config, RoundingLevel

from ippy.packmath.constraintsearch import depth_first_vp_search, greedy_vp_search, breadth_first_vp_search, \
    ordered_master_heuristic, depth_first_heuristic, beam_search, mixed_master_heuristic, \
    min_cost_heuristic, min_dynamic_cost_heuristic, value_picking_constraint_graph_search, null_heuristic
from ippy.packmath.constraintsearchviewer import NestingPlotCSEventHandler, EventConstraintSearchViewer, ConsoleCSEventHandler
from ippy.packmath.dynamiclength import get_min_length_for_non_empty_domain
from ippy.packmath.nestingconstraintsearch import ValuePickingNestingSearchProblem, minimum_domain_area_heuristic, \
    fpv_value_picker, rdv_value_picker, left_vertex_heuristic, final_length_upper_bound, \
    overlap_incentive, fast_layout_completion, least_domain_constraining_value_heuristic, minimum_domain_width_height_heuristic, minimum_domain_diameter_heuristic, \
    minimum_domain_boundary_length_heuristic, minimum_domain_area_length_heuristic, \
    minimum_domain_area_length_points_heuristic, analyze_free_space, \
    analyze_free_region_knapsack, min_bb_area_placement_picker, area_based_final_length_estimation, \
    current_usage_based_final_length_estimation, dominant_point_picker, minimize_convex_hull_waste, \
    min_dead_regions_area, overlap_balance, bigger_piece_bb_area
from ippy.packmath.problemfactory import create_fixed_origin_single_bin_size_problem, \
    create_expanded_packing_problem_from_file, create_fixed_origin_open_dimension_problem, \
    create_floating_origin_single_bin_size_problem, create_floating_origin_open_dimension_problem
from ippy.search.constraint import forward_check
from ippy.utils.datastructure import PriorityQueueWithRandomizedFunction, randomize, PriorityQueueWithFunction, \
    PriorityQueueWithFunctionRandomizedPop, BoundedPriorityQueueWithFunction
from ippy.utils.files import get_full_path
from ippy.view.plot import plot_solution_with_colors

root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)
# root_logger.setLevel(logging.WARNING)

__author__ = 'hs'

import unittest


class TwoTrianglesProblemTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        xml_filepath = get_full_path('benchmark', 'Personal', 'problem_bls_fails_nfpvs_succeeds.xml')
        expanded_problem = create_expanded_packing_problem_from_file(xml_filepath)

        cls.fixed_origin_single_bin_size_problem = create_fixed_origin_single_bin_size_problem(expanded_problem)
        # cls.floating_origin_single_bin_size_problem = create_floating_origin_single_bin_size_problem(expanded_problem)

        cls.value_picking_nsp = ValuePickingNestingSearchProblem(cls.fixed_origin_single_bin_size_problem)

    def test_fixed_origin_single_size_fpv_search(self):
        """Test backtracking graph search"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            # ConsoleCSEventHandler, NestingPlotCSEventHandler
        ])

        placements_node = depth_first_vp_search(self.value_picking_nsp, fpv_value_picker, viewer=viewer,
                                                cached_values=['nfps', 'contact_graph'])

        # plot_solution_with_colors(placements_node.assignment, {('piece0', 0): 'red', ('piece1', 0): 'blue'},
        #                           expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    def test_fixed_origin_single_size_rdv_search(self):
        """Test backtracking graph search"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            # ConsoleCSEventHandler, NestingPlotCSEventHandler
        ])

        placements_node = depth_first_vp_search(self.value_picking_nsp, rdv_value_picker, viewer=viewer,
                                                cached_values=['nfps', 'contact_graph'])

        # plot_solution_with_colors(placements_node.assignment, {('piece0', 0): 'red', ('piece1', 0): 'blue'},
        #                           expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    def test_fixed_origin_single_size_dpp(self):
        """Test backtracking graph search"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            # ConsoleCSEventHandler, NestingPlotCSEventHandler
        ])

        placements_node = depth_first_vp_search(self.value_picking_nsp, dominant_point_picker, viewer=viewer,
                                                cached_values=['nfps', 'contact_graph'])

        logging.debug(str(placements_node.cache.get('contact_graph')))

        # plot_solution_with_colors(placements_node.assignment, {('piece0', 0): 'red', ('piece1', 0): 'blue'},
        #                           expanded_problem=self.fixed_origin_single_bin_size_problem.data)



class HAndTwoRectanglesProblemTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        xml_filepath = get_full_path('benchmark', 'Personal', 'problem2_bls_fails_nfpvs_fails.xml')
        expanded_problem = create_expanded_packing_problem_from_file(xml_filepath)

        cls.fixed_origin_single_bin_size_problem = create_fixed_origin_single_bin_size_problem(expanded_problem)
        cls.floating_origin_single_bin_size_problem = create_floating_origin_single_bin_size_problem(expanded_problem)

        cls.value_picking_nsp = ValuePickingNestingSearchProblem(cls.fixed_origin_single_bin_size_problem)
        cls.value_picking_nsp_fo = ValuePickingNestingSearchProblem(cls.floating_origin_single_bin_size_problem)

    def test_fixed_origin_single_size_backtracking_graph_search(self):
        """Test backtracking graph search"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp,
                                             [ConsoleCSEventHandler, NestingPlotCSEventHandler])

        placements_node = depth_first_vp_search(self.value_picking_nsp, fpv_value_picker, viewer=viewer)

        if placements_node is not None:
            plot_solution_with_colors(placements_node.assignment,
                                      expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    def test_fixed_origin_single_size_backtracking_graph_search2(self):
        """Test backtracking graph search 2"""
        placements_node = depth_first_vp_search(self.value_picking_nsp, fpv_value_picker)

        if placements_node is not None:
            plot_solution_with_colors(placements_node.assignment, {('piece0', 0): 'red', ('piece1', 0): 'blue'},
                                  expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    def test_floating_origin_single_size_fpv(self):
        """Test floating-origin backtracking graph search"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_fo.cp,
                                             [ConsoleCSEventHandler, NestingPlotCSEventHandler])

        placements_node = depth_first_vp_search(self.value_picking_nsp_fo, fpv_value_picker, viewer=viewer)

        if placements_node is not None:
            plot_solution_with_colors(placements_node.assignment, {('piece0', 0): 'red', ('piece0', 1): 'pink', ('piece1', 0): 'blue'},
                                      expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    def test_floating_origin_single_size_min_bb_area(self):
        """Test floating-origin backtracking graph search"""
        placements_node = depth_first_vp_search(self.value_picking_nsp_fo, min_bb_area_placement_picker)

        if placements_node is not None:
            plot_solution_with_colors(placements_node.assignment, {('piece0', 0): 'red', ('piece0', 1): 'pink', ('piece1', 0): 'blue'},
                                      expanded_problem=self.fixed_origin_single_bin_size_problem.data)


    def test_floating_origin_single_size_min_bb_area_fc_cache(self):
        """Test floating-origin backtracking graph search"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp,
                                             [ConsoleCSEventHandler, NestingPlotCSEventHandler])

        placements_node = depth_first_vp_search(self.value_picking_nsp_fo, min_bb_area_placement_picker,
                                                filter_fun=forward_check, viewer=viewer, cached_values=['ifps', 'nfps'])

        # if placements_node is not None:
        #     plot_solution_with_colors(placements_node.assignment, {('piece0', 0): 'red', ('piece0', 1): 'pink', ('piece1', 0): 'blue'},
        #                               expanded_problem=self.fixed_origin_single_bin_size_problem.data)
        #

    def test_floating_origin_single_size_dpp(self):
        """Test floating-origin backtracking graph search"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp,
                                             [ConsoleCSEventHandler, NestingPlotCSEventHandler])

        placements_node = depth_first_vp_search(self.value_picking_nsp_fo, dominant_point_picker,
                                                filter_fun=forward_check, viewer=viewer,
                                                cached_values=['nfps', 'contact_graph'])


class TangramProblemTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # config.set_rounding_level(RoundingLevel.weak)
        config.set_rounding_level(RoundingLevel.raw)

        xml_filepath = get_full_path('benchmark', 'Personal', 'tangram', 'problem_tangram.xml')
        cls.expanded_problem = create_expanded_packing_problem_from_file(xml_filepath)

        cls.fixed_origin_single_bin_size_problem = create_fixed_origin_single_bin_size_problem(cls.expanded_problem)
        cls.floating_origin_single_bin_size_problem = create_floating_origin_single_bin_size_problem(cls.expanded_problem)

        cls.fixed_origin_open_dimension_problem = create_fixed_origin_open_dimension_problem(cls.expanded_problem)
        cls.floating_origin_open_dimension_problem = create_floating_origin_open_dimension_problem(cls.expanded_problem)

        cls.value_picking_nsp = ValuePickingNestingSearchProblem(cls.fixed_origin_single_bin_size_problem)
        cls.value_picking_nsp_fo = ValuePickingNestingSearchProblem(cls.floating_origin_single_bin_size_problem)
        cls.value_picking_nsp_odp = ValuePickingNestingSearchProblem(cls.fixed_origin_open_dimension_problem)
        cls.value_picking_nsp_odp_fo = ValuePickingNestingSearchProblem(cls.floating_origin_open_dimension_problem)

    def test_free(self):
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            ConsoleCSEventHandler,
            # NestingPlotCSEventHandler,
        ])

        custom_heuristic = null_heuristic

        heuristic = ordered_master_heuristic(
            depth_first_heuristic,
            minimum_domain_area_length_points_heuristic,
            # least_domain_constraining_value_heuristic,
            # custom_heuristic
        )

        fringe_factory = partial(PriorityQueueWithFunction, heuristic, viewer)

        # picker = rdv_value_picker
        picker = dominant_point_picker

        dynamic_cost_fun = get_min_length_for_non_empty_domain

        max_iterations = 2000

        cached_values = [
            'nfps',
            'ifps',
            'dynamic_ifps',
            'free_space',
            'dynamic_free_space',
            # 'contact_graph',
            'domains',
            'dynamic_domains'
        ]

        # advanced_filter = None
        advanced_filter = analyze_free_space  # dead region

        multistart = 1

        placements_node = value_picking_constraint_graph_search(
            self.value_picking_nsp,
            # self.value_picking_nsp_odp,
            fringe_factory,
            optimize=False,
            # optimize=True,
            value_picker=picker,
            dynamic_cost_fun=dynamic_cost_fun,
            viewer=viewer,
            max_iterations=max_iterations,
            cached_values=cached_values,
            cache_filter_fun=advanced_filter,
            multistart=multistart
        )

        logging.debug(str(viewer.stats))


    def test_fixed_origin_single_size_dfs_fc(self):
        """Test DFS"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp,
                                             [ConsoleCSEventHandler, NestingPlotCSEventHandler])

        # FIXME: parallelogram overlaps on medium triangle (before forward check was implemented)
        # either a bug on the NFP or the computation of the fit-polygon
        placements_node = depth_first_vp_search(self.value_picking_nsp, fpv_value_picker, filter_fun=forward_check,
                                                viewer=viewer, max_iterations=40)

        if placements_node is not None:
            plot_solution_with_colors(placements_node.assignment,
                                      expanded_problem=self.expanded_problem)

    # tries the big triangle many times in different positions, without progressing
    # plus many are symmetrical... really a waste of time
    def test_fixed_origin_single_size_bfs_fc(self):
        """Test BFS"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp,
                                             [ConsoleCSEventHandler, NestingPlotCSEventHandler])

        placements_node = breadth_first_vp_search(self.value_picking_nsp, fpv_value_picker, filter_fun=forward_check,
                                                  viewer=viewer, max_iterations=20)

        if placements_node is not None:
            plot_solution_with_colors(placements_node.assignment,
                                      expanded_problem=self.expanded_problem)

    def test_fixed_origin_single_size_fpv_greedy_deep_mrd_fc_no_cache(self):
        """Test greedy search with deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            # ConsoleCSEventHandler,
            # NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # don't use left here.... too slow. Even floating errors end stopping the program.
        # FIXME: odd shadow on domain in later iterations... Multipolygon is redundant so alpha
        # opacity is intensified? Need to cleanup...

        # 531 iterations, 52 nodes expanded, last fringe size 312, solution found (982 before... rounding?)
        placements_node = greedy_vp_search(self.value_picking_nsp, fpv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_heuristic,
                                                                              # left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=[])

        logging.debug(str(viewer.stats))

        # if placements_node is not None:
        #     plot_solution_with_colors(placements_node.assignment,
        #                               expanded_problem=self.expanded_problem)

    def test_fixed_origin_single_size_fpv_greedy_deep_mrd_fc(self):
        """Test greedy search with deeper and minimum remaining domain (MRD) first, and value picking from scratch"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # 881 iterations, 63 nodes expanded, last fringe size 465, solution found [with BUG: double boundary]
        # 531 iterations, 52 nodes expanded, last fringe size 312, solution found
        # 695 iterations, 86 nodes expanded, last fringe size 316, solution found (last result, with some NFP rounding)
        placements_node = greedy_vp_search(self.value_picking_nsp, fpv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_heuristic,
                                                                              # left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=[])

        logging.debug(str(viewer.stats))

        # if placements_node is not None:
        #     plot_solution_with_colors(placements_node.assignment,
        #                               expanded_problem=self.expanded_problem)

    def test_fixed_origin_single_size_rdv_greedy_deep_mrd1_fc(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # 534 iterations, 52 nodes expanded, last fringe size 312, solution found
        # 695 iterations, 86 nodes expanded, last fringe size 316, solution found (after NFP rounding)$
        # TODO: add randomness to make results more stable!

        # using pre-computation domain is clearly faster but it takes more time to find the solution...
        # is it only due to undeterministic issues (decrementing domain and computing IFP - union of NFP each
        # time gives different vertex orders, hence different exploration order)
        # or is there a real problem? (wrong domain, many useless vertices appear due to multiplicity of operations,
        # etc.)
        # FIND OUT: for instance, sort the vertices of FPV / domain by XY each time to eliminate undeterministic
        # issues; in practice values are put in a dict so more effective to change fringe sorting (heuristic)
        # solve the problem of shadows (redundant vertices I guess) in case get_unique_coords did not

        # -> Done, but much slower. Why? --> left is too deterministic; randomness helps actually
        # TODO: enfore randomness via multisearch and undeterministic stuff! allow multi run and return
        # the average stats for the viewer (prepare a master viewer!)
        placements_node = greedy_vp_search(self.value_picking_nsp, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_heuristic,
                                                                              # left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=[])

        logging.debug(str(viewer.stats))

        # if placements_node is not None:
        #     plot_solution_with_colors(placements_node.assignment,
        #                               expanded_problem=self.expanded_problem)


    def test_fixed_origin_single_size_rdv_greedy_deep_mrd1_fc_overlap(self):
        """
        Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) 1 first
        and maximum BB overlap

        """
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # 660 iterations, 83 nodes expanded, max fringe size 316, solution found (after NFP rounding)

        placements_node = greedy_vp_search(self.value_picking_nsp, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_heuristic,
                                                                              overlap_incentive
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=[])

        logging.debug(str(viewer.stats))


    def test_fixed_origin_single_size_rdv_greedy_deep_mrd2_fc(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) 2 first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # 705 iterations, 87 nodes expanded, max fringe size 317, solution found
        placements_node = greedy_vp_search(self.value_picking_nsp, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_length_heuristic,
                                                                              # left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=[])

        logging.debug(str(viewer.stats))

    def test_fixed_origin_single_size_rdv_greedy_deep_mrd3_fc(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) 3 first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # 665 iterations, 83 nodes expanded, max fringe size 313, solution found
        placements_node = greedy_vp_search(self.value_picking_nsp, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_length_points_heuristic,
                                                                              overlap_incentive
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=[])

        logging.debug(str(viewer.stats))

    def test_fixed_origin_single_size_rdv_greedy_deep_mrd3_fc_lcv(self):
        """
        Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) 3 first
        Least Constraining Value

        """
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # 650 iterations, 73 nodes expanded, max fringe size 315, solution found
        placements_node = greedy_vp_search(self.value_picking_nsp, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_length_points_heuristic,
                                                                              least_domain_constraining_value_heuristic,
                                                                              # overlap_incentive
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=[])

        logging.debug(str(viewer.stats))

    def test_fixed_origin_single_size_rdv_greedy_deep_mrd4_fc(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # 705 iterations, 87 nodes expanded, max fringe size 317, solution found
        placements_node = greedy_vp_search(self.value_picking_nsp, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_width_height_heuristic,
                                                                              # left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=[])

        logging.debug(str(viewer.stats))

    def test_fixed_origin_single_size_rdv_greedy_deep_mrd5_fc(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # 695 iterations, 86 nodes expanded, max fringe size 316, solution found
        placements_node = greedy_vp_search(self.value_picking_nsp, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_diameter_heuristic,
                                                                              # left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=[])

        logging.debug(str(viewer.stats))

    def test_fixed_origin_single_size_rdv_greedy_deep_mrd6_fc(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) 6 first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # 665 iterations, 83 nodes expanded, max fringe size 313, solution found
        placements_node = greedy_vp_search(self.value_picking_nsp, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_boundary_length_heuristic,
                                                                              # left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=[])

        logging.debug(str(viewer.stats))

    def test_fixed_origin_single_size_rdv_greedy_deep_mrd_fc_free_region(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            partial(ConsoleCSEventHandler, min_loglevel=20),
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # 549 iterations, 49 nodes expanded, max fringe size 316, solution found
        # forward check empty: 468, filter2 (dead region): 31

        placements_node = greedy_vp_search(self.value_picking_nsp, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_heuristic,
                                                                              # left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=['free_space'],
                                           cache_filter_fun=analyze_free_space)

        logging.debug(str(viewer.stats))

    def test_fixed_origin_single_size_rdv_greedy_deep_mrd_fc_free_region_random(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            partial(ConsoleCSEventHandler, min_loglevel=10),
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # without random on LCV
        # 398 iterations, 36 nodes expanded, last fringe size 315, solution found
        # FC: 334, filter 2: 27

        heuristic = ordered_master_heuristic(
            depth_first_heuristic,
            minimum_domain_area_length_points_heuristic,
            randomize(least_domain_constraining_value_heuristic, 0)
        )
        fringe_factory = partial(PriorityQueueWithFunction, heuristic, viewer)
        placements_node = value_picking_constraint_graph_search(self.value_picking_nsp, fringe_factory, optimize=False,
                                                                value_picker=rdv_value_picker, viewer=viewer,
                                                                max_iterations=100, cached_values=['free_space'],
                                                                cache_filter_fun=analyze_free_space)

        logging.debug(str(viewer.stats))

    def test_fixed_origin_single_size_rdv_greedy_deep_mrd_fc_free_region_randompop(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            partial(ConsoleCSEventHandler, min_loglevel=10),
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # without random on LCV
        # 398 iterations, 36 nodes expanded, last fringe size 315, solution found
        # FC: 334, filter 2: 27

        heuristic = ordered_master_heuristic(
            depth_first_heuristic,
            minimum_domain_area_length_points_heuristic,
            # least_domain_constraining_value_heuristic
        )
        random_span = 2
        fringe_factory = partial(PriorityQueueWithFunctionRandomizedPop, heuristic, random_span, viewer)
        placements_node = value_picking_constraint_graph_search(self.value_picking_nsp, fringe_factory, optimize=False,
                                                                value_picker=rdv_value_picker, viewer=viewer,
                                                                max_iterations=10, cached_values=['free_space'],
                                                                cache_filter_fun=analyze_free_space)

        logging.debug(str(viewer.stats))

    def test_fixed_origin_single_size_rdv_greedy_deep_mrd_fc_knapsack(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            partial(ConsoleCSEventHandler, min_loglevel=20),
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # 517 iterations, 47 nodes expanded, max fringe size 316, solution found
        # forward check empty: 446, filter2 (knapsack): 23 < dead region?? or more efficient so removed branches?!

        placements_node = greedy_vp_search(self.value_picking_nsp, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_heuristic,
                                                                              # left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=True,
                                           cache_filter_fun=analyze_free_region_knapsack)

        logging.debug(str(viewer.stats))

    def test_floating_origin_single_size_rdv_greedy_deep_mrd_fc_free_region(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_fo.cp, [
            partial(ConsoleCSEventHandler, min_loglevel=20),
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # 550 iterations, 49 nodes expanded, last fringe size 293, solution found
        # FC: 469, filter 2: 31

        placements_node = greedy_vp_search(self.value_picking_nsp_fo, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_heuristic,
                                                                              # left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200,
                                           cached_values=['free_space', 'contact_graph'],
                                           cache_filter_fun=analyze_free_space)

        logging.debug(str(viewer.stats))
        logging.debug(str(placements_node.cache.get('contact_graph')))

    def test_floating_origin_single_size_rdv_greedy_deep_mrd_fc_free_region_random(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_fo.cp, [
            partial(ConsoleCSEventHandler, min_loglevel=20),
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # ? iterations, ? nodes expanded, last fringe size ?, solution found
        # FC: ?, filter 2: ?
        heuristic = ordered_master_heuristic(depth_first_heuristic,
                                             minimum_domain_area_heuristic,
                                             # left_vertex_heuristic
                                             )
        fringe_factory = partial(PriorityQueueWithRandomizedFunction, heuristic, 1e-5, viewer)
        placements_node = value_picking_constraint_graph_search(self.value_picking_nsp_fo, fringe_factory,
                                                                optimize=False, value_picker=rdv_value_picker,
                                                                viewer=viewer, max_iterations=1200,
                                                                cached_values=['free_space'],
                                                                cache_filter_fun=analyze_free_space)

        logging.debug(str(viewer.stats))

    def test_fixed_origin_open_dimension_rdv_greedy_deep_mdc_mrd_fc(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_odp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # ? iterations, ? nodes expanded, last fringe size ?, solution found?
        placements_node = greedy_vp_search(self.value_picking_nsp_odp, rdv_value_picker, optimize=True,
                                           heuristic=ordered_master_heuristic(
                                               min_dynamic_cost_heuristic,
                                               depth_first_heuristic,
                                               minimum_domain_area_heuristic,
                                               min_cost_heuristic
                                           ), filter_fun=forward_check, viewer=viewer, max_iterations=500,
                                           cached_values=[])

        logging.debug(str(viewer.stats))

        # if placements_node is not None:
        #     plot_solution_with_colors(placements_node.assignment,
        #                               expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    def test_fixed_origin_open_dimension_rdv_greedy_deep_mdc_mrd_lcv_fc(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_odp.cp, [
            partial(ConsoleCSEventHandler, min_loglevel=20),
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # ? iterations, ? nodes expanded, last fringe size ?, solution found?
        placements_node = greedy_vp_search(self.value_picking_nsp_odp, rdv_value_picker, optimize=True,
                                           heuristic=ordered_master_heuristic(
                                               # min_dynamic_cost_heuristic,
                                               depth_first_heuristic,
                                               minimum_domain_area_length_points_heuristic,
                                               least_domain_constraining_value_heuristic,
                                               min_cost_heuristic
                                           ), filter_fun=forward_check, viewer=viewer, max_iterations=410,
                                           cached_values=[])

        logging.debug(str(viewer.stats))

    def test_fixed_origin_open_dimension_rdv_greedy_deep_mrd_fc_mixed(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_odp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # ? iterations, ? nodes expanded, last fringe size ?, solution found?
        placements_node = greedy_vp_search(self.value_picking_nsp_odp, rdv_value_picker, optimize=True,
                                           heuristic=mixed_master_heuristic(
                                               (depth_first_heuristic,
                                                minimum_domain_area_heuristic,
                                                min_cost_heuristic),
                                               (0.5, 0.3, 0.5)
                                           ), filter_fun=forward_check, viewer=viewer, max_iterations=500,
                                           cached_values=[])

        logging.debug(str(viewer.stats))

    def test_fixed_origin_open_dimension_rdv_greedy_deep_mrd_fc_global_eval(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_odp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # ? iterations, ? nodes expanded, last fringe size ?, solution found?
        placements_node = greedy_vp_search(self.value_picking_nsp_odp, rdv_value_picker, optimize=True,
                                           heuristic=ordered_master_heuristic(
                                               depth_first_heuristic,
                                               minimum_domain_area_heuristic,
                                               # BETTER: use min BB
                                               fast_layout_completion(rdv_value_picker, min_cost_heuristic)
                                           ), filter_fun=forward_check, viewer=viewer, max_iterations=500,
                                           cached_values=[])

        logging.debug(str(viewer.stats))

        # if placements_node is not None:
        #     plot_solution_with_colors(placements_node.assignment,
        #                               expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    def test_fixed_origin_open_dimension_rdv_greedy_deep_mrd_fc_area_based_final_len(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_odp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # ? iterations, ? nodes expanded, last fringe size ?, solution found?
        placements_node = greedy_vp_search(self.value_picking_nsp_odp, rdv_value_picker, optimize=True,
                                           heuristic=ordered_master_heuristic(
                                               depth_first_heuristic,
                                               minimum_domain_area_length_points_heuristic,
                                               area_based_final_length_estimation,
                                           ),
                                           filter_fun=forward_check,
                                           viewer=viewer,
                                           max_iterations=500,
                                           use_cache=True,
                                           cache_filter_fun=analyze_free_space
                                           )

        logging.debug(str(viewer.stats))

    def test_fixed_origin_open_dimension_rdv_greedy_deep_mrd_fc_convex_hull_usage_based_final_len(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_odp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # ? iterations, ? nodes expanded, last fringe size ?, solution found?
        placements_node = greedy_vp_search(self.value_picking_nsp_odp, rdv_value_picker, optimize=True,
                                           heuristic=ordered_master_heuristic(
                                               depth_first_heuristic,
                                               minimum_domain_area_length_points_heuristic,
                                               current_usage_based_final_length_estimation,
                                           ),
                                           filter_fun=forward_check,
                                           viewer=viewer,
                                           max_iterations=500,
                                           use_cache=True,
                                           cache_filter_fun=analyze_free_space
                                           )

        logging.debug(str(viewer.stats))

    def test_beam_search(self):
        """Test beam search on COP with global evaluation: min length and local evaluation: normalized length incentive"""
        alpha = 3
        beta = 2

        viewer = EventConstraintSearchViewer(self.value_picking_nsp_odp.cp, [
            ConsoleCSEventHandler,
            partial(NestingPlotCSEventHandler, nb_axes=beta),  # display layout with X ms between each step
        ])

        model_nodes = beam_search(self.value_picking_nsp_odp, rdv_value_picker, alpha, beta,
                                  fast_layout_completion(rdv_value_picker, overlap_incentive),
                                  minimum_domain_area_heuristic, filter_fun=forward_check, viewer=viewer,
                                  max_iterations=10, cached_values=[])

        # for model_node in model_nodes:
        #     plot_solution_with_colors(model_node.assignment,
        #                               expanded_problem=self.expanded_problem,
        #                               floating_width=True)

    def test_beam_search_floating_origin(self):
        """Test beam search on COP with global evaluation: min length and local evaluation: normalized length incentive"""
        alpha = 3
        beta = 2

        viewer = EventConstraintSearchViewer(self.value_picking_nsp_odp_fo.cp, [
            ConsoleCSEventHandler,
            partial(NestingPlotCSEventHandler, nb_axes=beta),  # display layout with X ms between each step
        ])

        model_nodes = beam_search(self.value_picking_nsp_odp_fo, rdv_value_picker, alpha, beta,
                                  fast_layout_completion(rdv_value_picker, overlap_incentive),
                                  minimum_domain_area_heuristic, filter_fun=forward_check, viewer=viewer,
                                  max_iterations=10, cached_values=[])

        # for model_node in model_nodes:
        #     plot_solution_with_colors(model_node.assignment,
        #                               expanded_problem=self.expanded_problem,
        #                               floating_width=True)


class TangramMissingSquareProblemTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        xml_filepath = get_full_path('benchmark', 'Personal', 'tangram', 'problem_tangram_missing_square.xml')
        cls.expanded_problem = create_expanded_packing_problem_from_file(xml_filepath)

        cls.fixed_origin_single_bin_size_problem = create_fixed_origin_single_bin_size_problem(cls.expanded_problem)
        cls.floating_origin_single_bin_size_problem = create_floating_origin_single_bin_size_problem(cls.expanded_problem)

        cls.fixed_origin_open_dimension_problem = create_fixed_origin_open_dimension_problem(cls.expanded_problem)
        cls.floating_origin_open_dimension_problem = create_floating_origin_open_dimension_problem(cls.expanded_problem)

        cls.value_picking_nsp = ValuePickingNestingSearchProblem(cls.fixed_origin_single_bin_size_problem)
        cls.value_picking_nsp_fo = ValuePickingNestingSearchProblem(cls.floating_origin_single_bin_size_problem)
        cls.value_picking_nsp_odp = ValuePickingNestingSearchProblem(cls.fixed_origin_open_dimension_problem)
        cls.value_picking_nsp_odp_fo = ValuePickingNestingSearchProblem(cls.floating_origin_open_dimension_problem)

    def test_fixed_origin_single_size_rdv_greedy_deep_mrd_fc_free_region(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            partial(ConsoleCSEventHandler, min_loglevel=20),
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # _ iterations, _ nodes expanded, max fringe size _, solution found
        # forward check empty: 468, filter2 (dead region): 31

        placements_node = greedy_vp_search(self.value_picking_nsp, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_heuristic,
                                                                              # left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=['free_space'],
                                           cache_filter_fun=analyze_free_region_knapsack)

        logging.debug(str(viewer.stats))

    def test_fixed_origin_open_dim_rdv_greedy_deep_mrd_fc_free_region(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_odp.cp, [
            partial(ConsoleCSEventHandler, min_loglevel=20),
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        # _ iterations, _ nodes expanded, max fringe size _, solution found
        # forward check empty: 468, filter2 (dead region): 31

        placements_node = greedy_vp_search(self.value_picking_nsp_odp, rdv_value_picker, optimize=True,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_heuristic,
                                                                              # left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=200, cached_values=['free_space'],
                                           cache_filter_fun=analyze_free_space)

        logging.debug(str(viewer.stats))


class DigheProblemTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        xml_filepath = get_full_path('benchmark', 'dighe', 'dighe1.xml')
        cls.expanded_problem = create_expanded_packing_problem_from_file(xml_filepath)

        cls.fixed_origin_single_bin_size_problem = create_fixed_origin_single_bin_size_problem(cls.expanded_problem)
        # cls.floating_origin_single_bin_size_problem = create_floating_origin_single_bin_size_problem(expanded_problem)

        cls.fixed_origin_open_dimension_problem = create_fixed_origin_open_dimension_problem(cls.expanded_problem)

        cls.value_picking_nsp = ValuePickingNestingSearchProblem(cls.fixed_origin_single_bin_size_problem)
        cls.value_picking_nsp_odp = ValuePickingNestingSearchProblem(cls.fixed_origin_open_dimension_problem)

    def test_fixed_origin_single_size_fpv_greedy_deep_mrd_fc(self):
        """Test greedy search with deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        placements_node = greedy_vp_search(self.value_picking_nsp, fpv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_heuristic,
                                                                              # left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200)

        logging.debug(str(viewer.stats))

        if placements_node is not None:
            plot_solution_with_colors(placements_node.assignment,
                                      expanded_problem=self.expanded_problem)

    def test_fixed_origin_single_size_rdv_greedy_deep_mrd_fc(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            # ConsoleCSEventHandler,
            # NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        placements_node = greedy_vp_search(self.value_picking_nsp, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_heuristic,
                                                                              left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=[])

        logging.debug(str(viewer.stats))

        # if placements_node is not None:
        #     plot_solution_with_colors(placements_node.assignment,
        #                               expanded_problem=self.expanded_problem)


    def test_fixed_origin_single_size_rdv_greedy_deep_mrd_fc_knapsack(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        placements_node = greedy_vp_search(self.value_picking_nsp, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_heuristic,
                                                                              left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=['free_space'],
                                           cache_filter_fun=analyze_free_region_knapsack)

        logging.debug(str(viewer.stats))

        # if placements_node is not None:
        #     plot_solution_with_colors(placements_node.assignment,
        #                               expanded_problem=self.expanded_problem)

    def test_fixed_origin_open_dimension_rdv_greedy_deep_mrd_fc_free_region(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_odp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        placements_node = greedy_vp_search(self.value_picking_nsp_odp, rdv_value_picker, optimize=True,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_length_points_heuristic,
                                                                              least_domain_constraining_value_heuristic,
                                                                              min_cost_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=['free_space'],
                                           cache_filter_fun=analyze_free_region_knapsack)  # TEST (still no solution, 0 filter2)

        logging.debug(str(viewer.stats))

        # if placements_node is not None:
        #     plot_solution_with_colors(placements_node.assignment,
        #                               expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    def test_fixed_origin_open_dimension_rdv_greedy_deep_mrd_fc_knapsack(self):
        """Test greedy search with Remaining Domain Vertex, deeper and minimum remaining domain (MRD) first"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_odp.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,  # display layout with X ms between each step
        ])

        placements_node = greedy_vp_search(self.value_picking_nsp_odp, rdv_value_picker, optimize=True,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_length_points_heuristic,
                                                                              least_domain_constraining_value_heuristic,
                                                                              min_cost_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=1200, cached_values=['free_space'],
                                           cache_filter_fun=analyze_free_space)

        logging.debug(str(viewer.stats))

        # if placements_node is not None:
        #     plot_solution_with_colors(placements_node.assignment,
        #                               expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    def test_beam_search(self):
        """Test beam search with global evaluation: and local evaluation:"""
        alpha = 3
        beta = 2

        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            ConsoleCSEventHandler,
            partial(NestingPlotCSEventHandler, nb_axes=beta),
        ])

        model_nodes = beam_search(self.value_picking_nsp, rdv_value_picker, 3, 2, final_length_upper_bound,
                                  overlap_incentive, viewer=viewer, max_iterations=10, cached_values=[],
                                  cache_filter_fun=None,
                                  dynamic=False)

        # fast_layout_completion(rdv_value_picker, overlap_incentive),
        # minimum_domain_area_heuristic,


        for model_node in model_nodes:
            plot_solution_with_colors(model_node.assignment,
                                      expanded_problem=self.expanded_problem)


class ShirtsProblemTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        xml_filepath = get_full_path('benchmark', 'improved_xml', 'shirts_improved.xml')
        cls.expanded_problem = create_expanded_packing_problem_from_file(xml_filepath)

        # only consider ODP
        cls.fixed_origin_open_dimension_problem = create_fixed_origin_open_dimension_problem(cls.expanded_problem)

        cls.value_picking_nsp_od = ValuePickingNestingSearchProblem(cls.fixed_origin_open_dimension_problem)

    def test_fixed_origin_single_size_dfs_fc(self):
        """Test DFS"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_od.cp,
                                             [ConsoleCSEventHandler, NestingPlotCSEventHandler])

        # FIXME: parallelogram overlaps on medium triangle (before forward check was implemented)
        # either a bug on the NFP or the computation of the fit-polygon
        placements_node = depth_first_vp_search(self.value_picking_nsp_od, fpv_value_picker, filter_fun=forward_check,
                                                viewer=viewer, max_iterations=40, cached_values=[])

        if placements_node is not None:
            plot_solution_with_colors(placements_node.assignment,
                                      expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    def test_fixed_origin_open_dimension_rdv_greedy_deep_mrd_fc(self):
        """Test backtracking graph search"""
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_od.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,
        ])

        placements_node = greedy_vp_search(self.value_picking_nsp_od, rdv_value_picker,
                                           heuristic=ordered_master_heuristic(depth_first_heuristic,
                                                                              minimum_domain_area_heuristic,
                                                                              # left_vertex_heuristic
                                                                              ), filter_fun=forward_check,
                                           viewer=viewer, max_iterations=10, cached_values=[])

        if placements_node is not None:
            plot_solution_with_colors(placements_node.assignment,
                                      expanded_problem=self.fixed_origin_open_dimension_problem.data)

    def test_beam_search(self):
        """Test beam search with global evaluation: and local evaluation:"""
        alpha = 3
        beta = 2

        viewer = EventConstraintSearchViewer(self.value_picking_nsp_od.cp, [
            ConsoleCSEventHandler,
            partial(NestingPlotCSEventHandler, nb_axes=beta),
        ])

        # fast_layout_completion(rdv_value_picker, overlap_incentive),
        # minimum_domain_area_heuristic,

        model_nodes = beam_search(self.value_picking_nsp_od, rdv_value_picker, alpha, beta,
                                  final_length_upper_bound,
                                  overlap_incentive,
                                  filter_fun=forward_check,
                                  viewer=viewer,
                                  max_iterations=10,
                                  cached_values=['ifps', 'dynamic_ifps', 'domains', 'dynamic_domains'],
                                  cache_filter_fun=None,
                                  dynamic=True)

        for model_node in model_nodes:
            plot_solution_with_colors(model_node.assignment,
                                      expanded_problem=self.expanded_problem)

    def test_free(self):
        viewer = EventConstraintSearchViewer(self.value_picking_nsp_od.cp, [
            ConsoleCSEventHandler,
            NestingPlotCSEventHandler,
        ])

        custom_heuristic = minimize_convex_hull_waste

        heuristic = ordered_master_heuristic(
            depth_first_heuristic,
            minimum_domain_area_length_points_heuristic,
            custom_heuristic
        )

        fringe_factory = partial(PriorityQueueWithFunction, heuristic, viewer)

        picker = rdv_value_picker

        cached_values = []

        advanced_filter = None

        placements_node = value_picking_constraint_graph_search(self.value_picking_nsp_od, fringe_factory, optimize=True,
                                                                value_picker=picker, viewer=viewer, max_iterations=3,
                                                                cached_values=cached_values,
                                                                cache_filter_fun=advanced_filter)

class FreeProblemTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # config.set_rounding_level(RoundingLevel.weak)
        config.set_rounding_level(RoundingLevel.raw)

        # xml_filepath = get_full_path('benchmark', 'Personal', 'tangram', 'problem_tangram.xml')
        # xml_filepath = get_full_path('benchmark', 'improved_xml', 'dagli_cleared.xml')
        # xml_filepath = get_full_path('benchmark', 'improved_xml', 'dighe1_improved.xml')
        # xml_filepath = get_full_path('benchmark', 'improved_xml', 'dighe2_improved.xml')
        # xml_filepath = get_full_path('benchmark', 'improved_xml', 'fu_cleared.xml')
        # xml_filepath = get_full_path('benchmark', 'improved_xml', 'jakobs2_cleared.xml')
        # xml_filepath = get_full_path('benchmark', 'improved_xml', 'blaz_cleared.xml')
        xml_filepath = get_full_path('benchmark', 'improved_xml', 'shirts_cleared.xml')
        cls.expanded_problem = create_expanded_packing_problem_from_file(xml_filepath)

        cls.fixed_origin_single_bin_size_problem = create_fixed_origin_single_bin_size_problem(cls.expanded_problem)
        cls.floating_origin_single_bin_size_problem = create_floating_origin_single_bin_size_problem(cls.expanded_problem)

        cls.fixed_origin_open_dimension_problem = create_fixed_origin_open_dimension_problem(cls.expanded_problem)
        cls.floating_origin_open_dimension_problem = create_floating_origin_open_dimension_problem(cls.expanded_problem)

        cls.value_picking_nsp = ValuePickingNestingSearchProblem(cls.fixed_origin_single_bin_size_problem)
        cls.value_picking_nsp_fo = ValuePickingNestingSearchProblem(cls.floating_origin_single_bin_size_problem)
        cls.value_picking_nsp_odp = ValuePickingNestingSearchProblem(cls.fixed_origin_open_dimension_problem)
        cls.value_picking_nsp_odp_fo = ValuePickingNestingSearchProblem(cls.floating_origin_open_dimension_problem)

    def test_free(self):
        viewer = EventConstraintSearchViewer(self.value_picking_nsp.cp, [
            ConsoleCSEventHandler,
            # NestingPlotCSEventHandler,
        ])

        custom_heuristic = null_heuristic

        heuristic = ordered_master_heuristic(
            depth_first_heuristic,
            # minimum_domain_area_length_points_heuristic,
            bigger_piece_bb_area,
            # min_dead_regions_area,
            # minimize_convex_hull_waste,
            # overlap_incentive,
            # overlap_balance,
            # least_domain_constraining_value_heuristic,
            # custom_heuristic
        )

        # fringe_factory = partial(PriorityQueueWithFunction, heuristic, viewer)
        fringe_factory = partial(BoundedPriorityQueueWithFunction, 500, heuristic, viewer)
        # fringe_factory = partial(PriorityQueueWithFunctionRandomizedPop, heuristic, 10, viewer)

        # picker = rdv_value_picker
        picker = min_bb_area_placement_picker
        # picker = dominant_point_picker

        dynamic_cost_fun = get_min_length_for_non_empty_domain

        max_iterations = 100

        cached_values = [
            'nfps',
            'ifps',
            'dynamic_ifps',
            'free_space',
            'dynamic_free_space',
            # 'contact_graph',
            'domains',
            'dynamic_domains'
        ]

        # advanced_filter = None
        advanced_filter = analyze_free_space  # dead region

        multistart = 1

        placements_node = value_picking_constraint_graph_search(
            # self.value_picking_nsp,
            self.value_picking_nsp_odp,
            fringe_factory,
            # optimize=False,
            optimize=True,
            value_picker=picker,
            dynamic_cost_fun=dynamic_cost_fun,
            viewer=viewer,
            max_iterations=max_iterations,
            cached_values=cached_values,
            cache_filter_fun=advanced_filter,
            multistart=multistart
        )

        logging.debug(str(viewer.stats))


if __name__ == '__main__':
    unittest.main()
