from matplotlib.collections import PatchCollection
from shapely.affinity import translate
from shapely.geometry import Point, Polygon
from ippy.utils.shapelyextension import get_placed_lot
from ippy.view.plot import add_geom_patches, plot_solution_with_colors
from matplotlib import patches

from matplotlib import animation
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.pyplot import gca

# fast debug plot
from ippy.view.plot import plot_geom_colors
from ippy.view.plot import plot_polygon_colors
from ippy.view.plot import plot_linestring_colors

# for pick_values
lot = self.cp.lot
get_placed_lot(lot, placements)
plot_geom_colors()

# direct layout plot
plot_solution_with_colors(placements, expanded_problem=self.cp.data)

plot_geom_colors((Point(0,0), 'b'))

fig = plt.figure(figsize=(10, 10))
axis = fig.add_subplot(111)

p = Polygon([(0, 0), (10, 0), (10, 10), (0, 10)], holes=[[(1, 1), (2, 1), (2, 2), (1, 2)]])

# add_geom_patches(axis, (p, {'facecolor': 'blue'}))
# geom_params_tuples= (p, {'facecolor': 'blue'})

# patch_list = []
patch = patches.Polygon(list(p.exterior.coords), lw=2, alpha=0.8, facecolor='blue')

polys = [Polygon(coords) for coords in [((0, 0), (10, 0), (10, 10)), ((20, 10), (10, 30), (-5, 10))]]
patch_list = [patches.Polygon(list(poly.exterior.coords), lw=2, alpha=0.8, facecolor='blue') for poly in polys]

# line
# line, = axis.plot()

min_x = p.bounds[0]
min_y = p.bounds[1]
max_x = p.bounds[2]
max_y = p.bounds[3]

# axis.add_patch(patch)

# provide xy limits to show all polygons + a small margin at a uniform scale
bigger_axis_range = max(max_x - min_x, max_y - min_y)  # uniform scale with bigger range as square side
axis.set_xlim(min_x - 1, min_x + bigger_axis_range + 1)
axis.set_ylim(min_y - 1, min_y + bigger_axis_range + 1)

def animate(i):
    # patch.set_alpha(i / 10.)
    axis.add_patch(patch_list[i])
    # print i

anim = animation.FuncAnimation(fig, animate, frames=2, interval=1000)
plt.show()

# plot partial assignment
assignment, new_assignment = {}, {}
cp = None
cp = self.cp
from ippy.view.plot import plot_geom_colors
from ippy.view.plot import plot_solution_with_colors
assignment = placements
assignment = node.assignment
cp = node.problem.cp
plot_solution_with_colors(assignment, expanded_problem=cp.data)
plot_solution_with_colors(new_assignment, expanded_problem=cp.data)
plot_solution_with_colors(next_assignment, expanded_problem=cp.data)

# IFP - NFP debug
plot_geom_colors((domains[tail][tail_orientation], 'b'), (tr_nfp, 'r'))

# NFP: Cuninghame-Green debug
plot_geom_colors((rotated_polygon1, 'b'), (rotated_polygon2, 'r'), (Point(polygon1_rightmost_bottom_vertex), 'y'), (Point(polygon2_leftmost_top_vertex), 'purple'))
