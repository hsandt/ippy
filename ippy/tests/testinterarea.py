from shapely.geometry import Polygon, MultiPolygon
from ippy.packmath.interarea import interarea
from ippy.packmath.problem import Placement
from ippy.parser.convert import to_shapely_object_list
from ippy.utils.files import get_full_path
from ippy.utils.shapelyextension import get_translated_polygons

__author__ = 'hs'

import unittest


class ConvexConvexCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'sample1b.wkt')
        piece_geom1, piece_geom2 = to_shapely_object_list(wkt_filepath)
        # cls.expanded_lot = {'polygon1': polygon_list[0], 'polygon2': polygon_list[1]}  # old format
        cls.expanded_lot = {('piece1', 0): piece_geom1, ('piece2', 0): piece_geom2}

    def test_interarea_positive(self):
        self.assertGreaterEqual(interarea(self.expanded_lot[('piece1', 0)], self.expanded_lot[('piece2', 0)]), False)

    def test_interarea_no_intersection(self):
        tr_expanded_lot = get_translated_polygons(self.expanded_lot, {
            ('piece1', 0): Placement((0, 0)),
            ('piece2', 0): Placement((10, 0))
        })

        # for golden area, build inter space manually and let Shapely
        # find out area
        interspace = Polygon([(0, 0), (10, 0), (10, 9), (14, 16), (0, 10), (10, 10), (5, 2)])
        self.assertAlmostEqual(interarea(tr_expanded_lot[('piece1', 0)], tr_expanded_lot[('piece2', 0)]), interspace.area)

    def test_interarea_intersection(self):
        tr_expanded_lot = get_translated_polygons(self.expanded_lot, {
            ('piece1', 0): Placement((0, 0)),
            ('piece2', 0): Placement((5, 0))
        })

        # for golden area, build inter space manually and let Shapely
        # find out area
        interspace = MultiPolygon([(((0, 0), (0, 2), (5, 2)), None), (((0, 10), (5 + 4./7, 10), (9, 16)), None)])
        self.assertAlmostEqual(interarea(tr_expanded_lot[('piece1', 0)], tr_expanded_lot[('piece2', 0)]), interspace.area)

    @classmethod
    def tearDownClass(cls):
        pass


class NonConvexNonConvexCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'sample1a.wkt')
        piece_geom1, piece_geom2 = to_shapely_object_list(wkt_filepath)
        cls.expanded_lot = {('piece1', 0): piece_geom1, ('piece2', 0): piece_geom2}

    def test_interarea_positive(self):
        self.assertGreaterEqual(interarea(self.expanded_lot[('piece1', 0)], self.expanded_lot[('piece2', 0)]), False)

    def test_interarea_no_intersection(self):
        tr_expanded_lot = get_translated_polygons(self.expanded_lot, {
            ('piece1', 0): Placement((10, 0)),
            ('piece2', 0): Placement((0, 0))
        })

        # for golden area, build inter space manually and let Shapely
        # find out area
        self.assertAlmostEqual(interarea(tr_expanded_lot[('piece1', 0)], tr_expanded_lot[('piece2', 0)]), 20)

    def test_interarea_intersection(self):
        tr_expanded_lot = get_translated_polygons(self.expanded_lot, {
            ('piece1', 0): Placement((8, 0)),
            ('piece2', 0): Placement((0, 0))
        })

        # for golden area, build inter space manually and let Shapely
        # find out area
        interspace = MultiPolygon([(((10, 10), (18, 10), (10, 19)), None), (((10, 0), (10, 4), (18, 10)), None)])
        self.assertAlmostEqual(interarea(tr_expanded_lot[('piece1', 0)], tr_expanded_lot[('piece1', 0)]), interspace.area)

    @classmethod
    def tearDownClass(cls):
        pass


if __name__ == '__main__':
    unittest.main()
