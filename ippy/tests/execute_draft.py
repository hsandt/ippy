import sys
from shapely.geometry import Polygon, Point
from ippy.utils.shapelyextension import PolygonWithBoundary

sys.path.append('/Users/hs/Projects/AI/Ippy')

from ippy.packmath.solver import NestingProblemSolver, NFPVerticesStrategy, BottomLeftStrategy
from ippy.parser.convert import xml_to_problem
from ippy.utils.files import get_full_path
from ippy.view.plot import plot_solution_with_colors, plot_geom_colors


# nfpv_solver = NestingProblemSolver(NFPVerticesStrategy())
#
# # xml_filepath = get_full_path('benchmark', 'Personal', 'xml_sample1.xml')
# xml_filepath = get_full_path('benchmark', 'Personal', 'problem_bls_fails_nfpvs_succeeds.xml')
# problem = xml_to_problem(xml_filepath)
#
# layout = nfpv_solver.search(problem)
#


# xml_filepath = get_full_path('benchmark', 'improved_xml', 'poly1a_cleared.xml')
# problem = xml_to_problem(xml_filepath)
#
# """Test BL strategy with variable width"""
# solver = NestingProblemSolver(BottomLeftStrategy())
# best_layout, upper_width = solver.search_variable_width(problem)
# plot_solution_with_colors(placements=upper_width, piece_instance_tuple_color_dict=best_layout.placements,
#                           container_width=upper_width, problem=problem)  # DEBUG
# print(best_layout.is_complete, best_layout.is_valid)

p = Polygon([(0, 0), (10, 0), (10, 10), (0, 10)], holes=[[(1, 1), (2, 1), (2, 2), (1, 2)],
                                                         [(3, 3), (5, 3), (5, 5), (3, 5)]])
pwb = PolygonWithBoundary(p, Point(-1, -1), True)
plot_geom_colors((pwb, 'r'))
