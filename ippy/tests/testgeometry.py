from math import sqrt
import numpy as np
from numpy.testing import assert_allclose
from shapely.geometry import LinearRing, LineString, MultiPoint

from ippy.utils.mathextension import round7
from ippy.utils.geometry import get_equation_params, get_signed_distance, Position, \
    get_points_orientation, Orientation, get_edge_relative_position, RelativePosition, check_if_point_inside_segment, \
    get_translation_bounds_to_fit_floating_container, get_floating_container_limit_bounds, get_vector_relative_position
from ippy.utils.shapelyextension import get_extreme_vertex_index_value, get_extreme_vertices
from ippy.utils.unittestextension import assertSequenceKeyEqual

import unittest

__author__ = 'huulong'


class GeometryTestCase(unittest.TestCase):

    # def test_to_vector_list(self):
    #     """Test converting a list of coordinate couples to a list of Vectors"""
    #     coord_tuple_list = [(1, 2), (0, 3), (4, -1)]
    #     golden_vector_list = [(1, 2), (0, 3), (4, -1)]
    #     self.assertSequenceEqual(to_vector_list(coord_tuple_list), golden_vector_list)
    #
    def test_get_equation_params(self):
        """Test getting the parameters of a half-plane equation from a segment"""
        segment = ((10, 4), (8, 9))
        golden_params = (5 / sqrt(29), 2 / sqrt(29), -sqrt(116))
        assertSequenceKeyEqual(self, get_equation_params(segment), golden_params, key=round7)

    def test_get_signed_distance(self):
        """Test computation of the signed distance to a line, provided a positive direction"""
        point = (11, 16)
        a, b, c = 5 / sqrt(29), 2 / sqrt(29), -sqrt(116)
        golden_distance = sqrt(29)
        self.assertAlmostEqual(get_signed_distance(point, a, b, c), golden_distance)

    def test_get_extreme_vertex_left(self):
        """Test computation of the leftmost vertex of a linear ring"""
        linearring = LinearRing([(-1, -2), (0, -1), (3, -5), (2, 8), (-2, 4)])
        idx, extreme_vertex = get_extreme_vertex_index_value(linearring, Position.left)
        self.assertEqual(idx, 4)
        assert_allclose(extreme_vertex, (-2, 4))

    def test_get_extreme_vertex_up(self):
        """Test computation of the topmost vertex of a linear ring"""
        linearring = LinearRing([(-1, -2), (0, -1), (3, -5), (2, 8), (-2, 4)])
        idx, extreme_vertex = get_extreme_vertex_index_value(linearring, Position.top)
        self.assertEqual(idx, 3)
        assert_allclose(extreme_vertex, (2, 8))

    def test_get_extreme_vertex_right_bottom(self):
        """Test computation of the rightmost - bottom vertex of a linear ring"""
        linearring = LinearRing([(3, 0), (11, 0), (10, 5), (3, 7), (0, 7)])
        idx, extreme_vertex = get_extreme_vertex_index_value(linearring, Position.right, Position.bottom)
        self.assertEqual(idx, 1)
        assert_allclose(extreme_vertex, (11, 0))

    def test_get_extreme_vertex_right_bottom2(self):
        """Test computation of the rightmost - bottom vertex of a linear ring"""
        linearring = LinearRing([(0.0, 0.0), (10.0, 2.0), (8.0, 5.0), (4.0, 4.0), (0.0, 0.0)])
        idx, extreme_vertex = get_extreme_vertex_index_value(linearring, Position.right, Position.bottom)
        self.assertEqual(idx, 1)
        assert_allclose(extreme_vertex, (10., 2.))

    def test_get_extreme_vertex_top_left(self):
        """Test computation of the topmost - left vertex of a linear ring"""
        linearring = LinearRing([(3, 0), (11, 0), (10, 5), (3, 7), (0, 7)])
        idx, extreme_vertex = get_extreme_vertex_index_value(linearring, Position.top, Position.left)
        self.assertEqual(idx, 4)
        assert_allclose(extreme_vertex, (0, 7))

    def test_get_extreme_vertices_top(self):
        """Test computation of the topmost vertices of a linear ring"""
        linearring = LinearRing([(3, 0), (11, 0), (10, 5), (3, 7), (0, 7)])
        top_vertices = get_extreme_vertices(linearring, Position.top)
        self.assertItemsEqual([(3, 7), (0, 7)], top_vertices)

    def test_get_extreme_vertices_right(self):
        """Test computation of the rightmost vertices of a collection of a linestring and a multipoint"""
        geom = LineString([(7, -1), (4, 2), (-5, 1), (7, 2), (7, -5), (6, -2)]) | MultiPoint([(0, 0), (7, 5)])
        right_vertices = get_extreme_vertices(geom, Position.right)
        self.assertItemsEqual([(7, -1), (7, 2), (7, -5), (7, 5)], right_vertices)

    def test_ccw_counter_clockwise(self):
        """Test CCW test when counter-clockwise"""
        self.assertEqual(get_points_orientation((0, 0), (1, 0), (0, 1)), Orientation.counter_clockwise)

    def test_ccw_counter_clockwise_threshold(self):
        """Test CCW test when counter-clockwise"""
        self.assertEqual(get_points_orientation((0, 0), (1, 0), (0, 1), threshold=1e-4), Orientation.counter_clockwise)

    def test_ccw_clockwise(self):
        """Test CCW test when clockwise"""
        self.assertEqual(get_points_orientation((0, 0), (1, 0), (0, -1)), Orientation.clockwise)

    def test_ccw_clockwise_threshold(self):
        """Test CCW test when clockwise"""
        self.assertEqual(get_points_orientation((0, 0), (1, 0), (0, -1), threshold=1e-4), Orientation.clockwise)

    def test_ccw_aligned(self):
        """Test CCW test when aligned"""
        self.assertEqual(get_points_orientation((0, 0), (1, 0), (-1, 0)), Orientation.aligned)

    def test_ccw_aligned_threshold(self):
        """Test CCW test when aligned"""
        self.assertEqual(get_points_orientation((0, 0), (1, 0), (-1, 1e-4), threshold=1e-4), Orientation.aligned)

    def test_get_edge_relative_position_left(self):
        """Test test_get_edge_relative_position: left case"""
        self.assertEqual(get_edge_relative_position([(0, 0), (0, 1)], [(-1, 0), (-2, -2)]), RelativePosition.left)

    def test_get_edge_relative_position_right(self):
        """Test test_get_edge_relative_position: right case"""
        self.assertEqual(get_edge_relative_position([(0, 0), (0, 1)], [(0, 0), (1, -1)]), RelativePosition.right)

    def test_get_edge_relative_position_right2(self):
        """Test test_get_edge_relative_position: right case, other edge's send vertex touches reference edge"""
        self.assertEqual(get_edge_relative_position([(0, 0), (0, 1)], [(1, 1), (0, 0)]), RelativePosition.right)

    def test_get_edge_relative_position_parallel(self):
        """Test test_get_edge_relative_position: parallel case"""
        self.assertEqual(get_edge_relative_position([(0, 0), (0, 1)], [(0, 0), (0, 0.5)]), RelativePosition.parallel)

    def test_get_edge_relative_position_parallel_threshold(self):
        """Test test_get_edge_relative_position: parallel case"""
        self.assertEqual(get_edge_relative_position([(0, 0), (0, 1)], [(0, 0), (-5e-5, 0.5)], threshold=1e-4), RelativePosition.parallel)

    def test_get_edge_relative_position_crossed(self):
        """Test test_get_edge_relative_position: crossed case"""
        self.assertEqual(get_edge_relative_position([(0, 0), (0, 1)], [(1, 2), (-1, 2)]), RelativePosition.crossed)

    def test_get_vector_relative_position_left(self):
        self.assertEqual(get_vector_relative_position((1, 0), (0, 1)), RelativePosition.left)

    def test_get_vector_relative_position_left_threshold(self):
        self.assertEqual(get_vector_relative_position((1, 0), (0, 1), threshold=1e-4), RelativePosition.left)

    def test_get_vector_relative_position_right(self):
        self.assertEqual(get_vector_relative_position((0, 1), (1, 0)), RelativePosition.right)

    def test_get_vector_relative_position_parallel(self):
        self.assertEqual(get_vector_relative_position((0, 1), (1e-4, 1), threshold=1e-4), RelativePosition.parallel)

    def test_get_vector_relative_position_never_crossed(self):
        self.assertNotEqual(get_vector_relative_position((0, 1), (1, 0)), RelativePosition.crossed)

    def test_check_if_point_inside_segment(self):
        """Test check_if_point_inside_segment: True"""
        self.assertTrue(check_if_point_inside_segment((0, 0), [(0, 0), (1, 0)]))

    def test_check_if_point_outside_segment(self):
        """Test check_if_point_inside_segment: False"""
        self.assertFalse(check_if_point_inside_segment((0.5, 0.5), [(0, 0), (1, 0)]))

    def test_get_floating_container_limit_bounds(self):
        """Test get_floating_container_limit_bounds"""
        a_bounds = (-1, -2, 1, 2)
        container_size = (10, 10)
        self.assertEqual(get_floating_container_limit_bounds(a_bounds, container_size), (-9, -8, 9, 8))

    # test on deprecated function
    @unittest.skip('Test on deprecated function: get_translation_bounds_to_fit_floating_container')
    def test_get_translation_bounds_to_fit_floating_container(self):
        """Test get_translation_bounds_to_fit_floating_container"""
        a_bounds = (-1, -2, 1, 2)
        b_bounds = (0, 0, 1, 1)
        container_size = (10, 10)
        np.testing.assert_allclose(get_translation_bounds_to_fit_floating_container(a_bounds, b_bounds, container_size), (-9, -8, 8, 7))


if __name__ == '__main__':
    unittest.main()
