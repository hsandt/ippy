from math import sqrt
from numpy import float_ as fl
from numpy.testing import assert_allclose
from shapely.affinity import translate
from shapely.geometry import GeometryCollection, Polygon, Point, box, LineString, MultiLineString
from ippy.packmath.nfp import nfp_cuninghame_green, get_potential_translation_vector, nfp_burke, \
    get_potential_translation_vector_from_edges, find_touchers, Toucher, get_edge_progressions, check_feasible_range, \
    filter_can_move, nfp_cuninghame_green_with_convex_decomposition, get_ifp_with_rectangular_container
from ippy.parser.convert import to_shapely_object_list
from ippy.utils.decorator import deprecated
from ippy.utils.files import get_full_path
from ippy.utils.geometry import Progression
from ippy.utils.shapelyextension import ComparableBaseGeometry, PolygonWithBoundary, round_coords
from ippy.utils.unittestextension import assertKeyEqual
from ippy.view.plot import add_geom_patches, PlotOnFailureContext, plot_linestring_colors, plot_polygon_colors, \
    plot_geom_colors

__author__ = 'hs'

import unittest

import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches

class NfpTestShapeCase(unittest.TestCase):
    """Test cases with simple polygon shapes, some convex and some non-convex"""

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'sample0.wkt')
        polygon_list = to_shapely_object_list(wkt_filepath)
        cls.polygon1, cls.polygon2, cls.polygon3, cls.polygon4, cls.polygon5, cls.polygon6, cls.polygon7, cls.polygon8 = polygon_list
        # TODO: VISUAL DEBUG

    def test_get_ifp_with_rectangular_container_case_empty_ifp(self):
        """Test IFP computation for a rectangular container for case: empty IFP"""
        # Caution: empty GeometryCollections are *not* equal in shapely. Use is_empty
        self.assertTrue(get_ifp_with_rectangular_container(self.polygon5, 5, 10).is_empty)

    def test_get_ifp_with_rectangular_container_case_point_ifp(self):
        """Test IFP computation for a rectangular container for case: IFP is a Point"""
        self.assertEqual(get_ifp_with_rectangular_container(self.polygon5, 10, 10), PolygonWithBoundary(boundary=Point(0, 0)))

    def test_get_ifp_with_rectangular_container_case_linestring_ifp(self):
        """Test IFP computation for a rectangular container for case: IFP is a LineString (in the Y direction)"""
        self.assertEqual(get_ifp_with_rectangular_container(self.polygon5, 10, 20), PolygonWithBoundary(boundary=LineString([(0, 0), (0, 10)])))

    def test_get_ifp_with_rectangular_container_case_rectangle_ifp(self):
        """Test IFP computation for a rectangular container for case: IFP is a rectangular Polygon"""
        self.assertEqual(get_ifp_with_rectangular_container(self.polygon5, 20, 30), PolygonWithBoundary(box(0, 0, 10, 20)))

    def test_get_ifp_with_rectangular_container_square1_r45(self):
        """Test IFP computation for a rectangular container for case: IFP is a rectangular Polygon"""
        golden_ifp = PolygonWithBoundary(box(10*sqrt(2)/2, 0, 20 - 10*sqrt(2)/2,  20 - 10*sqrt(2)))
        ifp = get_ifp_with_rectangular_container(self.polygon1, 20, 20, orientation=45)
        plot_geom_colors((golden_ifp, 'y'), (ifp, 'b'))
        self.assertEqual(ifp, golden_ifp)

    def test_nfp_cuninghame_squares1(self):
        """Test NFP computation by edge direction angle sort (Cuninghame-Green) for square-square case"""
        # add vertices on straight path is needed
        golden_nfp = PolygonWithBoundary(Polygon([(10, -10), (10, 10), (-10, 10), (-10, -10)]))
        nfp = nfp_cuninghame_green(self.polygon1, self.polygon1)
        self.assertEqual(nfp, golden_nfp)

    def test_nfp_cuninghame_square1_rotated_square2(self):
        """Test NFP computation with Cuninghame-Green method for one square - one square rotated of 45 degrees"""
        # add vertices on straight path is needed
        golden_nfp = PolygonWithBoundary(Polygon([(10, -5), (10, 5), (5, 10), (-5, 10), (-10, 5), (-10, -5), (-5, -10), (5, -10)]))
        nfp = nfp_cuninghame_green(self.polygon1, self.polygon2)
        self.assertEqual(nfp, golden_nfp)

    def test_nfp_cuninghame_boomerang5_trapezoid4(self):
        """Test NFP computation with Cuninghame-Green method for a trapezoidal and a boomerang-like shape"""
        # add vertices on straight path is needed
        golden_nfp = PolygonWithBoundary(Polygon([(0, -10), (10, -10), (10, -5), (5, 0), (1, 1), (0, 5), (-5, 10), (-10, 10), (-10, 0)]))
        nfp = nfp_cuninghame_green_with_convex_decomposition(self.polygon5, self.polygon4)

        with PlotOnFailureContext(
                (golden_nfp, 'yellow'),
                (nfp, 'blue'),
                (self.polygon5, 'green'),
                (self.polygon4, 'yellow'),
                ):
            self.assertEqual(nfp, golden_nfp)

    # ADD test for tangram with various rotations
    def test_nfp_square2_triangle3_orientation_45(self):
        """Test NFP computation with Cuninghame-Green method for square VS triangle rotated of 45 degrees"""
        golden_nfp = PolygonWithBoundary(Polygon(
            [(10 + 5. / sqrt(2), 5 - 5. / sqrt(2)), (5, 10), (-5. / sqrt(2), 5 - 5. / sqrt(2)),
             (5 - 5. / sqrt(2), -5. / sqrt(2)), (5 + 5. / sqrt(2), -5. / sqrt(2))]
        ))
        nfp = nfp_cuninghame_green_with_convex_decomposition(self.polygon2, self.polygon3, 0, 45)
        # plot_geom_colors((golden_nfp, 'y'), (nfp, 'b'))
        # that test is a success! you can plot to check (only small floating error)
        # TODO: complete round_coords and use check_equal_round of the test won't pass because
        # of a coord floating error on 1e-15
        # (or just use almost_equals on closed interior and boundary manually if you're sure about coord order)
        self.assertEqual(nfp, golden_nfp)

    def test_find_touchers_double_touch(self):
        """Test if find_touchers return the correct list of Touchers
        for a double touch between a non-convex and a convex polygon"""
        current_polygon1 = self.polygon5
        current_polygon2 = translate(self.polygon4, 1, 1)  # double touch
        touchers = find_touchers(current_polygon1, current_polygon2)
        golden_touchers = [
            Toucher(((10, 0), (2, 2)), ((6, 1), (11, 1)), Point(6, 1), (-4, 1)),
            Toucher(((10, 0), (2, 2)), ((1, 6), (6, 1)), Point(6, 1), (-4, 1)),
            Toucher(((2, 2), (0, 10)), ((1, 11), (1, 6)), Point(1, 6), (-1, 4)),
            Toucher(((2, 2), (0, 10)), ((1, 6), (6, 1)), Point(1, 6), (-1, 4))
            ]
        self.assertEqual(touchers, golden_touchers)

    def test_filter_can_move(self):
        """Test if only feasible touchers are returned"""
        current_polygon1 = self.polygon5
        current_polygon2 = translate(self.polygon4, 1, 1)  # double touch
        touchers = [
            Toucher(((10, 0), (2, 2)), ((6, 1), (11, 1)), Point(6, 1), (-4, 1)),
            Toucher(((10, 0), (2, 2)), ((1, 6), (6, 1)), Point(6, 1), (-4, 1)),
            Toucher(((2, 2), (0, 10)), ((1, 11), (1, 6)), Point(1, 6), (-1, 4)),
            Toucher(((2, 2), (0, 10)), ((1, 6), (6, 1)), Point(1, 6), (-1, 4))
        ]
        feasible_touchers = [
            Toucher(((2, 2), (0, 10)), ((1, 11), (1, 6)), Point(1, 6), (-1, 4)),
            Toucher(((2, 2), (0, 10)), ((1, 6), (6, 1)), Point(1, 6), (-1, 4))
        ]
        self.assertEqual(filter_can_move(current_polygon1, current_polygon2, touchers), feasible_touchers)


class ConvexPolygonCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'wkt_sample1.wkt')
        polygon_list = to_shapely_object_list(wkt_filepath)

        cls.container, cls.polygon1, cls.polygon2, _, _ = polygon_list

    def test_nfp_cuninghame_green_reflexive(self):
        """Test NFP computation by edge direction angle sort (Cuninghame-Green) for reflective case"""
        golden_nfp = PolygonWithBoundary(Polygon([(8., 5.), (-2., 3.), (-6., 2.), (-10., -2.), (-8., -5.), (2., -3.), (6., -2.), (10., 2.)]))
        nfp = nfp_cuninghame_green(self.polygon1, self.polygon1)
        self.assertEqual(nfp, golden_nfp)

    @unittest.skip('Do not test Burke algo until completed')
    def test_nfp_burke_convex(self):
        """Test NFP computation by orbiting from different start positions (Burke) for convex case"""
        golden_nfp = PolygonWithBoundary(Polygon([(8., 5.), (-2., 3.), (-6., 2.), (-10., -2.), (-8., -5.), (2., -3.), (6., -2.), (10., 2.)]))
        nfp = nfp_burke(self.polygon1, self.polygon1)
        self.assertEqual(nfp, golden_nfp)

    def test_get_edge_progressions_start_start(self):
        """Test if the intersection point provided is at the start of both edges."""
        self.assertTupleEqual(get_edge_progressions([(0, 0), (1, 0)], [(0, 0), (0, 1)], (0, 0)),
                              (Progression.start, Progression.start))

    def test_get_edge_progressions_start_middle(self):
        """Test if the intersection point provided is at the start and middle of the edges."""
        self.assertTupleEqual(get_edge_progressions([(0, 0), (1, 0)], [(0, -0.5), (0, 0.5)], (0, 0)),
                              (Progression.start, Progression.middle))

    def test_get_edge_progressions_start_end(self):
        """Test if the intersection point provided is at the start and end of the edges."""
        self.assertTupleEqual(get_edge_progressions([(0, 0), (1, 0)], [(0, 1), (0, 0)], (0, 0)),
                              (Progression.start, Progression.end))

    def test_get_edge_progressions_end_end(self):
            """Test if the intersection point provided is at the end and end of the edges."""
            self.assertTupleEqual(get_edge_progressions([(1, 0), (0, 0)], [(0, 1), (0, 0)], (0, 0)),
                                  (Progression.end, Progression.end))

    def test_get_edge_progressions_point_outside_edge(self):
        """Test if get_edge_progressions raise a ValueError if the point is outside one of the edges."""
        self.assertRaises(ValueError, get_edge_progressions, [(0, 0), (1, 0)], [(0, 0), (0, 1)], (1, 0))

    # NEW function

    def test_get_potential_translation_vector_from_edges_case10(self):
        """Test potential translation vector computation from edges: MS. -> remaining portion of stationary edge"""
        golden_potential_translation_vector = (0.7, 0)
        potential_translation_vector = get_potential_translation_vector_from_edges([(0, 0), (1, 0)], [(0.3, 0), (0.3, 1)])
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    def test_get_potential_translation_vector_from_edges_case9(self):
        """Test potential translation vector computation from edges: SM. -> remaining portion of orbiting edge"""
        golden_potential_translation_vector = (0, -0.3)
        potential_translation_vector = get_potential_translation_vector_from_edges([(0, 0.7), (1, 0.7)], [(0, 0), (0, 1)])
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    def test_get_potential_translation_vector_from_edges_case11(self):
        """Test potential translation vector computation from edges: ME. -> remaining portion of stationary edge"""
        golden_potential_translation_vector = (0.7, 0)
        potential_translation_vector = get_potential_translation_vector_from_edges([(0, 0), (1, 0)], [(0.3, -1), (0.3, 0)])
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    def test_get_potential_translation_vector_from_edges_case12(self):
        """Test potential translation vector computation from edges: EM. -> remaining portion of orbiting edge"""
        golden_potential_translation_vector = (0, 0.3)
        potential_translation_vector = get_potential_translation_vector_from_edges([(0, 0), (1, 0)], [(1, 0.7), (1, -0.3)])
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    def test_get_potential_translation_vector_from_edges_case14(self):
        """Test potential translation vector computation from edges: * v*P -> remaining portion of orbiting edge"""
        golden_potential_translation_vector = (0, 0)
        potential_translation_vector = get_potential_translation_vector_from_edges([(0, 0), (1, 0)], [(1, 0), (2, 0)])
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    def test_get_potential_translation_vector_from_edges_case1(self):
        """Test potential translation vector computation from edges: SSL -> full orbiting edge 1"""
        golden_potential_translation_vector = (0, -1)
        potential_translation_vector = get_potential_translation_vector_from_edges([(0, 0), (1, 0)], [(0, 0), (0, 1)])
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    def test_get_potential_translation_vector_from_edges_case2(self):
        """Test potential translation vector computation from edges: SSR -> full stationary edge 1"""
        golden_potential_translation_vector = (1, 0)
        potential_translation_vector = get_potential_translation_vector_from_edges([(0, 0), (1, 0)], [(0, 0), (0, -1)])
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    def test_get_potential_translation_vector_from_edges_case3(self):
        """Test potential translation vector computation from edges: SEL -> none"""
        golden_potential_translation_vector = (0, 0)
        potential_translation_vector = get_potential_translation_vector_from_edges([(0, 0), (1, 0)], [(0, 1), (0, 0)])
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    def test_get_potential_translation_vector_from_edges_case4(self):
        """Test potential translation vector computation from edges: SER -> full stationary edge 2"""
        golden_potential_translation_vector = (1, 0)
        potential_translation_vector = get_potential_translation_vector_from_edges([(0, 0), (1, 0)], [(0, -1), (0, 0)])
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    def test_get_potential_translation_vector_from_edges_case5(self):
        """Test potential translation vector computation from edges: ESL -> none"""
        golden_potential_translation_vector = (0, 0)
        potential_translation_vector = get_potential_translation_vector_from_edges([(0, 0), (1, 0)], [(1, 0), (1, 1)])
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    def test_get_potential_translation_vector_from_edges_case6(self):
        """Test potential translation vector computation from edges: ESR -> full orbiting edge 2"""
        golden_potential_translation_vector = (0, 1)
        potential_translation_vector = get_potential_translation_vector_from_edges([(0, 0), (1, 0)], [(1, 0), (1, -1)])
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    def test_get_potential_translation_vector_from_edges_case7(self):
        """Test potential translation vector computation from edges: EE* -> none"""
        golden_potential_translation_vector = (0, 0)
        potential_translation_vector = get_potential_translation_vector_from_edges([(0, 0), (1, 0)], [(1, -1), (1, 0)])
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    def test_get_potential_translation_vector_from_edges_case8(self):
        """Test potential translation vector computation from edges: **P -> any vector (opposite senses)"""
        golden_potential_translation_vector = (2, 0)  # function has been enhanced for maximum translation distance
        potential_translation_vector = get_potential_translation_vector_from_edges([(0, 0), (1, 0)], [(0, 0), (-1, 0)])
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    def test_get_potential_translation_vector_from_edges_empty_intersection_error(self):
        """Test potential translation vector computation from edges: ValueError if empty intersection"""
        self.assertRaises(ValueError, get_potential_translation_vector_from_edges, [(0, 0), (1, 0)], [(0.3, 0.1), (0.3, 1)])

    def test_get_potential_translation_vector_from_edges_cross_intersection_error(self):
        """Test potential translation vector computation from edges: ValueError if cross intersection"""
        self.assertRaises(ValueError, get_potential_translation_vector_from_edges, [(0, 0), (1, 0)], [(0.3, -0.1), (0.3, 1)])

    # OLD function

    @unittest.skip('get_potential_translation_vector is deprecated')
    def test_get_potential_translation_vector_stationary_middle(self):
        """Test potential translation vector computation: remaining portion of stationary edge"""
        golden_potential_translation_vector = (0.7, 0)
        potential_translation_vector = get_potential_translation_vector(fl([1, 0]), fl([0, 1]), 0.3, 1)
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    @unittest.skip('get_potential_translation_vector is deprecated')
    def test_get_potential_translation_vector_orbiting_middle(self):
        """Test potential translation vector computation: remaining portion of orbiting edge"""
        golden_potential_translation_vector = (0, -0.3)
        potential_translation_vector = get_potential_translation_vector(fl([1, 0]), fl([0, 1]), 0, 0.7)
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    @unittest.skip('get_potential_translation_vector is deprecated')
    def test_get_potential_translation_vector_case1(self):
        """Test potential translation vector computation: SSL -> full orbiting edge 1"""
        golden_potential_translation_vector = (0, -1)
        potential_translation_vector = get_potential_translation_vector(fl([1, 0]), fl([0, 1]), 0, 0)
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    @unittest.skip('get_potential_translation_vector is deprecated')
    def test_get_potential_translation_vector_case2(self):
        """Test potential translation vector computation: SSR -> full stationary edge 1"""
        golden_potential_translation_vector = (1, 0)
        potential_translation_vector = get_potential_translation_vector(fl([1, 0]), fl([0, -1]), 0, 0)
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    @unittest.skip('get_potential_translation_vector is deprecated')
    def test_get_potential_translation_vector_case3(self):
        """Test potential translation vector computation: SEL -> none"""
        potential_translation_vector = get_potential_translation_vector(fl([1, 0]), fl([0, -1]), 0, 1)
        self.assertEqual(potential_translation_vector, None)

    @unittest.skip('get_potential_translation_vector is deprecated')
    def test_get_potential_translation_vector_case4(self):
        """Test potential translation vector computation: SER -> full stationary edge 2"""
        golden_potential_translation_vector = (1, 0)
        potential_translation_vector = get_potential_translation_vector(fl([1, 0]), fl([0, 1]), 0, 1)
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    @unittest.skip('get_potential_translation_vector is deprecated')
    def test_get_potential_translation_vector_case5(self):
        """Test potential translation vector computation: ESL -> none"""
        potential_translation_vector = get_potential_translation_vector(fl([1, 0]), fl([0, 1]), 1, 0)
        self.assertEqual(potential_translation_vector, None)

    @unittest.skip('get_potential_translation_vector is deprecated')
    def test_get_potential_translation_vector_case6(self):
        """Test potential translation vector computation: ESR -> full orbiting edge 2"""
        golden_potential_translation_vector = (0, 1)
        potential_translation_vector = get_potential_translation_vector(fl([1, 0]), fl([0, -1]), 1, 0)
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    @unittest.skip('get_potential_translation_vector is deprecated')
    def test_get_potential_translation_vector_case7(self):
        """Test potential translation vector computation: EE* -> none"""
        potential_translation_vector = get_potential_translation_vector(fl([1, 0]), fl([0, 1]), 1, 1)
        self.assertEqual(potential_translation_vector, None)

    @unittest.skip('get_potential_translation_vector is deprecated')
    def test_get_potential_translation_vector_case8(self):
        """Test potential translation vector computation: **P -> any vector (opposite senses)"""
        golden_potential_translation_vector = (1, 0)
        potential_translation_vector = get_potential_translation_vector(fl([1, 0]), fl([-1, 0]), 0, 0)
        assert_allclose(potential_translation_vector, golden_potential_translation_vector)

    # END OLD FUNCTION
    
    def test_find_touchers_convex_reflexive(self):
        """Test if find_touchers return the correct list of Touchers for 2 identical convex polygons"""
        touchers = find_touchers(self.polygon1, translate(self.polygon1, 10, 2))  # rightmost matches leftmost
        golden_touchers = [
            Toucher(((0, 0), (10, 2)), ((10, 2), (20, 4)), Point(10, 2), (0, 0)),
            Toucher(((0, 0), (10, 2)), ((14, 6), (10, 2)), Point(10, 2), (0, 0)),
            Toucher(((10, 2), (8, 5)), ((10, 2), (20, 4)), Point(10, 2), (-2, 3)),
            Toucher(((10, 2), (8, 5)), ((14, 6), (10, 2)), Point(10, 2), (-2, 3)),
            ]
        self.assertEqual(touchers, golden_touchers)

    def test_check_feasible_range_cases_7_28(self):
        """Test check_feasible_range: line intersection -> right of static, left of moving"""
        toucher = Toucher(((0, 0), (1, 0)), ((0.5, 0), (-0.5, 0)))
        self.assertTupleEqual(check_feasible_range(toucher), (True, False))

    def test_check_feasible_range_case_27(self):
        """Test check_feasible_range: static touched in the middle (supposedly from the right) -> left of static"""
        toucher = Toucher(((0, 0), (1, 0)), ((1, 1), (0.5, 0)))
        self.assertTupleEqual(check_feasible_range(toucher), (True, None))

    def test_check_feasible_range_case_29(self):
        """Test check_feasible_range: moving touched in the middle (supposedly from the right) -> left of moving"""
        toucher = Toucher(((0, 0), (1, 0)), ((1, -0.5), (1, 0.5)))
        self.assertTupleEqual(check_feasible_range(toucher), (None, False))

    def test_check_feasible_range_case_classes_2_3_4(self):
        """Test check_feasible_range: edges touching at vertex, moving on left of static (re-orientated)
        -> left of static, left of moving"""
        toucher = Toucher(((0, 0), (1, 0)), ((-1, 1), (0, 0)), Point(0, 0))
        self.assertTupleEqual(check_feasible_range(toucher), (False, False))

    def test_check_feasible_range_case_classes_5_6(self):
        """Test check_feasible_range: edges touching at vertex, moving on right of static (re-orientated)
        -> right of static, right of moving"""
        toucher = Toucher(((1, 0), (0, 0)), ((0, 0), (0, -1)), Point(0, 0))
        self.assertTupleEqual(check_feasible_range(toucher), (True, True))


    def test_filter_can_move(self):
        """Test if only feasible touchers are returned"""
        current_polygon1 = self.polygon1
        current_polygon2 = translate(self.polygon1, 10, 2)  # rightmost matches leftmost
        touchers = [
            Toucher(((0, 0), (10, 2)), ((10, 2), (20, 4)), Point(10, 2), (0, 0)),
            Toucher(((0, 0), (10, 2)), ((14, 6), (10, 2)), Point(10, 2), (0, 0)),
            Toucher(((10, 2), (8, 5)), ((10, 2), (20, 4)), Point(10, 2), (-2, 3)),
            Toucher(((10, 2), (8, 5)), ((14, 6), (10, 2)), Point(10, 2), (-2, 3)),
            ]
        feasible_touchers = [
            Toucher(((10, 2), (8, 5)), ((10, 2), (20, 4)), Point(10, 2), (-2, 3)),
            Toucher(((10, 2), (8, 5)), ((14, 6), (10, 2)), Point(10, 2), (-2, 3)),

        ]
        self.assertEqual(filter_can_move(current_polygon1, current_polygon2, touchers), feasible_touchers)

    @classmethod
    def tearDownClass(cls):
        del cls.polygon1
        del cls.polygon2


class NfpWithLinesCase(unittest.TestCase):
    """Test cases with non-convex polygons so that the NFP contains LineStrings"""

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'sample3.wkt')
        polygon_list = to_shapely_object_list(wkt_filepath)
        cls.polygon1, cls.polygon2, cls.polygon3 = polygon_list[:3]

    def test_nfp_cuninghame_green_with_convex_decomposition_rectangle2_h3(self):
        """
        Test NFP computation with Cuninghame-Green method with convex decomposition
        for a rectangle polygon and a H-shaped polygon that fits the former inside its concavities.

        """
        golden_nfp = PolygonWithBoundary(Polygon([(8, -6), (8, 2), (-14, 2), (-14, -6)]),
                                         MultiLineString([
                                             LineString([(8, -6), (8, 2), (-14, 2), (-14, -6), (8, -6)]),
                                             LineString([(8, -2), (2, -2)]),
                                             LineString([(-14, -2), (-8, -2)])
                                         ]))
        nfp = nfp_cuninghame_green_with_convex_decomposition(self.polygon2, self.polygon3)

        # plot_polygon_colors(
        #     (golden_nfp.closed_interior, 'yellow'),
        #     (nfp.closed_interior, 'green'),
        # )
        # plot_linestring_colors(
        #     (golden_nfp.boundary, 'black'),
        #     (nfp.boundary, '#005500'),
        # )

        self.assertEqual(nfp, golden_nfp)




# class ConvexDecompositionPieceCase(unittest.TestCase):
#     """Test cases with isolated convex parts extracted from actual polygons used in other tests"""
#
#     @classmethod
#     def setUpClass(cls):
#         wkt_filepath = get_full_path('benchmark', 'Personal', 'sample3.wkt')
#         polygon_list = to_shapely_object_list(wkt_filepath)
#         cls.polygon1, cls.polygon2, cls.polygon3 = polygon_list[:3]
#
#     def test_nfp_cuninghame_green_h3_triangle_part_rectangle2(self):
#         """
#         Test NFP computation with Cuninghame-Green method between the top-right triangle part
#         of H-shaped polygon3 (after ear-clipping) and rectangle polygon2.
#
#         """
#         golden_nfp = PolygonWithBoundary(Polygon([(8, -6), (8, 2), (-10, 2), (-10, -6)]),
#                                          GeometryCollection([
#                                              LineString([(8, -6), (8, 2), (-10, 2), (-10, -6), (8, -6)]),
#                                              LineString([(8, -2), (4, -2)]),
#                                              LineString([(-10, -2), (-6, -2)]),
#                                          ]))
#         nfp = nfp_cuninghame_green(self.Polygon([((6.0, 4.0), (10.0, 4.0), (10.0, 6.0))]), self.polygon2)
#
#         with PlotOnFailureContext(
#             (golden_nfp.closed_interior, 'yellow'),
#             (nfp.closed_interior, 'blue'),
#             # (self.polygon3, 'purple'),
#             # (self.polygon2, 'cyan'),
#         ):
#             self.assertEqual(nfp, golden_nfp)


class TangramNFPTestCase(unittest.TestCase):
    """Test cases with shapes from the tangram problem, with 45-degree-step rotations"""

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'tangram.wkt')
        polygon_list = to_shapely_object_list(wkt_filepath)
        cls.big_triangle, cls.medium_triangle, cls.small_triangle, cls.square, cls.parallelogram = polygon_list

    def test_nfp_cuninghame_medium_triangle_r45_vs_parallelogram_r90(self):
        """Test NFP (Cuninghame-Green) of medium triangle (rot=45) vs parallelogram (rot=90)"""
        golden_nfp = PolygonWithBoundary(Polygon([(6, -2), (6, 2), (4, 4), (0, 4), (0, -4), (2, -6)]))
        nfp = nfp_cuninghame_green_with_convex_decomposition(self.medium_triangle, self.parallelogram, 45, 90)
        # plot_geom_colors((golden_nfp, 'y'), (nfp, 'b'))
        # DEBUG: again, very small error... since we know how our algo works, we know the order of the NFP
        # coordinates returned and can apply almost_equals if required
        # print golden_nfp
        # print nfp
        # self.assertEqual(nfp, golden_nfp)
        self.assertTrue(nfp.closed_interior.almost_equals(golden_nfp.closed_interior))
        self.assertTrue(nfp.boundary.almost_equals(golden_nfp.boundary))

    # reversed NFP
    def test_nfp_cuninghame_parallelogram_r90_vs_medium_triangle_r45(self):
        """Test NFP (Cuninghame-Green) of  parallelogram (rot=90) vs medium triangle (rot=45)"""
        # take the previous one and reverse
        golden_nfp = PolygonWithBoundary(Polygon([(6, -2), (6, 2), (4, 4), (0, 4), (0, -4), (2, -6)]))
        golden_nfp.scale(-1)
        nfp = nfp_cuninghame_green_with_convex_decomposition(self.parallelogram, self.medium_triangle, 90, 45)
        # plot_geom_colors((golden_nfp, 'y'), (nfp, 'b'))
        # plot_geom_colors((golden_nfp.closed_interior - nfp.closed_interior, 'b'))
        self.assertEqual(nfp, golden_nfp)

    def test_nfp_cuninghame_parallelogram_r90_vs_medium_triangle_rn45(self):
        """Test NFP (Cuninghame-Green) of  parallelogram (rot=90) vs medium triangle (rot=-45)"""
        golden_nfp = PolygonWithBoundary(Polygon([(4, -4 - 2*sqrt(2)), (4, 0), (0, 0), (-2*sqrt(2), -2*sqrt(2)), (-2*sqrt(2), -4*sqrt(2)), (4 - 2*sqrt(2), -4 - 4*sqrt(2))]))
        nfp = nfp_cuninghame_green_with_convex_decomposition(self.medium_triangle, self.parallelogram, -45, 45)
        # plot_geom_colors((golden_nfp, 'y'), (nfp, 'b'))
        self.assertEqual(nfp, golden_nfp)

    def test_nfp_cuninghame_parallelogram_r45_vs_medium_triangle_rn45(self):
        """Test NFP (Cuninghame-Green) of medium triangle (rot=-45) vs parallelogram (rot=90)"""
        # take the previous one and reverse
        golden_nfp = PolygonWithBoundary(Polygon([(4, -4 - 2*sqrt(2)), (4, 0), (0, 0), (-2*sqrt(2), -2*sqrt(2)), (-2*sqrt(2), -4*sqrt(2)), (4 - 2*sqrt(2), -4 - 4*sqrt(2))]))
        golden_nfp.scale(-1)
        nfp = nfp_cuninghame_green_with_convex_decomposition(self.parallelogram, self.medium_triangle, 45, -45)
        print nfp
        plot_geom_colors((golden_nfp, 'y'), (nfp, 'b'))
        self.assertEqual(nfp, golden_nfp)

    def test_nfp_cuninghame_xxx_vs_medium_triangle_rn90(self):
        """Test NFP (Cuninghame-Green) of medium triangle (rot=-45) vs parallelogram (rot=90)"""
        # take the previous one and reverse
        golden_nfp = PolygonWithBoundary(Polygon([(4, -4 - 2*sqrt(2)), (4, 0), (0, 0), (-2*sqrt(2), -2*sqrt(2)), (-2*sqrt(2), -4*sqrt(2)), (4 - 2*sqrt(2), -4 - 4*sqrt(2))]))
        golden_nfp.scale(-1)
        nfp = nfp_cuninghame_green_with_convex_decomposition(self.parallelogram, self.medium_triangle, 45, -45)
        print nfp
        plot_geom_colors((golden_nfp, 'y'), (nfp, 'b'))
        self.assertEqual(nfp, golden_nfp)


if __name__ == '__main__':
    unittest.main()
