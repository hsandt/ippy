from ippy.utils.decorator import deprecated

__author__ = 'huulong'

import unittest


# No true warning, so unless you catch it, cannot test
# class DeprecatedDecoratorTestCase(unittest.TestCase):
#     def test_call_deprecated_function(self):
#         @deprecated()
#         def deprecated_fun():
#             print 5
#         deprecated_fun()
#         self.assertRaises(DeprecationWarning, deprecated_fun)


if __name__ == '__main__':
    unittest.main()
