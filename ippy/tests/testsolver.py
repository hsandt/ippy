from itertools import combinations
import numpy as np
from numpy import float_ as fl
from numpy.testing.utils import assert_array_equal
from unittest import TestCase
from shapely.geometry import LineString, MultiLineString
from ippy.packmath.group import LeafPiece, CompositePiece

from ippy.packmath.problem import NestingLayout, ExpandedNestingProblem
from ippy.packmath.solver import NestingProblemSolver, BottomLeftStrategy, NFPVerticesStrategy, NFPVGStrategy, \
    FloatingOriginMinimizeBBStrategy
from ippy.packmath.intersection import intersects_area, intersects_phi
from ippy.parser.convert import xml_to_problem, to_shapely_object_list
from ippy.utils.files import get_full_path
from ippy.utils.geometry import Position
from ippy.utils.shapelyextension import get_translated_polygons, get_translated_polygons_from_problem, check_equal
from ippy.view.plot import PlotOnFailureContext, plot_polygon_colors, plot_solution_with_colors

__author__ = 'hs'

import unittest


class SolveTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.solver = NestingProblemSolver(BottomLeftStrategy())

        # TODO: use these setUps for tests that use it only
        xml_filepath = get_full_path('benchmark', 'Personal', 'problem_no_solution.xml')
        cls.problem1 = xml_to_problem(xml_filepath)
        cls.layout1 = cls.solver.search(cls.problem1)

        xml_filepath = get_full_path('benchmark', 'Personal', 'xml_sample1.xml')
        cls.problem2 = xml_to_problem(xml_filepath)
        cls.layout2 = cls.solver.search(cls.problem2)

        xml_filepath = get_full_path('benchmark', 'Personal', 'xml_extreme_solution_sample.xml')
        cls.problem3 = xml_to_problem(xml_filepath)
        cls.layout3 = cls.solver.search(cls.problem3)

    @classmethod
    def tearDownClass(cls):
        pass
        # del cls.problem1, cls.layout1
        # del cls.problem2, cls.layout2

    def test_solve_no_solution(self):
        """
        Test if the solver provides a valid but incomplete layout if there is no solutions

        """
        self.assertFalse(self.layout1.is_complete)
        self.assertTrue(self.layout1.is_valid)

    def test_solve_solution(self):
        """
        Test if the solver provides a valid layout if there is a solution
        and the solver could find it

        """

        # check if the layout is complete and valid
        self.assertTrue(self.layout2.is_complete)
        self.assertTrue(self.layout2.is_valid)

        # VISUAL DEBUG
        plot_solution_with_colors(self.layout2, {
            ('piece1', 0): 'red', ('piece2', 0): 'blue',
            ('piece3', 0): 'green', ('piece4', 0): 'orange'
        }, self.problem2)

        # check if the number of placements is correct (otherwise an empty placement always wins!)
        # optional since get_translated_polygons will check this too
        self.assertEqual(len(self.layout2.placements), len(self.problem2.lot))

        # compute the translated polygons (cheaper than doing it at time of intersection test)
        tr_expanded_lot = get_translated_polygons_from_problem(self.problem2, self.layout2.placements)

        # check for containment
        self.assert_polygons_in_container(tr_expanded_lot, self.problem2.get_expanded_container())

        # check for intersections between contained polygons
        self.assert_polygons_interior_do_not_intersect(tr_expanded_lot)

    def test_solve_extreme_solution(self):
        """
        Test if the solver provides a valid layout even if the pieces just touch the each other
        and the container, when the strategy is working

        """
        self.assertTrue(self.layout3.is_complete)
        self.assertTrue(self.layout3.is_valid)
        tr_polygons = get_translated_polygons_from_problem(self.problem3, self.layout3.placements)
        self.assert_polygons_in_container(tr_polygons, self.problem3.get_expanded_container())
        self.assert_polygons_interior_do_not_intersect(tr_polygons)

    def assert_polygons_in_container(self, expanded_lot, expanded_container):
        # itervalues() is enough but iteritems() may be useful for debugging polygon names
        for polygon in expanded_lot.itervalues():
            self.assertTrue(polygon.within(expanded_container[1]))  # seems to work...
            self.assertTrue(check_equal(polygon & expanded_container[1], polygon))
            # self.assertTrue((polygon & expanded_container[1]).almost_equals(polygon))
            # self.assertTrue((polygon & expanded_container[1]).equals_exact(polygon))
            # TODO: test with the same floating error examples as in test_intersection, but with containment

    def assert_polygons_interior_do_not_intersect(self, polygons):
        for polygon1, polygon2 in combinations(polygons.itervalues(), 2):
            # self.assertFalse(intersects_area(polygon1, polygon2))
            self.assertFalse(intersects_phi(polygon1, polygon2))


class BLSSucceedsTestCase(unittest.TestCase):
    """
    Test case for a very easy problem with a lot of space.
    Should be passed by all placement strategies.

    """

    @classmethod
    def setUpClass(cls):
        cls.bls_solver = NestingProblemSolver(BottomLeftStrategy())
        cls.nfpv_solver = NestingProblemSolver(NFPVerticesStrategy())

        xml_filepath = get_full_path('benchmark', 'Personal', 'xml_sample1.xml')
        cls.problem = xml_to_problem(xml_filepath)

    def test_bls_succeeds(self):
        """Test that BLS succeeds on the very easy problem"""
        layout = self.bls_solver.search(self.problem)
        self.assertTupleEqual((layout.is_complete, layout.is_valid), (True, True))
        # TODO: add more tests on placement to check the expected position of the pieces
        # this is feasible for a simple strategy like this

    def test_nfpv_succeeds(self):
        """Test that NFPV succeeds on the very easy problem"""
        layout = self.nfpv_solver.search(self.problem)

        # DEBUG
        plot_solution_with_colors(layout.placements, {
            ('piece1', 0): 'red', ('piece2', 0): 'blue',
            ('piece3', 0): 'green', ('piece4', 0): 'orange'
        }, self.problem)

        self.assertTupleEqual((layout.is_complete, layout.is_valid), (True, True))


class BLSFailsNFPVerticesSucceedsTestCase(unittest.TestCase):
    """
    TestCase where a Bottom-left placement strategy is certain to fail,
    but strategy with placement on any vertex on the NFP boundary succeeds.

    """

    @classmethod
    def setUpClass(cls):
        cls.bls_solver = NestingProblemSolver(BottomLeftStrategy())
        cls.nfpv_solver = NestingProblemSolver(NFPVerticesStrategy())

        xml_filepath = get_full_path('benchmark', 'Personal', 'problem_bls_fails_nfpvs_succeeds.xml')
        cls.problem = xml_to_problem(xml_filepath)

    def test_bls_fails(self):
        """Test that BLS fails on the easy problem"""
        layout = self.bls_solver.search(self.problem)

        plot_solution_with_colors(layout.placements, {('piece0', 0): 'red', ('piece1', 0): 'blue'},
                                  self.problem)  # DEBUG

        self.assertTupleEqual((layout.is_complete, layout.is_valid), (False, True))

    def test_nfpv_succeeds(self):
        """Test that NFPV succeeds on the easy problem"""
        layout = self.nfpv_solver.search(self.problem)

        plot_solution_with_colors(layout.placements, {('piece0', 0): 'red', ('piece1', 0): 'blue'},
                                  self.problem)  # DEBUG
        self.assertTupleEqual((layout.is_complete, layout.is_valid), (True, True))


class BLSFailsNFPVerticesFailsTestCase(unittest.TestCase):
    """
    TestCase where a Bottom-left placement strategy and placement on any vertex on the NFP boundary fail.
    NFPVG Strategy should succeed.

    Pieces: an H-shaped piece and 2 rectangular pieces that can only fit in the rectangular container
    if the rectangular pieces fit into the H-shape.

    WKT: uses pieces 2, 3 from sample3.wkt

    """

    @classmethod
    def setUpClass(cls):
        cls.bls_solver = NestingProblemSolver(BottomLeftStrategy())
        cls.nfpv_solver = NestingProblemSolver(NFPVerticesStrategy())
        cls.nfpvg_solver = NestingProblemSolver(NFPVGStrategy())

        xml_filepath = get_full_path('benchmark', 'Personal', 'problem2_bls_fails_nfpvs_fails.xml')
        cls.problem = xml_to_problem(xml_filepath)

    def test_bls_fails(self):
        """Test that BLS fails on the rather easy problem"""
        layout = self.bls_solver.search(self.problem)

        plot_solution_with_colors(layout.placements,
                                  {('piece0', 0): 'cyan', ('piece0', 1): 'cyan', ('piece1', 0): 'purple'},
                                  self.problem)  # DEBUG

        self.assertTupleEqual((layout.is_complete, layout.is_valid), (False, True))

    def test_nfpv_fails(self):
        """Test that NFPV fails on the rather easy problem"""
        layout = self.nfpv_solver.search(self.problem)

        self.assertTupleEqual((layout.is_complete, layout.is_valid), (False, True))

        plot_solution_with_colors(layout.placements,
                                  {('piece0', 0): 'cyan', ('piece0', 1): 'cyan', ('piece1', 0): 'purple'},
                                  self.problem)  # DEBUG

    def test_nfpv_with_grouping_succeeds(self):
        """Test that NFPV with grouping succeeds on the rather easy problem"""
        layout = self.nfpvg_solver.search(self.problem)

        self.assertTupleEqual((layout.is_complete, layout.is_valid), (True, True))

        # DEBUG
        plot_solution_with_colors(layout.placements, {
            ('piece0', 0): 'cyan',
            ('piece0', 1): 'cyan',
            ('piece1', 0): 'purple'
        }, self.problem)

    def test_floating_origin_minimize_bb_succeeds(self):
        """Test that FloatingOriginMinimizeBBStrategy succeeds on the rather easy problem"""
        solver = NestingProblemSolver(FloatingOriginMinimizeBBStrategy())
        layout = solver.search(self.problem)

        self.assertTupleEqual((layout.is_complete, layout.is_valid), (True, True))

        # DEBUG
        plot_solution_with_colors(layout.placements, {
            ('piece0', 0): 'cyan',
            ('piece0', 1): 'cyan',
            ('piece1', 0): 'purple'
        }, self.problem)

    # DEBUG
    def test_expected_solution(self):
        """Dummy test to display expected solution"""
        # layout = self.nfpv_solver.search(self.problem)

        # self.assertTupleEqual((layout.is_complete, layout.is_valid), (False, True))

        layout = NestingLayout([('piece0', 0), ('piece0', 1), ('piece1', 0)])
        layout.add_placement('piece0', 0, (0, 2))
        layout.add_placement('piece0', 1, (10, 2))
        layout.add_placement('piece1', 0, (2, 0))

        plot_solution_with_colors(layout.placements,
                                  {('piece0', 0): 'cyan', ('piece0', 1): 'cyan', ('piece1', 0): 'purple'},
                                  self.problem)  # DEBUG


class TestNFPVGStrategy(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'sample3.wkt')
        geometry_list = to_shapely_object_list(wkt_filepath)
        cls.h_polygon = geometry_list[3 - 1]
        cls.rect_polygon = geometry_list[2 - 1]

        problem_filepath = get_full_path('benchmark', 'Personal', 'problem2_bls_fails_nfpvs_fails.xml')
        cls.problem = xml_to_problem(problem_filepath)


    def test_check_bounds_variation_keypoint(self):
        dtype = [('x', float), ('y', float)]
        golden_key_positions = np.array([(0, 0), (-0.5, 1), (-1, 2)], dtype)
        relative_key_positions = NFPVGStrategy.check_bounds_variation_keypoint(
            fl([0, 0, 1, 2]), fl([-1, -2, 2, 1]), fl([-4, 8]))
        relative_key_positions = np.array(relative_key_positions, dtype)

        # sort arrays by X, otherwise Y
        golden_key_positions = np.sort(golden_key_positions, order=['x', 'y'])
        relative_key_positions = np.sort(relative_key_positions, order=['x', 'y'])

        assert_array_equal(relative_key_positions, golden_key_positions)

    def test_get_minimum_bounding_box_placement_along(self):
        """
        Test get_minimum_bounding_box_translation_along A, B and a part of NFP(A, B)
        where A: rectangle and B: H-shaped piece

        """
        nfp_part = MultiLineString([[(8, -2), (2, -2)], [(8, -6), (8, 2), (-14, 2), (-14, -6), (8, -6)]])
        min_placement, min_area = NFPVGStrategy.get_minimum_bounding_box_translation_along(self.rect_polygon, self.h_polygon, nfp_part)
        assert_array_equal(min_placement, fl([2, -2]))
        self.assertEqual(min_area, 16*6)

    def test_find_matching_pair(self):
        """
        Test if a pair of H-rectangle + a single rectangle piece are found when matching pairs
        with a threshold of 0.50 (5/24 at least)

        """
        expanded_problem = ExpandedNestingProblem(self.problem)
        component_pieces = NFPVGStrategy.find_low_bb_waste_pair(expanded_problem, 0.50)

        # since there are two identical pieces we cannot predict the instance idx
        # inside the group, but at least check basic information on the grouping
        self.assertEqual(len(component_pieces), 2)
        for component_piece in component_pieces:
            if isinstance(component_piece, LeafPiece):
                self.assertEqual(component_piece.piece_id, 'piece0')  # a rectangle
            elif isinstance(component_piece, CompositePiece):
                self.assertEqual(len(component_piece), 2)  # the H-shaped piece
                # sort children by piece id to compare
                child0, child1 = sorted(component_piece.children, key=lambda child: child.piece_id)
                self.assertTrue(isinstance(child0, LeafPiece))
                self.assertEqual(child0.piece_id, 'piece0')  # the rectangle
                self.assertTrue(isinstance(child1, LeafPiece))
                self.assertEqual(child1.piece_id, 'piece1')  # the H-shaped piece


class Poly1aTestCase(unittest.TestCase):
    """
    Test various strategies on the benchmark poly1a.xml

    """

    @classmethod
    def setUpClass(cls):
        xml_filepath = get_full_path('benchmark', 'improved_xml', 'poly1a_cleared.xml')
        cls.problem = xml_to_problem(xml_filepath)

    def test_bl(self):
        """Test BL strategy"""
        solver = NestingProblemSolver(BottomLeftStrategy())
        layout = solver.search(self.problem)
        plot_solution_with_colors(layout.placements, problem=self.problem)  # DEBUG
        self.assertTupleEqual((layout.is_complete, layout.is_valid), (True, True))

    def test_nfpv(self):
        """Test NFPV strategy"""
        solver = NestingProblemSolver(NFPVerticesStrategy())
        layout = solver.search(self.problem)
        plot_solution_with_colors(layout.placements, problem=self.problem)  # DEBUG
        self.assertTupleEqual((layout.is_complete, layout.is_valid), (True, True))

    def test_floating_origin_minimize_bb_succeeds(self):
        """Test that FloatingOriginMinimizeBBStrategy succeeds on the rather easy problem"""
        solver = NestingProblemSolver(FloatingOriginMinimizeBBStrategy())
        layout = solver.search(self.problem)
        plot_solution_with_colors(layout.placements, problem=self.problem)  # DEBUG
        self.assertTupleEqual((layout.is_complete, layout.is_valid), (True, True))

    def test_bl_variable_width(self):
        """Test BL strategy with variable width"""
        solver = NestingProblemSolver(BottomLeftStrategy())
        best_layout, upper_width = solver.search_variable_width(self.problem)
        plot_solution_with_colors(placements=best_layout,
                                  container_width=upper_width, problem=self.problem)  # DEBUG
        self.assertTupleEqual((best_layout.is_complete, best_layout.is_valid), (True, True))


class ShirtsTestCase(unittest.TestCase):
    """
    Shirts

    """

    @classmethod
    def setUpClass(cls):
        cls.bls_solver = NestingProblemSolver(BottomLeftStrategy())
        cls.nfpv_solver = NestingProblemSolver(NFPVerticesStrategy())

        xml_filepath = get_full_path('benchmark', 'improved_xml', 'shirts_cleared.xml')
        cls.problem = xml_to_problem(xml_filepath)

    def test_bls_fails(self):
        """Test that BLS succeeds on the easy problem"""
        layout = self.bls_solver.search(self.problem)

        plot_solution_with_colors(layout.placements, problem=self.problem)  # DEBUG

        self.assertTupleEqual((layout.is_complete, layout.is_valid), (False, True))

    def test_nfpv_succeeds(self):
        """Test that NFPV succeeds on the easy problem"""
        layout = self.nfpv_solver.search(self.problem)

        self.assertTupleEqual((layout.is_complete, layout.is_valid), (False, True))

        plot_solution_with_colors(layout.placements, {('piece0', 0): 'red', ('piece1', 0): 'blue'},
                                  self.problem)  # DEBUG



if __name__ == '__main__':
    unittest.main()
