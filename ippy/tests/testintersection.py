from ippy.packmath.intersection import intersects_area, intersects_phi
from ippy.packmath.phifunction import phi
from ippy.packmath.problem import Placement
from ippy.parser.convert import to_shapely_object_list
from ippy.utils.files import get_full_path
from ippy.utils.shapelyextension import get_translated_polygons

__author__ = 'huulong'

import unittest


class IntersectionTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'sample1a.wkt')
        polygon_list = to_shapely_object_list(wkt_filepath)
        assert len(polygon_list) == 2, 'There are not exactly 2 polygons in {0}'.format(wkt_filepath)
        cls.polygon_lot = {('polygon1', 0): polygon_list[0], ('polygon2', 0): polygon_list[1]}

    def test_intersects_area_after_translation_with_no_intersection(self):
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('polygon1', 0): Placement((0, 0)),
            ('polygon2', 0): Placement((5, 4))
        })
        self.assertFalse(intersects_area(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)]))

    def test_intersects_area_after_translation_with_touch(self):
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('polygon1', 0): Placement((0, 0)),
            ('polygon2', 0): Placement((6, 7))
        })
        self.assertFalse(intersects_area(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)]))

    def test_intersects_area_after_double_translation_with_touch(self):
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('polygon1', 0): Placement((2, 3)),
            ('polygon2', 0): Placement((8, 10))
        })
        self.assertFalse(intersects_area(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)]))

    def test_intersects_area_after_float_translation_with_touch(self):
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('polygon1', 0): Placement((0, 0)),
            ('polygon2', 0): Placement((4.0, 5.5))
        })
        self.assertFalse(intersects_area(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)]))

    def test_intersects_area_after_many_decimals_float_translation_with_touch(self):
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('polygon1', 0): Placement((0, 0)),
            ('polygon2', 0): Placement((4.6666666, 6.0))
        })
        self.assertFalse(intersects_area(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)]))

    def test_intersects_area_after_loose_float_translation_with_touch_for_loose_threshold(self):
        """Test if intersects_area returns False for a less precise translation and a looser threshold"""
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('polygon1', 0): Placement((0, 0)),
            ('polygon2', 0): Placement((4.66666, 6.0))
        })
        self.assertFalse(intersects_area(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)], threshold=1e-5))

    def test_intersects_area_after_loose_float_translation_toward_intersection(self):
        """Test if intersects_area returns True for a less precise translation but a normal threshold"""
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('polygon1', 0): Placement((0, 0)),
            ('polygon2', 0): Placement((4.66666, 6.0))
        })
        self.assertTrue(intersects_area(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)]))

    def test_intersects_area_after_translation_with_deep_intersection(self):
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('polygon1', 0): Placement((0, 0)),
            ('polygon2', 0): Placement((2, 1))
        })
        self.assertTrue(intersects_area(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)]))

    def test_intersects_phi_after_translation_with_no_intersection(self):
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('polygon1', 0): Placement((0, 0)),
            ('polygon2', 0): Placement((5, 4))
        })
        self.assertFalse(intersects_phi(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)]))

    def test_intersects_phi_after_translation_with_touch(self):
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('polygon1', 0): Placement((0, 0)),
            ('polygon2', 0): Placement((6, 7))
        })
        self.assertFalse(intersects_phi(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)]))

    def test_intersects_phi_after_many_decimals_float_translation_with_touch(self):
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('polygon1', 0): Placement((0, 0)),
            ('polygon2', 0): Placement((4.6666666, 6.0))
        })
        self.assertFalse(intersects_phi(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)]))

    def test_intersects_phi_after_loose_float_translation_with_touch_for_loose_threshold(self):
        """Test if intersects_phi returns False for a less precise translation and a looser threshold"""
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('polygon1', 0): Placement((0, 0)),
            ('polygon2', 0): Placement((4.66666, 6.0))
        })
        self.assertFalse(intersects_phi(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)], threshold=1e-5))

    def test_intersects_phi_after_loose_float_translation_toward_intersection(self):
        """Test if intersects_phi returns True for a less precise translation but a normal threshold"""
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('polygon1', 0): Placement((0, 0)),
            ('polygon2', 0): Placement((4.66666, 6.0))
        })
        self.assertTrue(intersects_phi(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)]))

    def test_intersects_phi_after_translation_with_deep_intersection(self):
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('polygon1', 0): Placement((0, 0)),
            ('polygon2', 0): Placement((2, 1))
        })
        self.assertTrue(intersects_phi(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)]))

    @classmethod
    def tearDownClass(cls):
        pass


class ContainmentTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'sample1b.wkt')
        polygon_list = to_shapely_object_list(wkt_filepath)
        assert len(polygon_list) == 2, 'There are not exactly 2 polygons in {0}'.format(wkt_filepath)
        cls.polygon_lot = {('piece1', 0): polygon_list[0], ('piece2', 0): polygon_list[1]}

    def test_intersection_containment(self):
        tr_polygon_lot = get_translated_polygons(self.polygon_lot, {
            ('piece1', 0): Placement((3.0909090909091, 1)),
            ('piece2', 0): Placement((0, 0))
        })
        # self.assertTrue(intersects_phi(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)]))
        print(tr_polygon_lot[('piece1', 0)])
        print(tr_polygon_lot[('piece2', 0)])
        print(tr_polygon_lot[('piece1', 0)] & tr_polygon_lot[('piece2', 0)])
        print(phi(tr_polygon_lot[('piece1', 0)], tr_polygon_lot[('piece2', 0)]))
        # if (tr_polygon_lot[('polygon1', 0)] & tr_polygon_lot[('polygon2', 0)]).equals(tr_polygon_lot[('polygon1', 0)]):
        # if phi(tr_polygon_lot[('polygon1', 0)], tr_polygon_lot[('polygon2', 0)]) == 0:
        #     print("containment")
        # else:
        #     print("intersection differs, no containment")
        #     raise Exception()


if __name__ == '__main__':
    unittest.main()
