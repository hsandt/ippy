import math
import numpy as np
from shapely.affinity import translate
from shapely.geometry import Polygon, Point, LineString, MultiLineString, box, MultiPoint
from shapely.geometry.collection import GeometryCollection
from shapely.geometry.multipolygon import MultiPolygon
from shapely.geometry.polygon import LinearRing

from ippy.utils.shapelyextension import ComparableBaseGeometry, count_edge, to_bounding_box, \
    get_bounding_box_area, get_bounding_box_absolute_waste, get_bounding_box_waste, PolygonExtended, is_multi, \
    check_equal, get_closed_interior, PolygonWithBoundary, round_coords, assert_validity, raycast, get_edges_from_coords, \
    get_edges, raycast_polygon, bounds_union, update_bounds, \
    get_cumulative_size, get_polygon_diameter, get_bounds_area, get_edge_length, get_min_width_or_height, get_min_size, \
    get_nb_vertices
from ippy.utils.unittestextension import assertKeyEqual

__author__ = 'hs'

import unittest


class PolygonExtendedTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.polygon = PolygonExtended([(0, 0), (3, 0), (3, 3), (0, 3)],
                                holes=[[(1, 1), (2, 1), (2, 2)]])

    def test_check_if_bounds_inside_true(self):
        self.assertTrue(self.polygon.check_if_bounds_inside((0, 0, 3, 4)))

    def test_check_if_bounds_inside_false(self):
        self.assertFalse(self.polygon.check_if_bounds_inside((1, 0, 3, 4)))



class ConvexPolygonCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.polygon_a = Polygon([(0, 0), (3, 0), (3, 3), (0, 3)],
                                holes=[[(1, 1), (2, 1), (2, 2)]])
        cls.polygon_b = Polygon([(0, -1), (3, 0), (5, 6), (-1, 0)],
                                holes=[[(1, 1), (2, 1), (2, 2)]])

        cls.multigeom = GeometryCollection([Point(0, 0), LineString([(1, 0), (1, 1)]),
                                            MultiLineString([[(1, 0), (3, 0)], [(3, 0), (3, 1)]])
                                            ])

    def test_comparable_base_geometry(self):
        a = ComparableBaseGeometry(self.polygon_a)
        temp = Polygon([(-1, 2), (2, 2), (2, -1), (-1, -1)],
                       holes=[[(0, 0), (1, 0), (1, 1)]])
        b = ComparableBaseGeometry(translate(temp, 1, 1))
        self.assertEqual(a, b)

    # TODO: add tests for PolygonWithBoundary
    # TODO: add other tests

    def test_count_edge(self):
        self.assertEqual(count_edge(self.polygon_a), 7)

    def test_to_bounding_box(self):
        golden_bounding_box = box(-1, -1, 5, 6)
        bounding_box = to_bounding_box(self.polygon_b)
        assertKeyEqual(self, bounding_box, golden_bounding_box, key=ComparableBaseGeometry)

    def test_get_bounds_area(self):
        self.assertEqual(get_bounds_area((0, 4, 2, 7)), 6)

    def test_get_bounds_area_error(self):
        self.assertRaises(ValueError, get_bounds_area, (0, 4, -1, 7))

    def test_get_bounding_box_area(self):
        self.assertEqual(get_bounding_box_area(self.polygon_b), 42)

    def test_get_bounding_box_absolute_waste(self):
        self.assertEqual(get_bounding_box_absolute_waste(self.polygon_a), 0.5)

    def test_get_bounding_box_waste(self):
        self.assertEqual(get_bounding_box_waste(self.polygon_a), 0.5 / (3*3))

    @classmethod
    def tearDownClass(cls):
        pass

    # TODO: tests for merging/rounding functions

class BasicFunctionTestCase(unittest.TestCase):
    def test_is_multi_true_multipoint(self):
        self.assertTrue(is_multi(MultiPoint([(0, 0), (0, 1)])))
    def test_is_multi_true_collection(self):
        self.assertTrue(is_multi(GeometryCollection([Point(0, 0)])))

    def test_is_multi_false_empty_collection(self):
        self.assertFalse(is_multi(GeometryCollection()))
    def test_is_multi_false_polygon(self):
        self.assertFalse(is_multi(Polygon([(0, 0), (3, 0), (3, 3)])))

    def test_check_equal_empty(self):
        self.assertTrue(check_equal(GeometryCollection(), GeometryCollection()))
    def test_check_equal_true(self):
        self.assertTrue(check_equal(Polygon([(0, 0), (3, 0), (3, 3)]), Polygon([(3, 3), (0, 0), (3, 0)])))
    def test_check_equal_false(self):
        self.assertFalse(check_equal(Point(1, 0), Point(0, 2)))

    def test_bounds_union(self):
        bounds1 = (0, 1, 2, 3)
        bounds2 = (-1, 0, 1, 4)
        self.assertEqual(bounds_union(bounds1, bounds2), (-1, 0, 2, 4))

    def test_bounds_union_empty(self):
        bounds1 = ()
        bounds2 = (-1, 0, 1, 4)
        self.assertEqual(bounds_union(bounds1, bounds2), (-1, 0, 1, 4))

    def test_update_bounds(self):
        mutable_bounds1 = [0, 1, 2, 3]
        bounds2 = (-1, 0, 1, 4)
        update_bounds(mutable_bounds1, bounds2)
        self.assertEqual(mutable_bounds1, [-1, 0, 2, 4])

    def test_update_bounds_empty(self):
        mutable_bounds1 = [0, 1, 2, 3]
        bounds2 = ()
        update_bounds(mutable_bounds1, bounds2)
        self.assertEqual(mutable_bounds1, [0, 1, 2, 3])

    # TODO: add get_extreme_vertex_index_value tests, including with floating errors

    # -- measurement utils tests --

    def test_get_nb_vertices(self):
        multipolygon = MultiPolygon([
            (
                ((0, 0), (5, 0), (4, 6), (-1, 5)),
                [
                    ((1, 1), (2, 1), (1, 2)),
                    ((3, 3), (3, 4), (2, 3))
                ]
            ),
            (
                ((0, -10), (1, -12), (4, -5)),
                []
            )
        ])
        self.assertEqual(get_nb_vertices(multipolygon), 13)

    def test_get_edge_length(self):
        edge = [(1., 2.), (-1., 3.)]
        self.assertEqual(get_edge_length(edge), math.sqrt(5))

    def test_get_cumulative_size(self):
        golden_size = 6, 11
        geom = box(0, 0, 5, 10) | LineString([(10, 0), (11, 1)]) | Point(4, 4)
        np.testing.assert_allclose(get_cumulative_size(geom), golden_size)

    @unittest.skip('Way linestring is split in multiple parts is not predictible')
    def test_get_cumulative_size_polygon_with_boundary_components(self):
        golden_size = 16, 21
        pwb = PolygonWithBoundary(box(0, 0, 5, 10), LineString([(0, 0), (1, 1)]) | Point(4, 4), True)
        np.testing.assert_allclose(get_cumulative_size(pwb.closed_interior) + get_cumulative_size(pwb.boundary), golden_size)

    def test_get_polygon_diameter(self):
        polygon = Polygon([(0, 0), (4, 0), (3, 1), (4, 2), (1, 5)])
        self.assertAlmostEqual(get_polygon_diameter(polygon), math.sqrt(34))

    def test_get_min_size(self):
        geom = Polygon([(0, 0), (4, 0), (3, 1), (4, 2), (1, 5)])
        self.assertEqual(get_min_size(geom), 4)

    def test_get_min_width_or_height(self):
        # resp. min sizes: 4, 3
        geoms = [Polygon([(0, 0), (4, 0), (1, 5)]), box(0, -2, 3, 2)]
        self.assertEqual(get_min_width_or_height(geoms), 3)

    def test_get_edges_from_coords(self):
        golden_edges = [((0, 0), (1, 0)), ((1, 0), (0, 1)), ((0, 1), (-1, 0))]
        edges = get_edges_from_coords([(0, 0), (1, 0), (0, 1), (-1, 0)], loop=False)
        self.assertEqual(edges, golden_edges)

    def test_get_edges_from_coords_loop(self):
        golden_edges = [((0, 0), (1, 0)), ((1, 0), (0, 1)), ((0, 1), (-1, 0)), ((-1, 0), (0, 0))]
        edges = get_edges_from_coords([(0, 0), (1, 0), (0, 1), (-1, 0)], loop=True)
        self.assertEqual(edges, golden_edges)

    def test_get_edges(self):
        golden_edges = [((0, 0), (2, 0)), ((2, 0), (3, 3)), ((3, 3), (0, 4)), ((0, 4), (0, 0)),
                        ((1, 1), (2, 1)), ((2, 1), (1, 2)), ((1, 2), (1, 1))]
        edges = get_edges(Polygon([(0, 0), (2, 0), (3, 3), (0, 4)], holes=[[(1, 1), (2, 1), (1, 2)]]))
        self.assertEqual(edges, golden_edges)

    def test_raycast(self):
        geom = MultiLineString([[(-1, 1), (1, 2)], [(0, 3), (-1, 2)]]) | Point(0, 1.6)
        hit = raycast((0, 0), (0, 3), geom)
        self.assertIsNotNone(hit)
        np.testing.assert_allclose(hit, (0, 1.5))

    def test_raycast_polygon(self):
        polygon = Polygon([(0, 0), (3, 0), (3, 3), (0, 4)], holes=[[(1, 1), (2, 1), (1, 2)]])
        hit = raycast_polygon((-1, 0), (3, 0), polygon, ignore_parallel_edges=False)
        self.assertIsNotNone(hit)
        np.testing.assert_allclose(hit, (0, 0))

    def test_raycast_polygon_None(self):
        polygon = Polygon([(0, 0), (3, 0), (3, 3), (0, 4)], holes=[[(1, 1), (2, 1), (1, 2)]])
        hit = raycast_polygon((-1, 0), (0, 2), polygon, ignore_parallel_edges=False)
        self.assertIsNone(hit)

    def test_raycast_polygon_ignore_parallel(self):
        polygon = Polygon([(0, 0), (3, 0), (3, 3), (0, 4)], holes=[[(1, 1), (2, 1), (1, 2)]])
        hit = raycast_polygon((-1, 1), (3, 0), polygon, ignore_parallel_edges=True)
        self.assertIsNotNone(hit)
        np.testing.assert_allclose(hit, (0, 1))


class PolygonWithBoundaryTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.collection = GeometryCollection(
            [
                MultiLineString([[(0, 0), (1, 0)], [(0, 0), (1, 0)]]),
                Point(3,3),
                Polygon([(0, 0), (0, 2), (1, 1)]),
                GeometryCollection(
                    [
                        Polygon([(-10, -10), (-10, 0), (0, -10)]),
                        MultiPoint([(100, 200), (300, 400)])
                    ]
                ),
                GeometryCollection(
                    [
                        LinearRing([(2, 2), (4, 4), (2, 6)])
                    ]
                )
            ]
        )

    def test_get_closed_interior_polygon(self):
        """Test if get_closed_interior only leaves Polygon is a complex collection"""
        polygon = Polygon([(-10, -10), (-10, 0), (0, -10)])
        collection_closed_interior = get_closed_interior(polygon)
        self.assertTrue(check_equal(collection_closed_interior, polygon))

    def test_get_closed_interior_collection(self):
        """Test if get_closed_interior only leaves Polygon is a complex collection"""
        golden_collection_closed_interior = GeometryCollection(
            [
                Polygon([(0, 0), (0, 2), (1, 1)]),
                GeometryCollection(
                    [
                        Polygon([(-10, -10), (-10, 0), (0, -10)]),
                    ]
                )
            ]
        )

        collection_closed_interior = get_closed_interior(self.collection)
        self.assertTrue(collection_closed_interior.type == 'GeometryCollection')
        self.assertTrue(check_equal(collection_closed_interior, golden_collection_closed_interior))

    def test_contains_or_touch_true(self):
        pwb = PolygonWithBoundary(box(0, 0, 10, 5))
        self.assertTrue(pwb.contains_or_touch(Point(10, 2)))

class RoundingTestCase(unittest.TestCase):
    """TestCase for rounding methods"""
    
    @classmethod
    def setUpClass(cls):
        cls.floating_polygon = Polygon([(0, 0), (10.236487, 0), (10.236488, 4), (0, 4.0001)], holes=[[(0.5, 0.5), (5, 0.555), (0.5, 3.0547)]])
        cls.floating_polygon_with_boundary = PolygonWithBoundary(cls.floating_polygon,
                                                                 boundary=LineString([(-1, -1.25045), (-2.567, -0.0055)]),
                                                                 trivial_boundary_from_interior=True)
        assert_validity(cls.floating_polygon)
        assert_validity(cls.floating_polygon_with_boundary.boundary)

    def test_round_coords_polygon_decimals3(self):
        golden_rounded_geom = Polygon([(0, 0), (10.236, 0), (10.236, 4), (0, 4.0)],
                                      holes=[[(0.5, 0.5), (5, 0.555), (0.5, 3.055)]])
        # assert_validity(golden_rounded_geom)
        rounded_geom = round_coords(self.floating_polygon, 3)
        # print rounded_geom.wkt
        self.assertTrue(check_equal(rounded_geom, golden_rounded_geom))

    def test_round_coords_polygon_with_boundary_decimals3(self):
        golden_rounded_closed_interior = Polygon([(0, 0), (10.236, 0), (10.236, 4), (0, 4.0)], holes=[[(0.5, 0.5), (5, 0.555), (0.5, 3.055)]])
        golden_rounded_extra_boundary = LineString([(-1, -1.250), (-2.567, -0.006)])  # 'extra' since the closed interior has its own boundary too
        # assert_validity(golden_rounded_closed_interior)
        # assert_validity(golden_rounded_boundary)
        golden_rounded_polygon_with_boundary = PolygonWithBoundary(golden_rounded_closed_interior,
                                                                   golden_rounded_extra_boundary, True)
        rounded_polygon_with_boundary = round_coords(self.floating_polygon_with_boundary, 3)
        print rounded_polygon_with_boundary
        self.assertEqual(rounded_polygon_with_boundary, golden_rounded_polygon_with_boundary)


if __name__ == '__main__':
    unittest.main()
