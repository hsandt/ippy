import os
from shapely.geometry import LineString, Point, Polygon, box, LinearRing
from ippy.packmath.problem import PieceData, NestingProblem
from ippy.parser.xmlproblem import parse_xml_to_problem, output_problem_to_xml
from ippy.utils.files import get_full_path
from ippy.utils.shapelyextension import RawBoundary, PolygonWithBoundary

__author__ = 'huulong'

import unittest


class TestXMLProblem(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.golden_xml_filepath = get_full_path('tests', 'goldenfiles', 'golden_problem.xml')

        # test problem (corresponds to golden_problem.xml; NFP coords are not necessarily true)
        polygon0 = box(0, 0, 10, 10)
        # polygon1 = Polygon([(0.0, 0.0), (10.0, 0.0), (10.0, 10.0), (0.0, 10.0), (0.0, 0.0)],
        #                    holes=[[(1.0, 1.0), (2.0, 1.0), (2.0, 2.0), (1.0, 2.0), (1.0, 1.0)]])
        polygon1 = Polygon([(0.0, 0.0), (10.0, 0.0), (10.0, 2.0), (5.0, 2.0), (5.0, 4.0), (10.0, 4.0),
                            (10.0, 10.0), (0.0, 10.0), (0.0, 0.0)],
                           holes=[[(2.0, 6.0), (4.0, 6.0), (4.0, 8.0), (2.0, 8.0), (2.0, 6.0)],
                                  [(5.0, 5.0), (9.0, 5.0), (9.0, 8.0), (5.0, 8.0), (5.0, 5.0)]])

        polygon2 = box(0, 0, 2, 2)

        ifp_raw_boundary1 = RawBoundary(points=[Point(0, 0)])

        ifp_exterior2 = box(0, 0, 8, 8).exterior
        ifp_exterior2_180 = box(0, 0, 8, 8).exterior

        nfp0_polygon_with_boundary = PolygonWithBoundary(Polygon([(10, -2), (10, 10), (-2, 10), (-2, -2)],
                                                                 holes=[[(5, 5), (7, 5), (7, 6), (5, 6)]]),
                                                         # boundary=GeometryCollection([LineString([(5, 2), (10, 2)]),
                                                         #                              Point(2, 6)])
                                                         )
        # it is difficult to extract a raw boundary from a boundary, although not impossible
        # for now, just input the raw boundary separately
        (nfp0_exterior,), (nfp0_hole,), _ = nfp0_polygon_with_boundary.extract_exteriors_holes_boundary_tuple()
        nfp0_raw_boundary = RawBoundary([LineString([(5, 2), (10, 2)])], [Point(2, 6)])

        # same for piece2 at 180 degrees (if you do not use extraction methods, build a LinearRing directly)
        nfp0_exterior_180 = LinearRing([(8, -2), (10, 10), (-2, 10), (-2, -2)])

        # should match golden_problem.xml
        container = ('board0', PieceData('board0', 1, [0], 'polygon0'))
        lot = {'piece1': PieceData('piece1', 1, [0], 'polygon1', ['hole0', 'hole1']), 'piece2': PieceData('piece2', 1, [0, 180],
                                                                                                 'polygon2')}
        metadata = {
            'nfps': {
                (('piece1', 'piece2'), (0, 0)): {
                    'idComponentsTypes': [('nfpPolygon0', 0), ('nfpHole0', -1)],
                    'idBoundaries': ['nfpBoundary0']
                    },
                (('piece1', 'piece2'), (0, 180)): {
                    'idComponentsTypes': [('nfpPolygon0-180', 0)],
                    'idBoundaries': []  # we can deduce boundary from closed interior, no problem
                    }
                },
            'ifps': {
                (('board0', 'piece1'), (0, 0)): {
                    'idComponentsTypes': [],
                    'idBoundaries': ['ifpBoundary1']
                },
                (('board0', 'piece2'), (0, 0)): {
                    'idComponentsTypes': [('ifpPolygon2', 0)],
                    'idBoundaries': []
                },
                (('board0', 'piece2'), (0, 180)): {
                    'idComponentsTypes': [('ifpPolygon2-180', 0)],
                    'idBoundaries': []
                }
            }
        }
        polygons = {
            'polygon0': polygon0.exterior,
            'polygon1': polygon1.exterior, 'hole0': polygon1.interiors[0], 'hole1': polygon1.interiors[1],
            'polygon2': polygon2.exterior,
            'ifpPolygon2': ifp_exterior2,
            'ifpPolygon2-180': ifp_exterior2_180,
            'nfpPolygon0': nfp0_exterior, 'nfpHole0': nfp0_hole,
            'nfpPolygon0-180': nfp0_exterior_180,
        }
        raw_boundaries = {
            'nfpBoundary0': nfp0_raw_boundary,
            'ifpBoundary1': ifp_raw_boundary1
        }
        name = 'ConvertTestCaseProblem'
        author = 'Me'
        date = '2015/04/28'
        description = 'A problem with NFP/IFP with boundaries.'
        vertices_orientation = 'counterclockwise'
        coordinates_origin = 'down-left'
        cls.problem = NestingProblem(container, lot, metadata, polygons, raw_boundaries, name, author, date, description,
                                     vertices_orientation, coordinates_origin)

    def test_parse_xml_to_problem(self):
        """Test conversion from nesting XML to NestingProblem: all data"""
        problem = parse_xml_to_problem(self.golden_xml_filepath)
        # compare the PackingProblem generated and the expected problem
        self.assertEqual(problem, self.problem)

    # cannot easily tune XML format with generateDS so test is not usable
    # FIXME: find how to tune generateDS export OR simply create golden file from generateDS export ;p
    @unittest.skip('Skip problem->XML test: XML format hard to control with generateDS')
    def test_output_problem_to_xml(self):
        """Test conversion from nesting XML to PackingProblem: all data"""
        mock_filepath = get_full_path('temp', 'mock_file.xml')
        output_problem_to_xml(self.problem, mock_filepath)

        # compare the NestingProblem generated and the expected problem
        with open(self.golden_xml_filepath, 'r') as golden_file, open(mock_filepath, 'r') as mock_file:
            # OPTIMIZE: prefer generator if list generation is too long, and adapt assertion test
            golden_lines = [line.strip() for line in list(golden_file)]  # ensure there is a newline at the end of the file
            output_lines = [line.strip() for line in list(mock_file)]
            self.assertEqual(output_lines, golden_lines)
            # clean-up mock file (usually on exception finally, here it is deliberately
            # prevented if assertion fails, in order to debug the output file afterward)
            os.remove(mock_filepath)

    def test_problem_to_xml_to_problem(self):
        """Test if conversion Problem -> XML -> Problem preserves Problem"""
        mock_filepath = get_full_path('temp', 'mock_file.xml')
        output_problem_to_xml(self.problem, mock_filepath)
        final_problem = parse_xml_to_problem(mock_filepath)

        self.assertEqual(final_problem, self.problem)
        os.remove(mock_filepath)

if __name__ == '__main__':
    unittest.main()

from unittest import TestCase

__author__ = 'huulong'
