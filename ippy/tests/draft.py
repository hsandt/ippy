from math import sqrt
import itertools
import functools
from lxml import etree
from ippy.parser.convert import xml_to_problem
from collections import Counter

__author__ = 'hs'

import numpy as np
from numpy import float_ as fl
from ippy.utils.iterables import flatten


from shapely.validation import explain_validity

# fast debug plot
from ippy.view.plot import plot_geom_colors
from ippy.view.plot import plot_polygon_colors
from ippy.view.plot import plot_linestring_colors

from shapely.geometry.polygon import orient

from ippy.view import plot
reload(plot)

# plot_geom_colors((nfp, 'b'))

# plot_polygon_colors((polygon, 'blue'))
# plot_linestring_colors((line, 'blue'))

from shapely.geometry.base import BaseGeometry
from shapely.geometry import GeometryCollection
from shapely.geometry import Point, MultiPoint, LineString, MultiLineString, LinearRing, box
from shapely.geometry import Polygon, MultiPolygon
from ippy.utils.shapelyextension import PolygonWithBoundary, get_edges_from_coords, assert_validity
from shapely import wkt
from ippy.utils.geometry import get_points_orientation
from ippy.utils.shapelyextension import lc, PolygonExtended
from ippy.packmath.problem import NestingLayout, Placement, PieceKey
from shapely.ops import linemerge, unary_union, cascaded_union
from shapely.geometry.polygon import orient
from shapely.affinity import translate, rotate

wkt.loads('POLYGON((0 0, 0 1, 1 1, 0 0))')
wkt.loads('LINEARRING(0 0, 0 1, 1 1, 2 2, 0 0)').is_valid  # False

# p = wkt.loads('')
p = box(0, 0, 5, 5)
p = Polygon([(1,2),(2,3),(0,4)])
p = Polygon([(1, 2), (2, 3), (3, 4), (2, 6), (1.000000000000001, 2)])
p = Polygon([(1, 2), (2, 3), (3, 4), (2, 6), (1.0000000000000001, 2)])
p = Polygon([(1, 2), (2, 3), (3, 4), (2, 6)])
p = Polygon([(0, 0), (10, 0), (10.54, 10), (0, 10)], holes=[[(1, 1.578), (2, 1), (2, 2), (1, 2)],
                                                         [(3, 3), (5, 3.56897), (5, 5)]])
list(p.exterior.coords)
[list(h.coords) for h in p.interiors]

# odd polygons
q = Polygon([(0, 0), (0, 0), (1, 0), (1, 0)])
q.is_valid
r = q.buffer(0)
r.wkt

q = Polygon([(0, 0), (1e-11, 0), (1e-11, 0)])
q.is_valid
r = q.buffer(0)
r.wkt

# a normal polygon: how is it affected by buffer?
q = Polygon([(1,2.0000001),(2.0000001,3.99999999),(0,4)])
q.is_valid
r = q.buffer(0)
r.wkt  # okay, preserved

# polygon with a back and forth edge inside, as Burke could produce if we do not use graph analysis
s = Polygon([(0, 0), (1, 0), (1, 0.5), (0.5, 0.5), (1, 0.5), (1, 1), (0, 1)])
s.is_valid
s.buffer(0).wkt  # with buffer, interior line disappeared (also a break vertex remains at the old crossing point)
plot_geom_colors((s, 'b'))
plot_geom_colors((s.buffer(0), 'r'))

# polygon with an exterior line (similar to IFP but as a normal Polygon)
s = Polygon([(0, 0), (1, 0), (1, 0.5), (2, 0.5), (1, 0.5), (1, 1), (0, 1)])
s.is_valid
s.buffer(0).wkt  # with buffer, exterior line disappeared (also a break vertex remains at the old crossing point)
plot_geom_colors((s, 'b'))
plot_geom_colors((s.buffer(0), 'r'))

# polygon with an interior line that leads to a complete hole circle, then comes back
s = Polygon([(0, 0), (3, 0), (3, 1), (1, 1), (1, 2), (2, 2), (2, 1), (3, 1), (3, 3), (0, 3)])
s.is_valid
s.buffer(0).wkt
plot_geom_colors((s, 'b'))
plot_geom_colors((s.buffer(0), 'r'))


# flat polygon
t = Polygon([(0, 0), (3, 0), (6, 0), (4, 0), (2, 0)])
t.is_valid
t.buffer(0).wkt
plot_geom_colors((t, 'b'))
plot_geom_colors((t.buffer(0), 'r'))

# count redundant points (first and last are duplicate, this is normal)
Counter(p.exterior.coords)
c = Counter(p.exterior.coords[:-1])
any([count > 1 for count in c.values()])

pwb = PolygonWithBoundary(p, Point(-1, -1), trivial_boundary_from_interior=True)

b1 = box(0, 0, 1, 1)
b2 = box(1, 0, 2, 1)
b1 & b2  # LineString!

# for multi-polygons:
mp = [p, p]
[list(p.exterior.coords) for p in mp]

# TEST: does shapely reduce multi polygon to polygon automatically?
# when polygons are touching:
m = MultiPolygon([box(0, 0, 5, 5), box(5, 0, 10, 5)])
m.wkt  # NO, still a MultiPolygon


# after part removal
m = MultiPolygon([box(0, 0, 5, 5), box(10, 0, 15, 5)])
m -= box(10, 0, 15, 5)
m.wkt  # YES, reduced to Polygon
# conclusion: in free space update, need to reconvert to MultiPolygon
# if we always want the same interface

p1 = Polygon([(1, 2), (2, 3), (3, 4), (2, 6)])
p1b = Polygon([(1, 2), (2, 3), (3, 4), (2, 6)])
p1c = Polygon([(2, 3), (3, 4), (2, 6), (1, 2)])
p2 = Polygon([(0, 0), (10, 0), (10, 10), (0, 10)], holes=[[(1, 1), (2, 1), (2, 2), (1, 2)]])
p3 = Polygon(p1)
p4 = p1
print p1 == p3
print p1 == p2
print p1 == p1b
print p1 == p1c
print id(p1)
print id(p4)
print id(p1b)
print id(p1c)
print id(p3)

# does shapely split an horizontal hourglass into a MultiPolyogn of two triangles?
b = box(0, 0, 10, 10)
hourglass = b - ( Polygon([(0, 0), (10, 0), (5, 5)]) | Polygon([(10, 10), (0, 10), (5, 5)]) )
hourglass.wkt

# POINT TESTS

MultiPoint([(0, 0), (1,2)]).contains(Point(0,0))  # True
MultiPoint([(0, 0), (1,2)]).touches(Point(0,0))  # False


l = LineString([(0, 0), (20, 20)])
l2 = LineString([(0, 0), (20, 20)])
lb = LineString([(20, 20), (0, 0)])
l3 = LineString(l)
print l == l2
print l == l3
print l == lb

# intersect Polygon with LineString
from ippy.utils.iterables import flatten
i = p2 & LineString([(0, 0), (20, 20)])
[list(l.coords) for l in i]
# list(flatten((l.coords for l in i)))

ml = MultiLineString([[(0, 0), (2, 0)], [(2, 0), (0, 0)], [(0, 0), (2, 0)]])
ml2 = linemerge(ml)
um = unary_union(ml)

mp = MultiPolygon([p1, p2])
[list(line.coords) for line in list(mp.boundary)]
from ippy.view.plot import plot_linestring_colors
plot_linestring_colors((mp.boundary, 'blue'))
from shapely.ops import linemerge, unary_union
merged_mp = linemerge(mp.boundary)
[list(line.coords) for line in list(merged_mp)]

for l in p.boundary:
    print(list(l.coords))

r = LinearRing([(1, 2.25), (2, 3), (0.54, 4)])
list(r.coords)

r2 = LinearRing([(-2, -1), (0, -4), (0, 0)])

s = LineString([(1, 2), (2, 3), (0, 4)])
list(s.coords)

from shapely.geometry import Point, LineString, LinearRing, box, Polygon, MultiPolygon

from ippy.view.plot import plot_linestring_colors
from ippy.view.plot import plot_polygon_colors

p = Polygon([(0, 0), (10, 0), (10, 10), (0, 10)], holes=[[(1, 1), (2, 1), (2, 2), (1, 2)]])
s = LineString([(1, 2), (2, 3), (0, 4)])
plot_polygon_colors((p, 'red'))
plot_linestring_colors((s, 'blue'))

mp = MultiPolygon([p])
plot_polygon_colors((mp, 'red'))

# plot_polygon_colors((MultiPolygon([Polygon(coords) for coords in coords_list]), 'red'))


from shapely.geometry import Point, LineString, LinearRing, box, MultiLineString
from shapely.ops import linemerge, polygonize, unary_union

l = LineString([(0, 0), (1, 0), (1, 1)])
l1 = LineString([(0, 0), (1, 0)])
l2 = LineString([(0, 0), (0, 1)])
ml = MultiLineString([l1, l2])
for x in ml:
    print(x)
l.equals(ml)
gc = GeometryCollection([l1, l2])
gc = GeometryCollection([l1, l2, ml])

nfp_part = MultiLineString([[(8, -6), (8, 2), (-14, 2), (-14, -6), (8, -6)], [(8, -2), (2, -2)]])
for i, part in enumerate(nfp_part):
    print(str(i) + ': ' + str(part))

ml1 = MultiLineString([[(0, 0), (1, 0)], [(1.5, 0), (3, 0)]])
mp1 = MultiPoint([(0, -1), (0, 1)])
ml2 = MultiLineString([[(0, 0), (1.5, 0)], [(2, -1), (2, 1)]])
mp2 = MultiPoint([(0, -1), (1, 1)])
plot_linestring_colors(
    (ml1, 'green'),
    (ml2, 'purple'),
)

ml_u = ml1 | ml2
mp_u = mp1 | mp2
plot_linestring_colors(
    (ml_u, 'blue'),
)

ml_i = ml1 & ml2
mp_i = mp1 & mp2
plot_linestring_colors(
    (ml_i, 'red'),
)

plot_linestring_colors(
    (ml1, 'green'),
    (Point(0, 0), 'green'),
)

linemerge(ml).equals(linemerge(gc))

from shapely.wkt import loads
from ippy.geometry.polytri import earclip_shapely_polygon
from shapely.ops import triangulate
from timeit import timeit

p_wkt = 'POLYGON ()'
p_wkt = 'POLYGON ((0 0, 10 0, 10 2, 5 2, 5 4, 10 4, 10 10, 0 10, 0 0), (2 6, 4 6, 4 8, 2 8, 2 6), (5 5, 9 5, 9 8, 5 8, 5 5))'
p = loads(p_wkt)
list(p.exterior.coords)
[list(h.coords) for h in p.interiors]

# Inkscape (shapely coords) -> WKT
from shapely.geometry import Polygon, MultiPolygon
from shapely import wkt
from math import sqrt

coords = []
coords = [(0, 0), (8, 0), (4, 4), (0, 0)]
coords = [(0, 0), (4*sqrt(2), 0), (2*sqrt(2), 2*sqrt(2)), (0, 0)]
coords = [(0, 0), (4, 0), (2, 2), (0, 0)]
coords = [(0, 0), (2*sqrt(2), 0), (2*sqrt(2), 2*sqrt(2)), (0, 2*sqrt(2), (0, 0))]
coords = [(0, 0), (4, 0), (6, 2), (2, 2), (0, 0)]
p = Polygon(coords)
p.wkt
wkt.dumps(p, trim=False)

golden_triangle_list = [((6, 4), (0, 0), (10, 0)),
                        ((6, 4), (10, 0), (10, 14)),
                        ((10, 14), (0, 14), (6, 10)),
                        ((6, 4), (10, 14), (6, 10))]

earclip_triangulation = earclip_shapely_polygon(p)
delaunay_triangulation = triangulate(p)  # eliminate outside triangles or use a constrained/conforming Delaunay!

# out: 34
timeit('earclip_triangulation = earclip_shapely_polygon(p)', setup='from shapely.wkt import loads; '
                 'from ippy.geometry.polytri import earclip_shapely_polygon; '
                 'p = loads("POLYGON ((0 0, 10 0, 10 14, 0 14, 6 10, 6 4, 0 0))"); ')

# out: 92 (since Delaunay triangulation is a better triangulation, and also triangulate the exterior part of the convex hull)
timeit('delaunay_triangulation = triangulate(p)', setup='from shapely.wkt import loads; '
                                                         'from shapely.ops import triangulate; '
                                                         'p = loads("POLYGON ((0 0, 10 0, 10 14, 0 14, 6 10, 6 4, 0 0))"); ')

import numpy as np
from numpy import float_ as fl
fl(1.) == fl(1.000000000000001)  # False
fl(1.) == fl(1.0000000000000001)  # True (actually due to Python, not numpy)

# %timeit ccw(fl(l[0]), fl(l[1]), p)
# %timeit np.dot(fl(l[1])-fl(l[0]), fl(p) - fl(l[0]))

# %%timeit
get_points_orientation(fl(l[0]), fl(l[1]), p)
np.dot(fl(l[1])-fl(l[0]), fl(p) - fl(l[0]))

fl(l)

sp = Point(p)
sl = LineString(l)
# %timeit sl.contains(sp)

# %%timeit sp = Point(p)
sl = LineString(l)
sl.contains(sp)

problem = xml_to_problem("ippy/benchmark/Personal/xml_sample1.xml")
problem.lot['polygon1'].wkt
problem.metadata['nfps'][('polygon1', 'polygon1')].wkt
list(problem.metadata['nfps'][('polygon2', 'polygon2')].exterior.coords)

def f(x, y):
    pass

# f((1,))
# f([1,])
# f([1, 2)
# f([1, 2],)
# f(1, )

import matplotlib.pyplot as plt
plt.plot([1,2,3,4])
plt.ylabel('some numbers')
plt.show()

# draw a square

import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches

verts = [
    (0., 0.), # left, bottom
    (0., 1.), # left, top
    (1., 1.), # right, top
    (1., 0.), # right, bottom
    (0., 0.), # ignored
    ]



codes = [Path.MOVETO,
         Path.LINETO,
         Path.LINETO,
         Path.LINETO,
         Path.CLOSEPOLY,
         ]

path = Path(verts, codes)

fig = plt.figure()
ax = fig.add_subplot(111)
patch = patches.PathPatch(path, facecolor='orange', lw=2)
ax.add_patch(patch)
ax.set_xlim(-2,2)
ax.set_ylim(-2,2)
plt.show()


import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
fig = plt.figure()
ax = fig.add_subplot(111)
patch = patches.PathPatch(Path([(0, 0), (1, 0), (0, 1)]), edgecolor='red', facecolor='none', lw=2)
ax.add_patch(patch)
ax.set_xlim(-2,2)
ax.set_ylim(-2,2)
plt.show()

###

verts = [
    (0., 0.), # left, bottom
    (0., 1.), # left, top
    (1., 1.), # right, top
    (1., 0.), # right, bottom
    ]


codes = [Path.MOVETO,
         Path.LINETO,
         Path.LINETO,
         Path.LINETO,
         ]

fig = plt.figure()
ax = fig.add_subplot(111)
patch = patches.Polygon(list(p.exterior.coords), facecolor='orange', lw=2)
ax.add_patch(patch)
ax.set_xlim(0,5)
ax.set_ylim(0,5)
plt.show()

# I/O

from ippy.utils.files import get_full_path
from lxml import etree

mock_file_path = get_full_path('temp', 'mock_file.txt')
with etree.xmlfile(mock_file_path, encoding='ISO-8859-1') as xf:
    xf.write_declaration(standalone=True)
    with xf.element('abcd'):
        pass

# test on weird NFPs
from shapely import wkt

nfp_wkt = 'POLYGON((8 -6, 8 -2, 4 -2, 8 -2, 8 2, -10 2, -10 -2, -6 -2, -10 -2, -10 -6, 8 -6))'
nfp = wkt.loads(nfp_wkt)
list(nfp.exterior.coords)  # okay, can deal with coordinates independently
nfp & LineString([(4, 0), (4, -5)])  # ERROR, Polygon is ill-formed (thin line in interior)

lr = LinearRing([(4, 0), (4, 6), (4, 8)])
lr.wkt
lr_wkt = 'LINEARRING(8 -6, 8 -2, 4 -2, 8 -2, 8 2, -10 2, -10 -2, -6 -2, -10 -2, -10 -6, 8 -6)'
lr = wkt.loads(lr_wkt)
list(lr.coords)  # okay

list((lr & LineString([(4, 0), (4, -5)])).coords)  # okay, Point(4, -2)

nfp_wkt = 'POLYGON((8 -6, 8 -2, 12 -2, 8 -2, 8 2, -10 2, -10 -2, -12 -2, -10 -2, -10 -6, 8 -6))'
nfp = wkt.loads(nfp_wkt)
list(nfp.exterior.coords)  # okay
list((nfp & LineString([(4, 0), (4, -5)])).coords)  # ok, LineString([(4, 0), (4, -5)]) (polygon is filled)
list((nfp & LineString([(11, 0), (11, -5)])).coords)  # ERROR, Polygon is ill-formed (thin line going toward interior)
# (using 8 -1.999 when going back on the right side from the spike works fine)

# Conclusion
# LineStrings and LinearRings only can cope with spikes and other thin (size of 0) regions
# However, intersections and unions of lines are different from the same operations with filled polygons
# Two solutions

# 1 - Work with complementary shapes. Indeed, an NFP is a set of infeasible positions, and does not
# include its boundary. For shapely, it is easier to work with the set of feasible positions, which is closed.
# Since we cannot consider infinite shapes in shapely, we need to compute the IFP first and work with
# IFP \ NFP each time, so that we have a finite but coherent feasible space.
# That said, Polygon can still not handle thin regions in the outer space (when complementing an NFP,
# a segment coming inside becomes a segment going outside, see Sample3: NFP(2,3))
# So we need to have a polygon for the main interior part, and LineStrings for added thin parts outside.
# The other sense, with NFP, is impossible, because a Polygon from which we substract a Point of Line remains unchanged.
# It means that, except by storing a polygon interior and a linestring to substract separately, it is impossible
# to represent this. For a complement, however, it is possible to represent positions inside on a thin line
# because everything is reversed.
# Of course, if complemented shapes also had interior thin regions it would be problematic.
# Fortunately, it does not happen because it would correspond to an exterior thin line (protuberance)
# from an NFP, which means a rotating polygon B would go back and forth in the outside space,
# which cannot happen but when B is a point and A has a thin line, so impossible outside academic cases.

# Even when NFP have holes, the same principles should apply.
# That is still a trick but can help us for most cases for now.

# 2 - Use another geometry library that supports interiors, exteriors, thin lines, etc.
# Check CGAL
# Polygons seem pretty generic there, let's try!!

# for now, we won't use XML and just generate the problems directly with polygons and lines
# in the worst case, we can use an approximation by widening a little thin edges

substracted_shape = box(0,0,10,10) - LineString([(5, 5), (10, 5)])
list(substracted_shape.exterior.coords)
list(substracted_shape.interiors)



# DRAFT from convert.py
def workshop():
    from lxml import etree
    from ippy.utils.files import get_full_path
    benchmark_filepath = get_full_path('benchmark', 'poly_2007-04-23', 'poly1a.xml')
    tree = etree.parse(benchmark_filepath)
    root = tree.getroot()
    ns = {'g': root.nsmap[None]}
    tree.getelementpath(root[6])
    p = root.find('./g:problem', namespaces=ns)
    print(p)
    # store polygon data
    polygon_list = []
    polygons = root.iterfind('./g:polygons/g:polygon', namespaces=ns)
    for polygon in polygons:
        print(polygon)
        # assume the polygon has no holes: only consider lines
        segments = polygon.findall('./g:lines/g:segment', namespaces=ns)
        coords = [(float(segment.get("x0")), float(segment.get("x1"))) for segment in segments]
        print(coords)
        polygon_list.append(Polygon(coords))
    print(polygon_list)
    # Polygon()
    # we assume the container has only one component
    container = p.find('./g:boards/g:piece/g:component', namespaces=ns)
    print(container)
    lot = p.find('./g:lot', namespaces=ns)
    print(lot)
    pieces = lot.findall('./g:piece', namespaces=ns)
    print(pieces)

    pp = PackingProblem(container)


from ippy.tests.testsolver import BLSFailsNFPVerticesFailsTestCase

import unittest
suite = unittest.TestSuite()
suite.addTest(BLSFailsNFPVerticesFailsTestCase('test_floating_origin_minimize_bb_succeeds'))
unittest.TextTestRunner(verbosity=5).run(suite)


from ippy.view.plot import plot_geom_colors
from shapely.geometry import Point, MultiPoint, LineString, MultiLineString, LinearRing, box

# DEBUG for min_bb on poly1a.xml
plot_geom_colors((self.closed_interior, 'r'), (other.closed_interior, 'b'))
plot_geom_colors((translated_nfp, 'r'), (nfp_union, 'b'))

# DEBUG draw for try_place_polygons_at_min_bb_inside_floating_container_from_idx
plot_geom_colors(
    (box(*a_bounds), 'red'),
    (box(*b_bounds), 'blue'),
    (box(*container_bounds), 'y'),
    (feasible_positions_with_boundary, 'green'),
    (expanded_problem.expanded_lot[('piece0', 0)], 'm'),
    (tr_placed_pieces_bounding_boxes[0], 'c'),
    (tr_placed_pieces_bounding_boxes[1], 'b')
)


# Breakpoint condition:
list(get_merged_close_successive_vertices(get_exterior_coords(geom))) == get_exterior_coords(geom)

# simple AI

# from simpleai.


# deepcopy

from ippy.packmath.problem import NestingLayout, Placement
from copy import deepcopy

l1 = NestingLayout([(0, 1), (1, 0), (2, 4)])
l1.add_placement(0, 1, (2, -3), 50)
l1.add_placement(1, 0, (2, 0), 10)
print(l1)
l2 = deepcopy(l1)
print(l2)
l1.add_placement(2, 4, (4, 5))
l2.placements[(0, 1)].translation = (99, 56)
l2.placements[(1, 0)].translation[1] = (77)
print('After modifications:')
print(l1)
print(l2)

# generateDS XML class generation
"""
cd ippy
generateDS.py -o packmath/nestingxml.py benchmark/nesting.xsd
generateDS.py -o packmath/nestingimprovedxml.py benchmark/nesting_improved.xsd

"""
from ippy.utils.files import get_full_path
problem_file_path = get_full_path('benchmark', 'Personal', 'problem_bls_fails_nfpvs_succeeds.xml')

from ippy.packmath import nestingimprovedxml
nesting_root = nestingimprovedxml.parse(problem_file_path)

import sys
nesting_root.exportChildren(sys.stdout, 1)
nesting_root.exportLiteralChildren(sys.stdout, 1, '')


# floating error issues

p = Polygon([(0, 0), (1e-20, 0), (0, 1e-20)])
p.buffer(0).wkt


# PICKLE

# import pickle
import cPickle as pickle
from ippy.utils.files import get_full_path

temp_path = get_full_path('temp', 'test.txt')

d = {'a': 3, 'b': 3}
pickle.dump(d, open(temp_path, 'wb'))

e = pickle.load(open(temp_path, 'rb'))
print e

# non-existing file
a = pickle.load()


# PYTHON

class A(object):
    def __init__(self, a):
        self.a = a

    def test(self, c):
        return self.__class__(c)

    def __str__(self):
        return '<A: {}>'.format(self.a)

class B(A):
    def __init__(self, b):
        super(B, self).__init__(b)

    def __str__(self):
        return '<B: {}>'.format(self.a)

a = A(3)
b = B(2)

c = a.test(4)
d = b.test(7)
print c
print type(c)
print d
print type(d)

# partial on all arguments to prepare a constructor

import functools

class C(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return '<C: {}, {}>'.format(self.x, self.y)

c = C(1, 2)
print c
factory1 = functools.partial(C, 1)
d = factory1(4)
print d
factory2 = functools.partial(C, -1, 5)
e = factory2()
print e
