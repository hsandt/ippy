from ippy.geometry.polytri import earclip, earclip_shapely_polygon
from ippy.parser.convert import to_shapely_object_list
from ippy.utils.files import get_full_path

__author__ = 'huulong'

import unittest


class PolyTriTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'sample1a.wkt')
        polygon_list = to_shapely_object_list(wkt_filepath)
        wkt_filepath = get_full_path('benchmark', 'Personal', 'sample1b.wkt')
        convex_polygon_list = to_shapely_object_list(wkt_filepath)
        assert len(polygon_list) == 2, 'There are not exactly 2 polygons in {0}'.format(wkt_filepath)
        cls.polygon1, cls.polygon2 = polygon_list
        cls.polygon3, cls.polygon4 = convex_polygon_list

    def test_earclip_non_convex_4vertices(self):
        """Test ear-clipping algorithm with non-convex polygon, 4 vertices"""
        triangle_list = list(earclip_shapely_polygon(self.polygon1))
        golden_triangle_list = [((0, 10), (0, 0), (2, 4)),
                                ((0, 10), (2, 4), (10, 10))]
        self.assertSequenceEqual(triangle_list, golden_triangle_list)

    def test_earclip_non_convex_6vertices(self):
        """Test ear-clipping algorithm with non-convex polygon, 6 vertices"""
        triangle_list = list(earclip_shapely_polygon(self.polygon2))
        golden_triangle_list = [((6, 4), (0, 0), (10, 0)),
                                ((6, 4), (10, 0), (10, 14)),
                                ((10, 14), (0, 14), (6, 10)),
                                ((6, 4), (10, 14), (6, 10))]
        self.assertSequenceEqual(triangle_list, golden_triangle_list)

    def test_earclip_convex_4vertices(self):
        """Test ear-clipping algorithm with convex polygon, 4 vertices"""
        triangle_list = list(earclip_shapely_polygon(self.polygon3))
        golden_triangle_list = [((0, 10), (0, 0), (5, 2)),
                                ((0, 10), (5, 2), (10, 10))]
        self.assertSequenceEqual(triangle_list, golden_triangle_list)

    def test_earclip_convex_6vertices(self):
        """Test ear-clipping algorithm with convex polygon, 6 vertices"""
        triangle_list = list(earclip_shapely_polygon(self.polygon4))
        golden_triangle_list = [((0, 9), (0, 0), (14, 0)),
                                ((0, 9), (14, 0), (16, 3)),
                                ((0, 9), (16, 3), (12, 14)),
                                ((0, 9), (12, 14), (4, 16))]
        self.assertSequenceEqual(triangle_list, golden_triangle_list)

    @classmethod
    def tearDownClass(cls):
        del cls.polygon1
        del cls.polygon2
        del cls.polygon3
        del cls.polygon4


if __name__ == '__main__':
    unittest.main()
