import logging

from ippy.packmath.nestingsolver import fit_polygon_vertices_sorter, left_vertices_sorter, min_bb_position_sorter, \
    biggest_piece_variable_selector, minimum_remaining_domain_area
from ippy.packmath.problemfactory import create_bin_packing_problem_from_file, \
    create_fixed_origin_single_bin_size_problem, create_floating_origin_single_bin_size_problem, \
    create_fixed_origin_open_dimension_problem, create_expanded_packing_problem_from_file
from ippy.search.constraint import backtracking_search, backtracking_search_min_cost
from ippy.utils.files import get_full_path
from ippy.view.constraintviewer import BaseConstraintViewer, ConsoleConstraintViewer
from ippy.view.nestingconstraintviewer import NestingPlotConstraintViewer, NestingPlotConsoleConstraintViewer
from ippy.view.plot import plot_solution_with_colors

root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)

__author__ = 'hs'

import unittest


class TwoTrianglesProblemTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        xml_filepath = get_full_path('benchmark', 'Personal', 'problem_bls_fails_nfpvs_succeeds.xml')
        cls.fixed_origin_single_bin_size_problem = create_bin_packing_problem_from_file(
            xml_filepath, create_fixed_origin_single_bin_size_problem)
        cls.floating_origin_single_bin_size_problem = create_bin_packing_problem_from_file(
            xml_filepath, create_floating_origin_single_bin_size_problem)

        cls.fixed_origin_open_dimension_problem = create_bin_packing_problem_from_file(
            xml_filepath, create_fixed_origin_open_dimension_problem)

    def test_fixed_origin_single_size_backtracking_search_left(self):
        """Test backtracking search on left-most positions selection, which should succeed unlike bottom-left"""
        viewer = BaseConstraintViewer()

        placements = backtracking_search(self.fixed_origin_single_bin_size_problem, value_sorter=left_vertices_sorter, viewer=viewer)
        self.assertNotEqual(placements, None)

        logging.debug(viewer.event_to_str())
        plot_solution_with_colors(placements, {('piece0', 0): 'red', ('piece1', 0): 'blue'},
                                  expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    def test_fixed_origin_single_size_backtracking_search_fpv(self):
        viewer = BaseConstraintViewer()

        placements = backtracking_search(self.fixed_origin_single_bin_size_problem, value_sorter=fit_polygon_vertices_sorter, viewer=viewer)
        self.assertNotEqual(placements, None)

        logging.debug(viewer.event_to_str())
        plot_solution_with_colors(placements, {('piece0', 0): 'red', ('piece1', 0): 'blue'},
                                  expanded_problem=self.fixed_origin_single_bin_size_problem.data)  # DEBUG

    def test_fixed_origin_single_size_backtracking_search_min_bb(self):
        """
        Test that minimum BB search succeeds thanks to checking all corners
        of the container at the beginning

        """
        viewer = BaseConstraintViewer()

        placements = backtracking_search(self.fixed_origin_single_bin_size_problem,
                                         value_sorter=min_bb_position_sorter,
                                         forward_checking=True,
                                         check_conflicts=True,
                                         viewer=viewer)
        self.assertNotEqual(placements, None)

        logging.debug(viewer.event_to_str())
        plot_solution_with_colors(placements, {('piece0', 0): 'red', ('piece1', 0): 'blue'},
                                  expanded_problem=self.fixed_origin_single_bin_size_problem.data)  # DEBUG

    def test_floating_origin_single_size_backtracking_search_left(self):
        """Floating Origin: backtracking, left vertices"""
        viewer = BaseConstraintViewer()

        placements = backtracking_search(self.floating_origin_single_bin_size_problem, value_sorter=left_vertices_sorter,
                                         viewer=viewer)
        self.assertNotEqual(placements, None)
        plot_solution_with_colors(placements, {('piece0', 0): 'red', ('piece1', 0): 'blue'},
                                  expanded_problem=self.floating_origin_single_bin_size_problem.data, floating_container=True)


    def test_floating_origin_single_size_backtracking_search_fpv(self):
        viewer = BaseConstraintViewer()

        placements = backtracking_search(self.floating_origin_single_bin_size_problem,
                                         value_sorter=fit_polygon_vertices_sorter, viewer=viewer)
        self.assertNotEqual(placements, None)

        logging.debug(viewer.event_to_str())
        plot_solution_with_colors(placements, {('piece0', 0): 'red', ('piece1', 0): 'blue'},
                                  expanded_problem=self.floating_origin_single_bin_size_problem.data, floating_container=True)

    def test_floating_origin_single_size_backtracking_search_min_bb(self):
        viewer = BaseConstraintViewer()

        placements = backtracking_search(self.floating_origin_single_bin_size_problem,
                                         value_sorter=min_bb_position_sorter, viewer=viewer)
        self.assertNotEqual(placements, None)

        logging.debug(viewer.event_to_str())
        plot_solution_with_colors(placements, {('piece0', 0): 'red', ('piece1', 0): 'blue'},
                                  expanded_problem=self.floating_origin_single_bin_size_problem.data, floating_container=True)

    def test_fixed_origin_open_dimension_backtracking_search_fpv(self):
        viewer_class = NestingPlotConsoleConstraintViewer

        # TODO: compute sum of width of all pieces as initial cost
        best_placements, best_width, = backtracking_search_min_cost(self.fixed_origin_open_dimension_problem, 1, 20,
                                                                    value_sorter=fit_polygon_vertices_sorter,
                                                                    viewer_class=viewer_class)

        self.assertNotEqual(best_placements, None)
        self.assertEqual(best_width, 10)

        plot_solution_with_colors(best_placements, {('piece0', 0): 'red', ('piece1', 0): 'blue'},
                                  expanded_problem=self.fixed_origin_open_dimension_problem.data,
                                  container_width=best_width)


class HAndTwoRectanglesProblemTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        xml_filepath = get_full_path('benchmark', 'Personal', 'problem2_bls_fails_nfpvs_fails.xml')
        expanded_problem = create_expanded_packing_problem_from_file(xml_filepath)

        cls.fixed_origin_single_bin_size_problem = create_fixed_origin_single_bin_size_problem(expanded_problem)
        cls.floating_origin_single_bin_size_problem = create_floating_origin_single_bin_size_problem(expanded_problem)

        cls.fixed_origin_open_dimension_problem = create_fixed_origin_open_dimension_problem(expanded_problem)

    def test_fixed_origin_single_size_backtracking_search_fpv(self):
        viewer = NestingPlotConstraintViewer(self.fixed_origin_single_bin_size_problem)

        placements = backtracking_search(self.fixed_origin_single_bin_size_problem,
                                         value_sorter=fit_polygon_vertices_sorter,
                                         check_conflicts=True,
                                         viewer=viewer)
        self.assertEqual(placements, None)

        logging.debug(viewer.event_to_str())
        logging.debug(str(viewer.stats))

    # fails very soon because tries to place small pieces first, either in contact or in corners
    def test_floating_origin_single_size_backtracking_search_fpv(self):
        viewer = NestingPlotConstraintViewer(self.floating_origin_single_bin_size_problem)

        placements = backtracking_search(self.floating_origin_single_bin_size_problem,
                                         value_sorter=fit_polygon_vertices_sorter,
                                         forward_checking=True,
                                         viewer=viewer)
        # self.assertEqual(placements, None)

        logging.debug(viewer.event_to_str())
        logging.debug(str(viewer.stats))

        plot_solution_with_colors(placements, {('piece0', 0): 'cyan', ('piece0', 1): 'cyan', ('piece1', 0): 'purple'},
                                  expanded_problem=self.floating_origin_single_bin_size_problem.data,
                                  floating_container=True)


    def test_fixed_origin_single_size_backtracking_search_minbb(self):
        viewer = NestingPlotConstraintViewer(self.floating_origin_single_bin_size_problem)

        placements = backtracking_search(self.fixed_origin_single_bin_size_problem,
                                         value_sorter=min_bb_position_sorter,
                                         check_conflicts=True,
                                         viewer=viewer)
        self.assertEqual(placements, None)

        logging.debug(viewer.event_to_str())
        logging.debug(str(viewer.stats))

    def test_floating_origin_single_size_backtracking_search_min_bb(self):
        """
        Test failure of floating origin min BB on single size problem,
        due to the lack of piece ordering.

        """
        viewer = NestingPlotConstraintViewer(self.floating_origin_single_bin_size_problem)

        placements = backtracking_search(self.floating_origin_single_bin_size_problem,
                                         value_sorter=min_bb_position_sorter, viewer=viewer)

        logging.debug(viewer.event_to_str())
        logging.debug(str(viewer.stats))

        self.assertEqual(placements, None)

    # SUCCESS: optimal width found (18)
    def test_floating_origin_single_size_backtracking_search_big_first_min_bb(self):
        """Test success of min BB combined with biggest piece first"""
        viewer = NestingPlotConstraintViewer(self.floating_origin_single_bin_size_problem)

        placements = backtracking_search(self.floating_origin_single_bin_size_problem,
                                         variable_selector=biggest_piece_variable_selector,
                                         value_sorter=min_bb_position_sorter,
                                         forward_checking=True,
                                         viewer=viewer)

        logging.debug(viewer.event_to_str())
        logging.debug(str(viewer.stats))

        self.assertNotEqual(placements, None)
        plot_solution_with_colors(placements, {('piece0', 0): 'cyan', ('piece0', 1): 'cyan', ('piece1', 0): 'purple'},
                                  expanded_problem=self.floating_origin_single_bin_size_problem.data,
                                  floating_container=True)

    # Finds solution, but too easy to see a difference with other variable selectors
    def test_fixed_origin_single_size_backtracking_search_min_domain_area_first_fpv(self):
        """Test fixed origin, single size problem: minimum remaining domain area, FPV"""
        viewer = NestingPlotConstraintViewer(self.fixed_origin_single_bin_size_problem)

        placements = backtracking_search(self.fixed_origin_single_bin_size_problem,
                                         variable_selector=minimum_remaining_domain_area,
                                         value_sorter=fit_polygon_vertices_sorter,
                                         forward_checking=True,
                                         viewer=viewer)

        logging.debug(viewer.event_to_str())
        logging.debug(str(viewer.stats))

        # self.assertNotEqual(placements, None)
        plot_solution_with_colors(placements, {('piece0', 0): 'cyan', ('piece0', 1): 'cyan', ('piece1', 0): 'purple'},
                                  expanded_problem=self.floating_origin_single_bin_size_problem.data,
                                  floating_container=False)

    def test_fixed_origin_open_dimension_backtracking_search_fpv(self):
        """Test best width obtained: 22 (not optimal)"""
        viewer = NestingPlotConstraintViewer(self.fixed_origin_open_dimension_problem)

        # TODO: compute sum of width of all pieces as initial cost
        best_placements, best_width, = backtracking_search_min_cost(self.fixed_origin_open_dimension_problem, 1, 50,
                                                                    value_sorter=fit_polygon_vertices_sorter,
                                                                    viewer_class=viewer)

        logging.debug('- Best assignment events below -')
        logging.debug(viewer.event_to_str())
        logging.debug(str(viewer.stats))

        self.assertNotEqual(best_placements, None)
        self.assertEqual(best_width, 22)

        # since the actual width is deduced from the placement and the whole placement
        # may be offset from the origin, we move the container in case it is offset to the right
        # plot_solution_with_colors(best_placements, {('piece0', 0): 'cyan', ('piece0', 1): 'cyan', ('piece1', 0): 'purple'},
        #                           expanded_problem=self.fixed_origin_open_dimension_problem.data,
        #                           floating_width=True, container_width=best_width)


    # best cost 64.94 (lower bound 64.05)
    # with backtracking, 726 backtracks, 807 assignments, 20 forward_check_empty, 812 iterations
    def test_fixed_origin_open_dimension_backtracking_search_min_bb(self):
        """Test best width obtained: 22 (not optimal)"""
        viewer_class = NestingPlotConsoleConstraintViewer

        best_placements, best_width, = backtracking_search_min_cost(self.fixed_origin_open_dimension_problem, 1, 50,
                                                                    value_sorter=min_bb_position_sorter,
                                                                    viewer_class=viewer_class)

        logging.debug('- Best assignment events below -')

        self.assertNotEqual(best_placements, None)
        self.assertEqual(best_width, 22)

        # since the actual width is deduced from the placement and the whole placement
        # may be offset from the origin, we move the container in case it is offset to the right
        # plot_solution_with_colors(best_placements, {('piece0', 0): 'cyan', ('piece0', 1): 'cyan', ('piece1', 0): 'purple'},
        #                           expanded_problem=self.fixed_origin_open_dimension_problem.data,
        #                           floating_width=True, container_width=best_width)

    # fails... open dimension can only help since for now it acts as a multi CSP, I do not understand why
    def test_fixed_origin_open_dimension_backtracking_search_min_bb_bigger_first(self):
        """Test best width obtained: 22 (not optimal)"""
        viewer_class = NestingPlotConsoleConstraintViewer

        best_placements, best_width, = backtracking_search_min_cost(self.fixed_origin_open_dimension_problem, 1, 50,
                                                                    variable_selector=biggest_piece_variable_selector,
                                                                    value_sorter=min_bb_position_sorter,
                                                                    viewer_class=viewer_class)

        logging.debug('- Best assignment events below -')

        self.assertNotEqual(best_placements, None)
        self.assertEqual(best_width, 22)

        # since the actual width is deduced from the placement and the whole placement
        # may be offset from the origin, we move the container in case it is offset to the right
        plot_solution_with_colors(best_placements, {('piece0', 0): 'cyan', ('piece0', 1): 'cyan', ('piece1', 0): 'purple'},
                                  expanded_problem=self.fixed_origin_open_dimension_problem.data,
                                  floating_width=True, container_width=best_width)


class ShirtsProblemTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        xml_filepath = get_full_path('benchmark', 'improved_xml', 'shirts_improved.xml')
        expanded_problem = create_expanded_packing_problem_from_file(xml_filepath)

        cls.fixed_origin_single_bin_size_problem = create_fixed_origin_single_bin_size_problem(expanded_problem)
        cls.floating_origin_single_bin_size_problem = create_floating_origin_single_bin_size_problem(expanded_problem)

        cls.fixed_origin_open_dimension_problem = create_fixed_origin_open_dimension_problem(expanded_problem)

    def test_fixed_origin_backtracking_search_fpv(self):
        viewer = ConsoleConstraintViewer()

        placements = backtracking_search(self.fixed_origin_single_bin_size_problem,
                                         value_sorter=fit_polygon_vertices_sorter,
                                         check_conflicts=False, viewer=viewer)
        self.assertNotEqual(placements, None)
        logging.debug('- Best assignment events below -')
        logging.debug(viewer.event_to_str())
        logging.debug(str(viewer.stats))
        plot_solution_with_colors(placements, expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    # TODO: VERY SLOW, need to PROFILE!!
    def test_fixed_origin_backtracking_search_min_domain_area_fpv(self):
        viewer = NestingPlotConstraintViewer(self.fixed_origin_single_bin_size_problem)

        placements = backtracking_search(self.fixed_origin_single_bin_size_problem,
                                         variable_selector=minimum_remaining_domain_area,
                                         value_sorter=fit_polygon_vertices_sorter,
                                         forward_checking=True,
                                         check_conflicts=False, viewer=viewer)

        self.assertNotEqual(placements, None)
        logging.debug('- Best assignment events below -')
        logging.debug(viewer.event_to_str())
        logging.debug(str(viewer.stats))
        plot_solution_with_colors(placements, expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    # Result: solution found for around 80
    def test_fixed_origin_backtracking_search_bigger_first_fpv(self):
        viewer = NestingPlotConstraintViewer(self.fixed_origin_single_bin_size_problem)

        placements = backtracking_search(self.fixed_origin_single_bin_size_problem,
                                         variable_selector=biggest_piece_variable_selector,
                                         value_sorter=fit_polygon_vertices_sorter,
                                         check_conflicts=False, viewer=viewer)
        self.assertNotEqual(placements, None)
        logging.debug('- Best assignment events below -')
        logging.debug(viewer.event_to_str())
        logging.debug(str(viewer.stats))
        plot_solution_with_colors(placements, expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    # solution found for actual width ~68
    def test_fixed_origin_backtracking_search_left(self):
        viewer = ConsoleConstraintViewer()

        placements = backtracking_search(self.fixed_origin_single_bin_size_problem, value_sorter=left_vertices_sorter, viewer=viewer)
        self.assertNotEqual(placements, None)

        logging.debug('- Best assignment events below -')
        logging.debug(viewer.event_to_str())
        logging.debug(str(viewer.stats))
        plot_solution_with_colors(placements, expanded_problem=self.fixed_origin_single_bin_size_problem.data)

    # no specific bottleneck, but too many backtracking -> veeery slow
    def test_fixed_origin_open_dimension_backtracking_search_fpv(self):
        viewer_class = NestingPlotConsoleConstraintViewer

        # TODO: compute sum of width of all pieces as initial cost
        best_placements, best_width, = backtracking_search_min_cost(self.fixed_origin_open_dimension_problem, 1, 70,
                                                                    value_sorter=fit_polygon_vertices_sorter,
                                                                    viewer_class=viewer_class)
        self.assertNotEqual(best_placements, None)
        # self.assertEqual(best_width, 1000)

        plot_solution_with_colors(best_placements, expanded_problem=self.fixed_origin_open_dimension_problem.data, container_width=best_width)

    # initial solution for w=70 found after 15 sec (since left finds one normally)
    # the second test never ends, because:
    # - min cost of 0 is absurd, the dichotomy is too slow
    # - in general it is hard to find a solution at the limit of the best width
    # and we don't know if we are just below that: stop after a nb of iterations
    # especially if the partial assignments are not very advanced!

    def test_fixed_origin_open_dimension_backtracking_search_left(self):
        viewer_class = NestingPlotConsoleConstraintViewer

        # TODO: compute sum of width of all pieces as initial cost
        best_placements, best_width, = backtracking_search_min_cost(self.fixed_origin_open_dimension_problem, 1, 70,
                                                                    value_sorter=left_vertices_sorter,
                                                                    viewer_class=viewer_class)
        self.assertNotEqual(best_placements, None)
        # self.assertEqual(best_width, 1000)

        plot_solution_with_colors(best_placements, expanded_problem=self.fixed_origin_open_dimension_problem.data, container_width=best_width)

    # best solution for cost: 65.5888888889
    def test_fixed_origin_open_dimension_backtracking_search_search_big_first_min_bb(self):
        viewer = NestingPlotConstraintViewer(self.fixed_origin_open_dimension_problem)

        # TODO: compute sum of width of all pieces as initial cost
        best_placements, best_width, = backtracking_search_min_cost(self.fixed_origin_open_dimension_problem, 1, 70,
                                                                    variable_selector=biggest_piece_variable_selector,
                                                                    value_sorter=min_bb_position_sorter,
                                                                    forward_checking=True, viewer_class=viewer_class)
        # self.assertNotEqual(best_placements, None)
        # self.assertEqual(best_width, 1000)

        # logging.debug(str(viewer.stats))

        plot_solution_with_colors(best_placements,
                                  expanded_problem=self.fixed_origin_open_dimension_problem.data,
                                  floating_width=True,
                                  container_width=best_width)


if __name__ == '__main__':
    unittest.main()
