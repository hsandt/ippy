import logging
import math
from shapely.affinity import translate
from ippy.packmath.nfp import nfp_cuninghame_green

from ippy.packmath.phifunction import phi, phi_convex_convex, phi_nfp
from ippy.parser.convert import to_shapely_object_list
from ippy.utils.files import get_full_path

from shapely.geometry.polygon import Polygon

__author__ = 'huulong'

import unittest

class PhiTestBasicShapeCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'sample0.wkt')
        polygon_list = to_shapely_object_list(wkt_filepath)
        cls.polygon1, cls.polygon2, cls.polygon3, cls.polygon4, cls.polygon5, cls.polygon6 = polygon_list
        # precompute NFPs in convex case
        cls.meta_nfp11 = nfp_cuninghame_green(cls.polygon1, cls.polygon1)
        # debug: manual input
        cls.meta_nfp11 = Polygon([(10, -10), (10, 10), (-10, 10), (-10, -10)])

    def test_nfp_cuninghame(self):
        pass

    @classmethod
    def tearDownClass(cls):
        pass


class PhiTestBasicShapeCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'wkt_sample1.wkt')
        polygon_list = to_shapely_object_list(wkt_filepath)

        cls.container, cls.polygon1, cls.polygon2, _, _ = polygon_list
        cls.meta_nfp11 = nfp_cuninghame_green(cls.polygon1, cls.polygon1)

    def test_intersection_convex0(self):
        """Test if phi returns negative value for intersecting polygons"""
        translation = (5, 0)
        translated_polygon = translate(self.polygon1, *translation)
        phi_nfp_value = phi_nfp(self.polygon1, translated_polygon, translation, self.meta_nfp11)
        self.assertAlmostEqual(phi_nfp_value, -5)

    def test_no_intersection_convex(self):
        """Test if phi returns positive value for non-intersecting polygons"""
        translation = (13, 2)
        translated_polygon = translate(self.polygon1, *translation)
        phi_nfp_value = phi_nfp(self.polygon1, translated_polygon, translation, self.meta_nfp11)
        self.assertAlmostEqual(phi_nfp_value, +3.0)

    def test_contact_convex(self):
        """Test if phi returns almost null value for polygons in contact"""
        translation = (8, 5)
        translated_polygon = translate(self.polygon1, *translation)
        phi_nfp_value = phi_nfp(self.polygon1, translated_polygon, translation, self.meta_nfp11)
        self.assertAlmostEqual(phi_nfp_value, 0)

    def test_intersection_convex(self):
        """Test if phi returns negative value for intersecting polygons"""
        translation = (-5, 0)
        translated_polygon = translate(self.polygon1, *translation)
        phi_nfp_value = phi_nfp(self.polygon1, translated_polygon, translation, self.meta_nfp11)
        self.assertAlmostEqual(phi_nfp_value, -9.0/math.sqrt(17))

    def test_contact2_convex(self):
        """Test if phi returns negative value for intersecting polygons"""
        translation = (8, 0)
        translated_polygon = translate(self.polygon1, *translation)
        phi_nfp_value = phi_nfp(self.polygon1, translated_polygon, translation, self.meta_nfp11)
        self.assertAlmostEqual(phi_nfp_value, 0)

    def test_triangulation_convex(self):
        """Test if triangulating a convex polygon to calculate phi gives the same result as without"""
        # not a Euclidian distance, tessellation altered the result;
        # the minimum test is that the phi-value is positive, but you can estimate the maximum
        # of [the most relevant vertex-edge distance] with vertex from one polygon, edge from the
        # other and the reverse
        self.assertAlmostEqual(phi(self.polygon1, self.polygon2, (11, 1)), 0.8, delta=0.1)

    @classmethod
    def tearDownClass(cls):
        del cls.polygon1
        del cls.polygon2


class PhiTestConvexConvexCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'sample1b.wkt')
        polygon_list = to_shapely_object_list(wkt_filepath)
        assert len(polygon_list) == 2, 'There are not exactly 2 polygons in {0}'.format(wkt_filepath)
        cls.polygon1, cls.polygon2 = polygon_list

    def test_no_intersection_convex(self):
        """Test if phi returns positive value for non-intersecting polygons"""
        translated_polygon2 = translate(self.polygon2, 11, 1)
        phi_value = phi_convex_convex(self.polygon1.exterior.coords, translated_polygon2.exterior.coords)
        self.assertAlmostEqual(phi_value, 1.0)

    def test_contact_convex(self):
        """Test if phi returns almost null value for polygons in contact"""
        translated_polygon2 = translate(self.polygon2, 10, 1)
        phi_value = phi_convex_convex(self.polygon1.exterior.coords, translated_polygon2.exterior.coords)
        self.assertAlmostEqual(phi_value, 0)

    def test_intersection_convex(self):
        """Test if phi returns negative value for intersecting polygons"""
        translated_polygon2 = translate(self.polygon2, 9, 2)
        phi_value = phi_convex_convex(self.polygon1.exterior.coords, translated_polygon2.exterior.coords)
        self.assertAlmostEqual(phi_value, -1.0)

    @classmethod
    def tearDownClass(cls):
        del cls.polygon1
        del cls.polygon2


class PhiTestNonConvexNonConvexCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'sample1a.wkt')
        polygon_list = to_shapely_object_list(wkt_filepath)
        assert len(polygon_list) == 2, 'There are not exactly 2 polygons in {0}'.format(wkt_filepath)
        cls.polygon1, cls.polygon2 = to_shapely_object_list(wkt_filepath)

    def test_no_intersection(self):
        """Test if phi returns positive value for non-intersecting polygons"""
        phi_value = phi(self.polygon1, self.polygon2, (5, 4), 0)
        self.assertAlmostEqual(phi_value, 1)  # vertex-edge distance, phi-value = euclidian distance

    def test_contact(self):
        """Test if phi returns almost null value for polygons in contact"""
        phi_value = phi(self.polygon1, self.polygon2, (6, 7), 0)
        self.assertAlmostEqual(phi_value, 0)

    def test_intersection(self):
        """Test if phi returns negative value for intersecting polygons"""
        phi_value = phi(self.polygon1, self.polygon2, (2, 1), 0)
        self.assertAlmostEqual(phi_value, -2)  # vertex-edge distance, phi-value = euclidian distance

    def test_triangulation_convex(self):
        """Test if triangulating a convex polygon to calculate phi gives the same result as without"""
        # not a Euclidian distance, tessellation altered the result;
        # the minimum test is that the phi-value is positive, but you can estimate the maximum
        # of [the most relevant vertex-edge distance] with vertex from one polygon, edge from the
        # other and the reverse
        self.assertAlmostEqual(phi(self.polygon1, self.polygon2, (11, 1)), 0.8, delta=0.1)

    @classmethod
    def tearDownClass(cls):
        del cls.polygon1
        del cls.polygon2


if __name__ == '__main__':
    unittest.main()
