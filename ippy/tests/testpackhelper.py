from shapely.geometry import MultiLineString, Point, MultiPoint
from ippy.packmath.packhelper import pick_regular_vertices

__author__ = 'hs'

import unittest


class TestPackHelperTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    def test_get_lot_bounds_with_placements(self):
        # TODO
        self.assertEqual(True, False)

    def test_get_lot_bounds(self):
        # TODO
        self.assertEqual(True, False)

    def test_pick_regular_vertices(self):
        # if we implement this, redundancies will also be removed
        linegeom = MultiLineString([[(0, 0), (0, 1)], [(1, 2), (1, 0), (0, 0)]]) | MultiPoint([(-1, -1), (10, 10)])
        interval = 0.6
        regular_vertices = pick_regular_vertices(linegeom, interval)

        # compare in any order...
        golden_regular_vertices = [
            (0, 0), (0, 0.5), (0, 1),
            (1, 2), (1, 1.5), (1, 1), (1, 0.5), (1, 0.),
            (1, 0), (0.5, 0), (0, 0),
            (-1, -1),
            (10, 10)
        ]
        self.assertItemsEqual(regular_vertices, golden_regular_vertices)

if __name__ == '__main__':
    unittest.main()
