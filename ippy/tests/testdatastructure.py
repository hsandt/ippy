import heapq
from ippy.utils.datastructure import BoundedPriorityQueue, PriorityQueueWithRandomizedFunction, \
    add_random_variation_values, PriorityQueue, Queue, Stack

__author__ = 'huulong'

import unittest


class DataStructureTestCase(unittest.TestCase):

    def test_stack_split_into_copies(self):
        stack = Stack()
        for i in xrange(10):
            stack.push(i)
        parts = stack.split_into_copies(3, 3)
        self.assertEqual(parts, [Stack([7, 8, 9]), Stack([4, 5, 6]), Stack([0, 1, 2, 3])])

    def test_queue_split_into_copies(self):
        queue = Queue()
        for i in xrange(10):
            queue.push(i)
        parts = queue.split_into_copies(3, 3)
        self.assertEqual(parts, [Queue([2, 1, 0]), Queue([5, 4, 3]), Queue([9, 8, 7, 6])])

    def test_pq_split_into_copies(self):
        pq = PriorityQueue()
        for i, l in enumerate(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']):
            pq.push(l, i)
        parts = pq.split_into_copies(4, 2)
        self.assertEqual(parts, [
            PriorityQueue([(0, 0, 'a'), (1, 1, 'b')], 9), PriorityQueue([(2, 2, 'c'), (3, 3, 'd')], 9),
            PriorityQueue([(4, 4, 'e'), (5, 5, 'f')], 9),
            PriorityQueue([(6, 6, 'g'), (7, 7, 'h'), (8, 8, 'i')], 9)
        ])

    def test_pq_split_into_copies_exact(self):
        pq = PriorityQueue()
        for i, l in enumerate(['a', 'b', 'c', 'd', 'e', 'f']):
            pq.push(l, i)
        parts = pq.split_into_copies(3, 2)
        self.assertEqual(parts, [
            PriorityQueue([(0, 0, 'a'), (1, 1, 'b')], 6), PriorityQueue([(2, 2, 'c'), (3, 3, 'd')], 6),
            PriorityQueue([(4, 4, 'e'), (5, 5, 'f')], 6)
        ])

    def test_pq_split_into_copies_too_much(self):
        pq = PriorityQueue()
        for i, l in enumerate(['a', 'b', 'c', 'd', 'e']):
            pq.push(l, i)
        parts = pq.split_into_copies(4, 2)
        self.assertEqual(parts, [
            PriorityQueue([(0, 0, 'a'), (1, 1, 'b')], 5), PriorityQueue([(2, 2, 'c'), (3, 3, 'd')], 5),
            PriorityQueue([(4, 4, 'e')], 5)  # only 3 parts instead of 4
        ])

    def test_bounded_priority_queue_push(self):
        # counter-example from Scrivener: Data Structure/Bounded Priority Queue
        # (use a dummy item, only priority matters)
        bounded_prioqueue = BoundedPriorityQueue(6)
        # to control exactly how elements are placed, place them by hand but check
        # validity
        #      0
        #   6     3
        # 7  8  4
        # do not forget to add count manually
        bounded_prioqueue.heap = [(0, 0, 'a'), (6, 1, 'b'), (3, 2, 'c'), (7, 3, 'd'), (8, 4, 'e'), (4, 5, 'f')]
        bounded_prioqueue.count = 6
        # not sure how to check invariant... heapify and hope nothing changes?

        # insert new node; 8 should be removed automatically
        bounded_prioqueue.push('g', 1)

        # check resulting heap
        #     0
        #  4      1
        # 7 6   3
        self.assertEqual(bounded_prioqueue.heap, [(0, 0, 'a'), (4, 5, 'f'), (1, 6, 'g'), (7, 3, 'd'), (6, 1, 'b'), (3, 2, 'c')])

    def test_randomized_priority_queue_with_function(self):
        random_pq = PriorityQueueWithRandomizedFunction(sum, 0.1)  # variation between -0.1 and +0.1
        random_pq.push([1, 2])
        random_pq.push([1, 2])
        random_pq.push([1, 3])
        random_pq.push([0, 3])
        random_pq.push([0, 2])
        print random_pq  # human verification
        # [0, 2] has the smallest sum, by more than 0.1 * 2
        self.assertEqual(random_pq.pop(), [0, 2])
        # the 3 next pops will pop [1, 2] twice, [0, 3] once, in any order (sum is 3, +variation)
        self.assertIn(random_pq.pop(), ([1, 2], [0, 3]))
        self.assertIn(random_pq.pop(), ([1, 2], [0, 3]))
        self.assertIn(random_pq.pop(), ([1, 2], [0, 3]))
        self.assertEqual(random_pq.pop(), [1, 3])

    def test_add_random_variation_values(self):
        values = [1, 2, (3, (4, 0))]
        randomized_values = add_random_variation_values(values, 0.5)
        print randomized_values  # human verification
        self.assertGreaterEqual(values[0], 0.5)
        self.assertLessEqual(values[0], 1.5)
        self.assertGreaterEqual(values[1], 1.5)
        self.assertLessEqual(values[1], 2.5)
        self.assertGreaterEqual(values[2][0], 2.5)
        self.assertLessEqual(values[2][0], 3.5)
        self.assertGreaterEqual(values[2][1][0], 3.5)
        self.assertLessEqual(values[2][1][0], 4.5)
        self.assertGreaterEqual(values[2][1][1], -0.5)
        self.assertLessEqual(values[2][1][1], 1.5)

    def test_prioqueue_pop_kth_smallest(self):
        pq = PriorityQueue()
        pq.push('A', 1)
        pq.push('B', 3)
        pq.push('C', 0)
        pq.push('D', 2)
        pq.push('E', -1)
        print pq
        self.assertEqual(pq.pop_kth_smallest(3), 'A')
        self.assertEqual(pq.pop_kth_smallest(3), 'D')
        self.assertEqual(pq.pop_kth_smallest(2), 'C')
        self.assertEqual(pq.pop_kth_smallest(3), 'B')  # item with biggest priority as index beyond heap length
        self.assertEqual(pq.pop_kth_smallest(1), 'E')

    def test_prioqueue_pop_kth_smallest_empty_heap_index_error(self):
        pq = PriorityQueue()
        self.assertRaises(IndexError, pq.pop_kth_smallest, 1)

    def test_prioqueue_pop_kth_smallest_non_positive_index_error(self):
        pq = PriorityQueue()
        self.assertRaises(IndexError, pq.pop_kth_smallest, 0)

if __name__ == '__main__':
    unittest.main()
