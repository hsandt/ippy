from collections import Counter
from numpy.testing import assert_array_equal
import numpy as np
from numpy import float_ as fl
from shapely import wkt
from shapely.geometry import box, MultiPolygon, Point, LineString, MultiPoint, MultiLineString
from ippy.packmath.nestingconstraintproblem import NestingCache, InputMinimizationNestingProblem, NestingDomain
from ippy.packmath.nestingconstraintsearch import ValuePickingNestingSearchProblem, NestingAssignmentSearchNode, \
    get_domain_size_orientation_average, get_region_fitting_pieces, get_living_space_area, get_dead_regions, \
    get_min_bb_translations_along, area_based_final_length_estimation, current_usage_based_final_length_estimation
from ippy.packmath.nestingsolver import _get_fit_polygon
from ippy.packmath.problem import Piece, Placement, PieceKey
from ippy.packmath.problemfactory import create_expanded_packing_problem_from_file, \
    create_fixed_origin_single_bin_size_problem, create_fixed_origin_open_dimension_problem
from ippy.parser.convert import to_shapely_object_list
from ippy.utils.files import get_full_path
from ippy.utils.hashabledict import hashdict
from ippy.utils.shapelyextension import PolygonExtended, check_equal, PolygonWithBoundary


__author__ = 'hs'

import unittest


class NestingConstraintSearchTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # REFACTOR: too many ops for a unit test; mock that as much as you can
        xml_filepath = get_full_path('benchmark', 'Personal', 'tangram', 'problem_tangram.xml')
        cls.expanded_problem = create_expanded_packing_problem_from_file(xml_filepath)
        cls.fixed_origin_single_bin_size_problem = create_fixed_origin_single_bin_size_problem(cls.expanded_problem)
        cls.value_picking_nsp = ValuePickingNestingSearchProblem(cls.fixed_origin_single_bin_size_problem)

        # see bug "iteration 34 dead zone not recognized.png" all but the 2nd small triangle at the top
        mock_assignment = hashdict({
            PieceKey('bigTriangle', 0): Placement((0, 0)),
            PieceKey('bigTriangle', 1): Placement((8, 0), 90),
            PieceKey('mediumTriangle', 0): Placement((4, 4), 135),
            PieceKey('smallTriangle', 0): Placement((0, 1.17157287525381), 45),
        })
        # unfortunately NestingAssignmentSearchNode builds next_piece_inst_idx_dict incrementally
        # so we need to pass it ourselves for now (the most important is the buggy square at the time of writing)
        next_piece_inst_idx_dict = {
            'smallTriangle': 1,
            'parallelogram': 0,
            'square': 0,
        }
        cls.mock_node = NestingAssignmentSearchNode(mock_assignment, problem=cls.value_picking_nsp, cached_values=[],
                                                    dynamic=False)
        cls.mock_node.next_piece_inst_idx_dict = next_piece_inst_idx_dict

        cls.expected_dead_region = PolygonExtended(
            wkt.loads('POLYGON ((0 0, 0 1.17157287525381, 2.82842712474619 4, 4 4, 0 0))'))
        cls.expected_living_region = PolygonExtended(wkt.loads('POLYGON ((0 8, 8 8, 4 4, 0 8))'))

        # update cache free space artificially
        # node = NestingAssignmentSearchNode()
        # too long, will just set it to what I want (if the bug comes from this step, I will redo it)
        cls.mock_node.cache.free_space = MultiPolygon([cls.expected_dead_region, cls.expected_living_region])

        # compute domains with _get_fit_polygon, ie IFP = union of NFPs
        cp = cls.mock_node.problem.cp
        for piece_key in cp.get_unassigned_variables(cls.mock_node.assignment):
            for orientation in cp.pieces[piece_key.id].orientations:
                cls.mock_node.domains[piece_key][orientation] = _get_fit_polygon(piece_key.id, orientation,
                                                                                 cls.mock_node.assignment, cp,
                                                                                 cls.mock_node.cache)

    def test_nesting_assignment_search_node_initial_init(self):
        initial_node = NestingAssignmentSearchNode(problem=self.value_picking_nsp, dynamic=False)
        golden_next_piece_inst_idx_dict = {
            'bigTriangle': 0,
            'mediumTriangle': 0,
            'smallTriangle': 0,
            'parallelogram': 0,
            'square': 0,
        }
        self.assertEqual(initial_node.next_piece_inst_idx_dict, golden_next_piece_inst_idx_dict)

    def test_nesting_assignment_search_node_successor_init(self):
        initial_node = NestingAssignmentSearchNode(problem=self.value_picking_nsp, dynamic=False)
        variable_assigned = PieceKey('bigTriangle', 0)
        next_assignment = self.value_picking_nsp.result(initial_node.assignment, variable_assigned, Placement((0, 0)))
        successor_node = NestingAssignmentSearchNode(next_assignment, initial_node, variable_assigned)
        golden_next_piece_inst_idx_dict = {
            'bigTriangle': 1,
            'mediumTriangle': 0,
            'smallTriangle': 0,
            'parallelogram': 0,
            'square': 0,
        }
        self.assertEqual(successor_node.next_piece_inst_idx_dict, golden_next_piece_inst_idx_dict)

    def test_nesting_assignment_search_node_last_instance_successor_init(self):
        """
        Test if the entry for a piece is removed from the next instance dictionary
        when the last instance has been placed

        """
        mock_node = NestingAssignmentSearchNode(problem=self.value_picking_nsp, dynamic=False)
        # mock the *next* assignment
        next_assignment = hashdict({
            PieceKey('bigTriangle', 0): Placement((0, 0)),
            PieceKey('bigTriangle', 1): Placement((0, 10)),
            PieceKey('smallTriangle', 0): Placement((10, 0)),
        })
        # the mocking we are interested in: next instance dictionary
        # assume we have already placed the meidum triangle, so no entry at all
        mock_node.next_piece_inst_idx_dict = {
            'bigTriangle': 1,
            'smallTriangle': 1,
            'parallelogram': 0,
            'square': 0,
        }

        # place the last big triangle
        variable_assigned = PieceKey('bigTriangle', 1)
        successor_node = NestingAssignmentSearchNode(next_assignment, mock_node, variable_assigned)
        golden_next_piece_inst_idx_dict = {
            'smallTriangle': 1,
            'parallelogram': 0,
            'square': 0,
        }
        self.assertEqual(successor_node.next_piece_inst_idx_dict, golden_next_piece_inst_idx_dict)

    def test_get_region_fitting_pieces(self):

        region_fitting_pieces = get_region_fitting_pieces(self.mock_node, [self.expected_dead_region, self.expected_living_region])

        # print region_fitting_pieces[0][0].wkt
        # self.assertItemsEqual([(self.expected_dead_region, Counter([])), (self.expected_living_region, Counter(['smallTriangle', 'parallelogram', 'square']))],
        #                       [(region, Counter(fitting_piece_ids)) for region, fitting_piece_ids in region_fitting_pieces])

        # use Counter to make order irrelevant
        self.assertEqual({region_idx: Counter(fitting_piece_ids) for region_idx, fitting_piece_ids in region_fitting_pieces.iteritems()},
            {0: Counter([]), 1: Counter(['smallTriangle', 'parallelogram', 'square'])})

    def test_get_dead_regions(self):
        dead_regions = get_dead_regions(self.mock_node)

        # print [str(region) for region in dead_regions]
        self.assertItemsEqual([self.expected_dead_region], dead_regions)

    def test_get_living_region_area(self):
        living_region_area = get_living_space_area(self.mock_node)
        # note: due to floating imprecision, dead.area is 4.000000000000001 but operation cleans that up
        self.assertEqual(living_region_area, 16.0)


class OpenDimensionSearchTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        xml_filepath = get_full_path('benchmark', 'Personal', 'tangram', 'problem_tangram.xml')
        cls.expanded_problem = create_expanded_packing_problem_from_file(xml_filepath)
        cls.cop = create_fixed_origin_open_dimension_problem(cls.expanded_problem)
        cls.value_picking_nsp = ValuePickingNestingSearchProblem(cls.cop)

        mock_assignment = {
            PieceKey('mediumTriangle', 0): Placement((0, 0), 45),  # area: 8
            PieceKey('smallTriangle', 0): Placement((0, 0), 0),    # area: 4
            PieceKey('smallTriangle', 1): Placement((6, 0), 0),    # rightmost at x=10
        }
        cls.mock_node = NestingAssignmentSearchNode(mock_assignment, problem=cls.value_picking_nsp, dynamic=True)

    # global heuristic tests
    def test_area_based_final_length_estimation(self):
        # expected_final_length_estimation = 10. / (8 + 4*2) * 64 (total piece area = tangram area = 8 * 8)
        expected_final_length_estimation = 40
        self.assertEqual(area_based_final_length_estimation(self.mock_node), expected_final_length_estimation)

    def test_current_usage_based_final_length_estimation(self):
        # convex hull of layout: (0, 0) (10, 0) (8, 2) (4, 4) (0, 4) -> area: 30
        # container height: 8
        # sum of placed pieces: 16
        # sum of all pieces: 64
        # expected_final_length_estimation = 64 / 8 / (16 / 30) = 8 / (8 / 15) = 15
        expected_final_length_estimation = 15.
        self.assertEqual(current_usage_based_final_length_estimation(self.mock_node), expected_final_length_estimation)


class NestingValuePickerTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        wkt_filepath = get_full_path('benchmark', 'Personal', 'sample3.wkt')
        geometry_list = to_shapely_object_list(wkt_filepath)
        cls.h_polygon = geometry_list[3 - 1]
        cls.rect_polygon = geometry_list[2 - 1]

    def test_get_min_bb_translations_along(self):
        """
        Test get_get_min_bb_translations_along A, B and a part of NFP(A, B)
        where A: rectangle and B: H-shaped piece

        """
        nfp_part = MultiLineString([[(8, -2), (2, -2)], [(-14, -2), (-8, -2)], [(8, -6), (8, 2), (-14, 2), (-14, -6), (8, -6)]])
        min_translations, min_area = get_min_bb_translations_along(self.rect_polygon, self.h_polygon, nfp_part)
        assert_array_equal(sorted(min_translations), [(-8, -2), (2, -2)])  # see NFP(rect, H) in sample3_axis_aligned_shapes.svg
        self.assertEqual(min_area, 16*6)


class NestingHeuristicTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    def test_get_domain_size_orientation_average(self):
        domain = NestingDomain({
            0: PolygonWithBoundary(box(0, 0, 10, 10), LineString([(10, 0), (15, 0)])),
            45: PolygonWithBoundary(box(0, 0, 20, 10), MultiPoint([(-1, 0), (-2, 0)]))
        })
        np.testing.assert_allclose(get_domain_size_orientation_average(domain), (150, 2.5, 1))



if __name__ == '__main__':
    unittest.main()
