from functools import partial
from ippy.packmath.constraintsearch import iterable_domain_value_picker, greedy_vp_search, ordered_master_heuristic, \
    depth_first_heuristic, min_cost_heuristic
from ippy.packmath.constraintsearchviewer import ConsoleCSEventHandler, EventConstraintSearchViewer
from ippy.packmath.knapsack1d import MultipleKnapsackProblem1D, ValuePickingMultipleKnapsackProblem1D
from ippy.search.constraint import check_if_conflicts

__author__ = 'hs'

import unittest


class MultipleKnapsackProblem1DTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.problem = MultipleKnapsackProblem1D([3, 2, 1], [2, 2, 2, 1])
        cls.vp_problem = ValuePickingMultipleKnapsackProblem1D(cls.problem)

    def test_init(self):
        """Test the problem initialization"""
        self.assertEqual(self.problem.order_sizes, [2, 2, 2, 1])
        self.assertEqual(self.problem.stock_sizes, [3, 2, 1, float('+inf')])
        self.assertEqual(self.problem.variables, [0, 1, 2, 3])
        self.assertEqual(self.problem.stock_indices, [0, 1, 2, -1])

    def test_global_constraint_true(self):
        """Test the global constraint method individually for constraint satisfaction"""
        placements = {0: 1, 1: 0, 3: 2}
        self.assertTrue(self.problem.global_constraint(placements))

    def test_global_constraint_false(self):
        """Test the global constraint method individually for constraint violation"""
        placements = {0: 0, 1: 2}
        self.assertFalse(self.problem.global_constraint(placements))

    def test_check_conflicts_false(self):
        """Test the global constraint method via check_if_conflicts for constraint satisfaction"""
        placements = {0: 0, 1: 1, 2: -1}
        self.assertFalse(check_if_conflicts(placements, self.problem))

    def test_check_conflicts_true(self):
        """Test the global constraint method via check_if_conflicts for constraint violation"""
        placements = {0: 0, 1: 0, 2: -1}
        self.assertTrue(check_if_conflicts(placements, self.problem))

    def test_create_domain_singleton(self):
        self.assertEqual(self.problem.create_domain_singleton(1, 2), {2})

    def test_get_cost_positive(self):
        assignment = {0: 1, 1: 1, 2: -1, 3: 0}
        self.assertEqual(self.problem.get_cost(assignment), 2)

    def test_get_cost_null(self):
        assignment = {0: 1, 1: 1, 2: 2, 3: 0}  # invalid assignment, but works for the test
        self.assertEqual(self.problem.get_cost(assignment), 0)

    def test_get_orders_in_stock(self):
        assignment = {0: 0, 1: 1, 2: -1, 3: 0}
        self.assertEqual([self.problem.get_orders_in_stock(assignment, i) for i in xrange(4)], [[0, 3], [1], [], [2]])

    def test_get_total_order_size_in_stock(self):
        assignment = {0: 0, 1: 1, 2: -1, 3: 0}
        self.assertEqual([self.problem.get_total_order_size_in_stock(assignment, i) for i in xrange(4)], [3, 2, 0, 2])

    def test_greedy_vp_search(self):
        viewer = EventConstraintSearchViewer(self.problem, [
            partial(ConsoleCSEventHandler, min_loglevel=20),
        ])
        best_node = greedy_vp_search(self.vp_problem, iterable_domain_value_picker, optimize=True,
                                     heuristic=ordered_master_heuristic(depth_first_heuristic, min_cost_heuristic),
                                     filter_fun=None, viewer=viewer, max_iterations=500, cached_values=None,
                                     cache_filter_fun=None)

        # there should always be a solution
        self.assertIsNotNone(best_node)
        self.assertEqual(best_node.cost, 2)


if __name__ == '__main__':
    unittest.main()
