# poly_tri.py PyPolygon2tri 1.0, triangulation by ear cutting algorithm.
# 2014 huulong: better modularization
# Copyright (C) 2007 Sebastian Santisi <s@ntisi.com.ar>
# Copyright (C) 1988 Evans & Sutherland Computer Corporation
"""
This version is a Python ported version from C by Sebastian Santisi.
C version available at
http://www.programmersheaven.com/download/15162/download.aspx

I've ported this algorithm because GLU Tesselator (gluTess* functions)
throws a stack overflow exception for Python in Windows.


C preamble:
/*
 * poly_tri.c
 *
 * Program to take a polygon definition and convert it into triangles
 * that may then be rendered by the standard triangle rendering
 * algorithms.  This assumes all transformations have been performed
 * already and cuts them up into optimal triangles based on their
 * screen-space representation.
 *
 *	Copyright (c) 1988, Evans & Sutherland Computer Corporation
 *
 * Permission to use all or part of this program without fee is
 * granted provided that it is not used or distributed for direct
 * commercial gain, the above copyright notice appears, and
 * notice is given that use is by permission of Evans & Sutherland
 * Computer Corporation.
 *
 *	Written by Reid Judd and Scott R. Nelson at
 *	Evans & Sutherland Computer Corporation (January, 1988)
 *
 * To use this program, either write your own "draw_triangle" routine
 * that can draw triangles from the definitions below, or modify the
 * code to call your own triangle or polygon rendering code.  Call
 * "draw_poly" from your main program.
 */
"""
import logging
from ippy.utils.geometry import get_points_orientation, get_orientation, Orientation


def no_interior(p1, p2, p3, v, poly_or):
    """
    Returns True if no other point in the vertex list is inside
    the triangle specified by the three points.  Returns
    False otherwise.
    """

    for p in v:
        if p == p1 or p == p2 or p == p3:
            # Don't bother checking against yourself
            continue
        # point on polygon edge is problematic: should be considered as inside (return False)
        # TODO: deal with such cases manually or use shapely intersection (not within!)
        # for now, however, such polygons will be considered ill-formed
        # and an Exception will be raised
        # However, it could happen normally if two edges are aligned in a Polygon
        # (not optimal definition but acceptable by Shapely), so prefer considering
        # the edge as inside probably
        orientation12 = get_points_orientation(p1, p2, p)
        orientation31 = get_points_orientation(p3, p1, p)
        orientation23 = get_points_orientation(p2, p3, p)
        if orientation31 == poly_or and \
                (orientation12 == Orientation.aligned or
                orientation23 == Orientation.aligned):
            # raise ValueError('Ill-formed Polygon, vertex {0} touches edges [{1}, {2}] or [{2}, {3}]'.format(p, p1, p2, p3))
            # point is considered inside, ignore
            # IMPROVE DEBUG: add a visual debug for that (animation if possible)
            # logging.info('Vertex {0} touches edges [{1}, {2}] or [{2}, {3}], '
            #                 'this is possible during ear-clipping but check your Polygon'.format(p, p1, p2, p3))
            return False
        if orientation12 != poly_or or \
                orientation31 != poly_or or \
                orientation23 != poly_or:
            # This point is outside
            continue
        # The point is inside
        return False
    # No points inside this triangle
    return True


def draw_triangle(p1, p2, p3, *args):
    """
    Rewrite this function, or pass a callback to draw_poly().
    """
    pass


def earclip_shapely_polygon(polygon, poly_orientation=None):
    """
    Return a generator that yield triangles composing the shapely polygon
    obtained via the ear-clipping method.

    Note that only the exterior of the shapely polygon is considered.

    Parameters
        polygon
            a shapely polygon

        poly_orientation
            optional parameter to indicate the orientation of the vertices in polygon

    Yield
        Triangles composing the polygon, via ear-clipping

    """
    v = polygon.exterior.coords[:-1]  # ignore the redundant last vertex
    return earclip(v, poly_orientation)

def earclip(v, poly_orientation=None):
    """
    Return a generator that yield triangles composing the polygon v
    obtained via the ear-clipping method.

    Note that this does not work for polygons with holes or self
    penetrations.

    Parameters
        v
            a sequence of vertices describing the polygon
            vertices must have their x/y coordinates callable via [0] and [1] resp.

        poly_orientation
            optional parameter to indicate the orientation of the vertices in v

    Yield
        Triangles composing the polygon contour v, via ear-clipping

    """

    if poly_orientation is None:
        poly_orientation = get_orientation(v)

    # use a copy of the list of vertices for safety
    v = v[:]
    # Pop clean triangles until nothing remains
    # TODO: use loops and computations through NumPy
    while len(v) >= 3:
        # when there is only one triangle left, this is the last ear
        if len(v) == 3:
            # if it is a flat triangle, however, do *not* yield it
            if not get_points_orientation(v[0], v[1], v[2]) == Orientation.aligned:
                yield (v[2], v[0], v[1])
            return
        # TODO: try a loop in which the index of the current vertex is preserved
        # after a yield, instead of starting a new for loop;
        # this would allow us to avoid the worst case case the starting vertex is bad
        # and we must go all the way from the beginning each time
        # i.e. init with cur_i = 0, then cur_i += 1 (modulo) at each iteration except when
        # an ear is found since the delete will automatically move all next vertices to
        # the left
        for cur_i in range(len(v)):
            prev_i = cur_i - 1  # the -1th item is the last item so works for cur_i == 0 too
            next_i = (cur_i + 1) % len(v)  # Wrap around on the ends
            # By definition, there are at least two ears;
            # we will iterate until the end only if poly_orientation
            # was incorrect, in which case an Exception will be raised
            vertex_orientation = get_points_orientation(v[prev_i], v[cur_i], v[next_i])
            # check flat triangle first because our no_interior test rejects
            # other vertices on edges adjacent to the current vertex
            # FIXME: not enough! I finally accepted such edge cases...
            if vertex_orientation == Orientation.aligned:
                # spike at this vertex; can happen after an ear has been clipped
                # see example: sample3_axis_alignes_shapes, polygon3
                # remove the spiky vertex but do not yield
                # (the resulting triangle is flat so not useful for nfp computation,
                # since adjacent triangles already contain the same edges)
                # FIXME: some edge cases may make this fail, check out!
                del v[cur_i]
                break
            if vertex_orientation == poly_orientation and \
                    no_interior(v[prev_i], v[cur_i], v[next_i], v, poly_orientation):
                # Same orientation as polygon
                # No points inside
                # Output this triangle
                yield (v[prev_i], v[cur_i], v[next_i])
                # Remove the triangle from the polygon
                del v[cur_i]
                break
        else:
            raise Exception('Error: Didn\'t find a triangle.\n')

    # Output the final triangle
    # callback(v[0], v[1], v[2], args)

