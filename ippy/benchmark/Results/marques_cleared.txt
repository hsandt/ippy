=========
Benchmark	Marques (Open Dimension)
File	/home/wing/Projects/AI/Ippy/ippy/benchmark/improved_xml/marques_cleared.xml
Time	2015-07-27 00:09:27
Platform	Ubuntu, Intel Core i7, 2.5GHz

Strategy parameters

Fringe	normal
Picker	rdv
Filter	none
1st heuristic	depth
2nd heuristic	mrd
3rd heuristic	overlap B
4th heuristic	none
Cached values	ifps, dynamic_ifps, nfps, free_space, dynamic_free_space, contact_graph, domains, dynamic_domains
Max iterations	5000
Bounded fringe limit	500
Random span	5
Picker sampling	No
Multistart	1
Force fixed length	0
Fixed length	None
Dynamic cost function	1
Execution level	release
Rounding level	raw

Finished reason	max iterations

Stats

best cost	108.0
max assignment	24 / 24
iterations	5000
expanded	4634
search time	389.628319
max fringe size	5676
best cost iter	960
best cost exp	959
best cost time	112.349189
forward check empty	0
filter2	0
optimize drop	365
old pops	0
new pops	5000


=========
Benchmark	Marques (Open Dimension)
File	/home/wing/Projects/AI/Ippy/ippy/benchmark/improved_xml/marques_cleared.xml
Time	2015-07-27 00:30:17
Platform	Ubuntu, Intel Core i7, 2.5GHz

Strategy parameters

Fringe	normal
Picker	rdv
Filter	none
1st heuristic	depth
2nd heuristic	mrd
3rd heuristic	overlap B
4th heuristic	none
Cached values	ifps, dynamic_ifps, nfps, free_space, dynamic_free_space, contact_graph, domains, dynamic_domains
Max iterations	5000
Bounded fringe limit	500
Random span	5
Picker sampling	No
Multistart	1
Force fixed length	0
Fixed length	None
Dynamic cost function	1
Execution level	release
Rounding level	raw

Finished reason	max iterations

Stats

=========
Benchmark	Marques (Open Dimension)
File	/home/wing/Projects/AI/Ippy/ippy/benchmark/improved_xml/marques_cleared.xml
Time	2015-07-27 00:44:45
Platform	Ubuntu, Intel Core i7, 2.5GHz

Strategy parameters

Fringe	normal
Picker	rdv
Filter	none
1st heuristic	depth
2nd heuristic	mrd
3rd heuristic	overlap B
4th heuristic	none
Cached values	ifps, dynamic_ifps, nfps, free_space, dynamic_free_space, contact_graph, domains, dynamic_domains
Max iterations	5000
Bounded fringe limit	500
Random span	5
Picker sampling	No
Multistart	1
Force fixed length	0
Fixed length	None
Dynamic cost function	1
Execution level	release
Rounding level	raw

Finished reason	max iterations

Stats

Best usage	0.640491452991
best cost	108.0
max assignment	24 / 24
iterations	5000
expanded	4634
search time	443.398687
max fringe size	5676
best cost iter	960
best cost exp	959
best cost time	106.910429
forward check empty	0
filter2	0
optimize drop	365
old pops	0
new pops	5000


