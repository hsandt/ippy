=========
Benchmark	Dighe2 (Single Size FO)
File	/home/wing/Projects/AI/Ippy/ippy/benchmark/improved_xml/dighe2_cleared.xml
Time	2015-07-31 00:28:24
Platform	Ubuntu, Intel Core i7, 2.5GHz

Strategy parameters

Fringe	bounded
Picker	min bb
Filter	dead region
1st heuristic	depth
2nd heuristic	bigger bb area
3rd heuristic	length
4th heuristic	none
Cached values	ifps, dynamic_ifps, nfps, free_space, dynamic_free_space, contact_graph, domains, dynamic_domains
Max iterations	1
Bounded fringe limit	500
Random span	10
Picker sampling	No
Multistart	1
Force fixed length	1
Fixed length	66.0
Dynamic cost function	1
Execution level	release
Rounding level	raw

Finished reason	max iterations

Stats

Fixed cost	infinity
