=========
Benchmark	Blaz (Open Dimension)
File	/home/wing/Projects/AI/Ippy/ippy/benchmark/improved_xml/blaz_cleared.xml
Time	2015-07-24 01:23:28
Platform	Ubuntu, Intel Core i7, 2.5GHz

Strategy parameters

Fringe	normal
Picker	rdv
Filter	dead region
1st heuristic	depth
2nd heuristic	mrd
3rd heuristic	min dead area
4th heuristic	min ch waste
Cached values	ifps, dynamic_ifps, nfps, free_space, dynamic_free_space, contact_graph, domains, dynamic_domains
Max iterations	3000
Bounded fringe limit	500
Random span	5
Picker sampling	No
Multistart	3
Force fixed length	0
Fixed length	None
Dynamic cost function	1
Execution level	release
Rounding level	raw

Finished reason	max iterations

Stats

best cost	32.6
max assignment	28 / 28
iterations	3000
expanded	355
search time	2656.95823
max fringe size	3348
best cost iter	83
best cost exp	82
best cost time	2319.406697
forward check empty	0
filter2	0
optimize drop	2644
old pops	0
new pops	3000


=========
Benchmark	Blaz (Open Dimension)
File	/home/wing/Projects/AI/Ippy/ippy/benchmark/improved_xml/blaz_cleared.xml
Time	2015-07-24 02:44:43
Platform	Ubuntu, Intel Core i7, 2.5GHz

Strategy parameters

Fringe	normal
Picker	rdv
Filter	dead region
1st heuristic	depth
2nd heuristic	mrd
3rd heuristic	min dead area
4th heuristic	min ch waste
Cached values	ifps, dynamic_ifps, nfps, free_space, dynamic_free_space, contact_graph, domains, dynamic_domains
Max iterations	3000
Bounded fringe limit	500
Random span	5
Picker sampling	No
Multistart	3
Force fixed length	0
Fixed length	None
Dynamic cost function	1
Execution level	release
Rounding level	raw

Finished reason	max iterations

Stats

best cost	32.6
max assignment	28 / 28
iterations	3000
expanded	355
search time	2657.99543
max fringe size	3348
best cost iter	83
best cost exp	82
best cost time	2322.300067
forward check empty	0
filter2	0
optimize drop	2644
old pops	0
new pops	3000

=========
Benchmark	Blaz (Open Dimension)
File	/home/wing/Projects/AI/Ippy/ippy/benchmark/improved_xml/blaz_cleared.xml
Time	2015-07-24 13:50:14
Platform	Ubuntu, Intel Core i7, 2.5GHz

Strategy parameters

Fringe	normal
Picker	regular
Filter	none
1st heuristic	depth
2nd heuristic	mrd
3rd heuristic	overlap I
4th heuristic	length
Cached values	ifps, dynamic_ifps, nfps, free_space, dynamic_free_space, contact_graph, domains, dynamic_domains
Max iterations	5000
Bounded fringe limit	500
Random span	5
Picker sampling	100
Multistart	1
Force fixed length	0
Fixed length	None
Dynamic cost function	1
Execution level	release
Rounding level	raw

Finished reason	max iterations

Stats

best cost	33.2666666667
max assignment	28 / 28
iterations	5000
expanded	856
search time	498.730667
max fringe size	8430
best cost iter	29
best cost exp	28
best cost time	159.824213
forward check empty	0
filter2	0
optimize drop	4143
old pops	0
new pops	5000


=========
Benchmark	Blaz (Single Size)
File	/home/wing/Projects/AI/Ippy/ippy/benchmark/improved_xml/blaz_cleared.xml
Time	2015-07-27 06:59:19
Platform	Ubuntu, Intel Core i7, 2.5GHz

Strategy parameters

Fringe	random pop
Picker	dominant point
Filter	dead region
1st heuristic	depth
2nd heuristic	mrd
3rd heuristic	min dead area
4th heuristic	min ch waste
Cached values	ifps, dynamic_ifps, nfps, free_space, dynamic_free_space, contact_graph, domains, dynamic_domains
Max iterations	1000
Bounded fringe limit	500
Random span	10
Picker sampling	No
Multistart	1
Force fixed length	0
Fixed length	None
Dynamic cost function	1
Execution level	release
Rounding level	raw

Finished reason	solution found

Stats

Fixed cost	(90.0, 15.0)
Actual final cost	32.9555555556
best cost	0
max assignment	28 / 28
iterations	38
expanded	37
search time	116.537916
max fringe size	384
best cost iter	0
best cost exp	0
best cost time	0
forward check empty	0
filter2	0
optimize drop	0
old pops	0
new pops	38


=========
Benchmark	Blaz (Single Size)
File	/home/wing/Projects/AI/Ippy/ippy/benchmark/improved_xml/blaz_cleared.xml
Time	2015-07-27 07:03:37
Platform	Ubuntu, Intel Core i7, 2.5GHz

Strategy parameters

Fringe	normal
Picker	dominant point
Filter	dead region
1st heuristic	depth
2nd heuristic	mrd
3rd heuristic	min dead area
4th heuristic	min ch waste
Cached values	ifps, dynamic_ifps, nfps, free_space, dynamic_free_space, contact_graph, domains, dynamic_domains
Max iterations	1000
Bounded fringe limit	500
Random span	10
Picker sampling	No
Multistart	1
Force fixed length	0
Fixed length	None
Dynamic cost function	1
Execution level	release
Rounding level	raw

Finished reason	solution found

Stats

Fixed cost	(90.0, 15.0)
Actual final cost	38.5
best cost	0
max assignment	28 / 28
iterations	29
expanded	28
search time	100.094537
max fringe size	286
best cost iter	0
best cost exp	0
best cost time	0
forward check empty	0
filter2	0
optimize drop	0
old pops	0
new pops	29


=========
Benchmark	Blaz (Open Dimension)
File	/home/wing/Projects/AI/Ippy/ippy/benchmark/improved_xml/blaz_cleared.xml
Time	2015-07-27 07:25:57
Platform	Ubuntu, Intel Core i7, 2.5GHz

Strategy parameters

Fringe	normal
Picker	rdv
Filter	dead region
1st heuristic	depth
2nd heuristic	mrd
3rd heuristic	min dead area
4th heuristic	min ch waste
Cached values	ifps, dynamic_ifps, nfps, free_space, dynamic_free_space, contact_graph, domains, dynamic_domains
Max iterations	3000
Bounded fringe limit	500
Random span	10
Picker sampling	No
Multistart	1
Force fixed length	0
Fixed length	None
Dynamic cost function	1
Execution level	release
Rounding level	raw

Finished reason	max iterations

Stats

=========
Benchmark	Blaz (Open Dimension)
File	/home/wing/Projects/AI/Ippy/ippy/benchmark/improved_xml/blaz_cleared.xml
Time	2015-07-27 07:55:05
Platform	Ubuntu, Intel Core i7, 2.5GHz

Strategy parameters

Fringe	normal
Picker	rdv
Filter	dead region
1st heuristic	depth
2nd heuristic	mrd
3rd heuristic	min dead area
4th heuristic	min ch waste
Cached values	ifps, dynamic_ifps, nfps, free_space, dynamic_free_space, contact_graph, domains, dynamic_domains
Max iterations	3000
Bounded fringe limit	500
Random span	10
Picker sampling	No
Multistart	1
Force fixed length	0
Fixed length	None
Dynamic cost function	1
Execution level	release
Rounding level	raw

Finished reason	max iterations

Stats

Best usage	0.662576687117
best cost	32.6
max assignment	28 / 28
iterations	3000
expanded	751
search time	1077.979385
max fringe size	3064
best cost iter	29
best cost exp	28
best cost time	781.442576
forward check empty	0
filter2	0
optimize drop	2248
old pops	0
new pops	3000


=========
Benchmark	Blaz (Single Size FO)
File	/home/wing/Projects/AI/Ippy/ippy/benchmark/improved_xml/blaz_cleared.xml
Time	2015-07-31 00:13:22
Platform	Ubuntu, Intel Core i7, 2.5GHz

Strategy parameters

Fringe	bounded
Picker	min bb
Filter	dead region
1st heuristic	depth
2nd heuristic	bigger bb area
3rd heuristic	length
4th heuristic	none
Cached values	ifps, dynamic_ifps, nfps, free_space, dynamic_free_space, contact_graph, domains, dynamic_domains
Max iterations	1
Bounded fringe limit	500
Random span	10
Picker sampling	No
Multistart	1
Force fixed length	1
Fixed length	66.0
Dynamic cost function	1
Execution level	release
Rounding level	raw

Finished reason	max iterations

Stats

Fixed cost	infinity
